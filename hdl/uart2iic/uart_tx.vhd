
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_tx is
	generic (
		FREQ_IN : natural := 100000000;
		BAUD    : natural := 115200
	);
	port (
		reset : in  std_logic;
		clk   : in  std_logic;
		-- The FIFO
		fifo_ack  : in  std_logic;
		fifo_rdy  : out std_logic;
		fifo_data : in  std_logic_vector(7 downto 0);
		-- The TX port
		tx : out std_logic
	);
end uart_tx;

architecture augh of uart_tx is

	-- Note: Data bits are sent as little endian

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	constant data_bits      : natural := 8;
	constant frames_nb      : natural := 1;
	constant clock_divider  : natural := FREQ_IN / BAUD;
	-- Bit width of a counter that counts from 0 to clock_divider-1
	constant clockdiv_width : natural := storebitsnb(clock_divider-1);
	-- Bit width of a counter that counts from 0 to data_bits-1
	constant bitcount_width : natural := 3;
	-- Bit width of a counter that counts from 0 to frames_nb
	constant framecnt_width : natural := 1;

	-- Types for little FSM
	type status_type is (fsm_start, fsm_bits, fsm_stop);

	-- Note: Init state is STOP to ensure at least one STOP symbol is generated
	signal tx_status          : status_type := fsm_stop;
	signal tx_status_next     : status_type := fsm_stop;
	signal tx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');
	signal tx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');
	signal tx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');
	signal tx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');
	-- An intermediate signal to cover cases where the fifo width is not a multiple of data_bits
	signal tx_fifo_tmp_data   : std_logic_vector(frames_nb*data_bits-1 downto 0);
	-- Frames of data bits. When there are several frames to send, the number 0 is the one being sent.
	signal tx_bits0           : std_logic_vector(data_bits-1 downto 0);
	signal tx_bits0_next      : std_logic_vector(data_bits-1 downto 0);
	-- Number of frames to send. Set to frames_nb at load from FIFO
	-- It decremented each time a frame is sent.
	signal tx_frames          : unsigned(framecnt_width-1 downto 0) := (others => '0');
	signal tx_frames_next     : unsigned(framecnt_width-1 downto 0) := (others => '0');

begin

	-- Sequential process
	process (clk, reset)
	begin
		if rising_edge(clk) then

			tx_status        <= tx_status_next;
			tx_bits_idx      <= tx_bits_idx_next;
			tx_frames        <= tx_frames_next;
			tx_clkdiv_cnt    <= tx_clkdiv_cnt_next;
			tx_bits0         <= tx_bits0_next;

			if reset = '1' then
				tx_status        <= fsm_stop;
				tx_bits_idx      <= (others => '0');
				tx_frames        <= (others => '0');
				tx_clkdiv_cnt    <= (others => '0');
			end if;

		end if;
	end process;

	-- Combinatorial process
	process (tx_status, tx_bits_idx, tx_frames, tx_clkdiv_cnt, tx_bits0, tx_fifo_tmp_data, fifo_ack)
	begin

		-- Default values for ports
		tx <= '1';
		fifo_rdy <= '0';
		-- Default values for internal signals
		tx_status_next     <= tx_status;
		tx_bits_idx_next   <= tx_bits_idx;
		tx_frames_next     <= tx_frames;
		tx_bits0_next      <= tx_bits0;

		-- Clock divider
		if tx_clkdiv_cnt = clock_divider-1 then
			tx_clkdiv_cnt_next <= (others => '0');
		else
			tx_clkdiv_cnt_next <= tx_clkdiv_cnt + 1;
		end if;

		-- Compute next state and generate TX
		if tx_status = fsm_start then
			-- Send the start bit
			tx <= '0';
			tx_bits_idx_next <= (others => '0');
			if tx_clkdiv_cnt = clock_divider-1 then
				tx_status_next <= fsm_bits;
			end if;
		elsif tx_status = fsm_bits then
			-- Send the data bits
			tx <= tx_bits0(0);
			if tx_clkdiv_cnt = clock_divider-1 then
				tx_bits0_next <= '0' & tx_bits0(data_bits-1 downto 1);
				if tx_bits_idx = data_bits-1 then
					tx_frames_next <= tx_frames - 1;
					tx_status_next <= fsm_stop;
				else
					tx_bits_idx_next <= tx_bits_idx + 1;
				end if;
			end if;
		elsif tx_status = fsm_stop then
			-- Send the stop bit
			if tx_clkdiv_cnt = clock_divider-1 then
				if tx_frames = 0 then
					-- Stay here until some data is available to send
					tx_clkdiv_cnt_next <= tx_clkdiv_cnt;
				else
					tx_status_next <= fsm_start;
				end if;
			end if;
		end if;

		-- Handle data input
		if tx_frames = 0 then
			fifo_rdy <= '1';
			if fifo_ack = '1' then
				tx_frames_next <= to_unsigned(frames_nb, framecnt_width);
				tx_bits0_next <= tx_fifo_tmp_data;
			end if;
		end if;

	end process;

	-- The intermediate signal for FIFO data input
	tx_fifo_tmp_data <= fifo_data;

end architecture;

