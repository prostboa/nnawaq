#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It generates the configuration of layers for special-purpose manual processing

# Network creation
source build_vgg.tcl

# Network configuration
source config_vgg.tcl

nn_set vd=dumpvgg fn=1
nn_dump_config

