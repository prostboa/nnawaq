
# Miscellaneous useful variables
set top VC709Gen${pciegen}x8If$pciesz
set part xcvu37p-fsvh2892-2L-e
# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]



# Note: For debug purposes, if you want to start from post- routing state,
# uncomment these lines and remove all above commands (except setting of parameters)
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $part

