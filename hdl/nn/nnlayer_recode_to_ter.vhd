
-- This block recodes data on-the-fly
-- Internally, there is one address register
-- The address is cleared only when the port clear = '1'
-- It is automatically incremented at each write or read

-- Specification :
-- Implementation is a 2-thresholds (3 steps) activation function, the 3 outputs are optionally configurable

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_recode_to_ter is
	generic(
		WDATA : natural := 12;
		SDATA : boolean := true;
		WOUT  : natural := 2;
		FSIZE : natural := 1024;
		PAR   : natural := 1;
		-- Identifier of neuron layer, for the constant memory component if any
		LAYER_ID : natural := 0;
		-- Parameters for memory usage
		CONST_PARAMS : boolean := false;
		-- To avoid storing outputs in memory
		OUT_STATIC : boolean := false;
		-- The outputs when not stored in memory
		OUT_LOW : integer := -1;
		OUT_MED : integer := 0;
		OUT_UP  : integer := 1;
		-- Width of the write port
		WWRITE : natural := 32;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk             : in  std_logic;
		-- Ports for address control
		addr_clear      : in  std_logic;
		-- Ports for Write into memory
		write_mode      : in  std_logic;
		write_data      : in  std_logic_vector(WWRITE-1 downto 0);
		write_enable    : in  std_logic;
		-- The user-specified frame size
		user_fsize      : in  std_logic_vector(15 downto 0);
		-- Data input
		data_in         : in  std_logic_vector(PAR*WDATA-1 downto 0);
		data_in_valid   : in  std_logic;
		data_in_ready   : out std_logic;
		-- Data output
		data_out        : out std_logic_vector(PAR*WOUT-1 downto 0);
		data_out_valid  : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room   : in  std_logic_vector(15 downto 0)
	);
end nnlayer_recode_to_ter;

architecture synth of nnlayer_recode_to_ter is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- The internal memory
	constant RAM_WDATA : natural := 2 * WDATA + to_integer(not OUT_STATIC) * (3 * WOUT);
	constant RAM_WIDTH : natural := PAR * RAM_WDATA;
	type ram_type is array (0 to FSIZE-1) of std_logic_vector(RAM_WIDTH-1 downto 0);
	signal ram : ram_type := (others => (others => '0'));

	attribute ram_style : string;
	--attribute ram_style of ram : signal is "distributed";

	-- The number of clock cycles to fill the memory width from the write port
	constant WR_CYCLES_NB : natural := (RAM_WIDTH + WWRITE - 1) / WWRITE;

	-- First stage buffers for Write mode
	signal write_mode1   : std_logic := '0';
	signal write_vector1 : std_logic_vector(WR_CYCLES_NB*WWRITE-1 downto 0) := (others => '0');
	signal write_envec1  : std_logic_vector(WR_CYCLES_NB-1 downto 0) := (others => '0');
	signal write_enable1 : std_logic := '0';
	-- A register to enable Write to memory and to disable it at end of memory size
	signal write_memwe1  : std_logic := '0';

	-- First stage buffers
	signal addr_clear1   : std_logic := '0';
	signal data_in1      : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid1   : std_logic := '0';

	-- Second stage buffers
	signal data_in2    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid2 : std_logic := '0';
	signal data_ram2   : std_logic_vector(RAM_WIDTH-1 downto 0) := (others => '0');

	-- Third stage buffers
	-- FIXME These are only useful for BRAM storage, which may not always be the case
	signal data_in3    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid3 : std_logic := '0';
	signal data_ram3   : std_logic_vector(RAM_WIDTH-1 downto 0) := (others => '0');

	-- Fourth stage buffers
	signal data_out4_n : std_logic_vector(PAR*WOUT-1 downto 0) := (others => '0');
	signal data_out4   : std_logic_vector(PAR*WOUT-1 downto 0) := (others => '0');
	signal data_valid4 : std_logic := '0';

	-- Prevent the post-memory register to be absorbed into the BRAM (better for delay)
	-- FIXME Only useful for BRAM based storage, but BRAM storage is not enforced
	attribute keep : string;
	attribute keep of data_ram3 : signal is "TRUE";

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';
	-- This register indicates this is the last neuron value
	signal reg_end_frame : std_logic := to_std_logic(LOCKED and (FSIZE=1));

	-- Use the parameter FSIZE as maximum memory size
	constant WADDR : natural := storebitsnb(FSIZE-1);

	-- The internal Address register
	signal reg_addr : unsigned(WADDR-1 downto 0) := (others => '0');
	-- Utility registers to reduce critical path
	signal buf_fsize_is1 : std_logic;
	signal buf_fsize_m2 : unsigned(WADDR-1 downto 0);

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		buf_fsize_is1 <= to_std_logic(unsigned(user_fsize) = 1);

		process(clk)
		begin
			if rising_edge(clk) then

				-- Note : Using a phony resize to avoid underflow with very small input
				buf_fsize_m2 <= resize(unsigned('1' & user_fsize) - 2, WADDR);
				--buf_fsize_m2 <= unsigned(user_fsize) - 2;

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_fsize_is1 <= to_std_logic(FSIZE = 1);
		buf_fsize_m2  <= to_unsigned(maximum(0, FSIZE-2), WADDR);

	end generate;

	-------------------------------------------------------------------
	-- State machine
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers for Write mode
			write_mode1   <= write_mode;
			write_enable1 <= '0';
			-- First stage buffers
			addr_clear1   <= addr_clear;
			data_in1      <= data_in;
			data_valid1   <= data_in_valid and buf_in_ready;

			-- Second stage buffers
			data_in2      <= data_in1;
			data_valid2   <= data_valid1;

			-- Note : Read and Write on the internal RAM are handled in separate generate statement
			-- Read signal : data_ram2
			-- data_ram2 <= ram(to_integer(reg_addr));

			-- Third stage buffers
			data_in3      <= data_in2;
			data_valid3   <= data_valid2;
			data_ram3     <= data_ram2;

			-- Fourth stage buffers
			data_out4     <= data_out4_n;
			data_valid4   <= data_valid3;

			-- The full-width memory write register
			if (write_mode1 = '1') and (write_enable = '1') then

				-- Rotate the enable vector
				if WR_CYCLES_NB > 1 then
					write_envec1 <= write_envec1(WR_CYCLES_NB-2 downto 0) & write_envec1(WR_CYCLES_NB-1);
				end if;

				-- Write the input word at the right place
				for i in 0 to WR_CYCLES_NB-1 loop
					if write_envec1(i) = '1' then
						if i < WR_CYCLES_NB-1 then
							write_vector1((i+1)*WWRITE-1 downto i*WWRITE) <= write_data;
						else
							write_vector1(write_vector1'high downto i*WWRITE) <= write_data((write_vector1'length - i*WWRITE)-1 downto 0);
							write_enable1 <= write_memwe1;
						end if;
					end if;
				end loop;

			end if;

			-- Out of write mode, clear the write enable registers
			if write_mode1 = '0' then
				write_envec1 <= (others => '0');
				write_enable1 <= '0';
				write_memwe1 <= '0';
			end if;
			-- Rising edge on write mode enables write in the first block
			if (write_mode = '1') and (write_mode1 = '0') then
				write_envec1(0) <= '1';
				write_memwe1 <= '1';
			end if;
			-- Inhibit write enable when the last write address is reached
			if (write_enable1 = '1') and (reg_end_frame = '1') then
				write_memwe1 <= '0';
			end if;

			-- Validate data entry in the pipeline
			buf_in_ready <= '0';
			if (unsigned(out_fifo_room) >= 8 + FIFOMARGIN) and (write_mode1 = '0') then
				buf_in_ready <= '1';
			end if;

			-- The Address register
			if (write_enable1 = '1') or (data_valid1 = '1') then
				reg_addr <= reg_addr + 1;
				reg_end_frame <= '0';
				if (reg_addr = buf_fsize_m2) or (buf_fsize_is1 = '1') then
					reg_end_frame <= '1';
				end if;
				if reg_end_frame = '1' then
					reg_addr <= (others => '0');
				end if;
			end if;

			-- Clear the address at entry and exit of write mode
			if write_mode /= write_mode1 then
				reg_addr <= (others => '0');
				reg_end_frame <= buf_fsize_is1;
			end if;

			-- General clear
			if addr_clear1 = '1' then
				write_mode1   <= '0';
				write_envec1  <= (others => '0');
				write_enable1 <= '0';
				write_memwe1  <= '0';
				data_valid1   <= '0';
				data_valid2   <= '0';
				data_valid3   <= '0';
				data_valid4   <= '0';
				reg_addr      <= (others => '0');
				reg_end_frame <= buf_fsize_is1;
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Instantiate the memory
	-------------------------------------------------------------------

	gen_mem : if CONST_PARAMS = false and RAM_WDATA > 0 generate

		type ram_type is array (0 to FSIZE-1) of std_logic_vector(RAM_WIDTH-1 downto 0);
		signal ram : ram_type := (others => (others => '0'));

		attribute ram_style : string;
		--attribute ram_style of ram : signal is "distributed";

	begin

		process(clk)
		begin
			if rising_edge(clk) then

				-- Handle Read and Write on the internal RAM
				-- This syntax enables the synthesis tool to use 1-port memory
				if write_enable1 = '1' then
					ram(to_integer(reg_addr)) <= write_vector1(RAM_WDATA-1 downto 0);
				elsif data_valid1 = '1' then
					data_ram2 <= ram(to_integer(reg_addr));
				end if;

			end if;  -- Rising edge of clock
		end process;

	end generate;

	gen_const : if CONST_PARAMS = true and RAM_WDATA > 0 generate

		signal sig_read : std_logic_vector(PAR*RAM_WDATA-1 downto 0) := (others => '0');

		component neurons_const_weights is
			generic (
				-- Size of output data
				WDATA : natural := 1;
				-- Size of the address input, in case it is used
				WADDR : natural := 1;
				-- Parallelism at output
				PAR_OUT : natural := 1;
				-- Identifier of neuron layer, in case a per-layer operation is desired
				LAYER_ID : natural := 0
			);
			port (
				addr_in  : in  std_logic_vector(WADDR-1 downto 0);
				data_out : out std_logic_vector(WDATA*PAR_OUT-1 downto 0)
			);
		end component;

	begin

		i_const : neurons_const_weights
			generic map (
				-- Size of output data
				WDATA => RAM_WDATA,
				-- Size of the address input, in case it is used
				WADDR => WADDR,
				-- Parallelism at output
				PAR_OUT => PAR,
				-- Identifier of neuron layer, in case a per-layer operation is desired
				LAYER_ID => LAYER_ID
			)
			port map (
				addr_in  => std_logic_vector(reg_addr),
				data_out => sig_read
			);

		process(clk)
		begin
			if rising_edge(clk) then

				if data_valid1 = '1' then
					data_ram2 <= sig_read;
				end if;

			end if;  -- Rising edge of clock
		end process;

	end generate;

	-------------------------------------------------------------------
	-- Instantiate the parallel data processing paths
	-------------------------------------------------------------------

	gen_par: for p in 0 to PAR-1 generate

		signal data_ram3_loc : std_logic_vector(RAM_WDATA-1 downto 0) := (others => '0');
		signal data_in3_loc  : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- These intermediate signals only exist for the purpose of debug
		signal thlow : std_logic_vector(WDATA-1 downto 0) := (others => '0');
		signal thup  : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- Comparators and multiplexer
		signal cmplow : std_logic := '0';
		signal cmpup  : std_logic := '0';
		signal muxres : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	begin

		-- Memory contents for this parallelism index
		data_ram3_loc <= data_ram3((p+1)*RAM_WDATA-1 downto p*RAM_WDATA);

		-- Extract the threshold values, for the sake of observing these in waveforms
		thlow <= data_ram3_loc(1*WDATA-1 downto 0*WDATA);
		thup  <= data_ram3_loc(2*WDATA-1 downto 1*WDATA);

		-- Comparators
		data_in3_loc <= data_in3((p+1)*WDATA-1 downto p*WDATA);
		cmplow <=
			'1' when (SDATA = false) and (unsigned(data_in3_loc) < unsigned(thlow)) else
			'1' when (SDATA = true)  and (  signed(data_in3_loc) <   signed(thlow)) else
			'0';
		cmpup <=
			'1' when (SDATA = false) and (unsigned(data_in3_loc) > unsigned(thup)) else
			'1' when (SDATA = true)  and (  signed(data_in3_loc) >   signed(thup)) else
			'0';

		-- Multiplexer
		gen_res : if OUT_STATIC = false generate
			muxres <=
				data_ram3(2*WDATA+1*WOUT-1 downto 2*WDATA+0*WOUT) when cmplow = '1' else
				data_ram3(2*WDATA+3*WOUT-1 downto 2*WDATA+2*WOUT) when cmpup = '1' else
				data_ram3(2*WDATA+2*WOUT-1 downto 2*WDATA+1*WOUT);
		else generate
			muxres <=
				std_logic_vector(to_signed(OUT_LOW, WOUT)) when cmplow = '1' else
				std_logic_vector(to_signed(OUT_UP,  WOUT)) when cmpup = '1' else
				std_logic_vector(to_signed(OUT_MED, WOUT));
		end generate;

		-- Assign result in global signal
		data_out4_n((p+1)*WOUT-1 downto p*WOUT) <= muxres;

	end generate;


	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready  <= buf_in_ready;

	data_out_valid <= data_valid4;
	data_out       <= data_out4;


end architecture;

