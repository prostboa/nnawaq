
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnlayer_recode_to_ter_bench is
	generic (

		WDATA : natural := 16;
		WOUT  : natural := 2;
		FSIZE : natural := 128;
		PAR   : natural := 1;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false;

		-- Number of images to simulate
		FRAMES : natural := 10;
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnlayer_recode_to_ter_bench;

architecture simu of nnlayer_recode_to_ter_bench is

	signal clk             : std_logic := '0';
	signal clk_next        : std_logic := '0';
	signal clear           : std_logic := '1';
	signal clk_want_stop   : boolean := false;

	-- Ports for Write into memory
	signal write_modeX      : std_logic := '0';  -- FIXME Signal renamed to avoid GHDL confusion with write_mode for file
	signal write_data      : std_logic_vector(2*WDATA+3*WOUT-1 downto 0) := (others => '0');
	signal write_enable    : std_logic := '0';
	-- The user-specified frame size
	signal user_fsize      : std_logic_vector(15 downto 0) := (others => '0');
	-- Data input
	signal data_in         : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_in_valid   : std_logic := '0';
	signal data_in_ready   : std_logic := '0';
	-- Data output
	signal data_out        : std_logic_vector(PAR*WOUT-1 downto 0) := (others => '0');
	signal data_out_valid  : std_logic := '0';
	-- The output data enters a FIFO. This indicates the available room.
	signal out_fifo_room   : std_logic_vector(15 downto 0) := (others => '0');

	-- Testbench only, to debug erroneous output vectors in waveforms
	signal tb_out_error         : boolean := false;
	signal tb_out_count_all     : integer := 0;
	signal tb_out_count_inframe : integer := 0;

	-- For observability purposes
	signal tb_data : integer := 0;
	signal tb_thlo : integer := 0;
	signal tb_thhi : integer := 0;

	-- https://en.wikipedia.org/wiki/Pseudorandom_binary_sequence
	function pseudorand31_init(pgony : boolean) return std_logic_vector is
	begin
		return std_logic_vector(to_unsigned(2, 31));
	end function ;
	function pseudorand31_next(vin : std_logic_vector(30 downto 0)) return std_logic_vector is
	begin
		return vin(29 downto 0) & (vin(30) xor vin(27));
	end function ;

	-- Return 1 if true, otherwise return 0
	function bool_to_nat(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	component nnlayer_recode_to_ter is
		generic (
			WDATA : natural := 12;
			WOUT  : natural := 2;
			FSIZE : natural := 1024;
			PAR   : natural := 1;
			-- Lock the layer parameters to the generic parameter value
			LOCKED : boolean := false
		);
		port (
			clk             : in  std_logic;
			-- Ports for address control
			addr_clear      : in  std_logic;
			-- Ports for Write into memory
			write_mode      : in  std_logic;
			write_data      : in  std_logic_vector(2*WDATA+3*WOUT-1 downto 0);
			write_enable    : in  std_logic;
			-- The user-specified frame size
			user_fsize      : in  std_logic_vector(15 downto 0);
			-- Data input
			data_in         : in  std_logic_vector(PAR*WDATA-1 downto 0);
			data_in_valid   : in  std_logic;
			data_in_ready   : out std_logic;
			-- Data output
			data_out        : out std_logic_vector(PAR*WOUT-1 downto 0);
			data_out_valid  : out std_logic;
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room   : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	comp_i : nnlayer_recode_to_ter
		generic map (
			WDATA => WDATA,
			WOUT  => WOUT,
			FSIZE => FSIZE + (bool_to_nat(not LOCKED) * FSIZE),
			PAR   => PAR,
			-- Lock the layer parameters to the generic parameter value
			LOCKED => LOCKED
		)
		port map (
			clk             => clk,
			-- Ports for address control
			addr_clear      => clear,
			-- Ports for Write into memory
			write_mode      => write_modeX,
			write_data      => write_data,
			write_enable    => write_enable,
			-- The user-specified frame size
			user_fsize      => user_fsize,
			-- Data input
			data_in         => data_in,
			data_in_valid   => data_in_valid,
			data_in_ready   => data_in_ready,
			-- Data output
			data_out        => data_out,
			data_out_valid  => data_out_valid,
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room   => out_fifo_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Run-time frame dimensions
	user_fsize <= std_logic_vector(to_unsigned(FSIZE, user_fsize'length));

	-- Data output
	out_fifo_room  <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

	-- Process that generates stimuli
	process
		variable seed : std_logic_vector(30 downto 0);
		variable invec : signed(WDATA-1 downto 0);
		variable thlo : integer;
		variable thhi : integer;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write config data data

		report "# Writing configuration";

		write_modeX <= '1';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		for i in 0 to FSIZE-1 loop

			-- Hardcoded pseudo-random thresholds
			thhi := i*64 + 15;
			if thhi > 2**(WDATA-2) - 100 then thhi := 2**(WDATA-2) - 100; end if;
			thlo := 0 - 2 * thhi;

			write_data <= (others => '0');
			write_data(WDATA-1 downto 0) <= std_logic_vector(to_signed(thlo, WDATA));
			write_data(2*WDATA-1 downto WDATA) <= std_logic_vector(to_signed(thhi, WDATA));
			write_data(2*WDATA+1*WOUT-1 downto 2*WDATA+0*WOUT) <= std_logic_vector(to_signed(-1, WOUT));
			write_data(2*WDATA+2*WOUT-1 downto 2*WDATA+1*WOUT) <= std_logic_vector(to_signed(0, WOUT));
			write_data(2*WDATA+3*WOUT-1 downto 2*WDATA+2*WOUT) <= std_logic_vector(to_signed(1, WOUT));

			-- Send the acknowledge
			write_enable <= '1';
			wait until rising_edge(clk);
			write_enable <= '0';

		end loop;

		write_modeX <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write frame data

		seed := pseudorand31_init(false);

		for f in 1 to FRAMES loop
			report "# Sending frame " & to_string(f);

			for i in 0 to FSIZE-1 loop

				-- Hardcoded pseudo-random thresholds
				thhi := i*64 + 15;
				if thhi > 2**(WDATA-2) - 100 then thhi := 2**(WDATA-2) - 100; end if;
				thlo := 0 - 2 * thhi;

				-- For some input vectors, clamp between the thresholds

				invec := signed(seed(WDATA-1 downto 0));
				if seed(30 downto 29) = "01" then
					if invec < thlo then
						invec := to_signed(thlo, WDATA);
					elsif invec > thhi then
						invec := to_signed(thhi, WDATA);
					end if;
				end if;
				-- Next pseudo-random value
				seed := pseudorand31_next(seed);

				data_in <= std_logic_vector(invec);

				-- Send the acknowledge
				data_in_valid <= '1';
				wait until rising_edge(clk) and (data_in_ready = '1');
				data_in_valid <= '0';

			end loop;  -- frame items

			-- Wait a bit before sending the next frame
			-- No wait for the first frame, then +1 cycle for each next frame
			if f > 1 then
				for i in 2 to minimum(f, 16) loop
					wait until rising_edge(clk);
				end loop;
			end if;

		end loop;  -- frame

		-- Waiting for end of simulation
		report "Input data is sent";
		wait;

	end process;

	-- Process that reads outputs
	process
		file data_outfile : text open write_mode is OUTPUT_FILE;
		variable out_line : line;
		variable want_out_vec : signed(1 downto 0);
		variable seed : std_logic_vector(30 downto 0);
		variable invec : signed(WDATA-1 downto 0);
		variable thlo : integer;
		variable thhi : integer;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		tb_out_count_all <= 0;

		seed := pseudorand31_init(false);

		for f in 1 to FRAMES loop
			report "# Receiving frame " & to_string(f);
			write(out_line, string'("# Beginning of frame ") & to_string(f));
			writeline(data_outfile, out_line);

			tb_out_count_inframe <= 0;

			for i in 0 to FSIZE-1 loop

				-- Wait for a valid output data
				-- FIXME This must be adapted to support PAR > 0
				wait until rising_edge(clk) and (data_out_valid = '1');

				-- Hardcoded pseudo-random thresholds
				thhi := i*64 + 15;
				if thhi > 2**(WDATA-2) - 100 then thhi := 2**(WDATA-2) - 100; end if;
				thlo := 0 - 2 * thhi;

				-- For some input vectors, clamp between the thresholds
				invec := signed(seed(WDATA-1 downto 0));
				if seed(30 downto 29) = "01" then
					if invec < thlo then
						invec := to_signed(thlo, WDATA);
					elsif invec > thhi then
						invec := to_signed(thhi, WDATA);
					end if;
				end if;
				-- Next pseudo-random value
				seed := pseudorand31_next(seed);

				-- Assign signals for observability purposes
				tb_data <= to_integer(invec);
				tb_thhi <= thhi;
				tb_thlo <= thlo;

				-- Compute the desired output vector
				if invec < thlo then
					want_out_vec := to_signed(-1, WOUT);
				elsif invec > thhi then
					want_out_vec := to_signed(1, WOUT);
				else
					want_out_vec := to_signed(0, WOUT);
				end if;

				-- Emit an error message
				tb_out_error <= false;
				if data_out /= std_logic_vector(want_out_vec) then
					tb_out_error <= true;
					report "ERROR Got " & to_string(signed(data_out)) & " expected " & to_string(want_out_vec) &
						" for f " & to_string(f) & " i " & to_string(i) & " input " & to_string(to_integer(invec)) & " thlo " & to_string(thlo) & " thhi " & to_string(thhi);
				end if;

				-- Write output data to file
				hwrite(out_line, data_out);
				if data_out /= std_logic_vector(want_out_vec) then
					write(out_line, string'("   ERROR expected "));
					hwrite(out_line, std_logic_vector(want_out_vec));
				end if;
				writeline(data_outfile, out_line);

				-- Update counter signals to visualize in waveforms
				tb_out_count_all     <= tb_out_count_all + 1;
				tb_out_count_inframe <= tb_out_count_inframe + 1;

			end loop;  -- inside frame

		end loop;  -- frame

		-- Write end message to the output file
		write(out_line, string'("# End of simulation"));
		writeline(data_outfile, out_line);

		-- End of simulation
		report "All output data is received";
		report "Stopping simulation";
		clk_want_stop <= true;
		wait;

	end process;

end architecture;

