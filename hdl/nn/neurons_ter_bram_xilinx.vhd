
-- This is a block of neurons, around one Xilinx BRAM
-- Dedicated to ternary weights

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

-- For BRAM macro declaration
library unimacro;
use unimacro.vcomponents.all;

entity neurons_ter_bram_xilinx is
	generic (
		-- Parameters for the neurons
		WDATA  : natural := 2;
		SDATA  : boolean := true;
		SFIXED : boolean := true;
		WACCU  : natural := 12;
		-- Parameters for BRAM usage
		NPERBLK : natural := 18;
		WRNB    : natural := 2;
		-- Parameters for frame and number of neurons
		WADDR  : natural := 10;
		FSIZE  : natural := 1024;
		NBNEU  : natural := 18
	);
	port (
		clk             : in  std_logic;
		-- Control signals
		ctrl_we_mode    : in  std_logic;
		ctrl_we_shift   : in  std_logic;
		ctrl_we_valid   : in  std_logic;
		ctrl_accu_clear : in  std_logic;
		ctrl_accu_add   : in  std_logic;
		ctrl_shift_en   : in  std_logic;
		ctrl_shift_copy : in  std_logic;
		-- Address in BRAM, for write and for read
		addr_in         : in  std_logic_vector(WADDR-1 downto 0);
		-- Ports for Write Enable
		we_prev         : in  std_logic;
		write_data      : in  std_logic_vector(2*NPERBLK*WRNB-1 downto 0);
		we_next         : out std_logic;
		-- Data input, 2 bits
		data_in         : in  std_logic_vector(WDATA-1 downto 0);
		data_in_signed  : in  std_logic;
		-- Scan chain to extract values
		sh_data_in      : in  std_logic_vector(WACCU-1 downto 0);
		sh_data_out     : out std_logic_vector(WACCU-1 downto 0);
		-- Sensors, for synchronization with the controller
		sensor_shift    : out std_logic;
		sensor_copy     : out std_logic;
		sensor_we_mode  : out std_logic;
		sensor_we_shift : out std_logic;
		sensor_we_valid : out std_logic
	);
end neurons_ter_bram_xilinx;

architecture synth of neurons_ter_bram_xilinx is

	-- The read data width for the BRAM
	constant WDATABRAM : natural := 2 * NBNEU;
	-- The BRAM data widthes
	constant WRD : natural := 2 * NPERBLK;
	constant WWR : natural := 2 * NPERBLK * WRNB;

	-- A data type to hold BRAM configuration
	type bram_t is record
		-- The block size to use: 18 or 36
		BLKSZ  : natural;
		-- The address width for the BRAMs
		WWADDR : natural;
		RWADDR : natural;
		-- The width for the WE port of the BRAM: 4 or 8 bits
		WWE    : natural;
	end record;

	function calc_waddr36k(dw : natural) return natural is
		variable aw: natural := 0;
	begin
		if dw <= 1 then
			aw := 15;
		elsif dw <= 2 then
			aw := 14;
		elsif dw <= 4 then
			aw := 13;
		elsif dw <= 9 then
			aw := 12;
		elsif dw <= 18 then
			aw := 11;
		elsif dw <= 36 then
			aw := 10;
		elsif dw <= 72 then
			aw := 9;
		end if;
		return aw;
	end function;

	function calc_bram(phony : boolean) return bram_t is
		variable bram: bram_t := (0, 0, 0, 0);
	begin
		-- Sanity checks
		if WWR > 72 then
			report "Implementation not possible because BRAM36 write width too large" severity FAILURE;
		end if;
		if WRD*FSIZE > 36 * 1024 then
			report "Implementation not possible because BRAM36 size exceeded" severity FAILURE;
		end if;
		-- Check if BRAM 36k is mandatory
		if WWR > 36 then
			bram.BLKSZ := 36;
		end if;
		if WRD*FSIZE > 18 * 1024 then
			bram.BLKSZ := 36;
		end if;
		-- Otherwise, use BRAM 18k
		if bram.BLKSZ = 0 then
			bram.BLKSZ := 18;
		end if;
		-- Calculate width of addresses
		bram.WWADDR := calc_waddr36k(WWR);
		bram.RWADDR := calc_waddr36k(WRD);
		if bram.BLKSZ = 18 then
			bram.WWADDR := bram.WWADDR - 1;
			bram.RWADDR := bram.RWADDR - 1;
		end if;
		-- Calculate width of WE signal
		-- FIXME Here we assume that WWR is an appropriate enough multiple of 8...
		bram.WWE := WWR / 8;
		if bram.WWE = 0 then
			bram.WWE := 1;
		end if;
		if bram.WWE > 8 then
			bram.WWE := 8;
		end if;
		return bram;
	end function;

	function gen_bram_str(sz : natural) return string is
	begin
		if sz = 18 then
			return "18Kb";
		end if;
		if sz = 36 then
			return "36Kb";
		end if;
		report "ERROR Wrong BRAM type" severity FAILURE;
		return "36Kb";
	end function;

	-- Select an appropriate BRAM configuration
	constant BRAM : bram_t := calc_bram(false);
	constant BRAM_STR : string := gen_bram_str(BRAM.BLKSZ);
	constant BRAM_WE  : std_logic_vector(BRAM.WWE-1 downto 0) := (others => '1');

	-- First stage buffers for control signals
	signal ctrl1_we_mode    : std_logic := '0';
	signal ctrl1_we_shift   : std_logic := '0';
	signal ctrl1_we_valid   : std_logic := '0';
	signal ctrl1_accu_clear : std_logic := '0';
	signal ctrl1_accu_add   : std_logic := '0';
	signal ctrl1_shift_en   : std_logic := '0';
	signal ctrl1_shift_copy : std_logic := '0';
	-- First stage buffer for BRAM address
	signal addr_in1         : std_logic_vector(WADDR-1 downto 0) := (others => '0');
	-- First stage buffer for Write Data
	signal write_data1      : std_logic_vector(WWR-1 downto 0) := (others => '0');
	-- First stage buffer for Input data
	signal data_in1         : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	signal data_in_signed1  : std_logic := '0';

	-- Second stage buffers for control signals
	signal ctrl2_accu_clear : std_logic := '0';
	signal ctrl2_accu_add   : std_logic := '0';
	signal ctrl2_shift_en   : std_logic := '0';
	signal ctrl2_shift_copy : std_logic := '0';
	-- Second stage buffer for Input data
	signal data_in2         : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	-- Signals to instantiate the BRAM
	signal bram_rd2         : std_logic_vector(WRD-1 downto 0) := (others => '0');

	-- Third stage buffers for control signals
	signal ctrl3_accu_clear : std_logic := '0';
	signal ctrl3_accu_add   : std_logic := '0';
	signal ctrl3_shift_en   : std_logic := '0';
	signal ctrl3_shift_copy : std_logic := '0';
	-- Third stage buffer for Input data
	signal data_in3         : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	-- Third stage buffer for BRAM output
	signal bram_rd3         : std_logic_vector(WDATABRAM-1 downto 0) := (others => '0');

	-- Prevent that reg to be absorbed into the BRAM (better for delay)
	attribute keep : string;
	attribute keep of bram_rd3 : signal is "TRUE";

	-- Fourth stage buffers for control signals
	signal ctrl4_accu_clear : std_logic := '0';
	signal ctrl4_shift_en   : std_logic := '0';
	signal ctrl4_shift_copy : std_logic := '0';
	-- Fourth stage buffer for Input data
	signal data_in4         : std_logic_vector(WACCU-1 downto 0) := (others => '0');

	-- The internal WE register
	signal regwe : std_logic := '0';
	-- The WE signal for the BRAM
	signal sigwe : std_logic := '0';
	-- Intermediate signals to instantiate the BRAM
	signal sigrdaddr : std_logic_vector(BRAM.RWADDR-1 downto 0);
	signal sigwraddr : std_logic_vector(BRAM.WWADDR-1 downto 0);

	-- The inter-neuron connexions
	signal sigshaccu_in  : std_logic_vector(NBNEU*WACCU-1 downto 0) := (others => '0');
	signal sigshaccu_out : std_logic_vector(NBNEU*WACCU-1 downto 0) := (others => '0');

	-- Component declaration: one neuron, Xilinx-specific implementation
	component neuron_ter_shift_xilinx is
		generic(
			WACCU : natural := 12
		);
		port(
			clk         : in  std_logic;
			-- Control signals
			accu_clear  : in  std_logic;
			accu_add    : in  std_logic;
			sh_copy     : in  std_logic;
			sh_enable   : in  std_logic;
			-- Data input, 2 bits
			weight_in   : in  std_logic_vector(1 downto 0);
			data_in     : in  std_logic_vector(WACCU-1 downto 0);
			-- Scan chain to extract values
			sh_data_in  : in  std_logic_vector(WACCU-1 downto 0);
			sh_data_out : out std_logic_vector(WACCU-1 downto 0)
		);
	end component;

begin

	-------------------------------------------------------------------
	-- Sequential process
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers for control signals
			ctrl1_we_mode    <= ctrl_we_mode;
			ctrl1_we_shift   <= ctrl_we_shift;
			ctrl1_we_valid   <= ctrl_we_valid;
			ctrl1_accu_clear <= ctrl_accu_clear;
			ctrl1_accu_add   <= ctrl_accu_add;
			ctrl1_shift_en   <= ctrl_shift_en;
			ctrl1_shift_copy <= ctrl_shift_copy;
			-- First stage buffer for BRAM address
			addr_in1         <= addr_in;
			-- First stage buffer for Write Data
			write_data1      <= write_data;
			-- First stage buffer for Input data
			data_in1         <= data_in;
			data_in_signed1  <= data_in_signed;

			-- Second stage buffers for control signals
			ctrl2_accu_clear <= ctrl1_accu_clear;
			ctrl2_accu_add   <= ctrl1_accu_add;
			ctrl2_shift_en   <= ctrl1_shift_en;
			ctrl2_shift_copy <= ctrl1_shift_copy;
			-- Second stage buffer for Input data
			data_in2         <= data_in1;

			-- Third stage buffers for control signals
			ctrl3_accu_clear <= ctrl2_accu_clear;
			ctrl3_accu_add   <= ctrl2_accu_add;
			ctrl3_shift_en   <= ctrl2_shift_en;
			ctrl3_shift_copy <= ctrl2_shift_copy;
			-- Third stage buffer for Input data
			data_in3         <= data_in2;
			-- Third stage buffer for BRAM output
			bram_rd3         <= bram_rd2(WDATABRAM-1 downto 0);

			-- Fourth stage buffers for control signals
			ctrl4_accu_clear <= ctrl3_accu_clear;
			ctrl4_shift_en   <= ctrl3_shift_en;
			ctrl4_shift_copy <= ctrl3_shift_copy;
			-- Fourth stage buffer for Input data - perform sign extension
			-- When signed, it's good to perform sign extension before last stage, to reduce fanout on data MSB (18 neu * 10 b = 180)
			if (WDATA = 1) or ((SFIXED = true) and (SDATA = false)) or ((SFIXED = false) and (data_in_signed1 = '0')) then
				data_in4 <= std_logic_vector(resize(unsigned(data_in3), WACCU));
			else
				data_in4 <= std_logic_vector(resize(signed(data_in3), WACCU));
			end if;

			-- The WE register
			if ctrl1_we_mode = '0' then
				regwe <= '0';
			else
				if ctrl1_we_shift = '1' then
					regwe <= we_prev;
				end if;
			end if;

		end if;  -- Rising edge of clock
	end process;



	-------------------------------------------------------------------
	-- Instantiate the BRAM
	-------------------------------------------------------------------

	sigwe     <= regwe and ctrl1_we_valid;
	sigrdaddr <= std_logic_vector(resize(unsigned(addr_in1), BRAM.RWADDR));
	sigwraddr <= std_logic_vector(resize(unsigned(addr_in1), BRAM.WWADDR));

	bram_i : BRAM_SDP_MACRO
		generic map (
			BRAM_SIZE => BRAM_STR,          -- Target BRAM, "18Kb" or "36Kb"
			DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6"
			WRITE_WIDTH => WWR,             -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			READ_WIDTH => WRD,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
			DO_REG => 0,                    -- Optional output register (0 or 1)
			INIT_FILE => "NONE",
			SRVAL => X"000000000000000000", -- Set/Reset value for port output
			WRITE_MODE => "READ_FIRST",     -- Specify "READ_FIRST" for same clock or synchronous clocks, or "WRITE_FIRST for asynchrononous clocks on ports
			INIT => X"000000000000000000"   -- Initial value on output port
		)
		port map (
			-- Output read data port, width defined by READ_WIDTH parameter
			DO     => bram_rd2,
			-- Input write data port, width defined by WRITE_WIDTH parameter
			DI     => write_data1,
			-- Input read address, width defined by read port depth
			RDADDR => sigrdaddr,
			-- 1-bit input read clock
			RDCLK  => clk,
			-- 1-bit input read port enable
			RDEN   => ctrl1_accu_add,
			-- 1-bit input read output register clock enable
			REGCE  => '1',
			-- 1-bit input reset
			RST    => '0',
			-- Input write enable, width defined by write port depth
			WE     => BRAM_WE,
			-- Input write address, width defined by write port depth
			WRADDR => sigwraddr,
			-- 1-bit input write clock
			WRCLK  => clk,
			-- 1-bit input write port enable
			WREN   => sigwe
		);



	-------------------------------------------------------------------
	-- Instantiate the neurons
	-------------------------------------------------------------------

	gen_neurons: for i in 0 to NBNEU-1 generate

		i_neuron: neuron_ter_shift_xilinx
			generic map (
				WACCU => WACCU
			)
			port map(
				clk         => clk,
				-- Control signals
				accu_clear  => ctrl4_accu_clear,
				accu_add    => ctrl3_accu_add,
				sh_copy     => ctrl4_shift_copy,
				sh_enable   => ctrl4_shift_en,
				-- Data input
				weight_in   => bram_rd3(i*2+1 downto i*2),
				data_in     => data_in4,
				-- Scan chain to extract values
				sh_data_in  => sigshaccu_in(i*WACCU+WACCU-1 downto i*WACCU),
				sh_data_out => sigshaccu_out(i*WACCU+WACCU-1 downto i*WACCU)
			);

	end generate;

	-- Input of scan chain
	sigshaccu_in(NBNEU*WACCU-1 downto (NBNEU-1)*WACCU) <= sh_data_in;

	-- Inter-neuron connections
	gen_neurons_conn: for i in 0 to NBNEU-2 generate
		sigshaccu_in(i*WACCU+WACCU-1 downto i*WACCU) <= sigshaccu_out((i+1)*WACCU+WACCU-1 downto (i+1)*WACCU);
	end generate;



	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	we_next <= regwe;

	sh_data_out <= sigshaccu_out(WACCU-1 downto 0);

	sensor_shift    <= ctrl4_shift_en;
	sensor_copy     <= ctrl4_shift_copy;
	sensor_we_mode  <= ctrl1_we_mode;
	sensor_we_shift <= ctrl1_we_shift;
	sensor_we_valid <= ctrl1_we_valid;

end architecture;


