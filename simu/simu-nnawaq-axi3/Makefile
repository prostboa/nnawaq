
################################
# Testcase-specific variables
################################

TOPNAME = nnawaq_axi3_bench

HDLDIR_BENCH = .

# Need Vivado simulation files for BRAM / DSP macros
#UNISIM_DIR   = ../unisim-vivado
#UNIMACRO_DIR = ../unimacro-vivado

GHDLGENERIC =

GHDLSIMUFLAGS =
#GHDLSIMUFLAGS = --stop-time=100us

WAVEFILE = wave.ghw

# Copy the network wrapper to dependencies
analyze   : nnawaq_axi3.vhd
compile   : nnawaq_axi3.vhd
simu      : nnawaq_axi3.vhd
simu-wave : nnawaq_axi3.vhd
simu-fst  : nnawaq_axi3.vhd


################################
# Most compilation and simulation recipes are in common Makefile
################################

include ../Makefile.inc


################################
# Recipes specific to this testcase
################################

.ONESHELL :

# Convenient alias recipe to force re-generation of network
gen-network : nnawaq_axi3.vhd

# Generate or re-generate the network within the AXI3 template
nnawaq_axi3.vhd : ../../hdl/nn-wrappers/nnawaq_axi3.vhd ../../hdl/nn-wrappers/nnawaq_axi3.template.vhd
	HERE=$$PWD
	cd ../../tools/nnawaq-tcl
	VHDL=1 CSTPARAMS=1 DATASET=mnist NET=lenet5 BOARD=axi3 ./run_network.tcl
	cd $$HERE
	cp ../../hdl/nn-wrappers/neurons_const_weights.vhd ./
	cp ../../hdl/nn-wrappers/nnawaq_axi3.vhd ./

# FIXME Need a kind of virtual target just to save the input frame file

.PRECIOUS : simu_output.txt

# FIXME The analyze recipe does not re-analyze when VHDL files have changed, need to manually re-analyze
simu_output.txt : nnawaq_axi3.vhd nnawaq_axi3_bench.vhd input_frames.txt
	$(MAKE) analyze
	ghdl -a $(GHDLFLAGS_WORK) nnawaq_axi3_bench.vhd nnawaq_axi3.vhd neurons_const_weights.vhd
	$(MAKE) simu

check-out : output_classes_golden.txt simu_output.txt
	diff -q output_classes_golden.txt simu_output.txt

all :
	$(MAKE) simu_output.txt
	$(MAKE) check-out

# This is for latency at processing 1-2 frames (to be launched manually)
latency : wave.fst
	gtkwave --script=gtkwave-latency.tcl

