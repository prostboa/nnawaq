#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It generates the network LeNet-5

# Input images : 28x28x1 8b unsigned (but input is stored 32x32 to get rid of padding)
# Output : 10 classes

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

if { ! [info exists freq]} {
	set freq 100
}
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

# Parameter useful for simulation tests
set inchan 1
if {[info exists env(INCHAN)]} {
	set inchan $env(INCHAN)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1
# To force usage of DSPs in neuron multiplications (0=auto, 1=logic, 2=DSP)
#set neu_mul_id ""
#set neu_mul_id custom_mul=1
set neu_mul_id custom_mul=2

nn_set f=32/32/$inchan
nn_set fn=1
nn_set inpar=1

nn_set in=8u
nn_set acts=8u
nn_set weights=8s

nn_set neu_style=2

# FIXME The width of configurable parameters for bias, scale and shift can be optimized
# FIXME These values are only meant to make configuration port width at most 32b to fit the current Zybo wrapper
# FIXME The width of bias should be at most the width of the input data, instead of a fixed width
nn_set norm_wbias=20
nn_set norm_wmul=7
nn_set norm_mulcst=0
nn_set norm_shrcst=0
nn_set norm_wshr=4
nn_set relu=0/255

nn_set no_fifo_win_pool=false
nn_set no_fifo_neu_relu=false
nn_set no_fifo_norm_relu=true

proc create_nn_conv_neu {win step pad neu nwin} {
	global neu_mul_id
	nn_layer_create window $win $step $pad $nwin
	nn_layer_create neuron $neu $neu_mul_id
	nn_layer_create norm
	nn_layer_create relu
}
proc create_nn_conv_pool {win step pad} {
	nn_layer_create window $win $step $pad
	nn_layer_create maxpool
}


# image 32x32x1 (padding is "embedded" from original images 28x28)
create_nn_conv_neu win=5 step=1 pad=0 neu=6 nwin=28

# image 28x28x6
create_nn_conv_pool win=2 step=2 pad=0
# image 14x14x6
create_nn_conv_neu win=5 step=1 pad=0 neu=16 nwin=10
# image 10x10x16
create_nn_conv_pool win=2 step=2 pad=0
# image 5x5x16

nn_layer_create flatten

# image 1x1x400
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1 step=1 pad=0
}
nn_layer_create neuron neu=120 $neu_mul_id
nn_layer_create norm
nn_layer_create relu

# image 1x1x120
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1 step=1 pad=0
}
nn_layer_create neuron neu=84 $neu_mul_id
nn_layer_create norm
nn_layer_create relu

# image 1x1x84
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1 step=1 pad=0
}
nn_layer_create neuron neu=10 $neu_mul_id
# image 1x1x10
nn_layer_create softmax
# image 1x1x1

# For information, print the network details
nn_print -cycles

# Slight truncation to reduce the DSP usage in NORM layer
# FIXME A better alternative would be to constrain the width after addition of bias (25b signed or 24b unsigned)
nn_layer_set neu2 wout=24

# Get the bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
puts "Applying parallelism levels for PAR=$par"
nn_autopar $par
puts "Applying time multiplexing to fastest layers"
nn_autotmux
puts "Applying more parallelism to reduce the number of physical neurons"
nn_maxparin

# For information, print the network details
nn_print -cycles

# Get the new bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

# Fix storage of win0, it may be estimated as 25 BRAM because there are 25 read ports
if {[nn_layer_get win0 par_out] == 25} {
	nn_layer_set win0 mem=lutram
}

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# To help tweak per-layer memory implem
#nn_print_mem -type-neu
#nn_print_mem -type-win
foreach layer [list win neu norm] { nn_print_mem -type-$layer }
nn_print_mem -total

