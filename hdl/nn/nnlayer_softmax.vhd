
-- This block applies SoftMax operation

-- FIXME Current limitation : May not scale well with large PAR_IN

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_softmax is
	generic(
		WDATA : natural := 8;
		SDATA : boolean := false;
		WOUT  : natural := 8;
		FSIZE : natural := 1024;
		PAR_IN : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk            : in  std_logic;
		clear          : in  std_logic;
		-- The user-specified frame size
		user_fsize     : in  std_logic_vector(WOUT-1 downto 0);
		-- Data input
		data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_softmax;

architecture synth of nnlayer_softmax is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : std_logic) return natural is
	begin
		if b = '1' then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- First stage buffers
	signal clear1      : std_logic := '0';
	signal data_in1    : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal data_valid1 : std_logic := '0';

	-- Signals to find the max of inputs, combinatorial
	signal best_data   : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	signal best_addr   : unsigned(WOUT-1 downto 0) := (others => '0');

	-- Second stage buffers
	signal data_accu2  : std_logic_vector(WDATA-1 downto 0) := (others => '0');
	signal data_addr2  : unsigned(WOUT-1 downto 0) := (others => '0');
	signal data_end2   : std_logic := '0';

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';
	-- This register indicats this is the last neuron value
	signal reg_end_frame : std_logic := to_std_logic(FSIZE = 1);

	-- Use the parameter FSIZE as maximum memory size
	constant WADDR : natural := storebitsnb(FSIZE-1);

	-- The internal Address register
	signal reg_addr : unsigned(WADDR-1 downto 0) := (others => '0');
	-- Utility registers to reduce critical path
	signal buf_fsize_is1 : std_logic;
	signal buf_fsize_m2 : unsigned(WADDR-1 downto 0);

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		buf_fsize_is1 <= to_std_logic(unsigned(user_fsize) = 1);

		process(clk)
		begin
			if rising_edge(clk) then

				-- Note : Using a phony resize to avoid underflow with very small input
				buf_fsize_m2 <= resize(unsigned('1' & user_fsize) - 2, WADDR);
				--buf_fsize_m2 <= unsigned(user_fsize) - 2;

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_fsize_is1 <= to_std_logic(FSIZE = 1);
		buf_fsize_m2  <= to_unsigned(maximum(0, FSIZE-2), WADDR);

	end generate;

	-------------------------------------------------------------------
	-- State machine
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers
			clear1        <= clear;
			data_in1      <= data_in;
			data_valid1   <= data_in_valid and buf_in_ready;

			-- Second stage buffers
			data_accu2    <= data_accu2;  -- The max value
			data_addr2    <= data_addr2;  -- The position of the max value
			data_end2     <= '0';         -- One-cycle flag that indicates validity of result

			-- Validate data entry in the pipeline
			buf_in_ready <= '0';
			if unsigned(out_fifo_room) >= 4 + FIFOMARGIN then
				buf_in_ready <= '1';
			end if;
			-- Even if there is not enough room in the pipeline, continue accepting inputs until the last word
			-- FIXME This code is not ready yet : when reg_end_frame becomes '1' it is too late
			--if reg_end_frame = '0' then
			--	buf_in_ready <= '1';
			--end if;

			-- The Address register
			if data_valid1 = '1' then
				reg_addr <= reg_addr + 1;
				reg_end_frame <= '0';
				if (reg_addr = buf_fsize_m2) or (buf_fsize_is1 = '1')then
					reg_end_frame <= '1';
				end if;
				if reg_end_frame = '1' then
					reg_addr <= (others => '0');
				end if;
			end if;

			-- Main datapath
			if data_valid1 = '1' then
				-- Main operation with combinatorial block
				if
					(reg_addr = 0) or
					((SDATA = false) and (unsigned(best_data) > unsigned(data_accu2))) or
					((SDATA =  true) and (  signed(best_data) >   signed(data_accu2)))
				then
					data_accu2 <= best_data;
					data_addr2 <= resize(reg_addr * PAR_IN, WOUT) + best_addr;
				end if;
				-- Output validity flag
				data_end2 <= reg_end_frame;
			end if;

			-- General clear
			if clear1 = '1' then
				data_valid1   <= '0';
				data_end2     <= '0';
				reg_addr      <= (others => '0');
				reg_end_frame <= buf_fsize_is1;
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Computation of the max of inputs, combinatorial
	-------------------------------------------------------------------

	-- FIXME This will not scale well with large PAR_IN

	process(all)
		variable var_data      : std_logic_vector(WDATA-1 downto 0);
		variable var_best_data : std_logic_vector(WDATA-1 downto 0);
		variable var_best_addr : unsigned(WOUT-1 downto 0);
	begin

		var_best_data := data_in1(WDATA-1 downto 0);
		var_best_addr := to_unsigned(0, WOUT);

		if PAR_IN > 1 then
			for i in 1 to PAR_IN-1 loop
				var_data := data_in1((i+1)*WDATA-1 downto i*WDATA);
				if
					((SDATA = false) and (unsigned(var_data) > unsigned(var_best_data))) or
					((SDATA =  true) and (  signed(var_data) >   signed(var_best_data)))
				then
					var_best_data := var_data;
					var_best_addr := to_unsigned(i, WOUT);
				end if;
			end loop;
		end if;

		best_data <= var_best_data;
		best_addr <= var_best_addr;

	end process;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready <= buf_in_ready;

	data_out       <= std_logic_vector(data_addr2);
	data_out_valid <= data_end2;

end architecture;

