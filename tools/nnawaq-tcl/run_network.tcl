#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It runs a VGG-like neural network in different conditions :
# - software execution, as CPU reference and for golden results
# - hardware accelerator already configured in the PCIe FPGA board

# Settings inherited from environment variables :
# - NET=<name> ...... name of neural network
# - DATASET=<name> .. name of dataset

# Environment variables to select what to do :
# - VHDL=1 .. generate VHDL
# - SW=1 .... run software execution
# - HW=1 .... run execution on hardware

# Other variables :
# - FRAMES=<f> ........ number of frames to process
# - HWBLIND=1 ......... assume the in-hardware network is known
# - PCIETIMEOUT=<ms> .. timeout of PCIe requests in milliseconds

# FIXME HW and HWBLIND execution may not need a tcl script for config (and also no network) if all parameters are built-in constants

global env

set cstparams 0
set vhdl 0
set swexec 0
set hwexec 0
set hwblind 0
set hwtimeout 1000ms
set frames 10

if {[info exists env(CSTPARAMS)]} {
	set cstparams 1
}
if {[info exists env(VHDL)]} {
	set vhdl 1
}
if {[info exists env(SW)]} {
	set swexec 1
}
if {[info exists env(HW)]} {
	set hwexec 1
}
if {[info exists env(HWBLIND)]} {
	set hwblind 1
}
if {[info exists env(PCIETIMEOUT)]} {
	set hwtimeout $env(PCIETIMEOUT)
}
if {[info exists env(FRAMES)]} {
	set frames $env(FRAMES)
}

# Select network

set network ""

if {[info exists env(NET)]} {
	set network $env(NET)
}

set tcl_build ""
set tcl_config ""

if { $network == "fc" } {
	set tcl_build  build_fc_mnist.tcl
	set tcl_config config_fc_mnist.tcl
} elseif { $network == "lenet5" } {
	set tcl_build  build_lenet5.tcl
	set tcl_config config_lenet5.tcl
} elseif { $network == "vgg" } {
	set tcl_build  build_vgg.tcl
	set tcl_config config_vgg.tcl
} elseif { $network == "mobilenetv1" } {
	set tcl_build  build_mobilenetv1.tcl
	set tcl_config config_mobilenetv1.tcl
} elseif { $network == "mobilenetv2" } {
	set tcl_build  build_mobilenetv2.tcl
	set tcl_config config_mobilenetv2.tcl
} elseif { $network == "resnet18" } {
	set tcl_build  build_resnet18.tcl
	set tcl_config config_resnet18.tcl
} elseif { $network == "resnet50" } {
	set tcl_build  build_resnet50.tcl
} elseif { $network == "wafer" } {
	set tcl_build  build_waferclass.tcl
	set tcl_config config_waferclass.tcl
} elseif { $network != "" }  {
	puts "Error : No config for network '$network'"
	exit 1
}

# Select board

set board ""
set board_ifw 0

if {[info exists env(BOARD)]} {
	set board $env(BOARD)
	if {$board == "riffa64"} {
		set board_ifw 64
		set freq 250
	} elseif {$board == "riffa128"} {
		set board_ifw 128
		set freq 250
	} elseif {$board == "vc709"} {
		set board_ifw 64
		set freq 250
	} elseif {$board == "zc706"} {
		set board_ifw 64
		set freq 250
	} elseif {$board == "zybo"} {
		set board_ifw 32
		set freq 100
	} elseif {$board == "axi3"} {
		set board_ifw 32
	} elseif {$board == "axi4lite"} {
		set board_ifw 32
	} elseif {$board == "vcu128"} {
		set board_ifw 32
		nn_set use_uram=1
		set freq 250
	} elseif {$board == "simu-const"} {
		set board_ifw 32
	} else {
		puts "Error : No config for board '$board'"
		exit 1
	}
}

if {[info exists env(IFW)]} {
	set board_ifw $env(IFW)
}

if {[info exists env(URAM)]} {
	nn_set use_uram=$env(URAM)
}

# Build the network

nn_set hw_timeout=$hwtimeout

if {$hwexec == 1} {

	puts "Reading the network from device"

	# Read network architecture from FPGA
	nn_hwacc_init
	nn_hwacc_build

	# Print the network, for visual confirmation
	nn_print

} else {

	# General parameters
	nn_set selout=0
	nn_set fifomon=0
	nn_set noregs=0
	nn_set rdonly=1

	# Network
	# If the network is specified, assume that other tcl files are set accordingly
	if { $network == "" } {
		puts "Error : Missing config for network"
		exit 1
	}

	# Data interface width
	if {$board_ifw >= 0} {
		nn_set ifw=$board_ifw
	} else {
		puts "Error : Need to specify a target board to have the interface width"
		exit 1
	}

	puts "Building the network"

	#nn_set no_fifo_win_neu_th=8
	#nn_set no_fifo_win_pool=true
	#nn_set no_fifo_neu_relu=true

	# Create network
	source $tcl_build

}

# Generate VHDL and exit

if {$vhdl == 1} {

	puts "Setting constant weights"

	# Load constant weights for appropriate layers
	if {[info exists env(DATASET)]} {
		# Count layers with no need for memory storage
		set num_layers_const 0
		foreach layer [nn_get layers -type-neu] {
			set memlines [expr [nn_layer_get $layer fsize_in] * [nn_layer_get $layer tmux] / [nn_layer_get $layer par_in] ]
			if { $memlines == 1 || $cstparams == 1 } {
				nn_layer_set $layer const_params=1
				incr num_layers_const
			}
		}
		foreach layer [nn_get layers -type-norm] {
			set memlines [expr [nn_layer_get $layer fsize_in] / [nn_layer_get $layer par_in] ]
			if { $memlines == 1 || $cstparams == 1 } {
				nn_layer_set $layer const_params=1
				incr num_layers_const
			}
		}
		foreach layer [nn_get layers -type-ter] {
			set memlines [expr [nn_layer_get $layer fsize_in] / [nn_layer_get $layer par_in] ]
			if { $memlines == 1 || $cstparams == 1 } {
				nn_layer_set $layer const_params=1
				incr num_layers_const
			}
		}
		# Load parameters for layers with constant parameters
		if { $num_layers_const > 0 } {
			source $tcl_config
			nn_load_config
		}
	}

	puts "Generating VHDL"

	if {$board == "riffa64" || $board == "riffa128" || $board == "vc709" || $board == "zc706"} {

		# Generate the VHDL implementation
		set fi ../../hdl/nn-wrappers/nnawaq_riffa.template.vhd
		set fo ../../hdl/nn-wrappers/nnawaq_riffa.vhd
		nn_genvhdl $fi $fo

	} elseif {$board == "zybo"} {

		# Generate the VHDL implementation
		# FIXME This should be moved as a shared non-project, non-IP, axi4 template wrapper (to be developped)
		set fi ../../boards/zybo/cnn_axi_master_1.0/hdl-template/cnn_axi_master_v1_0_S00_AXI.template.vhd
		set fo ../../boards/zybo/cnn_axi_master_1.0/hdl/cnn_axi_master_v1_0_S00_AXI.vhd
		nn_genvhdl $fi $fo

	} elseif {$board == "axi3" || $board == "vcu128"} {

		# Generate the VHDL implementation
		# FIXME This uses very low-perf data transfers with read and write to slave registers
		set fi ../../hdl/nn-wrappers/nnawaq_axi3.template.vhd
		set fo ../../hdl/nn-wrappers/nnawaq_axi3.vhd
		nn_genvhdl $fi $fo

	} elseif {$board == "axi4lite"} {

		# Generate the VHDL implementation
		# FIXME This uses very low-perf data transfers with read and write to slave registers
		set fi ../../hdl/nn-wrappers/nnawaq_axi4lite.template.vhd
		set fo ../../hdl/nn-wrappers/nnawaq_axi4lite.vhd
		nn_genvhdl $fi $fo

	} elseif {$board == "simu-const"} {

		# Generate the VHDL implementation
		set fi ../../hdl/nn-wrappers/nnawaq_simu_const.template.vhd
		set fo ../../hdl/nn-wrappers/nnawaq_simu_const.vhd
		nn_genvhdl $fi $fo

	} else {
		puts "Error : No VHDL wrapper config for board '$board'"
		exit 1
	}

	# Generate the VHDL file with constant weights, in case there are some
	set fi ../../hdl/nn-const/neurons_const_weights.vhd
	set fo ../../hdl/nn-wrappers/neurons_const_weights.vhd
	nn_genvhdl_const_weights $fi $fo

	exit 0

}

# Associate and load configuration files

source $tcl_config

nn_load_config

# Actual run

nn_set fn=$frames ol=last onl=0

# Note: Other useful options with raw output to ease debug (from obsolete command-line) :
# -osep ' ' -omask -ofmt '%02x' -ofmt-norm '%7.3g' -swexec-gen-in -swexec-mod 0

# Note: to get full speed, use option -noout
# Note: to only count output data, use option -freerun and observe reg 6

if {$swexec == 1} {

	# Execution in software
	nn_set o=out-sw.txt
	nn_swexec

} elseif {$hwexec == 1} {

	# Execution in FPGA
	nn_set o=out-hw.txt
	nn_hwacc_run

} elseif {$hwblind == 1} {

	# Execution in FPGA, blind mode : don't try to read the network architecture from the device
	nn_set o=out-hw.txt
	nn_hwacc_init
	nn_hwacc_run -blind

}

