#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It builds a convolutional neural network with log representation
# See paper : Low-precision logarithmic arithmetic for neural network accelerators (2022)

# Input images : 32x32x3 8b
# Output : 100 classes

puts "Creating LNS neural network"

# Settings inherited from environment variables

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

set neu_ref 64
if {[info exists env(NEU)]} {
	set neu_ref $env(NEU)
}

set nout 100
if {[info exists env(NOUT)]} {
	set nout $env(NOUT)
}

set indata 4u
if {[info exists env(INDATA)]} {
	set indata $env(INDATA)
}

set weights 5s
if {[info exists env(WEIGHTS)]} {
	set weights $env(WEIGHTS)
}

set freq 250
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1

set fx 32
set fy 32
set fz 3
if {[info exists env(FX)]} {
	set fx $env(FX)
}
if {[info exists env(FY)]} {
	set fy $env(FY)
}
if {[info exists env(FZ)]} {
	set fz $env(FZ)
}

nn_set fn=1
nn_set f=$fx/$fy/$fz

######################

nn_set neu_style=2

nn_set in=$indata
nn_set acts=4u
nn_set weights=$weights
nn_set outs=9s

# Setup custom multiplication operation
set custom_mul custom_mul=0/8s

# Declare custom component for ReLU + conversion to log format
nn_custom_layer_type -name relu_lns -entity nnlayer_relu_lns

puts "Summary of parameters"
puts "  FRAME .... $fx $fy $fz"
puts "  INDATA ... $indata"
puts "  WEIGHTS .. $weights"
puts "  NEU ...... $neu_ref"
puts "  NOUT ..... $nout"
puts "  PAR ...... $par"

# Helper procedures to create layers
# Note : Using minPool instead of usually maxPool because the activations are represented as -log(v)
proc conv {neu} {
	global custom_mul
	nn_layer_create window win=3x3 step=1x1 pad=1x1
	nn_layer_create neuron neu=$neu $custom_mul
	nn_layer_create relu_lns wout=4u
}
proc ccp {neu} {
	conv $neu
	conv $neu
	nn_layer_create window win=2x2 step=2x2 pad=0x0
	nn_layer_create minpool
}
proc cp {neu} {
	conv $neu
	nn_layer_create window win=2x2 step=2x2 pad=0x0
	nn_layer_create minpool
}

# Actually create the network
ccp [expr {1 * $neu_ref}]
ccp [expr {2 * $neu_ref}]
ccp [expr {4 * $neu_ref}]
cp  [expr {8 * $neu_ref}]

if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1x1 step=1x1 pad=0x0
}
nn_layer_create neuron neu=$nout $custom_mul

# For information, print the network topology
nn_print -cycles
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
nn_autopar $par
nn_autotmux
nn_print -cycles

# For information, print the network topology
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

# Distribution of memories in LUTRAM / BRAM :
# Increase the default threshold to fit the BRAM limits of board VC709
nn_set lut_threshold=2304

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# To help tweak per-layer memory implem
#nn_print_mem -type-neu
#nn_print_mem -type-win
foreach layer [list win neu] { nn_print_mem -type-$layer }
nn_print_mem -total

puts "Generating VHDL"

nn_set selout=1
nn_set fifomon=1
nn_set noregs=0
nn_set rdonly=1
nn_set ifw=64

# VHDL generation
set fi ../../boards/vc709/hdl-template/cnn_wrapper.template.vhd
set fo ../../boards/vc709/hdl/cnn_wrapper.vhd
nn_genvhdl $fi $fo

