
# Get interface width from VHDL file
set vhdl_ifw [exec sh -c "cat hdl/cnn_wrapper.vhd | grep 'constant CONFIG_IFW' | head -n 1 | tr -d ':=;' | awk '{print \$4}'"]
if { "$vhdl_ifw" eq "" } {
	puts "ERROR: Can't find PCIe interface width"
	exit 1
}

set pciegen 0
set pciesz 0

if { $vhdl_ifw eq 64 } {
	set pciegen 1
	set pciesz $vhdl_ifw
}
if { $vhdl_ifw eq 128 } {
	set pciegen 2
	set pciesz $vhdl_ifw
}
if { $pciegen eq 0 } {
	puts "ERROR: Invalid PCIe interface width: $vhdl_ifw"
	exit 1
}

# Miscellaneous useful variables
set top ZC706_Gen${pciegen}x4If$pciesz
set part xc7z045ffg900-2
# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]



# Note: For debug purposes, if you want to start from post- routing state,
# uncomment these lines and remove all above commands (except setting of parameters)
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $part

