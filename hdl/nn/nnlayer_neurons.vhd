
-- This is a neuron layer
-- This component can contain many neurons and have input and output parallelism
-- Pipeline order: multipliers, adder tree, accumulator, scan chain

-- About compression of ternary weights:
-- Use only with BRAM storage, WRNB=1, PACKED=true

-- About register storage:
-- Use only with PACKED=true and of course USE_LUTRAM=false

-- About time multiplexing (TIME_MUX > 1):
-- Purpose is to emulate a large number of logical neurons on a reduced number of physical neurons accumulators
--   - Weights are written and read in a contiguous way, up to address FSIZE * TIME_MUX - 1
--   - Signals accu_clear and shift_copy are sent to accumulators every FSIZE
-- So 3 registers are needed: one for address increment, one to count up to FSIZE-1, and one to count to TIME_MUX-1
-- Implementation is designed so that unused ones are removed by logic optimization

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity nnlayer_neurons is
	generic (
		-- Parameters for the neurons
		WDATA   : natural := 2;     -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 2;     -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WOUT    : natural := 12;    -- The accumulator bit width
		-- Parameters for memory usage
		NPERBLK : natural := 18;
		WRNB    : natural := 2;
		WWRITE  : natural := 64;
		USE_REGS   : boolean := false;
		USE_LUTRAM : boolean := false;
		USE_BRAM   : boolean := false;
		USE_URAM   : boolean := false;
		USE_CONST  : boolean := false;  -- To indicate weight values are built-in constants
		PACKED     : boolean := true;
		-- Identifier of neuron layer, mostly to be passed to custom internal components
		LAYER_ID   : natural := 0;
		-- For compression of weights in memory
		COMP_STYLE : natural := 0;  -- Compression style, 0 means no decoder on datapath
		COMP_WRAW  : natural := 0;  -- Size of raw data (must be a multiple of WWEIGHT)
		COMP_WENC  : natural := 0;  -- Size of an encoded word
		COMP_ENWR  : boolean := false;  -- Compression is implemented on Write side to be transparent to the controlling SW
		-- Parameters for frame and number of neurons
		FSIZE  : natural := 16;
		NBNEU  : natural := 16;
		-- Level of time multiplexing of the neuron accumulators
		TIME_MUX : natural := 1;
		-- Depth-Wise convolution mode : each activation goes to only one physical neuron
		DWCONV : boolean := false;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Options for neuron output
		SHREG_MUX       : boolean := false;
		SHREG_MUX_RADIX : natural := 18;
		-- Constant weights passed directly
		CSTWEIGHTS_NB  : natural := 1;
		CSTWEIGHTS_VEC : std_logic_vector(CSTWEIGHTS_NB*WWEIGHT-1 downto 0) := (others => '0');
		-- Identifier of multiplication operation
		CUSTOM_MUL_ID : natural := 0;
		CUSTOM_WMUL   : natural := 8;      -- Bit width of the multiplication result
		CUSTOM_SMUL   : boolean := false;  -- Signedness of the multiplication result
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port (
		clk            : in  std_logic;
		clear          : in  std_logic;
		-- Ports for Write Enable
		write_mode     : in  std_logic;
		write_data     : in  std_logic_vector(WWRITE-1 downto 0);
		write_enable   : in  std_logic;
		write_end      : out std_logic;
		-- The user-specified frame size and number of neurons
		user_fsize     : in  std_logic_vector(15 downto 0);
		user_nbneu     : in  std_logic_vector(15 downto 0);
		-- Data input, 2 bits
		data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Scan chain to extract values
		data_out       : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- Indicate to the parent component that we are reaching the end of the current frame
		end_of_frame   : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_neurons;

architecture synth of nnlayer_neurons is

	-- Compute the minimum number of bits needed to store the input value
	function log_in_base(vin : natural; b : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / b;
		end loop;
		return r;
	end function;

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- The total number of physical neurons
	constant NBNEU_PHY : natural := PAR_OUT * NBNEU;

	-- The number of input activations per physical neuron
	constant NEU_PAR_IN : natural :=
		to_integer(DWCONV = false) * PAR_IN +
		to_integer(DWCONV = true)  * ((PAR_IN + NBNEU_PHY - 1) / NBNEU_PHY);

	-- Default is generic implem of memory
	constant USE_AUTO : boolean := (USE_REGS = false) and (USE_LUTRAM = false) and (USE_BRAM = false) and (USE_URAM = false);

	-- For internal usage, ensure the TIME_MUX generic parameter is at least 1
	constant TIME_MUX_USE : natural := TIME_MUX + to_integer(TIME_MUX = 0);

	-- Max fanout for signals distributed to all BRAM-based blocks
	constant FANOUT : natural := 4;

	-- General configurations
	constant WEIGHTS_BIN_01  : boolean := (not SWEIGHT) and (WWEIGHT = 1) and (CUSTOM_MUL_ID = 0);
	constant WEIGHTS_BIN_SYM : boolean := SWEIGHT and (WWEIGHT = 1) and (CUSTOM_MUL_ID = 0);
	constant WEIGHTS_TERNARY : boolean := SWEIGHT and (WWEIGHT = 2) and (CUSTOM_MUL_ID = 0);

	-- Weight compression parameters
	-- Note : No decompressor for style 2t3b because this is fused within the multiplier
	constant COMP_ON_WRITE_PATH : boolean :=
		(COMP_STYLE /= 0) and (COMP_WRAW /= 0) and (COMP_WENC /= 0) and (COMP_WRAW /= COMP_WENC) and (COMP_ENWR = true);
	constant COMP_ON_READ_PATH : boolean :=
		(COMP_STYLE /= 0) and (COMP_WRAW /= 0) and (COMP_WENC /= 0) and (COMP_WRAW /= COMP_WENC) and (COMP_STYLE /= 2);

	-- For constant ternary weights
	-- Count the occurrences of weights -1 or +1, for one neuron
	function count_const_ter_weights_neu(po : natural; n : natural; v : std_logic_vector(1 downto 0)) return natural is
		variable num : natural := 0;
	begin
		num := 0;
		for i in po * n * NEU_PAR_IN to (po * n + 1) * NEU_PAR_IN - 1 loop
			if CSTWEIGHTS_VEC((i+1)*WWEIGHT-1 downto i*WWEIGHT) = v then
				num := num + 1;
			end if;
		end loop;
		return num;
	end function;

	-- For constant ternary weights, to ensure the depth of all adder trees is similar in all neurons
	-- Count the max number of occurrences of weights -1 and +1, max all neurons
	function count_const_ter_weights_max(phony : boolean) return natural is
		variable num : natural := 0;
	begin
		-- Return at least 1 so the vectors have a legal length
		num := 1;
		if (WEIGHTS_TERNARY = false) or (CSTWEIGHTS_NB <= 1) then
			return 1;
		end if;
		-- Scan weights for all neurons
		for po in 0 to PAR_OUT - 1 loop
			for n in 0 to NBNEU - 1 loop
				num := maximum(num, count_const_ter_weights_neu(po, n, "11"));
				num := maximum(num, count_const_ter_weights_neu(po, n, "01"));
			end loop;
		end loop;
		return num;
	end function;

	-- Only used if constant ternary weights are provided
	constant CSTWEIGHTS_TER_MAXNUM : natural := count_const_ter_weights_max(false);

	constant DATA_DELAY_CYCLES : natural :=
		to_integer(not PACKED) * (2 - to_integer(USE_LUTRAM)) +
		to_integer(PACKED) * 1;  -- delay is handled at instantiation of RAM

	constant WFSIZE   : natural := storebitsnb(FSIZE-1);
	constant WADDR_IN : natural := storebitsnb(FSIZE*TIME_MUX_USE-1);
	constant WADDR_SH : natural := storebitsnb(NBNEU-1);

	-- These are for non-packed version only, but needed here to get a value for FIFOMARGIN_USE
	-- The number of BRAM-based blocks to instantiate
	constant NEU_PER_BLOCK  : natural := maximum(NPERBLK / PAR_IN, 1);
	constant BLOCKS_PER_NEU : natural := (PAR_IN + NPERBLK - 1) / NPERBLK;
	-- The number of BRAM-based blocks to instantiate to get the memory width
	constant TOTALBLOCKS_PO : natural := BLOCKS_PER_NEU * ((NBNEU + NEU_PER_BLOCK - 1) / NEU_PER_BLOCK);
	constant TOTALBLOCKS    : natural := TOTALBLOCKS_PO * PAR_OUT;

	-- Take into account latency of distribution buffers + neurons + distributed add
	constant FIFOMARGIN_USE : natural :=
		FIFOMARGIN + storebitsnb(TOTALBLOCKS-1) + 6 + storebitsnb(NEU_PAR_IN-1) +
		to_integer(SHREG_MUX) * log_in_base(NBNEU, 18);

	signal reg_we_mode, reg_we_mode_n : std_logic := '0';
	signal sig_write_inc : std_logic := '0';
	signal sig_write_clr : std_logic := '0';

	signal reg_frame_doing, reg_frame_doing_n : std_logic := '0';
	signal reg_shift_doing, reg_shift_doing_n : std_logic := '0';

	signal reg_fr2sh_ready, reg_fr2sh_ready_n : std_logic := '0';
	signal reg_sh2fr_ready, reg_sh2fr_ready_n : std_logic := '0';

	signal reg_frame_beg, reg_frame_beg_n     : std_logic := to_std_logic(FSIZE <= 1);
	signal reg_shift_beg, reg_shift_beg_n     : std_logic := to_std_logic(NBNEU <= 1);

	signal reg_tmux_end, reg_tmux_end_n       : std_logic := to_std_logic(TIME_MUX <= 1);
	signal reg_frame_end, reg_frame_end_n     : std_logic := to_std_logic(FSIZE <= 1);
	signal reg_shift_end, reg_shift_end_n     : std_logic := to_std_logic(NBNEU <= 1);

	signal buf_user_fsize  : unsigned(15 downto 0);
	signal buf_user_nbneu  : unsigned(15 downto 0);
	signal buf_in_addr_end : unsigned(WADDR_IN-1 downto 0);
	signal buf_sh_addr_end : unsigned(WADDR_SH-1 downto 0);

	signal buf_out_ready   : std_logic := '0';
	signal reg_out_valid   : std_logic := '0';

	-- Internal address registers
	signal regtmux, regtmux_n       : integer range 0 to TIME_MUX_USE-1 := 0;
	signal regfsize_in, regfsize_in_n : unsigned(WFSIZE-1 downto 0) := (others => '0');
	signal regaddr_in, regaddr_in_n : unsigned(WADDR_IN-1 downto 0) := (others => '0');
	signal regaddr_sh, regaddr_sh_n : unsigned(WADDR_SH-1 downto 0) := (others => '0');
	-- Alias signal to select the right memory address register depending on whether TIME_MUX is used or not
	signal sigaddr_in : unsigned(WADDR_IN-1 downto 0) := (others => '0');

	-- Control signals to drive the blocks
	signal sigctrl_accu_add   : std_logic := '0';
	signal sigctrl_accu_clear : std_logic := '0';
	signal sigctrl_shift_en   : std_logic := '0';
	signal sigctrl_shift_copy : std_logic := '0';

	-- One signal to merge control inputs together, for clarity
	constant CTRLNB : natural := 4;
	constant CTRLIDX_CLR : natural := 0;
	constant CTRLIDX_ADD : natural := 1;
	constant CTRLIDX_CPY : natural := 2;
	constant CTRLIDX_SH  : natural := 3;
	signal allctrl : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');

	signal data_in_delay : std_logic_vector(WDATA*PAR_IN*DATA_DELAY_CYCLES-1 downto 0) := (others => '0');

	-- In mode DWConv, the input activations are not duplicated to all neurons
	constant NEUDIST_DATA_OUTNUM : natural :=
		to_integer(DWCONV = false) * NBNEU_PHY +
		to_integer(DWCONV = true)  * 1;

	signal neuinputs_allctrl    : std_logic_vector(NBNEU_PHY*CTRLNB-1 downto 0) := (others => '0');
	signal neuinputs_allweights : std_logic_vector(NBNEU_PHY*NEU_PAR_IN*WWEIGHT-1 downto 0) := (others => '0');
	signal neuinputs_alldata    : std_logic_vector(NEUDIST_DATA_OUTNUM*PAR_IN*WDATA-1 downto 0) := (others => '0');

	signal sensor_shift : std_logic := '0';

	-- Component declaration: Add/sub accumulator for ternary weight
	component accu_mul_ter_weight is
		generic(
			WDATA : natural := 8;
			SDATA : boolean := true;
			WACCU : natural := 8
		);
		port(
			clk       : in  std_logic;
			-- Control signals
			cmd_clear : in  std_logic;
			cmd_add   : in  std_logic;
			-- Data input
			weight_in : in  std_logic_vector(1 downto 0);
			data_in   : in  std_logic_vector(WDATA-1 downto 0);
			-- Accumulator output
			accu_out  : out std_logic_vector(WACCU-1 downto 0)
		);
	end component;

	-- Component declaration: distribution tree to limit fanout
	component distribuf is
		generic(
			WDATA :  natural := 32;
			NBOUT :  natural := 32;
			FANOUT : natural := 32
		);
		port(
			clk : in std_logic;
			-- Input
			idata : in std_logic_vector(WDATA-1 downto 0);
			-- Outputs
			odata : out std_logic_vector(WDATA*NBOUT-1 downto 0)
		);
	end component;

	-- Component declaration: distribution tree to limit fanout, exactly 1 stage
	component distribuf_1cy is
		generic(
			WDATA  : natural := 20;
			NBOUT  : natural := 20;
			MULT   : natural := 1;  -- Multiplier for expected load at each output
			FANOUT : natural := 20
		);
		port(
			clk : in std_logic;
			-- Input
			idata : in std_logic_vector(WDATA-1 downto 0);
			-- Output
			odata : out std_logic_vector(WDATA*NBOUT-1 downto 0)
		);
	end component;

	-- Component declaration: A pipelined adder tree
	component addtree is
		generic (
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY : boolean := false;
			BINARY_RADIX : natural := 0;
			TERNARY : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port (
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

	-- Component declaration: a pipelined AND-OR reduction tree
	component muxtree_decoded is
		generic(
			WDATA : natural := 8;
			NBIN  : natural := 8;
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			RADIX : natural := 18
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			sel      : in  std_logic_vector(NBIN-1 downto 0);
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WDATA-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

	-- Component to ease instantiation of BRAM to build a potentially much larger memory
	component largemem_block is
		generic (
			-- Parameters for memory
			WDATA   : natural := 8;
			CELLS   : natural := 1024;
			WADDR   : natural := 10;
			-- Optimization objective
			OPT_SPEED : boolean := false;
			-- A tag that may be given for each read
			WTAG    : natural := 1;
			-- Enable extra register after the BRAM, to improve timings
			REG_DO  : boolean := false;
			-- Enable extra register when there are multiple BRAMs in height
			REG_MUX : boolean := false
		);
		port (
			clk        : in  std_logic;
			clear      : in  std_logic;
			-- Write port
			write_addr : in  std_logic_vector(WADDR-1 downto 0);
			write_en   : in  std_logic;
			write_data : in  std_logic_vector(WDATA-1 downto 0);
			-- Read port, input
			read_addr  : in  std_logic_vector(WADDR-1 downto 0);
			read_en    : in  std_logic;
			read_itag  : in  std_logic_vector(WTAG-1 downto 0);
			-- Read port, output
			read_data  : out std_logic_vector(WDATA-1 downto 0);
			read_valid : out std_logic;
			read_otag  : out std_logic_vector(WTAG-1 downto 0)
		);
	end component;

	-- Component to ease instantiation of BRAM with different data width on read and write sides
	component largemem_block_asym is
		generic (
			-- Data width and depth
			WDATA   : natural := 2;
			CELLS   : natural := 1024;
			WADDR   : natural := 10;
			-- Desired geometry, read/write
			NPERBLK : natural := 18;
			WRNB    : natural := 2;
			-- Whether to use the output register
			REGDOEN : boolean := true;
			-- Optional tag
			TAGW    : natural := 1;
			TAGEN   : boolean := true;
			TAGZC   : boolean := true
		);
		port (
			clk        : in  std_logic;
			clear      : in  std_logic;
			-- Write side
			write_en   : in  std_logic;
			write_addr : in  std_logic_vector(WADDR-1 downto 0);
			write_data : in  std_logic_vector(WDATA*NPERBLK*WRNB-1 downto 0);
			-- Read side
			read_en    : in  std_logic;
			read_addr  : in  std_logic_vector(WADDR-1 downto 0);
			read_data  : out std_logic_vector(WDATA*NPERBLK-1 downto 0);
			-- Tag, input and output
			tag_in     : in  std_logic_vector(TAGW-1 downto 0);
			tag_out    : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

	-- Component to ease instantiation of URAM to build a potentially much larger memory
	component largemem_huge is
		generic (
			-- Parameters for memory
			WDATA   : natural := 8;
			CELLS   : natural := 1024;
			WADDR   : natural := 10;
			-- Optimization objective
			OPT_SPEED : boolean := false;
			-- A tag that may be given for each read
			WTAG    : natural := 1;
			-- Enable extra register after the memory blocks, to improve timings
			REG_DO  : boolean := false;
			-- Enable extra register when there are multiple blocks in height
			REG_MUX : boolean := false
		);
		port (
			clk        : in  std_logic;
			clear      : in  std_logic;
			-- Write port
			write_addr : in  std_logic_vector(WADDR-1 downto 0);
			write_en   : in  std_logic;
			write_data : in  std_logic_vector(WDATA-1 downto 0);
			-- Read port, input
			read_addr  : in  std_logic_vector(WADDR-1 downto 0);
			read_en    : in  std_logic;
			read_itag  : in  std_logic_vector(WTAG-1 downto 0);
			-- Read port, output
			read_data  : out std_logic_vector(WDATA-1 downto 0);
			read_valid : out std_logic;
			read_otag  : out std_logic_vector(WTAG-1 downto 0)
		);
	end component;

	-- Component to decompress 3b2t, keep one weight and do multiplication
	component terncompress_3b_2t_muladd is
		generic(
			WDATA : natural := 12;
			NBIN  : natural := 8;
			WOUT  : natural := 15;
			-- Compression-related information
			COM3B2T_EN  : boolean := false;
			COM3B2T_OFF : natural := 0;
			INSIZE      : natural := 16;
			-- Tag-related information
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true
		);
		port(
			clk       : in  std_logic;
			clear     : in  std_logic;
			-- Weight and data input
			weight_in : in  std_logic_vector(INSIZE-1 downto 0);
			data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			-- Data output
			data_out  : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in    : in  std_logic_vector(TAGW-1 downto 0);
			tag_out   : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

	-- Component to decompress 3b2t, keep one weight, do add/sub of data in internal accu
	component terncompress_3b_2t_accu_addsub is
		generic(
			WDATA : natural := 8;
			WACCU : natural := 8;
			COM3B2T_EN  : boolean := false;
			COM3B2T_IDX : natural := 0
		);
		port(
			clk       : in  std_logic;
			-- Control signals
			cmd_clear : in  std_logic;
			cmd_add   : in  std_logic;
			-- Data input
			bin_in    : in  std_logic_vector(2 downto 0);
			weight_in : in  std_logic_vector(1 downto 0);
			data_in   : in  std_logic_vector(WDATA-1 downto 0);
			-- Accumulator output
			accu_out  : out std_logic_vector(WACCU-1 downto 0)
		);
	end component;

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		process(clk)
		begin
			if rising_edge(clk) then

				buf_user_fsize <= unsigned(user_fsize);
				buf_user_nbneu <= unsigned(user_nbneu);

				buf_in_addr_end <= resize(buf_user_fsize - 2, WADDR_IN);
				buf_sh_addr_end <= resize(buf_user_nbneu - 2, WADDR_SH);

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_user_fsize <= to_unsigned(FSIZE, 16);
		buf_user_nbneu <= to_unsigned(NBNEU, 16);

		buf_in_addr_end <= to_unsigned(maximum(FSIZE, 2) - 2, WADDR_IN);
		buf_sh_addr_end <= to_unsigned(maximum(NBNEU, 2) - 2, WADDR_SH);

	end generate;

	-------------------------------------------------------------------
	-- FSM
	-------------------------------------------------------------------

	-- Sequential process

	process(clk)
	begin
		if rising_edge(clk) then

			-- Data output: Detect when there is enough room in output FIFO
			buf_out_ready <= '0';
			if unsigned(out_fifo_room) >= FIFOMARGIN_USE then
				buf_out_ready <= '1';
			end if;

			reg_frame_beg <= reg_frame_beg_n;
			reg_shift_beg <= reg_shift_beg_n;

			reg_tmux_end  <= reg_tmux_end_n;
			reg_frame_end <= reg_frame_end_n;
			reg_shift_end <= reg_shift_end_n;

			reg_we_mode <= reg_we_mode_n;

			regtmux     <= regtmux_n;
			regfsize_in <= regfsize_in_n;
			regaddr_in  <= regaddr_in_n;
			regaddr_sh  <= regaddr_sh_n;

			reg_frame_doing <= reg_frame_doing_n;
			reg_shift_doing <= reg_shift_doing_n;

			reg_fr2sh_ready <= reg_fr2sh_ready_n;
			reg_sh2fr_ready <= reg_sh2fr_ready_n;

			data_in_delay(data_in_delay'length-1 downto data_in_delay'length-WDATA*PAR_IN) <= data_in;
			if DATA_DELAY_CYCLES > 1 then
				data_in_delay(data_in_delay'length-WDATA*PAR_IN-1 downto 0) <= data_in_delay(data_in_delay'length-1 downto WDATA*PAR_IN);
			end if;

			reg_out_valid <= sensor_shift;

		end if;
	end process;

	-- Combinatorial process

	process(all)
		variable var_addr_inc  : std_logic := '0';
		variable var_frame_end : std_logic := '0';
		variable var_shift_end : std_logic := '0';
	begin

		-- Init variables
		var_addr_inc  := '0';
		var_frame_end := '0';
		var_shift_end := '0';

		-- Default values for registers

		reg_frame_beg_n <= reg_frame_beg;
		reg_shift_beg_n <= reg_shift_beg;

		reg_tmux_end_n  <= reg_tmux_end;
		reg_frame_end_n <= reg_frame_end;
		reg_shift_end_n <= reg_shift_end;

		reg_we_mode_n <= reg_we_mode;

		regtmux_n     <= regtmux;
		regfsize_in_n <= regfsize_in;
		regaddr_in_n  <= regaddr_in;
		regaddr_sh_n  <= regaddr_sh;

		reg_frame_doing_n <= reg_frame_doing;
		reg_shift_doing_n <= reg_shift_doing;

		reg_fr2sh_ready_n <= reg_fr2sh_ready;
		reg_sh2fr_ready_n <= reg_sh2fr_ready;

		-- Default values for outputs

		data_in_ready <= '0';

		-- Default values for neuron control signals

		sigctrl_accu_add   <= '0';
		sigctrl_accu_clear <= '0';
		sigctrl_shift_en   <= '0';
		sigctrl_shift_copy <= '0';

		--------------------------------------------------
		-- Frame input operation
		--------------------------------------------------

		-- Specifications :
		-- Inputs are accepted only when reg_frame_doing=1
		--   This bit stays at 1 for the entire duration of an input frame
		--   This bit advertises data_in_ready=1
		-- reg_frame_beg is set (buffered) at input of the first data of a frame, used to clear the accumulators
		-- reg_frame_end is set (buffered) at input of the last data of a frame, used to clear the address register and start the output state machine

		-- Note : Increment of address counter is done in common address increment code

		-- Increment address at Frame input
		if reg_frame_doing = '1' then
			-- Indicate to the input FIFO that we are reading a value
			data_in_ready <= '1';
			-- Process an input value
			if data_in_valid = '1' then
				-- Increment address at Frame input
				var_addr_inc := '1';
			end if;
		end if;

		--------------------------------------------------
		-- Memory address increment
		--------------------------------------------------

		-- Increment address at Write event
		if reg_we_mode = '1' then
			if sig_write_inc = '1' then
				var_addr_inc := '1';
			end if;
		end if;

		-- Common address increment code
		-- FIXME This common address increment code may no longer be suitable for non-packed memory implementation
		if var_addr_inc = '1' then
			-- Increment the address, actually both registers
			-- Logic optimization will remove the unused regaddr when there is no time mux
			regfsize_in_n <= regfsize_in + 1;
			regaddr_in_n <= regaddr_in + 1;
			-- Bufferize flags that indicate the end of a frame is reached, and the end of a tmux block is reached
			reg_frame_end_n <= '0';
			reg_tmux_end_n <= '0';
			if (regfsize_in = buf_in_addr_end) or (buf_user_fsize = 1) then
				reg_frame_end_n <= '1';
				if (TIME_MUX_USE <= 1) or (regtmux = TIME_MUX_USE-1) or ((regtmux = TIME_MUX_USE-2) and (buf_user_fsize = 1)) then
					reg_tmux_end_n <= '1';
				end if;
			end if;
			-- Handle when we are at the last data item of the frame, and of the tmux block if applicable
			if reg_frame_end = '1' then
				var_frame_end := '1';
				-- Clear the frame size counter
				regfsize_in_n <= (others => '0');
				-- Handle time multiplexing : Increment the tmux counter, clear address register
				if TIME_MUX_USE > 1 then
					if reg_tmux_end = '1' then
						reg_tmux_end_n <= '0';
						regtmux_n <= 0;
						regaddr_in_n <= (others => '0');
					else
						regtmux_n <= regtmux + 1;
					end if;
				end if;
			end if;
		end if;

		--------------------------------------------------
		-- Write mode
		--------------------------------------------------

		-- The we_mode register
		reg_we_mode_n <= write_mode;

		-- Note : Increment of address counter is done in common address increment code

		-- Clear address registers when entering Write mode
		-- Clear address registers when requested by the non-packed memory implementation
		if
			((reg_we_mode = '0') and (write_mode = '1')) or
			((reg_we_mode = '1') and (sig_write_clr = '1'))
		then
			reg_tmux_end_n <= to_std_logic(TIME_MUX <= 1);
			regtmux_n <= 0;
			regfsize_in_n <= (others => '0');
			regaddr_in_n  <= (others => '0');
		end if;

		-- Generate one Clear pulse when getting out of Write mode
		if (reg_we_mode = '1') and (write_mode = '0') then
			reg_tmux_end_n <= to_std_logic(TIME_MUX <= 1);
			regtmux_n     <= 0;
			regfsize_in_n <= (others => '0');
			regaddr_in_n  <= (others => '0');
			sigctrl_accu_clear <= '1';
		end if;

		--------------------------------------------------
		-- Frame input operation
		--------------------------------------------------

		-- Handle end of input frame
		if reg_frame_doing = '1' then
			if var_addr_inc = '1' then
				-- Process flags
				reg_frame_beg_n <= to_std_logic(buf_user_fsize <= 1);
				sigctrl_accu_clear <= reg_frame_beg;
				-- Send the data valid signal to neurons
				sigctrl_accu_add <= '1';
			end if;
			if var_frame_end = '1' then
				reg_frame_doing_n <= '0';
				reg_fr2sh_ready_n <= '1';
			end if;
		end if;

		--------------------------------------------------
		-- Neuron output operation
		--------------------------------------------------

		-- Data output: send the Shift Enable signal
		if reg_shift_doing = '1' then
			-- WARNING: We MUST be able to read the first value at the first cycle
			-- Because on the other side, the next frame can overwrite the accumulators
			if (buf_out_ready = '1') or (reg_shift_beg = '1') then
				reg_shift_beg_n <= '0';
				sigctrl_shift_copy <= reg_shift_beg;
				-- Send the shift enable signal to neurons - it acts as data valid on output side
				sigctrl_shift_en <= '1';
				-- Increment the address
				regaddr_sh_n <= regaddr_sh + 1;
				-- Bufferize a flag that indicates the end of shift is reached
				reg_shift_end_n <= '0';
				if (regaddr_sh = buf_sh_addr_end) or (buf_user_nbneu = 1) then
					reg_shift_end_n <= '1';
				end if;
				-- Handle when we are sending the Shift Enable for the result of the last neuron
				if reg_shift_end = '1' then
					var_shift_end := '1';
					-- Clear the address
					regaddr_sh_n <= (others => '0');
					-- Prepare to wait for the next frame
					reg_shift_doing_n <= '0';
					reg_sh2fr_ready_n <= '1';
				end if;
			end if;
		end if;

		--------------------------------------------------
		-- Change state, for frame and/or shift
		--------------------------------------------------

		-- Start an input frame when ready
		if (reg_frame_doing = '0' or var_frame_end = '1') and (reg_shift_doing = '0' or reg_sh2fr_ready = '1' or var_shift_end = '1') then
			reg_frame_doing_n <= '1';
			reg_frame_beg_n   <= '1';
			reg_frame_end_n   <= to_std_logic(buf_user_fsize <= 1);
			-- Clear the flag shift->frame
			reg_sh2fr_ready_n <= '0';
		end if;

		-- Start an output frame when ready
		if (reg_shift_doing = '0' or var_shift_end = '1') and (reg_fr2sh_ready = '1' or (reg_frame_doing = '1' and var_frame_end = '1')) then
			reg_shift_doing_n <= '1';
			reg_shift_beg_n   <= '1';
			reg_shift_end_n   <= to_std_logic(buf_user_nbneu <= 1);
			-- Clear the flag frame->shift
			reg_fr2sh_ready_n <= '0';
		end if;

		-- Write mode inhibits frame inputs in all circumstances
		if (reg_we_mode = '1') or (write_mode = '1') then
			reg_frame_doing_n <= '0';
		end if;

		-- The general clear signal has priority
		if clear = '1' then
			reg_frame_beg_n    <= to_std_logic(FSIZE <= 1);
			reg_shift_beg_n    <= to_std_logic(NBNEU <= 1);
			reg_tmux_end_n     <= to_std_logic(TIME_MUX <= 1);
			reg_frame_end_n    <= to_std_logic(FSIZE <= 1);
			reg_shift_end_n    <= to_std_logic(NBNEU <= 1);
			reg_we_mode_n      <= '0';
			regtmux_n          <= 0;
			regfsize_in_n      <= (others => '0');
			regaddr_in_n       <= (others => '0');
			regaddr_sh_n       <= (others => '0');
			reg_frame_doing_n  <= '0';
			reg_shift_doing_n  <= '0';
			reg_fr2sh_ready_n  <= '0';
			reg_sh2fr_ready_n  <= '0';
			-- Also clear stuff in the blocks
			sigctrl_accu_clear <= '1';
			sigctrl_accu_add   <= '0';
			sigctrl_shift_en   <= '0';
			sigctrl_shift_copy <= '0';
		end if;

		-- Some signals are unused if there is no TIME_MUX
		if TIME_MUX_USE <= 1 then
			reg_tmux_end_n <= '1';
			regtmux_n    <= 0;
			regaddr_in_n <= (others => '0');
		end if;

	end process;

	-- This output will be routed up to the communication interface (PCIe, AXI, ...), so it's good to have it buffered
	end_of_frame <= reg_frame_end;

	-- This output indicates to the wrapping controller that this block is fully configured
	-- FIXME Disabled for now
	write_end <= '0';

	-- Aggregate all input control signals
	allctrl(CTRLIDX_CLR) <= sigctrl_accu_clear;
	allctrl(CTRLIDX_ADD) <= sigctrl_accu_add;
	allctrl(CTRLIDX_CPY) <= sigctrl_shift_copy;
	allctrl(CTRLIDX_SH)  <= sigctrl_shift_en;

	-- Output data valid
	data_out_valid <= reg_out_valid;

	-- Select the right address register to send to neuron accumulators
	-- This is to let the logic synthesis tool the opportunity to remove one when unused
	gen_addr : if TIME_MUX <= 1 generate
		sigaddr_in <= regfsize_in;
	else generate
		sigaddr_in <= regaddr_in;
	end generate;


	-------------------------------------------------------------------
	-- Instantiate the memory blocks
	-------------------------------------------------------------------

	assert PACKED = true report "ERROR : Using non-packed weight storage is no longer maintained" severity failure;

	assert not (USE_REGS   = true and PACKED = false) report "ERROR : Using register-based weight storage is only permitted with packed storage" severity failure;
	assert not (USE_LUTRAM = true and PACKED = false) report "ERROR : Using LUTRAM-based weight storage is only permitted with packed storage" severity failure;
	assert not (USE_URAM   = true and PACKED = false) report "ERROR : Using URAM-based weight storage is only permitted with packed storage" severity failure;
	assert not (USE_CONST  = true and PACKED = false) report "ERROR : Using constant weights is only permitted with packed storage" severity failure;

	assert not (DWCONV = true and (PAR_IN mod NBNEU_PHY /= 0)) report "ERROR : Mode DWCONV requires PAR_IN be a multiple of the number of physical neurons" severity failure;
	assert not (DWCONV = true and PACKED = false) report "ERROR : Mode DWCONV is only permitted with packed storage" severity failure;

	gen_pack : if PACKED = true generate

		-- The ideal memory width, in bits
		constant NORMAL_MEM_WIDTH : natural := NBNEU_PHY * NEU_PAR_IN * WWEIGHT;
		-- The number of clock cycles to fill one width with the write port
		constant WIDTH_WR_NB    : natural := (NORMAL_MEM_WIDTH + WWRITE - 1) / WWRITE;
		-- The number of weights in one input write vector
		-- Note : This is only used for ternary compression of weights
		constant WR_WEIGHTS_NB  : natural := WWRITE / WWEIGHT;

		type mem_cfg_t is record
			-- The number of compressors for one write vector
			wr_comp_nb   : natural;
			-- The width of one write vector, possibly after compression
			wr_vec_width : natural;
			-- The number of decompressors for one full read
			rd_comp_nb   : natural;
			-- The data width
			data_width   : natural;
			-- The number of data words in width
			data_num     : natural;
			-- The raw memory width
			mem_width    : natural;
			-- To add a register stage right after the mem
			buf_prelut   : boolean;
			buf_postmem  : boolean;
			buf_postmul  : boolean;
		end record;

		function calc_mem_cfg(phony : natural) return mem_cfg_t is
			variable mem_cfg : mem_cfg_t := (0, 0, 0, 0, 0, 0, false, false, false);
		begin

			-- Default: Config with no compressor

			mem_cfg.wr_vec_width := WWRITE;
			mem_cfg.data_width   := WWEIGHT;
			mem_cfg.data_num     := NBNEU_PHY * NEU_PAR_IN;
			mem_cfg.mem_width    := NORMAL_MEM_WIDTH;

			-- Take decompression into account

			if (COMP_WRAW > 0) and (COMP_WENC > 0) then

				mem_cfg.wr_comp_nb   := (WR_WEIGHTS_NB*WWEIGHT + COMP_WRAW - 1) / COMP_WRAW;
				mem_cfg.wr_vec_width := mem_cfg.wr_comp_nb * COMP_WENC;
				mem_cfg.rd_comp_nb   := (NORMAL_MEM_WIDTH + COMP_WRAW - 1) / COMP_WRAW;

				mem_cfg.data_width   := COMP_WENC;
				mem_cfg.data_num     := mem_cfg.rd_comp_nb;

				mem_cfg.mem_width    := mem_cfg.rd_comp_nb * COMP_WENC;

			end if;

			-- Decide if need to instantiate a register stage after memory read
			-- No need for weight storage CONST and REG, and BRAM provides its own output buffering
			-- FIXME Ensure there is a parameter in BRAM wrapper to force creation of output buffer

			mem_cfg.buf_prelut  := false;
			mem_cfg.buf_postmem := false;
			mem_cfg.buf_postmul := false;

			-- Arbitrarily set one layer after lutram
			-- FIXME The case with a small LUTRAM + decoder may often fit in one cycle, but no visibility on this
			if USE_LUTRAM = true then
				mem_cfg.buf_postmem := true;
			end if;

			-- The memory of constant weights has async read behaviour + one register stage on read path
			-- In case the implementation can have many BRAM blocs in height, add another register stage that Vivado can move on the read path
			-- FIXME An attribute to allow Vivado to move the register should be placed on the register stage immediately after the sync read
			if
			  ((USE_BRAM = true) and (USE_CONST = true) and (CSTWEIGHTS_NB <= 1) and (FSIZE * TIME_MUX_USE > 64*1024)) or
			  ((USE_URAM = true) and (USE_CONST = true) and (CSTWEIGHTS_NB <= 1) and (FSIZE * TIME_MUX_USE > 4096))
			 then
				mem_cfg.buf_postmem := true;
			end if;

			return mem_cfg;
		end function;

		-- Compute the memory configuration
		constant MEM_CFG : mem_cfg_t := calc_mem_cfg(0);

		-- The number of bits used in the highest input write vector
		constant WIDTH_WR_LASTSIZE : natural := MEM_CFG.mem_width - (WIDTH_WR_NB - 1) * MEM_CFG.wr_vec_width;

		-- Intermediate signals to instantiate the memory
		signal mem_wa_sig : std_logic_vector(WADDR_IN-1 downto 0) := (others => '0');
		signal mem_ra_sig : std_logic_vector(WADDR_IN-1 downto 0) := (others => '0');
		signal mem_wd_sig : std_logic_vector(MEM_CFG.mem_width-1 downto 0) := (others => '0');
		signal mem_rd_sig : std_logic_vector(MEM_CFG.mem_width-1 downto 0) := (others => '0');
		signal mem_we_sig : std_logic := '0';
		signal mem_re_sig : std_logic := '0';

		--  Bufferize the BRAM read addr to have the right timings
		signal mem_ra_buf : std_logic_vector(WADDR_IN-1 downto 0) := (others => '0');
		--  Bufferize the BRAM output with registers, this is much better for delay
		signal mem_rd_buf : std_logic_vector(MEM_CFG.mem_width-1 downto 0) := (others => '0');

		-- Buffers for control signals
		signal reg_ctrl : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');

		-- Wrapper signals to apply the right delay on read data
		-- To propagate: control signals, activations
		signal premem_tag  : std_logic_vector(CTRLNB+PAR_IN*WDATA-1 downto 0) := (others => '0');
		signal postmem_tag : std_logic_vector(CTRLNB+PAR_IN*WDATA-1 downto 0) := (others => '0');
		signal postbuf_tag : std_logic_vector(CTRLNB+PAR_IN*WDATA-1 downto 0) := (others => '0');
		signal postdec_tag : std_logic_vector(CTRLNB+PAR_IN*WDATA-1 downto 0) := (others => '0');
		-- Alias signals at output of decoders
		signal postdec_ctrl : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');
		signal postdec_act  : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');

		-- Note:
		-- Signals to send to the neurons:
		--   control signals: add, clr, sh, cpy
		--   input activations
		-- The signal add is Read Enable for the memory
		-- So it must be sent 1-2 cycles before all other signals
		-- Which is good, we have 1-2 cycles to do some buffering of these high-fanout stuff
		-- FIXME Generally this is the case for the critical layer, but not for other layers
		-- FIXME Not for DWConv layers where each input activation is sent to exactly one physical neuron (where each phy neuron can receive more than one activation)

		-- Base fanout to create 1-cycle delay registers
		constant NEUDIST_FANOUT    : natural := 128;
		-- Multiplier for load at destination
		constant NEUDIST_MULT_CTRL : natural := 1;
		constant NEUDIST_MULT_DATA : natural := 1;

		-- Registers to detect the end of write operation
		signal reg_write_end, reg_write_end_n : std_logic := '0';

		-- Arrays of signals to instantiate the blocks
		signal wrvec_reg, wrvec_reg_n : std_logic_vector(MEM_CFG.wr_vec_width-1 downto 0) := (others => '0');
		signal wrvec_we, wrvec_we_n   : std_logic := '0';

		-- Arrays of signals to instantiate the blocks
		signal wrfull_reg : std_logic_vector(MEM_CFG.mem_width - 1 downto 0) := (others => '0');
		signal wrfull_arrwe, wrfull_arrwe_n : std_logic_vector(WIDTH_WR_NB - 1 downto 0) := (others => '0');
		signal wrfull_we, wrfull_we_n : std_logic := '0';

	begin

		-- FIXME It would improve strength and clarity of API if we also specify that WWRITE = WRNB * WWEIGHT (and a variant to support compression)
		--assert WWRITE mod WWEIGHT = 0 report "ERROR : Generic parameter WWRITE (" & natural'image(WWRITE) & ") must be divisible by WWIDTH (" & natural'image(WWEIGHT) & ") " severity failure;

		-- Just an information to ease preparing a simulation setup
		assert false report "INFO : Number of Write clock cycles is as most " & natural'image(WIDTH_WR_NB * FSIZE * TIME_MUX_USE) severity note;

		-------------------------------------------------------------------
		-- The write FSM
		-------------------------------------------------------------------

		-- Sequential process

		process(clk)
		begin
			if rising_edge(clk) then

				-- Mem write side, write registers
				reg_write_end <= reg_write_end_n;
				wrvec_reg     <= wrvec_reg_n;
				wrvec_we      <= wrvec_we_n;
				wrfull_arrwe  <= wrfull_arrwe_n;
				wrfull_we     <= wrfull_we_n;

				-- The full write register
				for i in 0 to WIDTH_WR_NB-1 loop
					if (wrfull_arrwe(i) = '1') and (wrvec_we = '1') then

						if i < WIDTH_WR_NB-1 then
							wrfull_reg((i+1) * MEM_CFG.wr_vec_width - 1 downto i * MEM_CFG.wr_vec_width) <= wrvec_reg;
						else
							wrfull_reg(wrfull_reg'high downto (WIDTH_WR_NB-1) * MEM_CFG.wr_vec_width) <= wrvec_reg(WIDTH_WR_LASTSIZE-1 downto 0);
						end if;

					end if;
				end loop;

				-- Mem read side
				mem_ra_buf <= std_logic_vector(sigaddr_in);

				-- Control signals related to mem read operations
				reg_ctrl  <= allctrl;

			end if;
		end process;

		-- Combinatorial process

		process(all)
		begin

			-- Default values for registers

			reg_write_end_n <= reg_write_end;
			wrvec_we_n      <= '0';
			wrfull_arrwe_n  <= wrfull_arrwe;
			wrfull_we_n     <= '0';

			-- Default values for neuron control signals

			sig_write_inc <= '0';
			sig_write_clr <= '0';

			-- Initialize WE register when entering write mode
			if (reg_we_mode = '0') and (write_mode = '1') then
				wrfull_arrwe_n(0) <= '1';
			end if;

			-- Accept input data
			if (reg_we_mode = '1') and (write_enable = '1') and (reg_write_end = '0') then
				wrvec_we_n <= '1';
			end if;

			-- Events at full write vector
			if wrvec_we = '1' then
				-- Rotate the WE flags
				if WIDTH_WR_NB > 1 then
					wrfull_arrwe_n <= wrfull_arrwe(WIDTH_WR_NB-2 downto 0) & wrfull_arrwe(WIDTH_WR_NB-1);
				end if;
				-- Set the mem WE flag, one cycle only
				if wrfull_arrwe(WIDTH_WR_NB-1) = '1' then
					wrfull_we_n <= '1';
				end if;
			end if;

			-- Ask for increment of the address
			if wrfull_we = '1' then
				sig_write_inc <= '1';
				-- Detect the end of the whole writing process
				if (reg_frame_end = '1') and (reg_tmux_end = '1') then
					reg_write_end_n <= '1';
					wrvec_we_n      <= '0';
					wrfull_we_n     <= '0';
				end if;
			end if;

			-- Generate one Clear pulse when getting out of Write mode
			if (reg_we_mode = '1') and (write_mode = '0') then
				reg_write_end_n <= '0';
				wrvec_we_n      <= '0';
				wrfull_arrwe_n  <= (others => '0');
				wrfull_we_n     <= '0';
			end if;

			-- The general clear signal has priority
			if clear = '1' then
				reg_write_end_n <= '0';
				wrvec_we_n      <= '0';
				wrfull_arrwe_n  <= (others => '0');
				wrfull_we_n     <= '0';
			end if;

		end process;


		-------------------------------------------------------------------
		-- Instantiate the compressor stage, if needed
		-------------------------------------------------------------------

		gen_wr_nocomp : if COMP_ON_WRITE_PATH = false generate

			wrvec_reg_n <= write_data;

		end generate;

		gen_wr_comp : if COMP_ON_WRITE_PATH = true generate

			signal wrapper : std_logic_vector(MEM_CFG.wr_comp_nb*COMP_WRAW-1 downto 0) := (others => '0');

			component neuron_custom_encode is
				generic(
					-- Parameters for port sizes
					WRAW  : natural := 1;  -- Size of raw data
					WENC  : natural := 1;  -- Size of encoded word
					-- Identifier of encoding style
					STYLE : natural := 0;
					-- Identifiers of layer and encoder
					LAYER_ID : natural := 0;
					ENC_ID   : natural := 0;
					-- Tag-related information
					TAGW  : natural := 1;
					TAGEN : boolean := true
				);
				port(
					clk     : in  std_logic;
					reset   : in  std_logic;
					-- Data signals
					in_raw  : in  std_logic_vector(WRAW-1 downto 0);
					out_enc : out std_logic_vector(WENC-1 downto 0);
					-- Tag, input and output
					tag_in  : in  std_logic_vector(TAGW-1 downto 0);
					tag_out : out std_logic_vector(TAGW-1 downto 0)
				);
			end component;

		begin

			wrapper(WWRITE-1 downto 0) <= write_data;

			gen_loop : for i in 0 to MEM_CFG.wr_comp_nb-1 generate

				enc : neuron_custom_encode
					generic map (
						-- Parameters for port sizes
						WRAW  => COMP_WRAW,  -- Size of raw data
						WENC  => COMP_WENC,  -- Size of encoded word
						-- Identifier of encoding style
						STYLE => COMP_STYLE,
						-- Identifiers of layer and encoder
						LAYER_ID => LAYER_ID,
						ENC_ID   => i,
						-- Tag-related information
						TAGW  => 1,
						TAGEN => false
					)
					port map (
						clk     => clk,
						reset   => clear,
						-- Data signals
						in_raw  => wrapper    ((i+1)*COMP_WRAW-1 downto i*COMP_WRAW),
						out_enc => wrvec_reg_n((i+1)*COMP_WENC-1 downto i*COMP_WENC),
						-- Tag, input and output
						tag_in  => (others => '0'),
						tag_out => open
					);

			end generate;

		end generate;


		-------------------------------------------------------------------
		-- Instantiate the memory component
		-------------------------------------------------------------------

		mem_wa_sig <= std_logic_vector(sigaddr_in);
		mem_we_sig <= wrfull_we;
		mem_wd_sig <= wrfull_reg;

		-- Note : These signals (read address and read enable) are pre-buffered
		-- This is to compensate for the post-buffering of other signals (control and activations) with distribuf components
		-- These buffering layers are needed because of the need to have at least one buffer between the input FIFO sync signals and weight read pipeline
		mem_ra_sig <= mem_ra_buf;
		mem_re_sig <= reg_ctrl(CTRLIDX_ADD);

		-- Generate the memory of weights, select the desired implementation

		premem_tag <= allctrl & data_in;

		gen_const_vec : if (USE_CONST = true) and (CSTWEIGHTS_NB > 1) generate

			assert FSIZE * TIME_MUX_USE = 1 report "Error : Incompatible parameters CONST, number of weights, FSIZE, TMUX" severity failure;
			assert CSTWEIGHTS_NB * WWEIGHT = mem_rd_sig'length report "Error : Incompatible parameter CSTWEIGHTS_NB with size/number of weights" severity failure;

			-- Constant weights
			mem_rd_sig <= CSTWEIGHTS_VEC;

			-- Control signals and activations
			postmem_tag <= premem_tag;

		end generate;

		gen_const_comp : if (USE_CONST = true) and (CSTWEIGHTS_NB <= 1) generate

			component neurons_const_weights is
				generic (
					-- Size of output data
					WDATA : natural := 1;
					-- Size of the address input, in case it is used
					WADDR : natural := 1;
					-- Parallelism at output
					PAR_OUT : natural := 1;
					-- Identifier of neuron layer, in case a per-layer operation is desired
					LAYER_ID : natural := 0
				);
				port (
					addr_in  : in  std_logic_vector(WADDR-1 downto 0);
					data_out : out std_logic_vector(WDATA*PAR_OUT-1 downto 0)
				);
			end component;

		begin

			gen_direct : if FSIZE * TIME_MUX_USE <= 1 generate

				-- Read side : Instantiate the phony component that emits the weights
				inst_const : neurons_const_weights
					generic map (
						-- Size of output data
						WDATA => WWEIGHT,
						-- Size of the address input, in case it is used
						WADDR => 1,
						-- Parallelism at output
						PAR_OUT => NBNEU_PHY * NEU_PAR_IN,
						-- Identifier of neuron layer, in case a per-layer operation is desired
						LAYER_ID => LAYER_ID
					)
					port map (
						addr_in  => (others => '0'),  -- Unused
						data_out => mem_rd_sig
					);

				-- Control signals and activations
				postmem_tag <= premem_tag;

			end generate;

			gen_lut : if (FSIZE * TIME_MUX_USE > 1) and (USE_LUTRAM = true) generate

				-- Intermediate Read Address signal to enable optional buffering
				signal prebuf_ra : std_logic_vector(WADDR_IN-1 downto 0) := (others => '0');

			begin

				gen_prebuf : if MEM_CFG.buf_prelut = true generate

					process(clk)
					begin
						if rising_edge(clk) then

							prebuf_ra <= mem_ra_sig;

							-- Control signals and activations
							postmem_tag <= premem_tag;

						end if;
					end process;

				else generate

					prebuf_ra <= mem_ra_sig;

					-- Control signals and activations
					postmem_tag <= premem_tag;

				end generate;

				-- Read side : Instantiate the behavioural component that emits the weights
				inst_const : neurons_const_weights
					generic map (
						-- Size of output data
						WDATA => MEM_CFG.data_width,
						-- Size of the address input, in case it is used
						WADDR => WADDR_IN,
						-- Parallelism at output
						PAR_OUT => MEM_CFG.data_num,
						-- Identifier of neuron layer, in case a per-layer operation is desired
						LAYER_ID => LAYER_ID
					)
					port map (
						addr_in  => prebuf_ra,
						data_out => mem_rd_sig
					);

			end generate;  -- LUT storage

			gen_bram : if (FSIZE * TIME_MUX_USE > 1) and (USE_BRAM = true or USE_URAM = true or USE_AUTO = true) generate

				-- FIXME Missing directive to enforce implementation in BRAM or URAM

				-- Local signal for read data
				signal loc_rd : std_logic_vector(MEM_CFG.mem_width-1 downto 0) := (others => '0');

			begin

				-- Read side : Instantiate the behavioural component that emits the weights
				inst_const : neurons_const_weights
					generic map (
						-- Size of output data
						WDATA => MEM_CFG.data_width,
						-- Size of the address input, in case it is used
						WADDR => WADDR_IN,
						-- Parallelism at output
						PAR_OUT => MEM_CFG.data_num,
						-- Identifier of neuron layer, in case a per-layer operation is desired
						LAYER_ID => LAYER_ID
					)
					port map (
						addr_in  => mem_ra_sig,
						data_out => loc_rd
					);

				-- Implement a buffer on output side to enable Vivado to correctly infer the BRAM
				process(clk)
				begin
					if rising_edge(clk) then

						if mem_re_sig = '1' then
							mem_rd_sig <= loc_rd;
						end if;

						-- Control signals and activations
						postmem_tag <= premem_tag;

					end if;
				end process;

			end generate;  -- BRAM storage

		end generate;

		gen_regs : if (USE_REGS = true) and (USE_CONST = false) generate

			assert FSIZE <= 1 report "ERROR : With register-based storage, FSIZE must be at most 1" severity failure;
			assert TIME_MUX <= 1 report "ERROR : With register-based storage, TIME_MUX must be at most 1" severity failure;

			-- Nothing in write side

			-- Read side
			mem_rd_sig <= wrfull_reg;

			-- Control signals and activations
			postmem_tag <= premem_tag;

		end generate;

		gen_lutram : if (USE_LUTRAM = true) and (USE_CONST = false) generate

			type memweight_type is array (0 to FSIZE*TIME_MUX_USE-1) of std_logic_vector(MEM_CFG.mem_width-1 downto 0);
			signal memweight : memweight_type;

			attribute ram_style : string;
			attribute ram_style of memweight : signal is "distributed";

			-- Intermediate Read Address signal to enable optional buffering
			signal prebuf_ra : std_logic_vector(WADDR_IN-1 downto 0) := (others => '0');

		begin

			-- Write side

			process(clk)
			begin
				if rising_edge(clk) then

					if mem_we_sig = '1' then
						memweight(to_integer(unsigned(mem_wa_sig))) <= mem_wd_sig;
					end if;

				end if;  -- Clock edge
			end process;

			-- Read side

			gen_prebuf : if MEM_CFG.buf_prelut = true generate

				process(clk)
				begin
					if rising_edge(clk) then

						prebuf_ra <= mem_ra_sig;

						-- Control signals and activations
						postmem_tag <= premem_tag;

					end if;
				end process;

			else generate

				prebuf_ra <= mem_ra_sig;

				-- Control signals and activations
				postmem_tag <= premem_tag;

			end generate;

			mem_rd_sig <= memweight(to_integer(unsigned(prebuf_ra)));

		end generate;  -- Use LUTRAM

		gen_bram : if (USE_BRAM = true) and (USE_CONST = false) generate

			mem : largemem_block
				generic map (
					-- Parameters for memory
					WDATA   => MEM_CFG.mem_width,
					CELLS   => FSIZE * TIME_MUX_USE,
					WADDR   => WADDR_IN,
					-- Optimization objective
					OPT_SPEED => false,
					-- A tag that may be given for each read
					WTAG    => premem_tag'length,
					-- Enable the DO register of the memory blocks
					REG_DO  => true,
					-- Enable extra register when there are multiple memory blocks in height
					REG_MUX => true
				)
				port map (
					clk        => clk,
					clear      => clear,
					-- Write port
					write_addr => mem_wa_sig,
					write_en   => mem_we_sig,
					write_data => mem_wd_sig,
					-- Read port, input
					read_addr  => mem_ra_sig,
					read_en    => mem_re_sig,
					read_itag  => premem_tag,
					-- Read port, output
					read_data  => mem_rd_sig,
					read_valid => open,
					read_otag  => postmem_tag
				);

		end generate;  -- Use BRAM

		gen_uram : if (USE_URAM = true) and (USE_CONST = false) generate

			mem : largemem_huge
				generic map (
					-- Parameters for memory
					WDATA   => MEM_CFG.mem_width,
					CELLS   => FSIZE * TIME_MUX_USE,
					WADDR   => WADDR_IN,
					-- Optimization objective
					OPT_SPEED => false,
					-- A tag that may be given for each read
					WTAG    => premem_tag'length,
					-- Enable the DO register of the memory blocks
					REG_DO  => true,
					-- Enable extra register when there are multiple memory blocks in height
					REG_MUX => true
				)
				port map (
					clk        => clk,
					clear      => clear,
					-- Write port
					write_addr => mem_wa_sig,
					write_en   => mem_we_sig,
					write_data => mem_wd_sig,
					-- Read port, input
					read_addr  => mem_ra_sig,
					read_en    => mem_re_sig,
					read_itag  => premem_tag,
					-- Read port, output
					read_data  => mem_rd_sig,
					read_valid => open,
					read_otag  => postmem_tag
				);

		end generate;  -- Use BRAM

		gen_auto : if (USE_AUTO = true) and (USE_CONST = false) generate

			type mem_type is array (0 to FSIZE * TIME_MUX_USE-1) of std_logic_vector(MEM_CFG.mem_width-1 downto 0);
			signal mem : mem_type := (others => (others => '0'));

		begin

			-- Implement an arbitrary synchronous 1-cycle operation
			process(clk)
			begin
				if rising_edge(clk) then

					if mem_we_sig = '1' then
						mem(to_integer(unsigned(mem_wa_sig))) <= mem_wd_sig;
					end if;

					if mem_re_sig = '1' then
						mem_rd_sig <= mem(to_integer(unsigned(mem_ra_sig)));
					end if;

					postmem_tag <= premem_tag;

				end if;  -- Clock edge
			end process;

		end generate;  -- Use AUTO


		-------------------------------------------------------------------
		-- Optional pipeline register after memory
		-------------------------------------------------------------------

		gen_rd_buf : if MEM_CFG.buf_postmem = true generate

			process(clk)
			begin
				if rising_edge(clk) then

					-- Mem read data
					mem_rd_buf <= mem_rd_sig;

					-- Control signals and activations
					postbuf_tag <= postmem_tag;

				end if;
			end process;

		end generate;

		gen_rd_nobuf : if MEM_CFG.buf_postmem = false generate

			-- Mem read data
			mem_rd_buf <= mem_rd_sig;

			-- Control signals and activations
			postbuf_tag <= postmem_tag;

		end generate;


		-------------------------------------------------------------------
		-- Instantiate the decompressor stage, if needed
		-------------------------------------------------------------------

		gen_rd_nocomp : if COMP_ON_READ_PATH = false generate

			neuinputs_allweights <= mem_rd_buf;

			postdec_tag <= postbuf_tag;

		end generate;

		gen_rd_comp2 : if (COMP_ON_READ_PATH = true) and (COMP_STYLE = 2) generate

			-- FIXME The destination signal may be undersized in corner case where the mem stores only 1 weight (1 neuron, PAR_IN=1)
			neuinputs_allweights(mem_rd_buf'length-1 downto 0) <= mem_rd_buf;

			postdec_tag <= postbuf_tag;

		end generate;

		gen_rd_comp3p : if (COMP_ON_READ_PATH = true) and (COMP_STYLE /= 2) generate

			signal wrapper : std_logic_vector(MEM_CFG.rd_comp_nb*COMP_WRAW-1 downto 0) := (others => '0');

			component neuron_custom_decode is
				generic(
					-- Parameters for port sizes
					WRAW  : natural := 1;  -- Size of raw data
					WENC  : natural := 1;  -- Size of encoded word
					-- Identifier of encoding style
					STYLE : natural := 0;
					-- Identifiers of layer and decoder
					LAYER_ID : natural := 0;
					DEC_ID   : natural := 0;
					-- Tag-related information
					TAGW  : natural := 1;
					TAGEN : boolean := true
				);
				port(
					clk     : in  std_logic;
					reset   : in  std_logic;
					-- Data signals
					in_enc  : in  std_logic_vector(WENC-1 downto 0);
					out_raw : out std_logic_vector(WRAW-1 downto 0);
					-- Tag, input and output
					tag_in  : in  std_logic_vector(TAGW-1 downto 0);
					tag_out : out std_logic_vector(TAGW-1 downto 0)
				);
			end component;

		begin

			gen_loop : for i in 0 to MEM_CFG.rd_comp_nb-1 generate

				decomp : neuron_custom_decode
					generic map (
						-- Parameters for port sizes
						WRAW  => COMP_WRAW,  -- Size of raw data
						WENC  => COMP_WENC,  -- Size of encoded word
						-- Identifier of encoding style
						STYLE => COMP_STYLE,
						-- Identifiers of layer and decoder
						LAYER_ID => LAYER_ID,
						DEC_ID   => i,
						-- Tag-related information
						TAGW  => CTRLNB+PAR_IN*WDATA,
						TAGEN => true
					)
					port map (
						clk     => clk,
						reset   => clear,
						-- Data signals
						in_enc  => mem_rd_buf((i+1)*COMP_WENC-1 downto i*COMP_WENC),
						out_raw => wrapper   ((i+1)*COMP_WRAW-1 downto i*COMP_WRAW),
						-- Tag, input and output
						tag_in  => postbuf_tag,
						tag_out => postdec_tag
					);

			end generate;

			neuinputs_allweights <= wrapper(neuinputs_allweights'length-1 downto 0);

		end generate;


		-------------------------------------------------------------------
		-- Instantiate the fanout distribution buffers
		-------------------------------------------------------------------

		-- Separate control signals and activations
		postdec_act  <= postdec_tag(postdec_act'length-1 downto 0);
		postdec_ctrl <= postdec_tag(CTRLNB+postdec_act'length-1 downto postdec_act'length);

		-- Fanout distribution tree: control signals
		i_buf_allctrl: distribuf_1cy
			generic map (
				WDATA  => CTRLNB,
				NBOUT  => PAR_OUT*NBNEU,
				MULT   => NEUDIST_MULT_CTRL,
				FANOUT => NEUDIST_FANOUT
			)
			port map (
				clk   => clk,
				idata => postdec_ctrl,
				odata => neuinputs_allctrl
			);

		-- Fanout distribution tree: data_in
		i_buf_data_in: distribuf_1cy
			generic map (
				WDATA  => PAR_IN*WDATA,
				NBOUT  => NEUDIST_DATA_OUTNUM,
				MULT   => NEUDIST_MULT_DATA,
				FANOUT => NEUDIST_FANOUT
			)
			port map (
				clk   => clk,
				idata => postdec_act,
				odata => neuinputs_alldata
			);

	end generate;  -- Generate the mem blocks, packed

	gen_nopack : if PACKED = false generate

		constant INT_WWRITE : natural := WRNB * NPERBLK * WWEIGHT;

		signal arr_allctrl        : std_logic_vector(TOTALBLOCKS*CTRLNB-1 downto 0) := (others => '0');
		signal arr_allctrl_outram : std_logic_vector(TOTALBLOCKS*CTRLNB-1 downto 0) := (others => '0');

		signal arr_allweights_outram : std_logic_vector(TOTALBLOCKS*NPERBLK*WWEIGHT-1 downto 0) := (others => '0');

		signal arr_addr_in     : std_logic_vector(TOTALBLOCKS*WADDR_IN-1 downto 0) := (others => '0');
		signal arr_data_in     : std_logic_vector(TOTALBLOCKS*PAR_IN*WDATA-1 downto 0) := (others => '0');

		-- Arrays of signals to instantiate the blocks
		signal arr_write_data   : std_logic_vector(TOTALBLOCKS*INT_WWRITE-1 downto 0) := (others => '0');
		signal arr_write_mode   : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');
		signal arr_write_enable : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');
		signal arr_write_shift  : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');

		-- All WE registers, for RAM blocks
		signal arr_we_sig : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');

		-- Registers for write operation
		signal reg_write_end, reg_write_end_n : std_logic := '0';
		signal buf_wr_addr_end  : unsigned(WADDR_IN-1 downto 0);
		signal sigctrl_we_shift : std_logic := '0';

	begin

		assert false report "INFO : Number of Write clock cycles is as most " & natural'image(TOTALBLOCKS * FSIZE * TIME_MUX_USE / WRNB) severity note;

		-------------------------------------------------------------------
		-- The write FSM
		-------------------------------------------------------------------

		-- Sequential process

		process(clk)
		begin
			if rising_edge(clk) then

				-- Note: WRNB is always a power of 2 so this synthesizes well
				-- FIXME There might be a multiplier here, we hope the multiplication by a constant gets optimized
				buf_wr_addr_end <= resize(((buf_user_fsize * TIME_MUX_USE) + WRNB - 1) / WRNB - 2, WADDR_IN);

				reg_write_end <= reg_write_end_n;

			end if;
		end process;

		-- Combinatorial process

		process(all)
		begin

			-- Default values for registers

			reg_write_end_n <= reg_write_end;

			-- Default values for signals

			sig_write_inc <= '0';
			sig_write_clr <= '0';

			sigctrl_we_shift <= '0';

			-- Main actions
			if (reg_we_mode = '1') and (write_enable = '1') then
				-- Ask for increment of the address
				sig_write_inc <= '1';
				-- Bufferize a flag that indicates the end of the block is reached
				reg_write_end_n <= '0';
				if sigaddr_in = buf_wr_addr_end then
					reg_write_end_n <= '1';
				end if;
				-- Check if one block is full
				if reg_write_end = '1' then
					-- Generate one WE Shift Block pulse to enable the next block
					sigctrl_we_shift <= '1';
					-- Ask for clear of the address
					sig_write_clr <= '1';
				end if;
			end if;

			-- Generate one Clear pulse when getting out of Write mode
			if (reg_we_mode = '1') and (write_mode = '0') then
				reg_write_end_n <= '0';
			end if;

			-- The general clear signal has priority
			if clear = '1' then
				reg_write_end_n  <= '0';
				sigctrl_we_shift <= '0';
			end if;

		end process;


		-------------------------------------------------------------------
		-- Instantiate the fanout distribution buffers
		-------------------------------------------------------------------

		-- Fanout distribution tree: write_data
		i_buf_write_data: distribuf
			generic map (
				WDATA  => INT_WWRITE,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk   => clk,
				idata => write_data(INT_WWRITE-1 downto 0),
				odata => arr_write_data
			);

		-- Fanout distribution tree: write_mode
		i_buf_write_mode: distribuf
			generic map (
				WDATA  => 1,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk      => clk,
				idata(0) => reg_we_mode,
				odata    => arr_write_mode
			);

		-- Fanout distribution tree: write_enable
		i_buf_write_en: distribuf
			generic map (
				WDATA  => 1,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk      => clk,
				idata(0) => write_enable,
				odata    => arr_write_enable
			);

		-- Fanout distribution tree: write_shift
		i_buf_write_sh: distribuf
			generic map (
				WDATA  => 1,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk      => clk,
				idata(0) => sigctrl_we_shift,
				odata    => arr_write_shift
			);

		-- Fanout distribution tree: control signals
		i_buf_allctrl: distribuf
			generic map (
				WDATA  => CTRLNB,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk   => clk,
				idata => allctrl,
				odata => arr_allctrl
			);

		-- Fanout distribution tree: address in the input frame
		i_buf_addr: distribuf
			generic map (
				WDATA  => WADDR_IN,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk   => clk,
				idata => std_logic_vector(sigaddr_in),
				odata => arr_addr_in
			);

		-- Fanout distribution tree: data_in
		i_buf_data_in: distribuf
			generic map (
				WDATA  => PAR_IN*WDATA,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk   => clk,
				idata => data_in_delay(WDATA*PAR_IN-1 downto 0),
				odata => arr_data_in
			);


		-------------------------------------------------------------------
		-- Instantiate the memory blocks
		-------------------------------------------------------------------

		gen_mem: for i in 0 to TOTALBLOCKS-1 generate

			signal reg_we : std_logic := '0';

			signal sig_wen     : std_logic := '0';
			signal sig_ren     : std_logic := '0';
			signal sig_wdata   : std_logic_vector(INT_WWRITE-1 downto 0);
			signal sig_rdata   : std_logic_vector(NPERBLK*WWEIGHT-1 downto 0);
			signal sig_addr_in : std_logic_vector(WADDR_IN-1 downto 0);

			signal sig_ctrl_in  : std_logic_vector(3 downto 0);
			signal sig_ctrl_out : std_logic_vector(3 downto 0);

		begin

			-- Handle write_enable register
			process(clk)
			begin
				if rising_edge(clk) then

					-- Special case for the first WE reg: it is triggered high and low by the main write mode reg
					if i = 0 then

						if arr_write_shift(i) = '1' then
							reg_we <= '0';
						end if;

						if reg_we_mode = '0' then
							reg_we <= '0';
						end if;

						if (reg_we_mode = '0') and (write_mode = '1') then
							reg_we <= '1';
						end if;

					-- Other WE regs: normal shift
					else

						if arr_write_shift(i) = '1' then
							reg_we <= arr_we_sig(i-1);
						end if;

						if arr_write_mode(i) = '0' then
							reg_we <= '0';
						end if;

					end if;

				end if;  -- Clock rising edge
			end process;

			arr_we_sig(i) <= reg_we;

			-- Get signals related to this mem block

			sig_ctrl_in <= arr_allctrl((i+1)*CTRLNB-1 downto i*4);

			sig_wen     <= reg_we and arr_write_enable(i);
			sig_ren     <= sig_ctrl_in(CTRLIDX_ADD);
			sig_wdata   <= arr_write_data((i+1)*INT_WWRITE-1 downto i*INT_WWRITE);
			sig_addr_in <= arr_addr_in((i+1)*WADDR_IN-1 downto i*WADDR_IN);

			-- Generate the weight memory: Select LUTRAM vs BRAM

			gen_lutram: if USE_LUTRAM = true generate

				type memweight_type is array (0 to FSIZE*TIME_MUX_USE-1) of std_logic_vector(NPERBLK*WWEIGHT-1 downto 0);
				signal memweight : memweight_type;

				attribute ram_style : string;
				attribute ram_style of memweight : signal is "distributed";

			begin

				process(clk)
				begin
					if rising_edge(clk) then

						if sig_wen = '1' then
							memweight(to_integer(unsigned(sig_addr_in))) <= sig_wdata;
						end if;

						if sig_ren = '1' then
							sig_rdata <= memweight(to_integer(unsigned(sig_addr_in)));
						end if;

						sig_ctrl_out <= sig_ctrl_in;

					end if;  -- Clock edge
				end process;

			end generate;  -- Use LUTRAM

			gen_bram: if USE_LUTRAM = false generate

				-- Instantiate the memory block
				i_ram: largemem_block_asym
					generic map (
						-- Data width and depth
						WDATA   => WWEIGHT,
						CELLS   => FSIZE * TIME_MUX_USE,
						WADDR   => WADDR_IN,
						-- Desired geometry, read/write
						NPERBLK => NPERBLK,
						WRNB    => WRNB,
						-- Whether to use the output register
						REGDOEN => true,
						-- Optional tag
						TAGW    => 4,
						TAGEN   => true,
						TAGZC   => false
					)
					port map (
						clk        => clk,
						clear      => '0',
						-- Write side
						write_en   => sig_wen,
						write_addr => sig_addr_in,
						write_data => sig_wdata,
						-- Read side
						read_en    => sig_ren,
						read_addr  => sig_addr_in,
						read_data  => sig_rdata,
						-- Tag, input and output
						tag_in     => sig_ctrl_in,
						tag_out    => sig_ctrl_out
					);

			end generate;  -- Use BRAM

			-- Set outputs related to this mem block

			arr_allweights_outram((i+1)*NPERBLK*WWEIGHT-1 downto i*NPERBLK*WWEIGHT) <= sig_rdata;

			arr_allctrl_outram((i+1)*CTRLNB-1 downto i*CTRLNB) <= sig_ctrl_out;

		end generate;  -- Generate the mem blocks


		-------------------------------------------------------------------
		-- Generate input vectors for the neurons
		-------------------------------------------------------------------

		gen_neuin: for po in 0 to PAR_OUT-1 generate

			gen_neu: for n in 0 to NBNEU-1 generate

				constant CURBLK       : natural := po * TOTALBLOCKS_PO + (n / NEU_PER_BLOCK) * BLOCKS_PER_NEU;
				constant CURWEIGHTBEG : natural := CURBLK * NPERBLK + (n mod NEU_PER_BLOCK) * PAR_IN;
				constant CURNEU       : natural := po * NBNEU + n;

				constant NEU_WSIZE : natural := PAR_IN * WWEIGHT;
				constant NEU_DSIZE : natural := PAR_IN * WDATA;

				signal weights : std_logic_vector(NEU_WSIZE-1 downto 0);
				signal datas   : std_logic_vector(NEU_DSIZE-1 downto 0);
				signal ctrl    : std_logic_vector(CTRLNB-1 downto 0);

			begin

				weights <= arr_allweights_outram ((CURWEIGHTBEG+PAR_IN)*WWEIGHT-1 downto CURWEIGHTBEG*WWEIGHT);
				datas   <= arr_data_in           ((CURBLK+1)*PAR_IN*WDATA-1 downto CURBLK*PAR_IN*WDATA);
				ctrl    <= arr_allctrl_outram    ((CURBLK+1)*CTRLNB-1 downto CURBLK*CTRLNB);

				neuinputs_allweights ((CURNEU+1)*NEU_WSIZE-1 downto CURNEU*NEU_WSIZE) <= weights;
				neuinputs_alldata    ((CURNEU+1)*NEU_DSIZE-1 downto CURNEU*NEU_DSIZE) <= datas;
				neuinputs_allctrl    ((CURNEU+1)*CTRLNB-1    downto CURNEU*CTRLNB)    <= ctrl;

			end generate;  -- Neurons inside one PAR_OUT

		end generate;  -- PAR_OUT

	end generate;  -- Generate the mem blocks, not packed


	-------------------------------------------------------------------
	-- Instantiate the neurons
	-------------------------------------------------------------------

	gen_parout: for po in 0 to PAR_OUT-1 generate

		signal arr_shreg_sig   : std_logic_vector(WOUT*NBNEU-1 downto 0);
		signal arr_shreg_muxen : std_logic_vector(NBNEU-1 downto 0);

		signal po_data_out     : std_logic_vector(WOUT-1 downto 0);
		signal po_data_valid   : std_logic;

		signal po_ctrl_outadd  : std_logic_vector(CTRLNB-1 downto 0);

	begin

		gen_neu: for n in 0 to NBNEU-1 generate

			constant CURNEU    : natural := po * NBNEU + n;

			constant NEU_WSIZE : natural := NEU_PAR_IN * WWEIGHT;
			constant NEU_DSIZE : natural := NEU_PAR_IN * WDATA;

			signal weight_in   : std_logic_vector(NEU_WSIZE-1 downto 0);
			signal data_in     : std_logic_vector(NEU_DSIZE-1 downto 0);

			signal data_outadd : std_logic_vector(WOUT-1 downto 0);
			signal regaccu     : std_logic_vector(WOUT-1 downto 0);
			signal regshift    : std_logic_vector(WOUT-1 downto 0);
			signal regmuxen    : std_logic;

			signal ctrl_inadd  : std_logic_vector(CTRLNB-1 downto 0);
			signal ctrl_outadd : std_logic_vector(CTRLNB-1 downto 0);

			signal sig_add1    : std_logic := '0';

		begin

			weight_in  <= neuinputs_allweights ((CURNEU+1)*NEU_WSIZE-1 downto CURNEU*NEU_WSIZE);
			data_in    <= neuinputs_alldata    ((CURNEU+1)*NEU_DSIZE-1 downto CURNEU*NEU_DSIZE);
			ctrl_inadd <= neuinputs_allctrl    ((CURNEU+1)*CTRLNB-1 downto CURNEU*CTRLNB);

			-- The multiplier + accumulator register

			-- Implementation specific to binary weights 0/1
			-- FIXME Handle CSTWEIGHTS_NB > 1
			gen_bin_01: if WEIGHTS_BIN_01 = true generate

				gen_parin1: if NEU_PAR_IN <= 1 generate

					ctrl_outadd <= ctrl_inadd;

					-- Reuse the compact component for ternary weight
					i_accu : accu_mul_ter_weight
						generic map (
							WDATA => WDATA,
							SDATA => SDATA,
							WACCU => WOUT
						)
						port map (
							clk       => clk,
							-- Control signals
							cmd_clear => ctrl_outadd(CTRLIDX_CLR),
							cmd_add   => ctrl_outadd(CTRLIDX_ADD),
							-- Data input
							weight_in(1) => '0',
							weight_in(0 downto 0) => weight_in,
							data_in   => data_in,
							-- Accumulator output
							accu_out  => regaccu
						);

				end generate;  -- NEU_PAR_IN = 1

				gen_parin2p: if NEU_PAR_IN >= 2 generate

					gen_data_bin : if (WDATA = 1) and (SDATA = false) generate

						-- Number of layers of the adder tree that can have no buffering in between
						-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
						constant REGEN_RECURS : natural := 2;

						component muladdtree_bin is
							generic(
								NBIN  : natural := 16;
								WOUT  : natural := 12;
								-- The type of operation to perform : false for operation AND, true for operation XNOR
								IS_XNOR : boolean := false;
								-- Tag-related information
								TAGW  : natural := 1;
								TAGEN : boolean := false;
								TAGZC : boolean := false;
								-- How to add pipeline registers
								REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
								REGIDX : natural := 0   -- Start index (from the leaves)
							);
							port(
								clk       : in  std_logic;
								clear     : in  std_logic;
								-- Weight and data input
								weight_in : in  std_logic_vector(NBIN-1 downto 0);
								data_in   : in  std_logic_vector(NBIN-1 downto 0);
								-- Data output
								data_out  : out std_logic_vector(WOUT-1 downto 0);
								-- Tag, input and output
								tag_in    : in  std_logic_vector(TAGW-1 downto 0);
								tag_out   : out std_logic_vector(TAGW-1 downto 0)
							);
						end component;

					begin

						muladd : muladdtree_bin
							generic map (
								NBIN  => NEU_PAR_IN,
								WOUT  => WOUT,
								-- The type of operation to perform : false for operation AND, true for operation XNOR
								IS_XNOR => true,
								-- Tag-related information
								TAGW  => CTRLNB,
								TAGEN => true,
								TAGZC => false,
								-- How to add pipeline registers
								REGEN  => REGEN_RECURS,
								REGIDX => 1
							)
							port map (
								clk       => clk,
								clear     => '0',
								-- Weight and data input
								weight_in => weight_in,
								data_in   => data_in,
								-- Data output
								data_out  => data_outadd,
								-- Tag, input and output
								tag_in    => ctrl_inadd,
								tag_out   => ctrl_outadd
							);

					end generate;

					gen_data_else : if (WDATA > 1) or (SDATA = true) generate

						-- Number of layers of the adder tree that can have no buffering in between
						-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
						constant REGEN_RECURS : natural := 2 - to_integer(SDATA);

						component muladdtree_bin_weights is
							generic(
								WDATA : natural := 16;
								SDATA : boolean := false;
								NBIN  : natural := 16;
								WOUT  : natural := 12;
								-- Tag-related information
								TAGW  : natural := 1;
								TAGEN : boolean := false;
								TAGZC : boolean := false;
								-- How to add pipeline registers
								REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
								REGIDX : natural := 0   -- Start index (from the leaves)
							);
							port(
								clk       : in  std_logic;
								clear     : in  std_logic;
								-- Weight and data input
								weight_in : in  std_logic_vector(NBIN-1 downto 0);
								data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
								-- Data output
								data_out  : out std_logic_vector(WOUT-1 downto 0);
								-- Tag, input and output
								tag_in    : in  std_logic_vector(TAGW-1 downto 0);
								tag_out   : out std_logic_vector(TAGW-1 downto 0)
							);
						end component;

					begin

						i_muladd : muladdtree_bin_weights
							generic map (
								WDATA => WDATA,
								SDATA => SDATA,
								NBIN  => NEU_PAR_IN,
								WOUT  => WOUT,
								-- Tag-related information
								TAGW  => CTRLNB,
								TAGEN => true,
								TAGZC => false,
								-- How to add pipeline registers
								REGEN  => REGEN_RECURS,
								REGIDX => 1
							)
							port map (
								clk       => clk,
								clear     => '0',
								-- Weight and data input
								weight_in => weight_in,
								data_in   => data_in,
								-- Data output
								data_out  => data_outadd,
								-- Tag, input and output
								tag_in    => ctrl_inadd,
								tag_out   => ctrl_outadd
							);

					end generate;

					-- The accumulator
					process(clk)
						variable varallclear : std_logic_vector(WOUT-1 downto 0);
						variable varalladd   : std_logic_vector(WOUT-1 downto 0);
					begin
						if rising_edge(clk) then

							varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
							varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

							regaccu <= std_logic_vector(signed(regaccu and not varallclear) + signed(data_outadd and varalladd));

						end if;
					end process;

				end generate;  -- NEU_PAR_IN >= 2

			end generate;  -- Binary weights 0/1

			-- Implementation specific to binary weights -1/+1
			-- FIXME binary weights -1/+1 should always use ternary implementations
			-- FIXME Handle CSTWEIGHTS_NB > 1
			gen_bin_sym: if WEIGHTS_BIN_SYM = true generate

				assert NEU_PAR_IN <= 1 report "ERROR : With binary weights -1/+1, only NEU_PAR_IN=1 is supported for now" severity failure;

				ctrl_outadd <= ctrl_inadd;

				-- Reuse the compact component for ternary weight
				-- Bit 0 of weights is always 1, bit 1 is the stored bit : 0 means +1, 1 means -1
				i_accu : accu_mul_ter_weight
					generic map (
						WDATA => WDATA,
						SDATA => SDATA,
						WACCU => WOUT
					)
					port map (
						clk       => clk,
						-- Control signals
						cmd_clear => ctrl_outadd(CTRLIDX_CLR),
						cmd_add   => ctrl_outadd(CTRLIDX_ADD),
						-- Data input
						weight_in(1 downto 1) => weight_in,
						weight_in(0) => '1',
						data_in   => data_in,
						-- Accumulator output
						accu_out  => regaccu
					);

			end generate;  -- Binary weights -1/+1

			-- Implementation specific to ternary weights provided as a constant vector
			gen_ter_cstw: if (WEIGHTS_TERNARY = true) and (CSTWEIGHTS_NB > 1) generate

				-- Count the number of weights -1 and +1
				constant NWEIGHTS_M1 : natural := count_const_ter_weights_neu(po, n, "11");
				constant NWEIGHTS_P1 : natural := count_const_ter_weights_neu(po, n, "01");
				-- Compute the max range of the output of the two adder trees
				-- Normally just storebitsnb(NUM-1) + WDATA, but need to cover corner case where NUM=0 where storebitsnb(0)=1
				constant WRES_M1 : natural := storebitsnb(maximum(NWEIGHTS_M1-1, 0)) - to_integer(NWEIGHTS_M1 = 0) + WDATA;
				constant WRES_P1 : natural := storebitsnb(maximum(NWEIGHTS_P1-1, 0)) - to_integer(NWEIGHTS_P1 = 0) + WDATA;

				-- The wrapper signals for activations associated to weights -1 and to weights +1
				signal wrap_act_m1 : std_logic_vector(CSTWEIGHTS_TER_MAXNUM*WDATA-1 downto 0) := (others => '0');
				signal wrap_act_p1 : std_logic_vector(CSTWEIGHTS_TER_MAXNUM*WDATA-1 downto 0) := (others => '0');

				-- Signals for output of adder trees
				signal wrap_res_m1 : std_logic_vector(WRES_M1-1 downto 0) := (others => '0');
				signal wrap_res_p1 : std_logic_vector(WRES_P1-1 downto 0) := (others => '0');

				-- Select activations corresponding to weights -1 and +1
				impure function func_select_activations(v : std_logic_vector(1 downto 0)) return std_logic_vector is
					variable idx_w : natural := 0;
					variable idx_d : natural := 0;
					variable vec : std_logic_vector(CSTWEIGHTS_TER_MAXNUM*WDATA-1 downto 0) := (others => '0');
				begin
					idx_w := 0;
					idx_d := 0;
					vec := (others => '0');
					for i in 0 to NEU_PAR_IN - 1 loop
						idx_w := po * n * NEU_PAR_IN + i;
						if CSTWEIGHTS_VEC((idx_w+1)*WWEIGHT-1 downto idx_w*WWEIGHT) = v then
							vec((idx_d+1)*WDATA-1 downto idx_d*WDATA) := data_in((i+1)*WDATA-1 downto i*WDATA);
							idx_d := idx_d + 1;
						end if;
					end loop;
					return vec;
				end function;

				-- Number of layers of the adder tree that can have no buffering in between
				-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
				constant REGEN_RECURS : natural := 2 - to_integer(SDATA);

			begin

				assert CSTWEIGHTS_NB = NBNEU_PHY * NEU_PAR_IN severity failure;
				assert FSIZE * TIME_MUX_USE <= 1 severity failure;

				-- Distribute activations to the wrappers for weights -1 and +1
				wrap_act_m1 <= func_select_activations("11");
				wrap_act_p1 <= func_select_activations("01");

				-- Adder trees for both groups of activations
				-- Dimension is intentionally the max of the wrappers for weights -1 and +1 to ensure same delay

				adder_m1 : addtree
					generic map (
						-- Data type and width
						WDATA => WDATA,
						SDATA => SDATA,
						NBIN  => CSTWEIGHTS_TER_MAXNUM,
						WOUT  => WRES_M1,
						-- User-specified radix, for testing purposes (0 means automatic)
						RADIX => 0,
						-- Special considerations about data nature
						BINARY => (WDATA = 1) and (SDATA = false),
						BINARY_RADIX => 0,
						TERNARY => (WDATA = 2) and (SDATA = true),  -- FIXME
						TERNARY_RADIX => 0,
						-- An optional tag
						TAGW  => CTRLNB,
						TAGEN => true,
						TAGZC => false,
						-- How to add pipeline registers
						REGEN  => REGEN_RECURS,
						REGIDX => 0
					)
					port map (
						clk      => clk,
						clear    => '0',
						-- Data, input and output
						data_in  => wrap_act_m1,
						data_out => wrap_res_m1,
						-- Tag, input and output
						tag_in   => (others => '0'),
						tag_out  => open
					);

				adder_p1 : addtree
					generic map (
						-- Data type and width
						WDATA => WDATA,
						SDATA => SDATA,
						NBIN  => CSTWEIGHTS_TER_MAXNUM,
						WOUT  => WRES_P1,
						-- User-specified radix, for testing purposes (0 means automatic)
						RADIX => 0,
						-- Special considerations about data nature
						BINARY => (WDATA = 1) and (SDATA = false),
						BINARY_RADIX => 0,
						TERNARY => (WDATA = 2) and (SDATA = true),  -- FIXME
						TERNARY_RADIX => 0,
						-- An optional tag
						TAGW  => CTRLNB,
						TAGEN => true,
						TAGZC => false,
						-- How to add pipeline registers
						REGEN  => REGEN_RECURS,
						REGIDX => 0
					)
					port map (
						clk      => clk,
						clear    => '0',
						-- Data, input and output
						data_in  => wrap_act_p1,
						data_out => wrap_res_p1,
						-- Tag, input and output
						tag_in   => ctrl_inadd,
						tag_out  => ctrl_outadd
					);

				-- Note : the outputs of the adder trees are bufferized, so no need to add other buffers before the accumulator

				-- Subtraction
				data_outadd <=
					std_logic_vector(resize(signed('0' & wrap_res_p1), WOUT) - resize(signed('0' & wrap_res_m1), WOUT)) when SDATA = false else
					std_logic_vector(resize(signed(wrap_res_p1), WOUT) - resize(signed(wrap_res_m1), WOUT));

				-- Accumulation : generic implementation is enough
				process(clk)
					variable varaccuand : std_logic_vector(WOUT-1 downto 0);
					variable vardataand : std_logic_vector(WOUT-1 downto 0);
					variable varres     : std_logic_vector(WOUT-1 downto 0);
				begin
					if rising_edge(clk) then
						varaccuand := regaccu and not ctrl_outadd(CTRLIDX_CLR);
						vardataand := data_outadd and ctrl_outadd(CTRLIDX_ADD);
						regaccu <= std_logic_vector(signed(varaccuand) + signed(vardataand));
					end if;
				end process;

			end generate;  -- Ternary weights, provided as constant vector

			-- Implementation specific to ternary weights
			gen_ter: if (WEIGHTS_TERNARY = true) and (CSTWEIGHTS_NB <= 1) generate

				-- Number of layers of the adder tree that can have no buffering in between
				-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
				constant REGEN_RECURS : natural := 1;

				-- Component declaration: Ternary multiplier + adder tree
				component muladdtree_ter_weights is
					generic(
						WDATA : natural := 12;
						NBIN  : natural := 8;
						WOUT  : natural := 15;
						-- Tag-related information
						TAGW  : natural := 1;
						TAGEN : boolean := true;
						TAGZC : boolean := true;
						-- How to add pipeline registers
						REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
						REGIDX : natural := 0   -- Start index (from the leaves)
					);
					port(
						clk       : in  std_logic;
						clear     : in  std_logic;
						-- Weight and data input
						weight_in : in  std_logic_vector(NBIN*2-1 downto 0);
						data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
						-- Data output
						data_out  : out std_logic_vector(WOUT-1 downto 0);
						-- Tag, input and output
						tag_in    : in  std_logic_vector(TAGW-1 downto 0);
						tag_out   : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

				-- Component declaration: Ternary multiplier + adder tree, multiplication merged into adder tree
				component muladdtree_ter_weights_merged is
					generic(
						-- Data type and width
						WDATA : natural := 8;
						NBIN  : natural := 20;
						WOUT  : natural := 12;
						-- Stage number, for internal implem
						STAGE : natural := 0;
						-- An optional tag
						TAGW  : natural := 1;
						TAGEN : boolean := true;
						TAGZC : boolean := true;
						-- How to add pipeline registers
						REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
						REGIDX : natural := 0   -- Start index (from the leaves)
					);
					port(
						clk      : in  std_logic;
						clear    : in  std_logic;
						-- Data, input and output
						data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
						data_out : out std_logic_vector(WOUT-1 downto 0);
						-- Multiplier and carry, input and output
						mult_in  : in  std_logic_vector(NBIN*2-1 downto 0);
						add1_in  : in  std_logic_vector(NBIN-1 downto 0);
						add1_out : out std_logic;
						-- Tag, input and output
						tag_in   : in  std_logic_vector(TAGW-1 downto 0);
						tag_out  : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

				-- Component declaration: Ternary multiplier + adder tree, multiplication merged into adder tree, 3-input only
				component muladdtree_ter_weights_merged_3in is
					generic(
						-- Data type and width
						WDATA : natural := 8;
						WOUT  : natural := 12;
						-- An optional tag
						TAGW  : natural := 1;
						TAGEN : boolean := true;
						TAGZC : boolean := false;
						-- How to add pipeline registers
						REGEN  : natural := 2;  -- 0=none, 1=all, else every EN stages
						REGIDX : natural := 0   -- Start index (from the leaves)
					);
					port(
						clk      : in  std_logic;
						clear    : in  std_logic;
						-- Data, input and output
						data_in  : in  std_logic_vector(3*WDATA-1 downto 0);
						data_out : out std_logic_vector(WOUT-1 downto 0);
						-- Multiplier and carry output
						mult_in  : in  std_logic_vector(3*2-1 downto 0);
						add1_out : out std_logic;
						-- Tag, input and output
						tag_in   : in  std_logic_vector(TAGW-1 downto 0);
						tag_out  : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

			begin

				gen_parin1: if (NEU_PAR_IN <= 1) and (COMP_STYLE /= 2) generate

					ctrl_outadd <= ctrl_inadd;

					i_accu : accu_mul_ter_weight
						generic map (
							WDATA => WDATA,
							SDATA => SDATA,
							WACCU => WOUT
						)
						port map (
							clk       => clk,
							-- Control signals
							cmd_clear => ctrl_outadd(CTRLIDX_CLR),
							cmd_add   => ctrl_outadd(CTRLIDX_ADD),
							-- Data input
							weight_in => weight_in,
							data_in   => data_in,
							-- Accumulator output
							accu_out  => regaccu
						);

				end generate;  -- NEU_PAR_IN = 1, no compression or compression 2t3b

				gen_parin1_2t3b: if (NEU_PAR_IN <= 1) and (COMP_STYLE = 2) generate
					constant CODE_IDX : natural := CURNEU / 2;
					constant CODE_REM : natural := CURNEU - CODE_IDX * 2;
				begin

					ctrl_outadd <= ctrl_inadd;

					i_accu : terncompress_3b_2t_accu_addsub
						generic map (
							WDATA => WDATA,
							WACCU => WOUT,
							COM3B2T_EN  => true,
							COM3B2T_IDX => CODE_REM
						)
						port map (
							clk       => clk,
							-- Control signals
							cmd_clear => ctrl_outadd(CTRLIDX_CLR),
							cmd_add   => ctrl_outadd(CTRLIDX_ADD),
							-- Data input
							bin_in    => neuinputs_allweights((CODE_IDX+1)*3-1 downto CODE_IDX*3),
							weight_in => "00",
							data_in   => data_in,
							-- Accumulator output
							accu_out  => regaccu
						);

				end generate;  -- NEU_PAR_IN = 1, compression = 2t3b

				gen_parin2p_2t3b: if (NEU_PAR_IN >= 2) and (COMP_STYLE = 2) generate
					constant CODE_BEG : natural := CURNEU * NEU_PAR_IN / 2;
					constant CODE_OFF : natural := CURNEU * NEU_PAR_IN - CODE_BEG * 2;
					constant NCODES   : natural := (CODE_OFF + NEU_PAR_IN + 2 - 1) / 2;
				begin

					-- The ternary multiplier + adder tree
					i_muladd : terncompress_3b_2t_muladd
						generic map (
							WDATA => WDATA,
							NBIN  => NEU_PAR_IN,
							WOUT  => WOUT,
							-- Compression-related information
							COM3B2T_EN  => true,
							COM3B2T_OFF => CODE_OFF,
							INSIZE      => NCODES * 3,
							-- Tag-related information
							TAGW  => CTRLNB,
							TAGEN => true,
							TAGZC => false
						)
						port map (
							clk       => clk,
							clear     => '0',
							-- Weight and data input
							weight_in => neuinputs_allweights((CODE_BEG+NCODES)*3-1 downto CODE_BEG*3),
							data_in   => data_in,
							-- Data output
							data_out  => data_outadd,
							-- Tag, input and output
							tag_in    => ctrl_inadd,
							tag_out   => ctrl_outadd
						);

				end generate;  -- NEU_PAR_IN >= 2, compression = 2

				gen_parin2p: if (NEU_PAR_IN >= 2) and (COMP_STYLE /= 2) generate

					gen_small: if (WDATA < 4) or (NEU_PAR_IN <= 1) generate

						-- The ternary multiplier + adder tree, separated
						i_muladd : muladdtree_ter_weights
							generic map (
								WDATA => WDATA,
								NBIN  => NEU_PAR_IN,
								WOUT  => WOUT,
								-- Tag-related information
								TAGW  => CTRLNB,
								TAGEN => true,
								TAGZC => false,
								-- How to add pipeline registers
								REGEN  => REGEN_RECURS,  -- 0=none, 1=all, else every EN stages
								REGIDX => 0   -- Start index (from the leaves)
							)
							port map (
								clk       => clk,
								clear     => '0',
								-- Weight and data input
								weight_in => weight_in,
								data_in   => data_in,
								-- Data output
								data_out  => data_outadd,
								-- Tag, input and output
								tag_in    => ctrl_inadd,
								tag_out   => ctrl_outadd
							);

					end generate;  -- WDATA < 4 or NEU_PAR_IN <= 1

					gen_in3: if (WDATA >= 4) and (NEU_PAR_IN = 3) generate

						-- Adder tree that does ternary multiplication inside for low price
						i_muladd : muladdtree_ter_weights_merged_3in
							generic map (
								-- Data type and width
								WDATA => WDATA,
								WOUT  => WOUT,
								-- An optional tag
								TAGW  => CTRLNB,
								TAGEN => true,
								TAGZC => false,
								-- How to add pipeline registers
								REGEN  => 2,  -- 0=none, 1=all, else every EN stages
								REGIDX => 1   -- Start index (from the leaves)
							)
							port map (
								clk      => clk,
								clear    => '0',
								-- Data, input and output
								data_in  => data_in,
								data_out => data_outadd,
								-- Multiplier and carry, input and output
								mult_in  => weight_in,
								add1_out => sig_add1,
								-- Tag, input and output
								tag_in   => ctrl_inadd,
								tag_out  => ctrl_outadd
							);

					end generate;  -- WDATA >= 4 and NEU_PAR_IN = 3

					gen_in4p: if (WDATA >= 4) and (NEU_PAR_IN > 1) and (NEU_PAR_IN /= 3) generate

						-- Adder tree that does ternary multiplication inside for low price
						i_muladd : muladdtree_ter_weights_merged
							generic map (
								-- Data type and width
								WDATA => WDATA,
								NBIN  => NEU_PAR_IN,
								WOUT  => WOUT,
								-- An optional tag
								TAGW  => CTRLNB,
								TAGEN => true,
								TAGZC => false,
								-- How to add pipeline registers
								REGEN  => 2,  -- 0=none, 1=all, else every EN stages
								REGIDX => 1,  -- Start index (from the leaves)
								-- Stage number, for internal implem
								STAGE => 0
							)
							port map (
								clk      => clk,
								clear    => '0',
								-- Data, input and output
								data_in  => data_in,
								data_out => data_outadd,
								-- Multiplier and carry, input and output
								mult_in  => weight_in,
								add1_in  => (others => '0'),
								add1_out => sig_add1,
								-- Tag, input and output
								tag_in   => ctrl_inadd,
								tag_out  => ctrl_outadd
							);

					end generate;  -- WDATA >= 4 and NEU_PAR_IN > 1 and NEU_PAR_IN /= 3

				end generate;  -- NEU_PAR_IN >= 2, no compression or compression /= 2t3b

				-- Instantiate accumulator when not already done
				gen_parin2p_accu: if NEU_PAR_IN >= 2 generate

					gen_small: if (WDATA < 4) or (NEU_PAR_IN <= 1) generate

						process(clk)
							variable varallclear : std_logic_vector(WOUT-1 downto 0);
							variable varalladd   : std_logic_vector(WOUT-1 downto 0);
						begin
							if rising_edge(clk) then

								varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
								varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

								regaccu <= std_logic_vector(signed(regaccu and not varallclear) + signed(data_outadd and varalladd));

							end if;
						end process;

					end generate;  -- WDATA < 4 or NEU_PAR_IN <= 1

					gen_in2: if (WDATA >= 4) and (NEU_PAR_IN = 2) generate

						process(clk)
							variable varallclear : std_logic_vector(WOUT-1 downto 0);
							variable varalladd   : std_logic_vector(WOUT downto 0);
							variable varallnot   : std_logic_vector(WOUT-1 downto 0);
							variable varaccuand  : std_logic_vector(WOUT downto 0);
							variable vardataand  : std_logic_vector(WOUT downto 0);
							variable varres      : std_logic_vector(WOUT downto 0);
						begin
							if rising_edge(clk) then

								varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
								varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add
								varallnot   := (others => sig_add1);                  -- To negate data

								varaccuand := (regaccu and not varallclear) & '1';
								vardataand := ((data_outadd xor varallnot) & sig_add1) and varalladd;

								varres := std_logic_vector(signed(varaccuand) + signed(vardataand));

								regaccu <= varres(WOUT downto 1);

							end if;
						end process;

					end generate;  -- WDATA >= 4 and NEU_PAR_IN = 2

					gen_in3p: if (WDATA >= 4) and (NEU_PAR_IN > 2) generate

						process(clk)
							variable varallclear : std_logic_vector(WOUT-1 downto 0);
							variable varalladd   : std_logic_vector(WOUT downto 0);
							variable varaccuand  : std_logic_vector(WOUT downto 0);
							variable vardataand  : std_logic_vector(WOUT downto 0);
							variable varres      : std_logic_vector(WOUT downto 0);
						begin
							if rising_edge(clk) then

								varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
								varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

								varaccuand := (regaccu and not varallclear) & '1';
								vardataand := (data_outadd & sig_add1) and varalladd;

								-- FIXME This seems to use more LUTs than without handling of add1... but it should easily fit in the LUT5!
								varres := std_logic_vector(signed(varaccuand) + signed(vardataand));

								regaccu <= varres(WOUT downto 1);

							end if;
						end process;

					end generate;  -- WDATA >= 4 and NEU_PAR_IN > 2

				end generate;  -- NEU_PAR_IN >= 2

			end generate;  -- Ternary weights

			-- Custom neuron implementation, with multiplication / add tree / accumulation
			gen_custom: if CUSTOM_MUL_ID /= 0 generate

				signal data_outmul : std_logic_vector(NEU_PAR_IN*CUSTOM_WMUL-1 downto 0);
				signal ctrl_outmul : std_logic_vector(CTRLNB-1 downto 0);

				-- Number of layers of the adder tree that can have no buffering in between
				-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
				constant REGEN_RECURS : natural := 2 - to_integer(CUSTOM_SMUL);

				component neuron_custom_mul is
					generic(
						-- Parameters for data and signedness
						WDATA   : natural := 2;     -- The data bit width
						SDATA   : boolean := true;  -- The data signedness
						WWEIGHT : natural := 2;     -- The weight bit width
						SWEIGHT : boolean := true;  -- The weight signedness
						WMUL    : natural := 8;     -- The multiplication bit width
						-- Identifiers of layer
						LAYER_ID : natural := 0;
						-- Identifier of neuron layer, in case a per-layer operation is desired
						CUSTOM_MUL_ID : natural := 0;
						-- Tag-related information
						TAGW  : natural := 1;
						TAGEN : boolean := true
					);
					port(
						clk       : in  std_logic;
						-- Control signals
						data_in   : in  std_logic_vector(WDATA-1 downto 0);
						weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
						-- Data output
						mul_out   : out std_logic_vector(WMUL-1 downto 0);
						-- Tag, input and output
						tag_in    : in  std_logic_vector(TAGW-1 downto 0);
						tag_out   : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

			begin

				-- The multipliers
				gen_mul: for i in 0 to NEU_PAR_IN - 1 generate
					signal ctrl_outmul_local : std_logic_vector(CTRLNB-1 downto 0);
					constant TAGEN : boolean := (i = 0);
				begin

					i_mul: neuron_custom_mul
						generic map (
							-- Parameters for data and signedness
							WDATA   => WDATA,    -- The data bit width
							SDATA   => SDATA,    -- The data signedness
							WWEIGHT => WWEIGHT,  -- The weight bit width
							SWEIGHT => SWEIGHT,  -- The weight signedness
							WMUL    => CUSTOM_WMUL,  -- The multiplication bit width
							-- Identifiers of layer
							LAYER_ID => LAYER_ID,
							-- Identifier of neuron layer, in case a per-layer operation is desired
							CUSTOM_MUL_ID => CUSTOM_MUL_ID,
							-- Tag-related information
							TAGW  => CTRLNB,
							TAGEN => TAGEN
						)
						port map (
							clk       => clk,
							-- Control signals
							data_in   => data_in((i+1)*WDATA-1 downto i*WDATA),
							weight_in => weight_in((i+1)*WWEIGHT-1 downto i*WWEIGHT),
							-- Data output
							mul_out   => data_outmul((i+1)*CUSTOM_WMUL-1 downto i*CUSTOM_WMUL),
							-- Tag, input and output
							tag_in    => ctrl_inadd,
							tag_out   => ctrl_outmul_local
						);

					gen_ctrl_outmul : if TAGEN = true generate
						ctrl_outmul <= ctrl_outmul_local;
					end generate;

				end generate;

				-- The adder tree
				adder : addtree
					generic map (
						-- Data type and width
						WDATA => CUSTOM_WMUL,
						SDATA => CUSTOM_SMUL,
						NBIN  => NEU_PAR_IN,
						WOUT  => WOUT,
						-- User-specified radix, for testing purposes (0 means automatic)
						RADIX => 0,
						-- Special considerations about data nature
						BINARY => false,
						BINARY_RADIX => 0,
						TERNARY => false,
						TERNARY_RADIX => 0,
						-- An optional tag
						TAGW  => CTRLNB,
						TAGEN => true,
						TAGZC => false,
						-- How to add pipeline registers
						REGEN  => REGEN_RECURS,
						REGIDX => 0
					)
					port map (
						clk      => clk,
						clear    => '0',
						-- Data, input and output
						data_in  => data_outmul,
						data_out => data_outadd,
						-- Tag, input and output
						tag_in   => ctrl_outmul,
						tag_out  => ctrl_outadd
					);

				-- The accumulator
				process(clk)
					variable varallclear : std_logic_vector(WOUT-1 downto 0);
					variable varalladd   : std_logic_vector(WOUT-1 downto 0);
				begin
					if rising_edge(clk) then

						varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
						varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

						if CUSTOM_SMUL = false then
							regaccu <= std_logic_vector(unsigned(regaccu and not varallclear) + unsigned(data_outadd and varalladd));
						else
							regaccu <= std_logic_vector(signed(regaccu and not varallclear) + signed(data_outadd and varalladd));
						end if;

					end if;
				end process;

			end generate;  -- Custom neuron implmentation

			-- Generic neuron implementation, with multiplication / add tree / accumulation
			gen_else: if (WEIGHTS_BIN_01 = false) and (WEIGHTS_BIN_SYM = false) and (WEIGHTS_TERNARY = false) and (CUSTOM_MUL_ID = 0) generate

				gen_parin1: if NEU_PAR_IN <= 1 generate

					ctrl_outadd <= ctrl_inadd;

					-- The accumulator
					process(clk)
						variable varmul      : std_logic_vector(WOUT-1 downto 0);
						variable varallclear : std_logic_vector(WOUT-1 downto 0);
						variable varalladd   : std_logic_vector(WOUT-1 downto 0);
					begin
						if rising_edge(clk) then

							varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
							varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

							-- Multiplication
							if (SDATA = false) and (SWEIGHT = false) then
								varmul := std_logic_vector(resize(unsigned(data_in) * unsigned(weight_in), WOUT));
							elsif (SDATA = true) and (SWEIGHT = false) then
								varmul := std_logic_vector(resize(signed(data_in) * signed('0' & weight_in), WOUT));
							elsif (SDATA = false) and (SWEIGHT = true) then
								varmul := std_logic_vector(resize(signed('0' & data_in) * signed(weight_in), WOUT));
							else
								varmul := std_logic_vector(resize(signed(data_in) * signed(weight_in), WOUT));
							end if;

							-- Accumulation
							if (SDATA = false) and (SWEIGHT = false) then
								regaccu <= std_logic_vector(unsigned(regaccu and not varallclear) + unsigned(varmul and varalladd));
							else
								regaccu <= std_logic_vector(signed(regaccu and not varallclear) + signed(varmul and varalladd));
							end if;

						end if;
					end process;

				end generate;  -- NEU_PAR_IN = 1

				gen_parin2p: if NEU_PAR_IN >= 2 generate

					constant WMUL_ARITH : natural := WDATA + WWEIGHT - to_integer(SDATA and SWEIGHT);
					constant WMUL : natural := minimum(WMUL_ARITH, WOUT);

					signal data_outmul : std_logic_vector(NEU_PAR_IN*WMUL-1 downto 0);
					signal ctrl_outmul : std_logic_vector(CTRLNB-1 downto 0);

					-- Number of layers of the adder tree that can have no buffering in between
					-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
					constant REGEN_RECURS : natural := 2 - to_integer(SDATA or SWEIGHT);

				begin

					-- The multiplication layer
					process(clk)
						variable vardata   : std_logic_vector(WDATA-1 downto 0);
						variable varweight : std_logic_vector(WWEIGHT-1 downto 0);
						variable varmul    : std_logic_vector(WMUL-1 downto 0);
					begin
						if rising_edge(clk) then

							-- Also bufferize the control signals
							ctrl_outmul <= ctrl_inadd;

							for i in 0 to NEU_PAR_IN - 1 loop

								vardata   := data_in((i+1)*WDATA-1 downto i*WDATA);
								varweight := weight_in((i+1)*WWEIGHT-1 downto i*WWEIGHT);

								if (SDATA = false) and (SWEIGHT = false) then
									varmul := std_logic_vector(resize(unsigned(vardata) * unsigned(varweight), WMUL));
								elsif (SDATA = true) and (SWEIGHT = false) then
									varmul := std_logic_vector(resize(signed(vardata) * signed('0' & varweight), WMUL));
								elsif (SDATA = false) and (SWEIGHT = true) then
									varmul := std_logic_vector(resize(signed('0' & vardata) * signed(varweight), WMUL));
								else
									-- Both are signed
									varmul := std_logic_vector(resize(signed(vardata) * signed(varweight), WMUL));
								end if;

								data_outmul((i+1)*WMUL-1 downto i*WMUL) <= varmul;

							end loop;

						end if;
					end process;

					-- The adder tree
					adder : addtree
						generic map (
							-- Data type and width
							WDATA => WMUL,
							SDATA => SDATA or SWEIGHT,
							NBIN  => NEU_PAR_IN,
							WOUT  => WOUT,
							-- User-specified radix, for testing purposes (0 means automatic)
							RADIX => 0,
							-- Special considerations about data nature
							BINARY => false,
							BINARY_RADIX => 0,
							TERNARY => false,
							TERNARY_RADIX => 0,
							-- An optional tag
							TAGW  => CTRLNB,
							TAGEN => true,
							TAGZC => false,
							-- How to add pipeline registers
							REGEN  => REGEN_RECURS,
							REGIDX => 0
						)
						port map (
							clk      => clk,
							clear    => '0',
							-- Data, input and output
							data_in  => data_outmul,
							data_out => data_outadd,
							-- Tag, input and output
							tag_in   => ctrl_outmul,
							tag_out  => ctrl_outadd
						);

					-- The accumulator
					process(clk)
						variable varallclear : std_logic_vector(WOUT-1 downto 0);
						variable varalladd   : std_logic_vector(WOUT-1 downto 0);
					begin
						if rising_edge(clk) then

							varallclear := (others => ctrl_outadd(CTRLIDX_CLR));  -- Command clear
							varalladd   := (others => ctrl_outadd(CTRLIDX_ADD));  -- Command add

							if (SDATA = false) and (SWEIGHT = false) then
								regaccu <= std_logic_vector(unsigned(regaccu and not varallclear) + unsigned(data_outadd and varalladd));
							else
								regaccu <= std_logic_vector(signed(regaccu and not varallclear) + signed(data_outadd and varalladd));
							end if;

						end if;
					end process;

				end generate;  -- NEU_PAR_IN >= 2

			end generate;  -- Generic neuron implmentation

			-- The neuron output mechanism

			-- Connexion to the scan chain
			arr_shreg_sig((n+1)*WOUT-1 downto n*WOUT) <= regshift;

			-- Neuron output: big scan chain
			gen_scanchain: if SHREG_MUX = false generate

				-- The scan chain
				process(clk)
				begin
					if rising_edge(clk) then

						regshift <= regshift;
						if ctrl_outadd(CTRLIDX_SH) = '1' then
							-- Shift
							regshift <= (others => '0');
							if n < NBNEU - 1 then
								regshift <= arr_shreg_sig((n+2)*WOUT-1 downto (n+1)*WOUT);
							end if;
						end if;
						if ctrl_outadd(CTRLIDX_CPY) = '1' then
							-- Copy
							regshift <= regaccu;
						end if;

					end if;
				end process;

			end generate;  -- SHREG_MUX = false

			-- Neuron output: use a MUX with 1-bit Enable signal propagated through a scan chain
			gen_outmux: if SHREG_MUX = true generate

				-- The scan chain of Enable bits
				process(clk)
				begin
					if rising_edge(clk) then

						if ctrl_outadd(CTRLIDX_SH) = '1' then
							-- Shift
							regmuxen <= '0';
							if n > 0 then
								regmuxen <= arr_shreg_muxen(n-1);
							end if;
						end if;
						if ctrl_outadd(CTRLIDX_CPY) = '1' then
							-- Copy
							regshift <= regaccu;
							regmuxen <= '0';
							if n = 0 then
								regmuxen <= '1';
							end if;
						end if;

					end if;
				end process;

				-- Connexion to the scan chain
				arr_shreg_muxen(n) <= regmuxen;

			end generate;  -- SHREG_MUX = true

			gen_do: if n = 0 generate
				po_ctrl_outadd <= ctrl_outadd;
			end generate;

		end generate;  -- Neurons inside one PAR_OUT

		-- Neuron output: big scan chain
		gen_scanchain: if SHREG_MUX = false generate

			po_data_out <= arr_shreg_sig(WOUT-1 downto 0);
			po_data_valid <= po_ctrl_outadd(CTRLIDX_SH);

		end generate;  -- SHREG_MUX = false

		-- Neuron output: multiplexer with AND-OR tree
		gen_outmux: if SHREG_MUX = true generate

			-- The AND+OR operation
			i_mux: muxtree_decoded
				generic map (
					WDATA => WOUT,
					NBIN  => NBNEU,
					TAGW  => 1,
					TAGEN => true,
					TAGZC => true,
					RADIX => SHREG_MUX_RADIX
				)
				port map (
					clk      => clk,
					clear    => clear,
					-- Data, input and output
					sel      => arr_shreg_muxen,
					data_in  => arr_shreg_sig,
					data_out => po_data_out,
					-- Tag, input and output
					tag_in(0)  => po_ctrl_outadd(CTRLIDX_SH),
					tag_out(0) => po_data_valid
				);

		end generate;  -- SHREG_MUX = true

		-- Set the data_out
		data_out((po+1)*WOUT-1 downto po*WOUT) <= po_data_out;

		gen_do_valid: if po = 0 generate
			sensor_shift <= po_data_valid;
		end generate;

	end generate;  -- PAR_OUT

end architecture;

