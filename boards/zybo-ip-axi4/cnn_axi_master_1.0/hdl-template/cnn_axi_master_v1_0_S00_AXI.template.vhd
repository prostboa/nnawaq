
--------------------------------------------------
-- Register mapping
--------------------------------------------------
--
-- reg 0 (partial R/W)
--   15-00:16 : Number of data items per frame
--   31-16:16 : Max number of data items per frame (read-only)
--
-- reg 1 (partial R/W)
--   15-00:16 : Number of neurons in first stage
--   31-16:16 : Max number of neurons in first stage (read-only)
--
-- reg 2 (partial R/W)
--   15-00:16 : Number of neurons in second stage
--   31-16:16 : Max number of neurons in second stage (read-only)
--
-- reg 3 (partial R/W)
--   03-00:4  : What the PC is sending
--              0000 = nothing
--              0001 = frame data
--              0010 = config for level 1
--              0100 = config for recoding 1-2
--              1000 = config for level 2
--      08:1  : clear all (not written)
--      09:1  : Read master AXI busy state
--
-- reg 4 (unused)
-- reg 5 (unused)
--
-- reg 6 (R/W)
--   31-00:32 : Number of NN output values to write back to DDR
--
-- reg 7 (unused)
-- reg 8 (unused)
-- reg 9 (unused)
--
-- reg 10 (R/W)
--   31-00:32 : Address for DDR read
--
-- reg 11 (R/W)
--   31-00:32 : Address for DDR write
--
-- reg 12 (R/W)
--   31-00:32 : Number of bursts for DDR Read. Start on write.
--
-- reg 13 (R/W)
--   31-00:32 : Number of bursts for DDR write. Start on write.
--
-- reg 14 (read only)
--   07-00:8  : count of fifo between level 1 and recoding 1-2
--   15-08:8  : count of fifo between recoding 1-2 and level 2
--   23-16:8  : count of fifo after level 2
--
-- reg 15 (read only)
--   31-16:16 : fifo rdy/ack signals, in and out: 12 signals
--
--------------------------------------------------
-- Protocol description
--------------------------------------------------
--
-- The beginning of each frame is aligned in 32-bit boundary


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnn_axi_master_v1_0_S00_AXI is
	generic (

		-- Users to add parameters here
		-- User parameters ends

		-- Do not modify the parameters beyond this line

		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here

		mymaster_addr_inw : out std_logic_vector(31 downto 0);
		mymaster_addr_inr : out std_logic_vector(31 downto 0);

		mymaster_burstnb_inw : out std_logic_vector(31 downto 0);
		mymaster_burstnb_inr : out std_logic_vector(31 downto 0);

		mymaster_startw : out std_logic;
		mymaster_startr : out std_logic;
		mymaster_busyw  : in std_logic;
		mymaster_busyr  : in std_logic;
		mymaster_sensor : in std_logic_vector(31 downto 0);  -- For various debug signals

		mymaster_fifor_data : in std_logic_vector(31 downto 0);
		mymaster_fifor_en   : in std_logic;
		mymaster_fifor_cnt  : out std_logic_vector(15 downto 0);

		mymaster_fifow_data : out std_logic_vector(31 downto 0);
		mymaster_fifow_en   : in std_logic;
		mymaster_fifow_cnt  : out std_logic_vector(15 downto 0);

		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Write channel Protection type. This signal indicates the
		--   privilege and security level of the transaction, and whether
		--   the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Write address valid. This signal indicates that the master signaling
		--   valid write address and control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that the slave is ready
		--   to accept an address and associated control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write data (issued by master, acceped by Slave)
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold
		--   valid data. There is one write strobe bit for each eight
		--   bits of the write data bus.
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write valid. This signal indicates that valid write data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Write response. This signal indicates the status of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the channel is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address (issued by master, acceped by Slave)
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
		--   and security level of the transaction, and whether the
		--   transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
		--   is signaling valid read address and control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
		--   ready to accept an address and associated control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end cnn_axi_master_v1_0_S00_AXI;

architecture arch_imp of cnn_axi_master_v1_0_S00_AXI is

	------------------------------------------------
	-- Helper functions
	------------------------------------------------

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	function to_std_logic(b: boolean) return std_logic is
	begin
		if b = false then
			return '0';
		end if;
		return '1';
	end function;

	function to_std_logic(b: std_logic) return std_logic is
	begin
		return b;
	end function;

	function to_std_logic_vector(b: std_logic) return std_logic_vector is
		variable r : std_logic_vector(0 downto 0);
	begin
		r(0) := b;
		return r;
	end function;

	-- Compute the power of 2 that is greater or equal to the input
	function uppow2(vin : natural) return natural is
		variable v : natural := 1;
	begin
		v := 1;
		loop
			exit when v >= vin;
			v := v * 2;
		end loop;
		return v;
	end function;

	------------------------------------------------
	-- Signals for AXI interface
	------------------------------------------------

	-- Alias names to ease code reuse
	signal CLK : std_logic := '0';
	signal RST : std_logic := '0';

	-- AXI4LITE signals
	signal axi_awaddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready : std_logic;
	signal axi_wready  : std_logic;
	signal axi_bresp   : std_logic_vector(1 downto 0);
	signal axi_bvalid  : std_logic;
	signal axi_araddr  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready : std_logic;
	signal axi_rdata   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp   : std_logic_vector(1 downto 0);
	signal axi_rvalid  : std_logic;

	-- Example-specific design signals
	-- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	-- ADDR_LSB is used for addressing 32/64 bit registers/memories
	-- ADDR_LSB = 2 for 32 bits (n downto 2)
	-- ADDR_LSB = 3 for 64 bits (n downto 3)
	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 3;

	-- The FIFOs that interface with the DDR interface
	constant DDRFIFOS_DEPTH : natural := 64;
	constant DDRFIFOS_CNTW  : natural := 8;

	-- The signals that interface with the DDR FIFO : RX side
	signal inst_rdbuf_clear    : std_logic := '0';
	signal inst_rdbuf_in_data  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal inst_rdbuf_in_ack   : std_logic := '0';
	signal inst_rdbuf_in_rdy   : std_logic := '0';
	signal inst_rdbuf_in_cnt   : std_logic_vector(DDRFIFOS_CNTW-1 downto 0) := (others => '0');
	signal inst_rdbuf_out_data : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal inst_rdbuf_out_ack  : std_logic := '0';
	signal inst_rdbuf_out_rdy  : std_logic := '0';
	signal inst_rdbuf_out_cnt  : std_logic_vector(DDRFIFOS_CNTW-1 downto 0) := (others => '0');

	-- The signals that interface with the DDR FIFO : TX side
	signal inst_wrbuf_clear    : std_logic := '0';
	signal inst_wrbuf_in_data  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal inst_wrbuf_in_ack   : std_logic := '0';
	signal inst_wrbuf_in_rdy   : std_logic := '0';
	signal inst_wrbuf_in_cnt   : std_logic_vector(DDRFIFOS_CNTW-1 downto 0) := (others => '0');
	signal inst_wrbuf_out_data : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal inst_wrbuf_out_ack  : std_logic := '0';
	signal inst_wrbuf_out_rdy  : std_logic := '0';
	signal inst_wrbuf_out_cnt  : std_logic_vector(DDRFIFOS_CNTW-1 downto 0) := (others => '0');

	------------------------------------------------
	-- Signals to make reset last longer
	--------------------------------------------------

	constant RSTVAL_IN  : std_logic := '0';
	constant RSTVAL_GEN : std_logic := '1';

	constant RESET_DURATION : natural := 64;
	signal reset_counter : unsigned(15 downto 0) := (others => '0');
	signal reset_reg : std_logic := '1';

	------------------------------------------------
	-- Signals for user logic register space
	--------------------------------------------------

	-- Number of Slave Registers 16
	signal slv_reg0  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg1  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg2  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg3  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg4  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg5  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg6  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg7  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg8  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg9  : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg10 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg11 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg12 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg13 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg14 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg15 : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');

	signal slv_reg_rden   : std_logic := '0';
	signal slv_reg_wren   : std_logic := '0';
	signal slv_reg_rdaddr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0) := (others => '0');
	signal slv_reg_wraddr : std_logic_vector(OPT_MEM_ADDR_BITS downto 0) := (others => '0');
	signal slv_reg_rddata : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg_wrdata : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal slv_reg_wstrb  : std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0) := (others => '0');

	------------------------------------------------
	-- Config registers for the network layers
	------------------------------------------------

	-- AUTOGEN CONFIG NB BEGIN

	-- AUTOGEN CONFIG NB END

	-- The scan chain of config registers
	signal config_chain_regs : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs       : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_next  : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_read  : std_logic_vector(31 downto 0) := (others => '0');

	-- The commands for the scan chain of config registers
	signal config_chain_shift : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_get   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_set   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_def   : std_logic := '0';

	----------------------------------------------------
	-- Definitions for the neural network
	----------------------------------------------------

	-- AUTOGEN CST DECL BEGIN

	-- AUTOGEN CST DECL END

	-- AUTOGEN CONST WEIGHTS VEC BEGIN

	-- AUTOGEN CONST WEIGHTS VEC END

	-- This function applies constant bits to the config regs vector
	function config_regs_apply_const(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST BEGIN

		-- AUTOGEN REGS SETCONST END

		return config_regs_var;
	end function;

	-- This function applies constant bits to the config regs vector, for reset or for locking user fields
	function config_regs_apply_const_locked(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST LOCKED BEGIN

		-- AUTOGEN REGS SETCONST LOCKED END

		return config_regs_var;
	end function;

	constant WDATA_ROUND_POW2 : natural := uppow2(FIRSTFIFO_DATAW);
	constant INPAR_ROUND_POW2 : natural := uppow2(FIRSTFIFO_PAR);

	signal req_start_recv : std_logic := '0';
	signal req_start_send : std_logic := '0';

	constant CST_RECV_FRAME   : std_logic_vector(5 downto 0) := (others => '1');
	constant CST_SEND_DEFAULT : std_logic_vector(7 downto 0) := (others => '1');

	signal cur_recv1 : std_logic_vector(5 downto 0);
	signal cur_recv2 : std_logic_vector(9 downto 0);
	signal cur_send  : std_logic_vector(7 downto 0);
	signal send_is_last : std_logic := '1';

	signal cur_freerun : std_logic := '0';

	-- Alias signals to abstract the actual HW interface in the main implementation (RX direction)
	signal alias_rx_data  : std_logic_vector(CONFIG_IFW-1 downto 0);
	signal alias_rx_valid : std_logic := '0';
	signal alias_rx_ready : std_logic := '0';

	-- Alias signals to abstract the actual HW interface in the main implementation (TX direction)
	signal alias_tx_valid : std_logic := '0';
	signal alias_tx_ready : std_logic := '0';

	-- Buffer to read inputs from the main data channel, or from slave registers
	signal rxbuf_data,  rxbuf_data_n  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal rxbuf_nbits, rxbuf_nbits_n : unsigned(7 downto 0) := (others => '0');  -- Number of bits in the buffer

	-- Counters for input values received from the main data channel
	signal rxcnt_cur, rxcnt_cur_n : unsigned(31 downto 0) := (others => '0');  -- Number of values obtained

	-- Buffer to sent output to the main data channel
	signal txbuf_data,  txbuf_data_n  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal txbuf_nbits, txbuf_nbits_n : unsigned(7 downto 0) := (others => '0');  -- Number of bits in the buffer

	-- Enable signal for sending output values to the main data channel
	signal txrun_en, txrun_en_n         : std_logic := '0';
	signal txrun_gotall, txrun_gotall_n : std_logic := '0';

	-- Counters for output values sent to the main data channel
	signal txcnt_want             : unsigned(31 downto 0) := (others => '0');  -- Number of values to send
	signal txcnt_cur, txcnt_cur_n : unsigned(31 downto 0) := (others => '0');  -- Number of values obtained

	-- Signals for distributed MUX, to monitor the FIFOs
	signal monitorfifo_in  : std_logic_vector(CONFIG_FIFOS_NB*12-1 downto 0) := (others => '0');
	signal monitorfifo_out : std_logic_vector(12-1 downto 0) := (others => '0');

	-- Signals for scatter-gather, to select the output layer
	signal selout_en_in   : std_logic;
	signal selout_en_out  : std_logic_vector(CONFIG_SELOUT_NB-1 downto 0) := (others => '0');
	signal selout_gat_in  : std_logic_vector(CONFIG_SELOUT_NB*33-1 downto 0) := (others => '0');
	signal selout_gat_out : std_logic_vector(33-1 downto 0) := (others => '0');
	signal selout_sca_in  : std_logic_vector(16-1 downto 0) := (others => '0');
	signal selout_sca_out : std_logic_vector(CONFIG_SELOUT_NB*16-1 downto 0) := (others => '0');

	-- The FIFO at the output of the scatter-gather component
	signal seloutfifo_clear    : std_logic := '0';
	signal seloutfifo_in_data  : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_in_ack   : std_logic := '0';
	signal seloutfifo_in_cnt   : std_logic_vector(16-1 downto 0) := (others => '0');
	signal seloutfifo_out_data : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_out_rdy  : std_logic := '0';
	signal seloutfifo_out_ack  : std_logic := '0';

	----------------------------------------------------
	-- Components
	----------------------------------------------------

	-- The circular buffer / FIFO component
	component fifo_with_counters is
		generic (
			DATAW : natural := 32;
			DEPTH : natural := 8;
			CNTW  : natural := 16
		);
		port (
			reset         : in  std_logic;
			clk           : in  std_logic;
			fifo_in_data  : in  std_logic_vector(DATAW-1 downto 0);
			fifo_in_rdy   : out std_logic;
			fifo_in_ack   : in  std_logic;
			fifo_in_cnt   : out std_logic_vector(CNTW-1 downto 0);
			fifo_out_data : out std_logic_vector(DATAW-1 downto 0);
			fifo_out_rdy  : out std_logic;
			fifo_out_ack  : in  std_logic;
			fifo_out_cnt  : out std_logic_vector(CNTW-1 downto 0)
		);
	end component;

	-- A distributed multiplexer
	-- Used to observe the status of the FIFOs
	component muxtree_bin is
		generic(
			WDATA : natural := 8;
			NBIN  : natural := 20;
			WSEL  : natural := 12
		);
		port(
			clk      : in  std_logic;
			-- Selection input
			sel      : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in    : in  std_logic;
			en_out   : out std_logic_vector(NBIN-1 downto 0);
			-- Data input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	-- Scatter-gather component to select output layer, if anabled
	component scattergather is
		generic(
			WGATHER  : natural := 8;
			WSCATTER : natural := 1;
			NBIN     : natural := 20;
			WSEL     : natural := 12;
			EGATHER  : boolean := true;
			ESCATTER : boolean := false;
			RADIX    : natural := 2;
			REGALL   : boolean := false
		);
		port(
			clk         : in  std_logic;
			-- Selection input
			sel         : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in       : in  std_logic;
			en_out      : out std_logic_vector(NBIN-1 downto 0);
			-- Gather data, input and output
			gather_in   : in  std_logic_vector(NBIN*WGATHER-1 downto 0);
			gather_out  : out std_logic_vector(WGATHER-1 downto 0);
			-- Scatter data, input and output
			scatter_in  : in  std_logic_vector(WSCATTER-1 downto 0);
			scatter_out : out std_logic_vector(NBIN*WSCATTER-1 downto 0)
		);
	end component;

	-- AUTOGEN COMP DECL BEGIN

	-- AUTOGEN COMP DECL END

	-- AUTOGEN SIG DECL BEGIN

	-- AUTOGEN SIG DECL END

begin

	-- Alias names to ease code reuse
	CLK <= S_AXI_ACLK;
	RST <= S_AXI_ARESETN;

	----------------------------------
	-- AXI functionality
	----------------------------------

	-- I/O Connections assignments

	S_AXI_AWREADY <= axi_awready;
	S_AXI_WREADY  <= axi_wready;
	S_AXI_BRESP   <= axi_bresp;
	S_AXI_BVALID  <= axi_bvalid;
	S_AXI_ARREADY <= axi_arready;
	S_AXI_RDATA   <= axi_rdata;
	S_AXI_RRESP   <= axi_rresp;
	S_AXI_RVALID  <= axi_rvalid;

	-- State machine for AXI Write operations
	process (S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			if S_AXI_ARESETN = '0' then
				axi_wready  <= '0';
				axi_awready <= '0';
				axi_awaddr  <= (others => '0');
			else

				-- Implement axi_wready generation
				-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both S_AXI_AWVALID and S_AXI_WVALID are asserted.

				if axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1' then
					-- Slave is ready to accept write data when there is a valid write address and write data on the write address and data bus.
					-- This design expects no outstanding transactions.
					axi_wready <= '1';
				else
					axi_wready <= '0';
				end if;

				-- Implement axi_awready generation
				-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both S_AXI_AWVALID and S_AXI_WVALID are asserted.

				if axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' then
					-- Slave is ready to accept write address when there is a valid write address and write data on the write address and data bus.
					-- This design expects no outstanding transactions.
					axi_awready <= '1';
					-- Write Address latching
					axi_awaddr <= S_AXI_AWADDR;
				else
					axi_awready <= '0';
				end if;

			end if;
		end if;
	end process;

	-- Implement memory mapped register select and write logic generation
	-- The write data is accepted and written to memory mapped registers when
	-- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
	-- Write strobes are used to select byte enables of slave registers while writing.
	-- Slave register write enable is asserted when valid address and data are available
	-- and the slave is ready to accept the write address and write data.
	-- State machine for AXI Write operations
	process (S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			if S_AXI_ARESETN = '0' then
				slv_reg_wren <= '0';
			else

				-- Note: Buffering these signals is optional. It improves routing.
				slv_reg_wren   <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID;
				slv_reg_wraddr <= axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
				slv_reg_wrdata <= S_AXI_WDATA;
				slv_reg_wstrb  <= S_AXI_WSTRB;

			end if;
		end if;
	end process;

	-- State machine for AXI Write response
	process (S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			if S_AXI_ARESETN = '0' then
				axi_bvalid  <= '0';
				axi_bresp   <= "00"; --need to work more on the responses
			else

				-- The write response and response valid signals are asserted by the slave
				-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
				-- This marks the acceptance of address and indicates the status of write transaction.
				if axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0' then
					axi_bvalid <= '1';
					axi_bresp  <= "00";
				end if;

				-- Check if bready is asserted while bvalid is high
				-- (there is a possibility that bready is always asserted high)
				if S_AXI_BREADY = '1' and axi_bvalid = '1' then
					axi_bvalid <= '0';
				end if;

			end if;
		end if;
	end process;

	-- State machine for AXI Read operation
	process (S_AXI_ACLK)
	begin
		if rising_edge(S_AXI_ACLK) then
			if S_AXI_ARESETN = '0' then
				axi_arready <= '0';
				axi_araddr  <= (others => '0');
				axi_rvalid  <= '0';
				axi_rresp   <= "00";
				axi_rdata   <= (others => '0');
			else

				-- Get the read address
				-- axi_arready is asserted for one S_AXI_ACLK clock cycle when S_AXI_ARVALID is asserted.
				-- The read address is also latched when S_AXI_ARVALID is asserted.

				if axi_arready = '0' and S_AXI_ARVALID = '1' then
					-- Indicates that the slave has accepted the valid read address
					axi_arready <= '1';
					-- Read Address latching
					axi_araddr  <= S_AXI_ARADDR;
				else
					axi_arready <= '0';
				end if;

				-- Send the read data
				-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both S_AXI_ARVALID and axi_arready are asserted.
				-- The slave registers data are available on the axi_rdata bus at this instance.
				-- The assertion of axi_rvalid marks the validity of read data on the bus and axi_rresp indicates the status of read transaction.

				if axi_arready = '1' then
					axi_rdata <= slv_reg_rddata;
				end if;

				if axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0' then
					-- Valid read data is available at the read data bus
					axi_rvalid <= '1';
					axi_rresp  <= "00"; -- 'OKAY' response
				end if;

				if (axi_rvalid = '1' and S_AXI_RREADY = '1') then
					-- Read data is accepted by the master
					axi_rvalid <= '0';
				end if;

			end if;  -- S_AXI_ARESETN = '1'
		end if;  -- rising_edge(S_AXI_ACLK)
	end process;

	-- Alias signals to be used by the user design

	slv_reg_rden   <= axi_arready;
	slv_reg_rdaddr <= axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);

	alias_rx_data  <= inst_rdbuf_out_data;
	alias_rx_valid <= inst_rdbuf_out_rdy;

	alias_tx_valid <= inst_wrbuf_in_rdy;


	------------------------------------------------
	-- Configuration registers for the network layers
	------------------------------------------------

	-- Combinational process that generates the next value to config registers
	process (all)
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	begin

		-- Init at zero if registers are locked
		if (CONFIG_NOREGS = true) or (CONFIG_LOCKREGS = true) then
			config_regs_var := (others => '0');
		else

			config_regs_var := config_regs;

			for i in 0 to CONFIG_CHAIN_NB-1 loop
				if config_chain_set(i) = '1' then
					config_regs_var(i*32+31 downto i*32) := config_chain_regs(i*32+31 downto i*32);
				end if;
			end loop;

		end if;

		-- Apply constant bits
		config_regs_var := config_regs_apply_const(config_regs_var);

		-- Set default values, or write reset functionality
		-- Or, systematically overwrite that config if the registers are locked or not implemented
		if (config_chain_def = '1') or (CONFIG_RDONLY = true) or (CONFIG_NOREGS = true) then
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
		end if;

		config_regs_next <= config_regs_var;

	end process;

	gen_config_regs : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) generate

		process (CLK)
		begin
			if rising_edge(CLK) then
				config_regs <= config_regs_next;
			end if;
		end process;

	end generate;

	gen_config_noregs : if (CONFIG_NOREGS = true) or (CONFIG_RDONLY = true) generate
		config_regs <= config_regs_next;
	end generate;

	-- If the network config is locked, then the config is stored into a small memory
	gen_config_read_rom : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = true) generate

		type config_regs_rom_type is array (0 to CONFIG_CHAIN_NB-1) of std_logic_vector(31 downto 0);

		function config_regs_gen_mem_init(phony : boolean) return config_regs_rom_type is
			variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
			variable rom_init_var : config_regs_rom_type := (others => (others => '0'));
		begin
			config_regs_var := (others => '0');
			config_regs_var := config_regs_apply_const(config_regs_var);
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
			for i in 0 to CONFIG_CHAIN_NB-1 loop
				rom_init_var(i) := config_regs_var(i*32+31 downto i*32);
			end loop;
			return rom_init_var;
		end function;

		constant config_regs_rom : config_regs_rom_type := config_regs_gen_mem_init(false);

		constant READ_INDEX_SIZE : natural := storebitsnb(CONFIG_CHAIN_NB);
		signal read_index : unsigned(READ_INDEX_SIZE-1 downto 0) := (others => '0');

	begin

		-- Assign the read word
		config_regs_read <= config_regs_rom(to_integer(read_index));

		-- Update the word index
		process (CLK)
		begin
			if rising_edge(CLK) then

				if config_chain_shift(0) = '1' then
					-- Apply resizing in order to avoid simulation errors due to overflow
					read_index <= resize(resize(read_index, READ_INDEX_SIZE+1) + 1, READ_INDEX_SIZE);
				end if;

				if (config_chain_get(0) = '1') or (reset_reg = RSTVAL_GEN) then
					read_index <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- If the network config is not locked, then the config is read from the registers
	gen_config_read_chain : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) generate

		-- Assign the read word
		config_regs_read <= config_chain_regs(31 downto 0);

	end generate;


	----------------------------------
	-- Main design functionality
	----------------------------------

	-- Alias signals
	cur_recv1 <= slv_reg4(5 downto 0);
	cur_recv2 <= slv_reg4(15 downto 6);
	cur_send  <= slv_reg4(23 downto 16);

	cur_freerun <= slv_reg3(1);

	txcnt_want <= unsigned(slv_reg6);

	-- Main sequential process: write to config registers, implement all synchronous registers
	process (CLK)
		variable tmpvar_slv_reg         : std_logic_vector(31 downto 0) := (others => '0');
		variable tmpvar_slv_reg_mask_we : std_logic_vector(31 downto 0) := (others => '0');
	begin
		if rising_edge(CLK) then

			-- Hold reset active for a certain duration
			if reset_counter > 0 then
				reset_counter <= reset_counter - 1;
				reset_reg <= RSTVAL_GEN;
			else
				reset_reg <= not RSTVAL_GEN;
			end if;
			-- Generate reset
			if RST = RSTVAL_IN then
				reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
				reset_reg <= RSTVAL_GEN;
			end if;

			-- Default/reset assignments
			req_start_recv <= '0';
			req_start_send <= '0';

			-- Buffers for the main data channel, read direction
			rxbuf_data  <= rxbuf_data_n;
			rxbuf_nbits <= rxbuf_nbits_n;

			-- Buffers for output registers
			txbuf_data  <= txbuf_data_n;
			txbuf_nbits <= txbuf_nbits_n;
			txcnt_cur   <= txcnt_cur_n;
			txrun_en    <= txrun_en_n;
			txrun_gotall <= txrun_gotall_n;

			-- Commands for config registers
			config_chain_shift <= (others => '0');
			config_chain_get   <= (others => '0');
			config_chain_set   <= (others => '0');
			config_chain_def   <= '0';
			if reset_reg = RSTVAL_GEN then
				config_chain_def <= '1';
			end if;

			-- Bufferize the flag to select the output layer, scatter-gather or last FIFO
			send_is_last <= '0';
			if (CONFIG_SELOUT = false) or (cur_send = CST_SEND_DEFAULT) then
				send_is_last <= '1';
			end if;

			-- Write to register
			if slv_reg_wren = '1' then
				case slv_reg_wraddr is

					when b"0000" =>
						-- Slave register 0
						-- Frame size. Only some bits are writable.
						tmpvar_slv_reg_mask_we := x"0000FFFF";
						tmpvar_slv_reg := (slv_reg_wrdata and tmpvar_slv_reg_mask_we) or (slv_reg0 and not tmpvar_slv_reg_mask_we);

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg0(byte_index*8+7 downto byte_index*8) <= tmpvar_slv_reg(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"0001" =>
						-- Slave register 1
						-- Number of neurons in first stage. Only some bits are writable.
						tmpvar_slv_reg_mask_we := x"0000FFFF";
						tmpvar_slv_reg := (slv_reg_wrdata and tmpvar_slv_reg_mask_we) or (slv_reg1 and not tmpvar_slv_reg_mask_we);

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg1(byte_index*8+7 downto byte_index*8) <= tmpvar_slv_reg(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

						-- Shift configuration registers
						if slv_reg3(8) = '1' then
							config_chain_shift <= (others => '1');
						end if;

					when b"0010" =>
						-- Slave register 2
						-- Number of neurons in second stage. Only some bits are writable.
						tmpvar_slv_reg_mask_we := x"0000FFFF";
						tmpvar_slv_reg := (slv_reg_wrdata and tmpvar_slv_reg_mask_we) or (slv_reg2 and not tmpvar_slv_reg_mask_we);

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg2(byte_index*8+7 downto byte_index*8) <= tmpvar_slv_reg(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"0011" =>
						-- Slave register 3
						-- Misc status & control flags. Only some bits are writable.
						tmpvar_slv_reg_mask_we := x"00000102";
						tmpvar_slv_reg := (slv_reg_wrdata and tmpvar_slv_reg_mask_we) or (slv_reg3 and not tmpvar_slv_reg_mask_we);

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg3(byte_index*8+7 downto byte_index*8) <= tmpvar_slv_reg(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

						-- Detect the clear requests
						if slv_reg_wrdata(0) = '1' then
							reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
							reset_reg <= RSTVAL_GEN;
						end if;

						-- Config registers
						if CONFIG_NOREGS = false then
							-- Config registers: Get request
							if slv_reg_wrdata(9) = '1' then
								config_chain_get <= (others => '1');
							end if;
							-- Config registers: Set request
							if slv_reg_wrdata(10) = '1' then
								config_chain_set <= (others => '1');
							end if;
						end if;

					when b"0100" =>
						-- Slave register 4

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg4(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"0101" =>
						-- Slave register 5

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg5(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"0110" =>
						-- Slave register 6
						-- Write the number of result values to send to the main data channel

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg6(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

						-- Generate a pulse to start sending results on main data channel + clear regs
						req_start_send <= '1';

					when b"0111" =>
						-- Slave register 7

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg7(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1000" =>
						-- Slave register 8

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg8(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1001" =>
						-- Slave register 9

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg9(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1010" =>
						-- Slave register 10

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg10(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1011" =>
						-- Slave register 11

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg11(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1100" =>
						-- Slave register 12

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg12(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1101" =>
						-- Slave register 13

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg13(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1110" =>
						-- Slave register 14

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg14(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when b"1111" =>
						-- Slave register 15

						-- Respective byte enables are asserted as per write strobes
						for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
							if ( slv_reg_wstrb(byte_index) = '1' ) then
								slv_reg15(byte_index*8+7 downto byte_index*8) <= slv_reg_wrdata(byte_index*8+7 downto byte_index*8);
							end if;
						end loop;

					when others =>

				end case;  -- Address
			end if;  -- Write enable

			-- Logic for config registers
			if CONFIG_NOREGS = false then

				-- Read config register
				if (slv_reg_rden = '1') and (unsigned(slv_reg_rdaddr) = 1) and (slv_reg3(8) = '1') then
					config_chain_shift <= (others => '1');
				end if;

			end if;

			-- Logic for config registers
			if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) then

				-- Config registers: operations shift, get, set
				for i in 0 to CONFIG_CHAIN_NB-1 loop
					-- Shift
					if config_chain_shift(i) = '1' then
						-- The slave register 1 is fed at the end of the chain
						if i < CONFIG_CHAIN_NB - 1 then
							config_chain_regs(i*32+31 downto i*32) <= config_chain_regs((i+1)*32+31 downto (i+1)*32);
						else
							config_chain_regs(i*32+31 downto i*32) <= slv_reg1;
						end if;
					end if;
					-- Get
					if config_chain_get(i) = '1' then
						config_chain_regs(i*32+31 downto i*32) <= config_regs(i*32+31 downto i*32);
					end if;
					-- Set : this is implemented in separate process
				end loop;

			end if;  -- (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false)

			if reset_reg = RSTVAL_GEN then

				slv_reg0  <= (others => '0');
				slv_reg1  <= (others => '0');
				slv_reg2  <= (others => '0');
				slv_reg3  <= (others => '0');
				slv_reg4  <= (others => '0');
				slv_reg5  <= (others => '0');
				slv_reg6  <= (others => '0');
				slv_reg7  <= (others => '0');
				slv_reg8  <= (others => '0');
				slv_reg9  <= (others => '0');
				slv_reg10 <= (others => '0');
				slv_reg11 <= (others => '0');
				slv_reg12 <= (others => '0');
				slv_reg13 <= (others => '0');
				slv_reg14 <= (others => '0');
				slv_reg15 <= (others => '0');

				-- The accelerator ID
				slv_reg0 <=
					std_logic_vector(to_unsigned(VERSION_MIN, 8)) &  -- Minor version
					std_logic_vector(to_unsigned(VERSION_MAJ, 8)) &  -- Major version
					std_logic_vector(to_unsigned(78, 8)) &  -- ASCII for N
					std_logic_vector(to_unsigned(78, 8));   -- ASCII for N

				slv_reg3(2)            <= to_std_logic(CONFIG_SELOUT);
				slv_reg3(3)            <= to_std_logic(CONFIG_FIFOMON);
				slv_reg3( 7 downto  4) <= std_logic_vector(to_unsigned(storebitsnb(WDATA_ROUND_POW2)-1, 4));
				slv_reg3(11)           <= to_std_logic(CONFIG_RDONLY);
				slv_reg3(17 downto 12) <= std_logic_vector(to_unsigned(CONFIG_IFW / 8, 6));
				slv_reg3(31 downto 24) <= std_logic_vector(to_unsigned(CONFIG_CHAIN_NB, 8));

				slv_reg4( 5 downto  0) <= CST_RECV_FRAME;
				slv_reg4(23 downto 16) <= CST_SEND_DEFAULT;

			end if;  -- Reset

		end if;  -- Clock
	end process;

	-- Combinatorial process - Control signals to receive items from main data channel or from registers
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Defaults for FIFO channels : no operation
		alias_rx_ready <= '0';
		inst_firstfifo_in_ack <= '0';

		-- Select bits from the buffer of the main data channel
		for i in 0 to FIRSTFIFO_PAR-1 loop
			inst_firstfifo_in_data((i+1)*FIRSTFIFO_DATAW-1 downto i*FIRSTFIFO_DATAW) <= rxbuf_data(i*WDATA_ROUND_POW2+FIRSTFIFO_DATAW-1 downto i*WDATA_ROUND_POW2);
		end loop;

		-- Default next values for registers
		rxbuf_data_n  <= rxbuf_data;
		rxbuf_nbits_n <= rxbuf_nbits;
		rxcnt_cur_n   <= rxcnt_cur;

		-- Receive items from main data channel or from writes to registers
		if cur_recv1 = CST_RECV_FRAME then

			-- There is at least one item in the buffer
			if rxbuf_nbits >= WDATA_ROUND_POW2 * INPAR_ROUND_POW2 then
				-- Validate data for first FIFO
				inst_firstfifo_in_ack <= '1';
				if inst_firstfifo_in_rdy = '1' then
					-- Shift the data buffer
					if CONFIG_IFW > WDATA_ROUND_POW2*INPAR_ROUND_POW2 then
						rxbuf_data_n(rxbuf_data'high-WDATA_ROUND_POW2*INPAR_ROUND_POW2 downto 0) <= rxbuf_data(rxbuf_data'high downto WDATA_ROUND_POW2*INPAR_ROUND_POW2);
					end if;
					-- Update the counters
					rxbuf_nbits_n <= rxbuf_nbits - WDATA_ROUND_POW2 * INPAR_ROUND_POW2;
					rxcnt_cur_n   <= rxcnt_cur + FIRSTFIFO_PAR;
					-- Detect when the buffer becomes empty
					if rxbuf_nbits < 2 * WDATA_ROUND_POW2 * INPAR_ROUND_POW2 then
						var_buf_end := true;
					end if;
				end if;
			end if;  -- End send items to the NN

			-- Get the next data buffer
			if (rxbuf_nbits < WDATA_ROUND_POW2 * INPAR_ROUND_POW2) or (var_buf_end = true) then

				-- Clear the bit counter
				rxbuf_nbits_n <= to_unsigned(0, rxbuf_nbits_n'length);

				-- Fill the next data buffer directly from the main data channel
				alias_rx_ready <= '1';
				if alias_rx_valid = '1' then
					rxbuf_data_n  <= alias_rx_data;
					rxbuf_nbits_n <= to_unsigned(CONFIG_IFW, rxbuf_nbits_n'length);
				end if;

			end if;  -- End get the next data buffer

		-- Handle when the received data is to write config into layers
		else

			-- Indicate to the main data channel that we are always ready to get the next buffer
			alias_rx_ready <= '1';

		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			alias_rx_ready <= '0';
			inst_firstfifo_in_ack <= '0';

			rxbuf_nbits_n <= to_unsigned(0, rxbuf_nbits_n'length);
			rxcnt_cur_n   <= to_unsigned(0, rxcnt_cur_n'length);

		end if;

	end process;

	-- Combinatorial process - Control signals for output values
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Default next values for registers
		txbuf_data_n  <= txbuf_data;
		txbuf_nbits_n <= txbuf_nbits;
		txcnt_cur_n   <= txcnt_cur;
		txrun_en_n    <= txrun_en;
		txrun_gotall_n <= txrun_gotall;

		-- Defaults for FIFO channels : no operation
		alias_tx_ready        <= '0';
		inst_lastfifo_out_ack <= '0';
		seloutfifo_out_ack    <= '0';

		if cur_freerun = '1' then

			-- Free run mode: accept all output values, count them and drop them
			inst_lastfifo_out_ack <= '1';
			seloutfifo_out_ack <= '1';
			if (send_is_last = '1' and inst_lastfifo_out_rdy = '1') or (send_is_last = '0' and seloutfifo_out_rdy = '1') then
				txcnt_cur_n <= txcnt_cur + 1;
			end if;

		elsif txrun_en = '0' then

			txbuf_nbits_n <= (others => '0');

			if (req_start_send = '1') and (txcnt_want > 0) then
				txrun_en_n <= '1';
				txcnt_cur_n <= (others => '0');
			end if;

		else

			-- Send the buffer to the main data channel
			if (txbuf_nbits = CONFIG_IFW) or (txcnt_cur >= txcnt_want) then
				alias_tx_ready <= '1';
				if alias_tx_valid = '1' then
					txbuf_nbits_n <= to_unsigned(0, txbuf_nbits_n'length);
					var_buf_end := true;
				end if;
			end if;

			-- Get the output values out of the FIFO, enqueue them in the output buffer
			if ((txbuf_nbits < CONFIG_IFW) or (var_buf_end = true)) and (txcnt_cur < txcnt_want) then
				inst_lastfifo_out_ack <= '1';
				seloutfifo_out_ack <= '1';
				if (send_is_last = '1' and inst_lastfifo_out_rdy = '1') or (send_is_last = '0' and seloutfifo_out_rdy = '1') then
					-- Shift the data buffer
					if CONFIG_IFW > 32 then
						txbuf_data_n(txbuf_data'high-32 downto 0) <= txbuf_data(txbuf_data'high downto 32);
					end if;
					if send_is_last = '1' then
						txbuf_data_n(txbuf_data'high downto txbuf_data'high-31) <= std_logic_vector(resize(signed(inst_lastfifo_out_data), 32));
					else
						txbuf_data_n(txbuf_data'high downto txbuf_data'high-31) <= seloutfifo_out_data;
					end if;
					-- Increment the output counters
					txbuf_nbits_n <= txbuf_nbits + 32;
					txcnt_cur_n   <= txcnt_cur + 1;
					-- Adapt the count when the previous buffer is precisely being sent to the main data channel
					if var_buf_end = true then
						txbuf_nbits_n <= to_unsigned(32, txbuf_nbits_n'length);
					end if;
				end if;
			end if;

			-- All desired results have been obtained
			if txcnt_cur >= txcnt_want then
				txrun_en_n <= '0';
				txrun_gotall_n <= '1';
			end if;

		end if;

		-- Fill the TX channel with junk data, to ensure the interface generates a last flush operation
		if txrun_gotall = '1' then
			alias_tx_ready <= '1';
		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			alias_tx_ready        <= '0';
			inst_lastfifo_out_ack <= '0';
			seloutfifo_out_ack    <= '0';

			txbuf_nbits_n <= (others => '0');
			txcnt_cur_n   <= (others => '0');
			txrun_en_n    <= '0';
			txrun_gotall_n <= '0';

		end if;

	end process;

	-- Combinatorial process - Read register, it's a big MUX
	process (all)
	begin

		slv_reg_rddata <= (others => '0');

		-- Address decoding for reading registers
		case slv_reg_rdaddr is

			when b"0000" =>
				-- Slave register 0
				slv_reg_rddata <= slv_reg0;

			when b"0001" =>
				-- Slave register 1
				slv_reg_rddata <= slv_reg1;

			when b"0010" =>
				-- Slave register 2
				slv_reg_rddata <= slv_reg2;

			when b"0011" =>
				-- Slave register 3
				slv_reg_rddata <= slv_reg3;

				slv_reg_rddata(8)  <= reset_reg;

				slv_reg_rddata(9)  <= mymaster_busyw;
				slv_reg_rddata(10) <= mymaster_busyr;

				slv_reg_rddata(11) <= txrun_en;
				slv_reg_rddata(12) <= txrun_gotall;

			when b"0100" =>
				-- Slave register 4
				--slv_reg_rddata <= slv_reg4;

				slv_reg_rddata <= mymaster_sensor;

			when b"0101" =>
				-- Slave register 5
				slv_reg_rddata <= slv_reg5;

			when b"0110" =>
				-- Slave register 6
				--slv_reg_rddata <= slv_reg6;

				-- Read the number of result values that got out of the CNN
				slv_reg_rddata <= std_logic_vector(txcnt_cur);

			when b"0111" =>
				-- Slave register 7
				--slv_reg_rddata <= slv_reg7;

				-- Read the number of input items sent to the CNN
				slv_reg_rddata <= std_logic_vector(rxcnt_cur);

			when b"1000" =>
				-- Slave register 8
				slv_reg_rddata <= slv_reg8;

			when b"1001" =>
				-- Slave register 9
				slv_reg_rddata <= slv_reg9;

			when b"1010" =>
				-- Slave register 10
				slv_reg_rddata <= slv_reg10;

			when b"1011" =>
				-- Slave register 11
				slv_reg_rddata <= slv_reg11;

			when b"1100" =>
				-- Slave register 12
				slv_reg_rddata <= slv_reg12;

			when b"1101" =>
				-- Slave register 13
				slv_reg_rddata <= slv_reg13;

			when b"1110" =>
				-- Slave register 14
				slv_reg_rddata <= slv_reg14;

			when b"1111" =>
				-- Slave register 15
				slv_reg_rddata <= slv_reg15;
				slv_reg_rddata(11 downto 0) <= monitorfifo_out;

			when others =>
				slv_reg_rddata <= (others => '0');

		end case;

	end process;


	----------------------------------
	-- FIFO to hold data read from DDR
	----------------------------------

	-- Instantiate the FIFO for the read values
	i_rdbuf : fifo_with_counters
		generic map (
			DATAW => 32,
			DEPTH => DDRFIFOS_DEPTH,
			CNTW  => DDRFIFOS_CNTW
		)
		port map (
			clk           => S_AXI_ACLK,
			reset         => inst_rdbuf_clear,
			fifo_in_data  => inst_rdbuf_in_data,
			fifo_in_rdy   => inst_rdbuf_in_rdy,
			fifo_in_ack   => inst_rdbuf_in_ack,
			fifo_in_cnt   => inst_rdbuf_in_cnt,
			fifo_out_data => inst_rdbuf_out_data,
			fifo_out_rdy  => inst_rdbuf_out_rdy,
			fifo_out_ack  => inst_rdbuf_out_ack,
			fifo_out_cnt  => inst_rdbuf_out_cnt
		);

	inst_rdbuf_clear   <= reset_reg or req_start_recv;
	inst_rdbuf_in_data <= mymaster_fifor_data;
	inst_rdbuf_in_ack  <= mymaster_fifor_en;
	inst_rdbuf_out_ack <= alias_rx_ready;


	----------------------------------
	-- FIFO to write to DDR
	----------------------------------

	i_wrbuf : fifo_with_counters
		generic map (
			DATAW => 32,
			DEPTH => DDRFIFOS_DEPTH,
			CNTW  => DDRFIFOS_CNTW
		)
		port map (
			clk           => S_AXI_ACLK,
			reset         => inst_wrbuf_clear,
			fifo_in_data  => inst_wrbuf_in_data,
			fifo_in_rdy   => inst_wrbuf_in_rdy,
			fifo_in_ack   => inst_wrbuf_in_ack,
			fifo_in_cnt   => inst_wrbuf_in_cnt,
			fifo_out_data => inst_wrbuf_out_data,
			fifo_out_rdy  => inst_wrbuf_out_rdy,
			fifo_out_ack  => inst_wrbuf_out_ack,
			fifo_out_cnt  => inst_wrbuf_out_cnt
		);

	-- Set inputs
	inst_wrbuf_clear   <= reset_reg or req_start_send;
	inst_wrbuf_in_data <= txbuf_data;
	inst_wrbuf_in_ack  <= alias_tx_ready;
	inst_wrbuf_out_ack <= mymaster_fifow_en;


	----------------------------------
	-- Talk to the Master port
	----------------------------------

	mymaster_fifow_data <= inst_wrbuf_out_data;

	mymaster_fifor_cnt <= std_logic_vector(resize(unsigned(inst_rdbuf_in_cnt), 16));
	mymaster_fifow_cnt <= std_logic_vector(resize(unsigned(inst_wrbuf_out_cnt), 16));

	mymaster_addr_inr <= slv_reg10;
	mymaster_addr_inw <= slv_reg11;

	mymaster_burstnb_inr <= slv_reg12;
	mymaster_burstnb_inw <= slv_reg13;

	mymaster_startr <= req_start_recv;
	mymaster_startw <= req_start_send;


	------------------------------------------------
	-- Miscellaneous components around the network pipeline
	--------------------------------------------------

	-- The MUX to observe the state of FIFOs

	gen_fifomon: if CONFIG_FIFOMON = true generate

		fifomon : muxtree_bin
			generic map (
				WDATA => 12,
				NBIN  => CONFIG_FIFOS_NB,
				WSEL  => 8
			)
			port map (
				clk       => clk,
				-- Selection input
				sel       => slv_reg15(31 downto 24),
				-- Enable input and output
				en_in     => '1',
				en_out    => open,
				-- Data input and output
				data_in   => monitorfifo_in,
				data_out  => monitorfifo_out
			);

	end generate;

	-- The scatter-gather component to select the output layer

	gen_selout: if CONFIG_SELOUT = true generate

		scagat : scattergather
			generic map (
				WGATHER  => 33,
				WSCATTER => 16,
				NBIN     => CONFIG_SELOUT_NB,
				WSEL     => 8,
				EGATHER  => true,
				ESCATTER => true,
				RADIX    => 2,
				REGALL   => true
			)
			port map (
				clk         => clk,
				-- Selection input
				sel         => cur_send,
				-- Enable input and output
				en_in       => selout_en_in,
				en_out      => selout_en_out,
				-- Gather data, input and output
				gather_in   => selout_gat_in,
				gather_out  => selout_gat_out,
				-- Scatter data, input and output
				scatter_in  => selout_sca_in,
				scatter_out => selout_sca_out
			);

		selout_en_in  <= not send_is_last;

		-- Warning: Simply subtracting 8 (or 16 or 32) to the count created bugs,
		-- the FIFO can still get full when the output is stalled, and an overflow was sent to the layers, which didn't stop
		-- The following solution seems OK
		selout_sca_in <= seloutfifo_in_cnt when unsigned(seloutfifo_in_cnt) > 24 else (others => '0');

		seloutfifo_in_data <= selout_gat_out(31 downto 0);
		seloutfifo_in_ack  <= selout_gat_out(32);

		fifo : fifo_with_counters
			generic map (
				DATAW => 32,
				DEPTH => 64,
				CNTW  => 16
			)
			port map (
				clk           => CLK,
				reset         => seloutfifo_clear,
				fifo_in_data  => seloutfifo_in_data,
				fifo_in_rdy   => open,
				fifo_in_ack   => seloutfifo_in_ack,
				fifo_in_cnt   => seloutfifo_in_cnt,
				fifo_out_data => seloutfifo_out_data,
				fifo_out_rdy  => seloutfifo_out_rdy,
				fifo_out_ack  => seloutfifo_out_ack,
				fifo_out_cnt  => open
			);

		seloutfifo_clear <= '1' when reset_reg = RSTVAL_GEN else '0';

	end generate;


	-- AUTOGEN COMP INST BEGIN

	-- AUTOGEN COMP INST END


end arch_imp;
