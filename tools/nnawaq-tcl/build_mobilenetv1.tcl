#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It generates a quantized version of network MobileNet-v1
# See paper : MobileNets: Efficient Convolutional Neural Networks for Mobile Vision Applications (2017)
# See paper : MobileNetV2: Inverted Residuals and Linear Bottlenecks (2018)
# See paper : Searching for MobileNetV3 (2019)

# Input images : 224x224x3 8b
# Output : 1000 classes

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

if { ! [info exists freq]} {
	set freq 250
}
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1

nn_set f=224/224/3
nn_set fn=1
nn_set inpar=1

nn_set in=8s
nn_set acts=4u
nn_set weights=4s

nn_set neu_style=2

nn_set round_near=1

# Default settings for activations
nn_set norm_wbias=8
nn_set norm_wmul=16
nn_set norm_wshr=4
nn_set relu=0/15

nn_set no_fifo_win_pool=false
nn_set no_fifo_neu_relu=false
nn_set no_fifo_norm_relu=true

set last_bn ""
set last_relu ""

# Utility procedure to create one conv neuron layer
proc create_nn_conv_neu {win step pad neu nwin} {
	nn_layer_create window $win $step $pad $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu
}

# Utility procedure to create one DWConv 3x3 + one Conv 1x1 neuron layers
# Parameter "step" applies to DWConv layer, parameter "neu" applies to 1x1 conv layer
proc create_nn_dwconv3_conv1 {step neu} {
	global last_bn
	global last_relu
	# The number of neurons is the image size FZ
	set dwneu [nn_get fsize_z]
	# Get the scalar step value, remove the leading part "step="
	set stepval [string range $step 5 end]
	set fx [nn_get fsize_x]
	set nwin [expr $fx / $stepval]
	# Create layers
	nn_layer_create window win=3 $step pad=1 nwin=$nwin dwconv=1
	nn_layer_create neuron neu=$dwneu
	nn_layer_create norm
	nn_layer_create relu
	nn_layer_create window win=1 step=1 pad=0 nwin=$nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	set last_bn [nn_get last]
	nn_layer_create relu
	set last_relu [nn_get last]
}

# Utility procedure to create one conv AvgPool layer
# Note : Hardcode an extra BUFY size to ensure the internal buffer is large enough for 2 frames
proc create_nn_conv_avgpool {win step pad nwin} {
	nn_layer_create window $win $step $pad $nwin bufy=14
	nn_layer_create avgpool
}

# Create the network:

# image 224x224x3

create_nn_conv_neu win=3 step=2 pad=1 neu=32 nwin=112
# image 112x112x32

create_nn_dwconv3_conv1 step=1 neu=64
# image 112x112x64
create_nn_dwconv3_conv1 step=2 neu=128
# image 56x56x128
create_nn_dwconv3_conv1 step=1 neu=128
# image 56x56x128
create_nn_dwconv3_conv1 step=2 neu=256
# image 28x28x256
create_nn_dwconv3_conv1 step=1 neu=256
# image 28x28x256
create_nn_dwconv3_conv1 step=2 neu=512
# image 14x14x512

for {set i 1} {$i < 5} {incr i} {
	create_nn_dwconv3_conv1 step=1 neu=512
}
# image 14x14x512

create_nn_dwconv3_conv1 step=2 neu=1024
# image 7x7x1024
# FIXME This is shown as step=2 in orig publication but would not match other image sizes
create_nn_dwconv3_conv1 step=1 neu=1024
# image 7x7x1024

create_nn_conv_avgpool win=7 step=7 pad=0 nwin=1
# image 1x1x1024

# Output neuron layer, fully connected
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1 step=1 pad=0 bufy=2
}
nn_layer_create neuron neu=1000
set neu_last [nn_get last]
# image 1x1x1000
nn_layer_create softmax
# image 1x1x1

# Hardcoded activation widths for select layers
nn_layer_set $last_bn wout=8
nn_layer_set $last_relu relu=0/255
# Hardcoded weight widths for select layers
nn_layer_set neu0 weights=8s
nn_layer_set $neu_last weights=8s

# For information, print the network details
nn_print -cycles

# Get the bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
puts "Applying parallelism levels for PAR=$par"
nn_autopar $par
puts "Applying time multiplexing to fastest layers"
nn_autotmux
puts "Applying more parallelism to reduce the number of physical neurons"
nn_maxparin

# For information, print the network details
nn_print -cycles

# Get the new bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# To help tweak per-layer memory implem
#nn_print_mem -type-neu
#nn_print_mem -type-win
foreach layer [list win neu norm] { nn_print_mem -type-$layer }
nn_print_mem -total

