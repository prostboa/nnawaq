#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It builds a VGG-like neural network designed to classify images for CIFAR10 / CIFAR100 / GTSRB datasets

# Settings inherited from environment variables

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

set vgg_neu 64
if {[info exists env(NEU)]} {
	set vgg_neu $env(NEU)
}

set nout 10
if {[info exists env(NOUT)]} {
	set nout $env(NOUT)
}

set indata 8s
if {[info exists env(INDATA)]} {
	set indata $env(INDATA)
}

set weights ter
if {[info exists env(WEIGHTS)]} {
	set weights $env(WEIGHTS)
}

if { ! [info exists freq]} {
	set freq 250
}
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1

set fx 32
set fy 32
set fz 3
if {[info exists env(FX)]} {
	set fx $env(FX)
}
if {[info exists env(FY)]} {
	set fy $env(FY)
}
if {[info exists env(FZ)]} {
	set fz $env(FZ)
}

nn_set fn=1
nn_set f=$fx/$fy/$fz

nn_set in=$indata
nn_set acts=2s
nn_set weights=$weights

######################

nn_set neu_style=2

puts "Summary of parameters :"
puts "  FSIZE .... $fx $fy $fz"
puts "  INDATA ... $indata"
puts "  WEIGHTS .. $weights"
puts "  NEU ...... $vgg_neu"
puts "  NOUT ..... $nout"
puts "  PAR ...... $par"

# Helper procedures to create layers
proc conv {neu} {
	nn_layer_create window win=3x3 step=1x1 pad=1x1
	nn_layer_create neuron neu=$neu
	nn_layer_create ternarize
}
proc ccp {neu} {
	conv $neu
	conv $neu
	nn_layer_create window win=2x2 step=2x2 pad=0x0
	nn_layer_create maxpool
}

puts "Creating the neural network"

# The convolution layers
ccp [expr {1 * $vgg_neu}]
ccp [expr {2 * $vgg_neu}]
ccp [expr {4 * $vgg_neu}]

# The fully connected layers
nn_layer_create flatten

if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1x1 step=1x1 pad=0x0
}
nn_layer_create neuron neu=[expr {8*$vgg_neu}]
nn_layer_create ternarize
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1x1 step=1x1 pad=0x0
}
nn_layer_create neuron neu=[expr {8*$vgg_neu}]
nn_layer_create ternarize

# The output neuron layer
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1x1 step=1x1 pad=0x0
}
nn_layer_create neuron neu=$nout
# image 1x1xN
nn_layer_create softmax
# image 1x1x1

# For information, print the network details
nn_print -cycles

# Get the bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
puts "Applying parallelism levels for PAR=$par"
nn_autopar $par
puts "Applying time multiplexing to fastest layers"
nn_autotmux
puts "Applying more parallelism to reduce the number of physical neurons"
nn_maxparin

# For information, print the network details
nn_print -cycles

# Get the new bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# Set ternary compression
# This requires that memory implementation is set in layers, so this needs to be done after nn_finalize_hw_config
if {False} {
	# Variants with global options
	# Note : Ideally these global options should work before nn_finalize_hw_config, but that's not the case right now
	#nn_set tercomp_bram=3/5
	#nn_set tercomp_bram=5/8
	#nn_set tercomp_fc=3/5
	#nn_set tercomp_fc=5/8
	# Variants more layer-specific
	#foreach layer [nn_get layers -type-neu -mem-bram] { nn_layer_set $layer tercomp=3t5b }
	foreach layer [nn_get layers -type-neu -mem-bram] { nn_layer_set $layer tercomp=5t8b }
	# Need to re-run finalization of HW to recompute the memory width
	nn_finalize_hw_config
}

# To help tweak per-layer memory implem
#nn_print_mem -only-neu
#nn_print_mem -only-win
foreach layer [list win neu ter] { nn_print_mem -type-$layer }
nn_print_mem -total

