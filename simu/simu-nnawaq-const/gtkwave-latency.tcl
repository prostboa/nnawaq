
gtkwave::loadFile "wave.fst"

# The clock period is 10 ns
set period 10000000
# Set the initial time marker after reset is cleared (100 clock cycles)
gtkwave::setMarker [expr 100 * $period]


set sig "nnawaq_axi3_bench.user_inst.i_softmax0.data_out_valid"
set num_found [ gtkwave::addSignalsFromList $sig ]
set num_found [ gtkwave::highlightSignalsFromList $sig ]
if { $num_found == 0 } {
	puts "Error signal not found : $sig"
	exit 1
}


set prev_time [ gtkwave::getMarker ]

# posedge
set new_time [ gtkwave::findNextEdge ]
set nclk [expr ( $new_time - $prev_time ) / $period ]
puts "Output : time $new_time, after $nclk clock cycles"
if { $nclk == 0 } {
	puts "Error : Next edge not found"
	exit 1
}
set prev_time $new_time
# negedge
gtkwave::findNextEdge

# posedge
set new_time [ gtkwave::findNextEdge ]
set nclk [expr ( $new_time - $prev_time ) / $period ]
puts "Output : time $new_time, after $nclk clock cycles"
if { $nclk == 0 } {
	puts "Error : Next edge not found"
	exit 1
}
set prev_time $new_time
# negedge
gtkwave::findNextEdge


exit 0

