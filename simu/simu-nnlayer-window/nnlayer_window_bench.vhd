
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnlayer_window_bench is
	generic (

		-- Data
		WDATA  : natural := 32;
		-- Dimensions: width, height, depth
		DIMX   : natural := 3;
		DIMY   : natural := 3;
		DIMZ   : natural := 2;
		-- Window size
		WINX   : natural := 2;
		WINY   : natural := 2;
		WINZ   : natural := 1;
		-- Step/stride values
		STEPX  : natural := 1;
		STEPY  : natural := 1;
		STEPZ  : natural := 1;
		-- Number of times the window is used in each dimension
		NWINX  : natural := 4;
		NWINY  : natural := 4;
		NWINZ  : natural := 2;
		-- Padding size at the beginning of each dimension
		PADX   : natural := 1;
		PADY   : natural := 1;
		-- The height on axis Y of the internal buffers, minimum is WINY, max is DIMY, 0 means auto
		BUFY   : natural := 0;
		-- Internal storage type, leave all to false for automatic decision
		USE_LUTRAM : boolean := false;
		USE_BRAM   : boolean := false;
		USE_URAM   : boolean := false;
		-- Number of times to repeat the window contents before going to the next window position
		REPEAT : natural := 1;
		-- Parallelism : number of cells to write at one time
		PAR_IN : natural := 1;
		-- Parallelism : number of cells to read at one time
		-- PAR_OUT / PAR_OZ must be 1, or a divisor of WINX, or a multiple of WINX and a divisor of WINX*WINY
		PAR_OUT : natural := 1;
		-- Parallelism : Output side on Z dimension
		-- This must be a divisor of PAR_OUT, and a multiple of PAR_IN
		PAR_OZ : natural := 1;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false;

		-- Number of images to simulate
		FRAMES : natural := 10;
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnlayer_window_bench;

architecture simu of nnlayer_window_bench is

	signal clk             : std_logic := '0';
	signal clk_next        : std_logic := '0';
	signal clear           : std_logic := '1';
	signal clk_want_stop   : boolean := false;

	-- Run-time frame dimensions
	signal user_fsize_x    : std_logic_vector(15 downto 0) := (others => '0');
	signal user_fsize_y    : std_logic_vector(15 downto 0) := (others => '0');
	signal user_fsize_z    : std_logic_vector(15 downto 0) := (others => '0');
	-- Run-time window step on each dimension
	signal user_step_x     : std_logic_vector(7 downto 0) := (others => '0');
	signal user_step_y     : std_logic_vector(7 downto 0) := (others => '0');
	signal user_step_z     : std_logic_vector(7 downto 0) := (others => '0');
	-- Run-time number of times the window is used in each dimension
	signal user_nwin_x     : std_logic_vector(15 downto 0) := (others => '0');
	signal user_nwin_y     : std_logic_vector(15 downto 0) := (others => '0');
	signal user_nwin_z     : std_logic_vector(15 downto 0) := (others => '0');
	-- Run-time padding size at the beginning of each dimension
	signal user_begpad_x   : std_logic_vector(7 downto 0);
	signal user_begpad_y   : std_logic_vector(7 downto 0);

	-- Data input
	signal data_in         : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal data_in_rdy     : std_logic := '0';
	signal data_in_ack     : std_logic := '0';

	-- Data output
	signal data_out        : std_logic_vector(PAR_OUT*WDATA-1 downto 0) := (others => '0');
	signal data_out_rdy    : std_logic := '0';
	signal data_out_room   : std_logic_vector(15 downto 0) := (others => '0');

	-- Testbench only, to debug erroneous output vectors in waveforms
	signal tb_out_error         : boolean := false;
	signal tb_out_count_all     : integer := 0;
	signal tb_out_count_inframe : integer := 0;

	function resize(vin : std_logic_vector; size : natural) return std_logic_vector is
	begin
		return std_logic_vector(resize(unsigned(vin), size));
	end function ;

	component nnlayer_window is
		generic (
			-- Data
			WDATA  : natural := 8;
			-- Dimensions: width, height, depth
			DIMX   : natural := 32;
			DIMY   : natural := 32;
			DIMZ   : natural := 3;
			-- Window size
			WINX   : natural := 3;
			WINY   : natural := 3;
			WINZ   : natural := 1;
			-- Step/stride values
			STEPX  : natural := 1;
			STEPY  : natural := 1;
			STEPZ  : natural := 1;
			-- Number of times the window is used in each dimension
			NWINX  : natural := 32;
			NWINY  : natural := 32;
			NWINZ  : natural := 3;
			-- Padding size at the beginning of each dimension
			PADX   : natural := 1;
			PADY   : natural := 1;
			-- The height on axis Y of the internal buffers, minimum is WINY, max is DIMY, 0 means auto
			BUFY   : natural := 0;
			-- Internal storage type, leave all to false for automatic decision
			USE_LUTRAM : boolean := false;
			USE_BRAM   : boolean := false;
			USE_URAM   : boolean := false;
			-- Number of times to repeat the window contents before going to the next window position
			REPEAT : natural := 1;
			-- Parallelism : number of cells to write at one time
			PAR_IN : natural := 1;
			-- Parallelism : number of cells to read at one time
			-- PAR_OUT / PAR_OZ must be 1, or a divisor of WINX, or a multiple of WINX and a divisor of WINX*WINY
			PAR_OUT : natural := 1;
			-- Parallelism : Output side on Z dimension
			-- This must be a divisor of PAR_OUT, and a multiple of PAR_IN
			PAR_OZ : natural := 1;
			-- Lock the layer parameters to the generic parameter value
			LOCKED : boolean := false
		);
		port (
			clk             : in  std_logic;
			clear           : in  std_logic;
			-- Run-time frame dimensions
			user_fsize_x    : in  std_logic_vector(15 downto 0);
			user_fsize_y    : in  std_logic_vector(15 downto 0);
			user_fsize_z    : in  std_logic_vector(15 downto 0);
			-- Run-time window step on each dimension
			user_step_x     : in  std_logic_vector(7 downto 0);
			user_step_y     : in  std_logic_vector(7 downto 0);
			user_step_z     : in  std_logic_vector(7 downto 0);
			-- Run-time number of times the window is used in each dimension
			user_nwin_x     : in  std_logic_vector(15 downto 0);
			user_nwin_y     : in  std_logic_vector(15 downto 0);
			user_nwin_z     : in  std_logic_vector(15 downto 0);
			-- Run-time padding size at the beginning of each dimension
			user_begpad_x   : in  std_logic_vector(7 downto 0);
			user_begpad_y   : in  std_logic_vector(7 downto 0);
			-- Data input
			data_in         : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
			data_in_rdy     : out std_logic;
			data_in_ack     : in  std_logic;
			-- Data output
			data_out        : out std_logic_vector(PAR_OUT*WDATA-1 downto 0);
			data_out_rdy    : out std_logic;
			data_out_room   : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	comp_i: nnlayer_window
		generic map (
			-- Data
			WDATA  => WDATA,
			-- Dimensions: width, height, depth
			DIMX   => DIMX,
			DIMY   => DIMY,
			DIMZ   => DIMZ,
			-- Window size
			WINX   => WINX,
			WINY   => WINY,
			WINZ   => WINZ,
			-- Step/stride values
			STEPX  => STEPX,
			STEPY  => STEPY,
			STEPZ  => STEPZ,
			-- Number of times the window is used in each dimension
			NWINX  => NWINX,
			NWINY  => NWINY,
			NWINZ  => NWINZ,
			-- Padding size at the beginning of each dimension
			PADX   => PADX,
			PADY   => PADY,
			-- The height on axis Y of the internal buffers, minimum is WINY, max is DIMY, 0 means auto
			BUFY   => BUFY,
			-- Internal storage type, leave all to false for automatic decision
			USE_LUTRAM => USE_LUTRAM,
			USE_BRAM   => USE_BRAM,
			USE_URAM   => USE_URAM,
			-- Number of times to repeat the window contents before going to the next window position
			REPEAT => REPEAT,
			-- Parallelism : number of cells to write at one time
			PAR_IN => PAR_IN,
			-- Parallelism : number of cells to read at one time
			-- PAR_OUT / PAR_OZ must be 1, or a divisor of WINX, or a multiple of WINX and a divisor of WINX*WINY
			PAR_OUT => PAR_OUT,
			-- Parallelism : Output side on Z dimension
			-- This must be a divisor of PAR_OUT, and a multiple of PAR_IN
			PAR_OZ => PAR_OZ,
			-- Lock the layer parameters to the generic parameter value
			LOCKED => LOCKED
		)
		port map (
			clk             => clk,
			clear           => clear,
			-- Run-time frame dimensions
			user_fsize_x    => user_fsize_x,
			user_fsize_y    => user_fsize_y,
			user_fsize_z    => user_fsize_z,
			-- Run-time window step on each dimension
			user_step_x     => user_step_x,
			user_step_y     => user_step_y,
			user_step_z     => user_step_z,
			-- Run-time number of times the window is used in each dimension
			user_nwin_x     => user_nwin_x,
			user_nwin_y     => user_nwin_y,
			user_nwin_z     => user_nwin_z,
			-- Run-time padding size at the beginning of each dimension
			user_begpad_x   => user_begpad_x,
			user_begpad_y   => user_begpad_y,
			-- Data input
			data_in         => data_in,
			data_in_rdy     => data_in_rdy,
			data_in_ack     => data_in_ack,
			-- Data output
			data_out        => data_out,
			data_out_rdy    => data_out_rdy,
			data_out_room   => data_out_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- Run-time frame dimensions
	user_fsize_x    <= std_logic_vector(to_unsigned(DIMX, user_fsize_x'length));
	user_fsize_y    <= std_logic_vector(to_unsigned(DIMY, user_fsize_y'length));
	user_fsize_z    <= std_logic_vector(to_unsigned(DIMZ, user_fsize_z'length));
	-- Run-time window step on each dimension
	user_step_x     <= std_logic_vector(to_unsigned(STEPX, user_step_x'length));
	user_step_y     <= std_logic_vector(to_unsigned(STEPY, user_step_y'length));
	user_step_z     <= std_logic_vector(to_unsigned(STEPZ, user_step_z'length));
	-- Run-time number of times the window is used in each dimension
	user_nwin_x     <= std_logic_vector(to_unsigned(NWINX, user_nwin_x'length));
	user_nwin_y     <= std_logic_vector(to_unsigned(NWINY, user_nwin_y'length));
	user_nwin_z     <= std_logic_vector(to_unsigned(NWINZ, user_nwin_z'length));
	-- Run-time padding size at the beginning of each dimension
	user_begpad_x   <= std_logic_vector(to_unsigned(PADX, user_begpad_x'length));
	user_begpad_y   <= std_logic_vector(to_unsigned(PADY, user_begpad_y'length));

	-- Data output
	data_out_room   <= std_logic_vector(to_unsigned(64, data_out_room'length));

	-- Process that generates stimuli
	process
		variable one_data_in : std_logic_vector(31 downto 0) := (others => '0');
		variable ndata_par : natural := 0;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write frame data

		ndata_par := 0;
		data_in <= (others => '0');

		for f in 1 to FRAMES loop
			report "# Sending frame " & to_string(f);

			for y in 0 to DIMY-1 loop
				for x in 0 to DIMX-1 loop
					for z in 0 to DIMZ*PAR_OZ-1 loop

						-- Generate a recognizable input vector
						one_data_in := (others => '0');
						one_data_in(31 downto 28) := x"F";
						one_data_in(27 downto 24) := std_logic_vector(to_unsigned(f, 4));
						one_data_in(23 downto 16) := std_logic_vector(to_unsigned(x, 8));
						one_data_in(15 downto  8) := std_logic_vector(to_unsigned(y, 8));
						one_data_in( 7 downto  0) := std_logic_vector(to_unsigned(z, 8));

						-- Enqueue in the input vector
						data_in((ndata_par+1)*32-1 downto ndata_par*32) <= one_data_in;

						ndata_par := ndata_par + 1;
						if ndata_par = PAR_IN then
							-- Send the acknowledge
							data_in_ack <= '1';
							wait until rising_edge(clk) and (data_in_rdy = '1');
							data_in_ack <= '0';
							-- Reset the counter of input values per input vector
							ndata_par := 0;
							data_in <= (others => '0');
						end if;

					end loop;  -- z
				end loop;  -- x
			end loop;  -- y

			-- Clear inputs just for clarity of waveforms
			data_in_ack <= '0';
			data_in <= (others => '0');

			-- Wait a bit before sending the next frame
			-- No wait for the first frame, then +1 cycle for each next frame
			if f > 1 then
				for i in 2 to f loop
					wait until rising_edge(clk);
				end loop;
			end if;

		end loop;  -- frame

		-- Waiting for end of simulation
		report "Input data is sent";
		wait;

	end process;

	-- Process that reads outputs
	process
		file data_outfile : text open write_mode is OUTPUT_FILE;
		variable out_line : line;
		variable get_out_vec : std_logic_vector(31 downto 0);
		variable want_out_vec : std_logic_vector(31 downto 0);
		variable vec_coordx : integer;
		variable vec_coordy : integer;
		variable vec_coordz : integer;
		variable vec_validx : boolean;
		variable vec_validy : boolean;
		variable vec_validz : boolean;
		variable ndata_par : natural := 0;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		tb_out_count_all <= 0;

		ndata_par := 0;

		for f in 1 to FRAMES loop
			report "# Receiving frame " & to_string(f);
			write(out_line, string'("# Beginning of frame ") & to_string(f));
			writeline(data_outfile, out_line);

			tb_out_count_inframe <= 0;

			for wy in 0 to NWINY-1 loop
				write(out_line, string'("# Beginning of row of windows Y=") & to_string(wy));
				writeline(data_outfile, out_line);

				for wx in 0 to NWINX-1 loop
					write(out_line, string'("# Beginning of window on X=") & to_string(wx));
					writeline(data_outfile, out_line);

					-- FIXME The desired functionality would be : REPEAT is done under NWINZ and not the opposite
					-- but currently the main application only supports WINZ=1 and setting NWINZ instead at run-time, so REPEAT was moved out of NWINZ
					for r in 0 to REPEAT-1 loop

						for wz in 0 to NWINZ-1 loop

							for z in 0 to WINZ-1 loop
								for y in 0 to WINY-1 loop
									for x in 0 to WINX-1 loop

										for pz in 0 to PAR_OZ-1 loop

											-- Wait for a valid output data
											if ndata_par = 0 then
												wait until rising_edge(clk) and (data_out_rdy = '1');
											end if;

											-- Generate the pixel coordinates
											vec_coordx := STEPX * wx - PADX + x;
											vec_coordy := STEPY * wy - PADY + y;
											vec_coordz := (STEPZ * wz + z) * PAR_OZ + pz;
											vec_validx := (vec_coordx >= 0) and (vec_coordx < DIMX);
											vec_validy := (vec_coordy >= 0) and (vec_coordy < DIMY);
											vec_validz := (vec_coordz >= 0) and (vec_coordz < DIMZ*PAR_OZ);

											-- Generate the expected output vector
											want_out_vec := (others => '0');
											if (vec_validx = true) and (vec_validy = true) and (vec_validz = true) then
												want_out_vec(31 downto 28) := x"F";
												want_out_vec(27 downto 24) := std_logic_vector(to_unsigned(f, 4));
												want_out_vec(23 downto 16) := std_logic_vector(to_unsigned(vec_coordx, 8));
												want_out_vec(15 downto  8) := std_logic_vector(to_unsigned(vec_coordy, 8));
												want_out_vec( 7 downto  0) := std_logic_vector(to_unsigned(vec_coordz, 8));
											end if;

											-- Select the right part of the output vector
											get_out_vec := data_out((ndata_par+1)*32-1 downto ndata_par*32);

											-- Emit an error message
											tb_out_error <= false;
											if get_out_vec /= want_out_vec then
												tb_out_error <= true;
												report "ERROR Got " & to_string(get_out_vec) & " expected " & to_string(want_out_vec) &
													" for f " & to_string(f) & " wy " & to_string(wy) & " wx " & to_string(wx) & " wz " & to_string(wz) & " x " & to_string(x) & " y " & to_string(y) & " z " & to_string(z) & " pz " & to_string(pz);
											end if;

											-- Write output data to file
											hwrite(out_line, get_out_vec);
											if get_out_vec /= want_out_vec then
												write(out_line, string'("   ERROR expected "));
												hwrite(out_line, want_out_vec);
											end if;
											writeline(data_outfile, out_line);

											-- Update counter signals to visualize in waveforms
											tb_out_count_all     <= tb_out_count_all + 1;
											tb_out_count_inframe <= tb_out_count_inframe + 1;

											-- Next output value from the output vector
											ndata_par := ndata_par + 1;
											if ndata_par = PAR_OUT then
												ndata_par := 0;
											end if;

										end loop;  -- partial Z-first of size PAR_IN

									end loop;  -- win x
								end loop;  -- win y
							end loop;  -- win z

						end loop;  -- repeat

					end loop;  -- nwin z
				end loop;  -- nwin x
			end loop;  -- nwin y

		end loop;  -- frame

		-- Write end message to the output file
		write(out_line, string'("# End of simulation"));
		writeline(data_outfile, out_line);

		-- End of simulation
		report "All output data is received";
		report "Stopping simulation";
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


