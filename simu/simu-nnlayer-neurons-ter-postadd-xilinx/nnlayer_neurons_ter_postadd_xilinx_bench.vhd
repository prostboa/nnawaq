
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity nnlayer_neurons_ter_postadd_xilinx_bench is
end nnlayer_neurons_ter_postadd_xilinx_bench;

architecture simu of nnlayer_neurons_ter_postadd_xilinx_bench is

	constant WDATA  : natural := 16;
	constant SDATA  : boolean := true;
	constant SFIXED : boolean := true;
	constant WACCU  : natural := 16;
	constant WOUT   : natural := 16;

	constant NPERBLK : natural := 18;
	constant WRNB    : natural := 2;

	constant FSIZE  : natural := 128;
	constant NBNEU  : natural := 128;

	constant PAR_IN  : natural := 4;
	constant PAR_OUT : natural := 2;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant USE_FSIZE : natural := 16;
	constant USE_NBNEU : natural := 16;

	signal input_vector : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal output_counter : natural := 0;

	-- Ports for Write Enable
	signal write_mode     : std_logic := '0';
	signal write_idx      : std_logic_vector(9 downto 0) := (others => '0');
	signal write_data     : std_logic_vector(2*NPERBLK*WRNB-1 downto 0) := (others => '0');
	signal write_enable   : std_logic := '0';
	signal write_end      : std_logic := '0';

	-- Run-time window step on each dimension
	signal user_fsize     : std_logic_vector(15 downto 0) := (others => '0');
	signal user_nbneu     : std_logic_vector(15 downto 0) := (others => '0');

	-- Data input
	signal data_in         : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal data_in_signed  : std_logic := '0';
	signal data_in_valid   : std_logic := '0';
	signal data_in_ready   : std_logic := '0';

	-- Data output
	signal data_out        : std_logic_vector(PAR_OUT*WACCU-1 downto 0) := (others => '0');
	signal data_out_valid  : std_logic := '0';

	signal end_of_frame    : std_logic := '0';

	signal out_fifo_room   : std_logic_vector(15 downto 0) := (others => '0');

	component nnlayer_neurons_ter_postadd_xilinx is
		generic (
			-- Parameters for the neurons
			WDATA  : natural := 2;
			SDATA  : boolean := true;
			SFIXED : boolean := true;
			WACCU  : natural := 12;
			WOUT   : natural := 12;
			-- Parameters for BRAM usage
			NPERBLK : natural := 18;
			WRNB    : natural := 2;
			-- Parameters for frame and number of neurons
			FSIZE  : natural := 1024;
			NBNEU  : natural := 1024;
			-- Parameters for input and output parallelism
			PAR_IN  : natural := 1;
			PAR_OUT : natural := 1;
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN : natural := 0
		);
		port (
			clk            : in  std_logic;
			clear          : in  std_logic;
			-- Ports for Write Enable
			write_mode     : in  std_logic;
			write_idx      : in  std_logic_vector(9 downto 0);
			write_data     : in  std_logic_vector(2*NPERBLK*WRNB-1 downto 0);
			write_enable   : in  std_logic;
			write_end      : out std_logic;
			-- The user-specified frame size and number of neurons
			user_fsize     : in  std_logic_vector(15 downto 0);
			user_nbneu     : in  std_logic_vector(15 downto 0);
			-- Data input, 2 bits
			data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
			data_in_signed : in  std_logic;
			data_in_valid  : in  std_logic;
			data_in_ready  : out std_logic;
			-- Scan chain to extract values
			data_out       : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
			data_out_valid : out std_logic;
			-- Indicate to the parent component that we are reaching the end of the current frame
			end_of_frame   : out std_logic;
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room  : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	comp_i: nnlayer_neurons_ter_postadd_xilinx
		generic map (
			-- Parameters for the neurons
			WDATA  => WDATA,
			SDATA  => SDATA,
			SFIXED => SFIXED,
			WACCU  => WACCU,
			WOUT   => WOUT,
			-- Parameters for BRAM usage
			NPERBLK => NPERBLK,
			WRNB    => WRNB,
			-- Parameters for frame and number of neurons
			FSIZE  => FSIZE,
			NBNEU  => NBNEU,
			-- Parameters for input and output parallelism
			PAR_IN  => PAR_IN,
			PAR_OUT => PAR_OUT,
			-- The output data enters a FIFO. This indicates the available room.
			FIFOMARGIN => 0
		)
		port map (
			clk            => clk,
			clear          => clear,
			-- Ports for Write Enable
			write_mode     => write_mode,
			write_idx      => write_idx,
			write_data     => write_data,
			write_enable   => write_enable,
			write_end      => write_end,
			-- The user-specified frame size and number of neurons
			user_fsize     => user_fsize,
			user_nbneu     => user_nbneu,
			-- Data input, 2 bits
			data_in        => data_in,
			data_in_signed => data_in_signed,
			data_in_valid  => data_in_valid,
			data_in_ready  => data_in_ready,
			-- Scan chain to extract values
			data_out       => data_out,
			data_out_valid => data_out_valid,
			-- Indicate to the parent component that we are reaching the end of the current frame
			end_of_frame   => end_of_frame,
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room  => out_fifo_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- The user-specified frame size and number of neurons
	user_fsize <= std_logic_vector(to_unsigned(USE_FSIZE / PAR_IN, user_fsize'length));
	user_nbneu <= std_logic_vector(to_unsigned(USE_NBNEU / PAR_OUT, user_nbneu'length));

	-- Data input
	data_in        <= input_vector;
	data_in_signed <= '1';

	-- Process that generates input vectors: data only
	process(clk)
	begin
		if rising_edge(clk) then

			if (data_in_valid = '1') and (data_in_ready = '1') then
				for i in 0 to PAR_IN-1 loop
					input_vector((i+1)*WDATA-1 downto i*WDATA) <= std_logic_vector(unsigned(input_vector((i+1)*WDATA-1 downto i*WDATA)) + PAR_IN);
				end loop;
			end if;

			if clear = '1' then
				for i in 0 to PAR_IN-1 loop
					input_vector((i+1)*WDATA-1 downto i*WDATA) <= std_logic_vector(to_unsigned(i, WDATA));
				end loop;
			end if;

		end if;
	end process;

	-- Process that drives output side
	process(clk, clk_want_stop)
		variable data_out_got  : std_logic_vector(WACCU-1 downto 0);
		variable data_out_want : natural;
		variable var_errors_nb : integer := 0;
		variable var_tests_nb  : integer := 0;
	begin
		if rising_edge(clk) then

			if data_out_valid = '1' then
				output_counter <= output_counter + 1;

				for i in 0 to PAR_OUT-1 loop
					data_out_got  := data_out((i+1)*WACCU-1 downto i*WACCU);
					data_out_want := (output_counter / (USE_NBNEU / PAR_OUT)) * USE_FSIZE*USE_FSIZE + USE_FSIZE*(USE_FSIZE-1)/2;
					if unsigned(data_out_got) /= data_out_want then
						report
							"ERROR Wrong output index " & natural'image(output_counter) & "." & natural'image(i) &
							" got " & integer'image(to_integer(unsigned(data_out_got))) &
							" expected " & integer'image(data_out_want);
						var_errors_nb := var_errors_nb + 1;
					end if;
					var_tests_nb := var_tests_nb + 1;
				end loop;

			end if;

			if clear = '1' then
				output_counter <= 0;
			end if;

		end if;

		if clk_want_stop = true then
			report "Results : " & natural'image(var_tests_nb) & " test vectors, " & natural'image(var_errors_nb) & " errors";
		end if;

	end process;

	-- Process that generates stimuli
	process
	begin

		-- Data output
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Ports for Write Enable
		write_mode   <= '0';
		write_idx    <= (others => '0');
		write_data   <= (others => '0');
		write_enable <= '0';

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write configuration: all 1

		for i in 0 to PAR_IN * PAR_OUT - 1 loop

			write_mode   <= '1';
			write_idx    <= std_logic_vector(to_unsigned(i, write_idx'length));

			wait until rising_edge(clk);
			wait until rising_edge(clk);
			wait until rising_edge(clk);
			wait until rising_edge(clk);

			write_data   <= X"555555555555555555";
			write_enable <= '1';

			for i in 0 to 512 / PAR_IN /PAR_OUT loop
				wait until rising_edge(clk);
			end loop;

		end loop;

		write_mode   <= '0';
		write_enable <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write frame data

		data_in_valid <= '1';

		for i in 0 to 128 loop
			wait until rising_edge(clk);

			-- Here: change the date of the interruption by the fifo out
			if output_counter = USE_NBNEU / PAR_OUT + 0 then
				out_fifo_room <= std_logic_vector(to_unsigned(0, out_fifo_room'length));
				for j in 0 to 30 loop
					wait until rising_edge(clk);
				end loop;
				out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));
			end if;

		end loop;

		data_in_valid <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Observe output
		-- Let some time to get all output values

		for i in 0 to 1000 loop
			wait until rising_edge(clk);
		end loop;

		-- End of simulation
		report "Simulation is ending" severity note;
		clk_want_stop <= true;
		wait;

	end process;

end architecture;


