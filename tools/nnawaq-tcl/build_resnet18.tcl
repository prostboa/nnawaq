#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It generates a quantized version of networks ResNet-18 and ResNet-34, with "original" identity mapping
# See paper : Deep Residual Learning for Image Recognition (2015)

# Architecture chosen  :
# See paper : Identity Mappings in Deep Residual Networks (2016)
#   Variant (a) on Figure 4 about the position of BN and ReLU layers
# See paper : Deep Residual Learning for Image Recognition (2015)
#   Variant (B) that uses 1x1 convolutions to perform identity mappings when image depth changes

# Input images : 224x224x3 8b
# Output : 1000 classes

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

if { ! [info exists freq]} {
	set freq 250
}
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

set resnet 18
if {[info exists env(RESNET)]} {
	set resnet $env(RESNET)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1
# Optional to reduce resources by having more PAR_IN and less physical neurons
set use_maxparin 1

nn_set f=224/224/3
nn_set fn=1
nn_set inpar=1

nn_set in=8s
nn_set acts=4u
nn_set weights=4s

nn_set neu_style=2

nn_set round_near=1

# Default settings for activations
nn_set norm_wbias=8
nn_set norm_wmul=16
nn_set norm_wshr=4
nn_set relu=0/15

nn_set no_fifo_win_pool=false
nn_set no_fifo_neu_relu=false
nn_set no_fifo_norm_relu=true

set last_bn [list ]

# Utility procedure to create one conv neuron layer
proc create_nn_conv_neu {win step pad neu nwin} {
	nn_layer_create window $win $step $pad $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu
}

# Utility procedure to create one conv MaxPool layer
proc create_nn_conv_maxpool {win step pad nwin} {
	nn_layer_create window $win $step $pad $nwin
	nn_layer_create maxpool
}

# Utility procedure to create one conv AvgPool layer
# Note : Hardcode an extra BUFY size to ensure the internal buffer is large enough for 2 frames
proc create_nn_conv_avgpool {win step pad nwin} {
	nn_layer_create window $win $step $pad $nwin bufy=14
	nn_layer_create avgpool
}

# This procedure creates 2 conv neurons layers, variant "plain" with no identity mapping
proc create_nn_conv_neu_plain {neu nwin} {
	# Check if the image is reduced at this stage
	set prev_neu neu=[nn_get fsize_z]
	set s 1
	if {"$prev_neu" != "$neu"} {
		set s 2
	}

	nn_layer_create window win=3 step=$s pad=1 $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu

	nn_layer_create window win=3 step=1 pad=1 $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu
}

# This procedure creates 2 conv neurons layers + identity mapping path
# Parameter idx : the index of this 2-layer block in a series of identical 3-layer block
proc create_nn_conv_neu_idmap {neu nwin idx} {
	global last_bn

	# To determine if need to add a 1x1 convolution on the identity mapping path, to adapt to a different number of neurons
	set prev_neu neu=[nn_get fsize_z]

	# Determine the stride of the first layer
	set s 1
	if {$idx == 0 && "$prev_neu" != "$neu"} {
		set s 2
	}

	# Clear any previous list of BNs that have to be over-quantized
	set last_bn [list ]

	nn_layer_create fork
	set fork_id [nn_get last]

	nn_layer_create window win=3 step=$s pad=1 $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu

	nn_layer_create window win=3 step=1 pad=1 $nwin
	nn_layer_create neuron $neu
	nn_layer_create norm
	set branch_id [nn_get last]

	lappend last_bn [nn_get last]

	# Note : the size of the window buffer is increased to cope with unbalanced branches
	nn_layer_create window win=1 step=1 pad=0 $nwin bufy=9 prev=$fork_id

	# This convolution neuron layer performs adaptation to a different image depth
	if {"$prev_neu" != "$neu"} {
		nn_layer_create neuron $neu
		nn_layer_create norm
		lappend last_bn [nn_get last]
	}

	nn_layer_create cat prev=$branch_id prev=[nn_get last]
	nn_layer_create add
	nn_layer_create relu
}

# Create the network:

# image 224x224x3

create_nn_conv_neu win=7 step=2 pad=0 neu=64 nwin=112
# image 112x112x64
create_nn_conv_maxpool win=3 step=2 pad=0 nwin=56
# image 56x56x64

set num1 2
set num2 2
set num3 2
set num4 2

if { $resnet == 34 } {
	set num1 3
	set num2 4
	set num3 6
	set num4 3
}

for {set i 0} {$i < $num1} {incr i} {
	create_nn_conv_neu_idmap neu=64 nwin=56 $i
}
# image 56x56x64

for {set i 0} {$i < $num2} {incr i} {
	create_nn_conv_neu_idmap neu=128 nwin=28 $i
}
# image 28x28x128

for {set i 0} {$i < $num3} {incr i} {
	create_nn_conv_neu_idmap neu=256 nwin=14 $i
}
# image 14x14x256

for {set i 0} {$i < $num4} {incr i} {
	create_nn_conv_neu_idmap neu=512 nwin=7 $i
}
# image 7x7x512

# Save the last ReLU layer because its activations are larger
set last_relu [nn_get last]

create_nn_conv_avgpool win=7 step=7 pad=0 nwin=1
# image 1x1x512

# Output neuron layer, fully connected
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1 step=1 pad=0
}
nn_layer_create neuron neu=1000
set neu_last [nn_get last]
# image 1x1x1000
nn_layer_create softmax
# image 1x1x1

# Hardcoded activation widths for select layers
foreach layer $last_bn { nn_layer_set $layer wout=8 }
nn_layer_set $last_relu relu=0/255
# Hardcoded weight widths for select layers
nn_layer_set neu0 weights=8s
nn_layer_set $neu_last weights=8s

# For information, print the network details
nn_print -cycles

# Get the bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
puts "Applying parallelism levels for PAR=$par"
nn_autopar $par
puts "Applying time multiplexing to fastest layers"
nn_autotmux

if { $use_maxparin != 0 } {
	puts "Applying more parallelism to reduce the number of physical neurons"
	nn_maxparin
}

# For information, print the network details
nn_print -cycles

# Get the new bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

# Distribution of memories in LUTRAM / BRAM to fit the board VC709
if { $use_maxparin != 0 } {
	nn_set lut_threshold=576
} elseif { $par <= 2 } {
	nn_set lut_threshold=2304
} elseif { $par == 4 } {
	nn_set lut_threshold=576
	nn_layer_set neu10 mem=lut
} elseif { $par == 8 } {
	nn_set lut_threshold=576
	nn_layer_set neu10 mem=lut
	nn_layer_set neu17 mem=lut
}

# Experimental : enable per-layer compression of weights
if {False} {
	#nn_layer_set neu1  comp=18/18/12  # lutram
	#nn_layer_set neu2  comp=18/18/12  # lutram
	#nn_layer_set neu3  comp=18/18/12  # lutram
	#nn_layer_set neu4  comp=18/18/12  # lutram
	#nn_layer_set neu5  comp=18/18/12  # lutram
	#nn_layer_set neu6  comp=18/18/12  # lutram
	#nn_layer_set neu7  comp=18/18/12  # lutram
	#nn_layer_set neu8  comp=18/18/12  # lutram
	#nn_layer_set neu9  comp=18/18/12  # lutram

	nn_layer_set neu10 comp=18/18/14
	nn_layer_set neu11 comp=18/18/14
	#nn_layer_set neu12 comp=18/18/12
	nn_layer_set neu13 comp=18/18/14
	nn_layer_set neu14 comp=18/18/14

	nn_layer_set neu15 comp=18/18/15
	nn_layer_set neu16 comp=18/18/15
	#nn_layer_set neu17 comp=18/18/15
	nn_layer_set neu18 comp=18/18/15
	nn_layer_set neu19 comp=18/18/13

	#nn_layer_set neu19 comp=18/18/12  # 250 bram18k
}

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# To help tweak per-layer memory implem
#nn_print_mem -type-neu
#nn_print_mem -type-win
foreach layer [list win neu norm] { nn_print_mem -type-$layer }
nn_print_mem -total

