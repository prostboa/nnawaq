
-- This custom multiplication component implements "multiplication" for inputs in log format
-- The result is standard linear representation that can be processed by adder tree and accumulator
-- The multiplication operation is common to all layers

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity neuron_custom_mul is
	generic(
		-- Parameters for data and signedness
		WDATA   : natural := 4;     -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 5;     -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WMUL    : natural := 8;     -- The multiplication bit width
		-- Identifiers of layer
		LAYER_ID : natural := 0;
		-- Identifier of neuron layer, in case a per-layer operation is desired
		CUSTOM_MUL_ID : natural := 0;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true
	);
	port(
		clk       : in  std_logic;
		-- Control signals
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
		-- Data output
		mul_out   : out std_logic_vector(WMUL-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end neuron_custom_mul;

architecture synth of neuron_custom_mul is

	signal add_res  : unsigned(4 downto 0) := (others => '0');

	signal conv_in  : std_logic_vector(5 downto 0) := (others => '0');
	signal conv_res : std_logic_vector(WMUL-1 downto 0) := (others => '0');

begin

	assert (WDATA = 4) and (SDATA = false) report "Component custom_mul is hardcoded for 4b unsigned input" severity failure;
	assert (WWEIGHT = 5) and (SWEIGHT = true) report "Component custom_mul is hardcoded for 5b signed input" severity failure;

	-- The multiplication, which is an addition in log representation
	-- 4b + 4b -> 5b (4 LUTs)

	add_res <= resize(unsigned('0' & data_in) + unsigned('0' & weight_in(WWEIGHT-2 downto 0)), WWEIGHT);

	-- Conversion of representation : log -> linear
	-- This is a 6-bit input ROM (8 LUTs)

	conv_in <= weight_in(WWEIGHT-1) & std_logic_vector(add_res);
	with conv_in select conv_res <=
		"01000000" when "000000",
		"00101101" when "000001",
		"00100000" when "000010",
		"00010111" when "000011",
		"00010000" when "000100",
		"00001011" when "000101",
		"00001000" when "000110",
		"00000110" when "000111",
		"00000100" when "001000",
		"00000011" when "001001",
		"00000010" when "001010",
		"00000001" when "001011",
		"00000001" when "001100",
		"00000001" when "001101",
		"00000001" when "001110",
		"00000000" when "001111",
		"00000000" when "010000",
		"00000000" when "010001",
		"00000000" when "010010",
		"00000000" when "010011",
		"00000000" when "010100",
		"00000000" when "010101",
		"00000000" when "010110",
		"00000000" when "010111",
		"00000000" when "011000",
		"00000000" when "011001",
		"00000000" when "011010",
		"00000000" when "011011",
		"00000000" when "011100",
		"00000000" when "011101",
		"00000000" when "011110",
		"00000000" when "011111",
		"11000000" when "100000",
		"11010011" when "100001",
		"11100000" when "100010",
		"11101001" when "100011",
		"11110000" when "100100",
		"11110101" when "100101",
		"11111000" when "100110",
		"11111010" when "100111",
		"11111100" when "101000",
		"11111101" when "101001",
		"11111110" when "101010",
		"11111111" when "101011",
		"11111111" when "101100",
		"11111111" when "101101",
		"11111111" when "101110",
		"00000000" when "101111",
		"00000000" when "110000",
		"00000000" when "110001",
		"00000000" when "110010",
		"00000000" when "110011",
		"00000000" when "110100",
		"00000000" when "110101",
		"00000000" when "110110",
		"00000000" when "110111",
		"00000000" when "111000",
		"00000000" when "111001",
		"00000000" when "111010",
		"00000000" when "111011",
		"00000000" when "111100",
		"00000000" when "111101",
		"00000000" when "111110",
		"00000000" when "111111",
		"--------" when others;

	-- Pipeline registers

	process(clk)
	begin
		if rising_edge(clk) then

			mul_out <= conv_res;

		end if;
	end process;

	-- Pipeline registers to propagate the tag in parallel

	gen_noag : if TAGEN = false generate

		tag_out <= (others => '0');

	end generate;

	gen_tag : if TAGEN = true generate

		process(clk)
		begin
			if rising_edge(clk) then
				tag_out <= tag_in;
			end if;
		end process;

	end generate;

end architecture;

