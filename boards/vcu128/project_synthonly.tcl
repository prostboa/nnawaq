
# Note: This is a non-project script
# For more information, please refer to the Xilinx document UG894
# "Vivado Design Suite User Guide: Using TCL Scripting", version 2014.1, page 11
# For details about the TCL commands, please refer to the Xilinx document UG835
# "Vivado Design Suite Tcl Command Reference Guide"

# Miscellaneous useful variables
set top nnawaq_axi3
set part xcvu37p-fsvh2892-2L-e
# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]

set hwoptdir ./hdl/hwopt
set nndir    ./hdl/nn

# Setup design sources

set sources_vhd [concat \
	[glob ./hdl/*.vhd] \
	[glob $hwoptdir/*.vhd] \
	[glob $nndir/*.vhd] \
]

foreach f $sources_vhd {
	read_vhdl -vhdl2008 $f
}

# No limit for message: Sequential element unused
set_msg_config -id {[Synth 8-3333]} -limit 1000000
# No limit for message: Constant propagation
set_msg_config -id {[Synth 8-3332]} -limit 1000000
# No limit for message: Unconnected port
set_msg_config -id {[Synth 8-3331]} -limit 1000000
# No limit for message: Tying undriven pin to constant 0
set_msg_config -id {[Synth 8-3295]} -limit 1000000
# Hide messages: Sub-optimal pipelining for BRAM
set_msg_config -id {[Synth 8-4480]} -suppress

# Run logic synthesis
synth_design -top $top -keep_equivalent_registers -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -flatten_hierarchy full -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -resource_sharing on -directive AreaOptimized_high

# Run logic optimization
opt_design
# Warning : This seems not useful with 2015.3, and crashes with 2017.2
#opt_design -directive ExploreArea

# Report timing and utilization estimates
report_utilization     -file $outputDir/post_synth_util.rpt
report_timing_summary  -file $outputDir/post_synth_timing_summary.rpt

# Write design checkpoint
write_checkpoint -force $outputDir/post_synth.dcp

