
-- This is a 3D memory block
-- Data is written one by one (or more), sequentially
-- Data is read one by one (or more), according to a 3D sliding window
--
-- Circular buffer implementation
-- There may be padding around the actual frame data
--
-- Geometry of the window:
--      ______________________________
--     /                             /|
--    /          (Padding)          / |
--   /    _____________________    /  |
--  /    /                    /|  /   |
-- /____/____________________/_|_/    |
-- |   /____________________/  | |    |
-- |   |                    |  | |    |
-- |   |  Part being read   | /| |    |
-- |   |____________________|/ | |    |
-- |   |                    |  | |    |
-- |   | Part being written | /| |    |
-- |   |____________________|/ | |    |
-- |   |                    |  | |    |
-- |   |                    | /  |    /
-- |   |____________________|/   |   /
-- |                             |  /
-- |           (Padding)         | /
-- |_____________________________|/
--
-- Axis:
--
--     / Z
--    /____ X
--    |
--    | Y
--
-- Filling order: Z, X, Y
-- Reading order: X, Y, Z
--
-- Specifications :
--   Internally, input data is stored in memories of data width PAR_OZ * WDATA
--   Internally, there are PAR_OUT/PAR_OZ duplicate memories that are read with different addresses
--   Even though PAR_IN is used an internal multiplier for the data width stored in memories
--   Even though the reading order is X->Y->Z, there is some Z-first behaviour if PAR_OZ > 1
--   DIMZ is the number of multiples of PAR_OZ input values to write a full Z dimension (DIMZ * PAR_OZ / PAR_IN clock cycles)
--   NWINZ is the number of multiples of PAR_OZ output values to read a full Z dimension
--   The generics WINZ/STEPZ correspond to a special feature, not needed for usual neural networks, leave these to 1 and use NWINZ = DIMZ
--
-- Requirements :
--   The size of the window is not larger than the image : WINX <= DIMX, WINY <= DIMY
--   The output parallelism (PAR_OUT) is a multiple of the output parallelism on Z dimension (PAR_OZ)
--   The output parallelism on Z dimension (PAR_OZ) is a multiple of the input parallelism (PAR_IN)
--   The output parallelism (divided by PAR_OZ) either divides WINX, or it is a multiple of WINX and it divides WINX*WINY

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_window is
	generic (
		-- Data
		WDATA  : natural := 8;
		-- Dimensions: width, height, depth
		DIMX   : natural := 32;
		DIMY   : natural := 32;
		DIMZ   : natural := 3;
		-- Window size
		WINX   : natural := 3;
		WINY   : natural := 3;
		WINZ   : natural := 1;
		-- Step/stride values
		STEPX  : natural := 1;
		STEPY  : natural := 1;
		STEPZ  : natural := 1;
		-- Number of times the window is used in each dimension
		NWINX  : natural := 32;
		NWINY  : natural := 32;
		NWINZ  : natural := 3;
		-- Padding size at the beginning of each dimension
		PADX   : natural := 1;
		PADY   : natural := 1;
		-- The height on axis Y of the internal buffers, minimum is WINY, max is DIMY, 0 means auto
		BUFY   : natural := 0;
		-- Internal storage type, leave all to false for automatic decision
		USE_LUTRAM : boolean := false;
		USE_BRAM   : boolean := false;
		USE_URAM   : boolean := false;
		-- Number of times to repeat the window contents before going to the next window position
		REPEAT : natural := 1;
		-- Parallelism : number of cells to write at one time
		PAR_IN : natural := 1;
		-- Parallelism : number of cells to read at one time
		-- PAR_OUT / PAR_OZ must be 1, or a divisor of WINX, or a multiple of WINX and a divisor of WINX*WINY
		PAR_OUT : natural := 1;
		-- Parallelism : Output size on Z dimension
		-- This must be a multiple of PAR_IN, and a divisor of PAR_OUT
		PAR_OZ : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port (
		clk             : in  std_logic;
		clear           : in  std_logic;
		-- Run-time frame dimensions
		user_fsize_x    : in  std_logic_vector(15 downto 0);
		user_fsize_y    : in  std_logic_vector(15 downto 0);
		user_fsize_z    : in  std_logic_vector(15 downto 0);
		-- Run-time window step on each dimension
		user_step_x     : in  std_logic_vector(7 downto 0);
		user_step_y     : in  std_logic_vector(7 downto 0);
		user_step_z     : in  std_logic_vector(7 downto 0);
		-- Run-time number of times the window is used in each dimension
		user_nwin_x     : in  std_logic_vector(15 downto 0);
		user_nwin_y     : in  std_logic_vector(15 downto 0);
		user_nwin_z     : in  std_logic_vector(15 downto 0);
		-- Run-time padding size at the beginning of each dimension
		user_begpad_x   : in  std_logic_vector(7 downto 0);
		user_begpad_y   : in  std_logic_vector(7 downto 0);
		-- Data input
		data_in         : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		data_in_rdy     : out std_logic;
		data_in_ack     : in  std_logic;
		-- Data output
		data_out        : out std_logic_vector(PAR_OUT*WDATA-1 downto 0);
		data_out_rdy    : out std_logic;
		data_out_room   : in  std_logic_vector(15 downto 0)
	);
end nnlayer_window;

architecture synth of nnlayer_window is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from std_logic
	function to_integer(b : std_logic) return integer is
		variable i : integer := 0;
	begin
		i := 1 when b = '1' else 0;
		return i;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- Perform preliminary verifications
	function func_preliminary_verif(phony : boolean) return boolean is
	begin

		assert (WINX > 0) and (WINY > 0) and (WINZ > 0) and (PAR_IN > 0) and (PAR_OUT > 0) and (PAR_OZ > 0)
			report "Error these parameters must be > 0 : WINX, WINY, WINZ, PAR_IN, PAR_OUT, PAR_OZ"
			severity failure;

		assert (PAR_OUT mod PAR_OZ) = 0
			report "Error need PAR_OUT to be a multiple of PAR_OZ"
			severity failure;

		assert (PAR_OZ mod PAR_IN) = 0
			report "Error need PAR_OZ to be a multiple of PAR_IN"
			severity failure;

		assert (to_integer(USE_LUTRAM) + to_integer(USE_BRAM) + to_integer(USE_URAM)) <= 1
			report "Error At most one of these parameters can be true : USE_LUTRAM, USE_BRAM"
			severity failure;

		return true;
	end function;

	-- This constant is only used to have the verification function actually launched before the rest of the VHDL code is elaborated
	constant PHONY_VERIF : boolean := func_preliminary_verif(true);

	-- Compute the parallelism factors to obtain from WINX and WINY
	-- These parallelism levels will be obtained with complete memory replication, so this is much more expensive than parallelism on PAR_OZ
	constant PAR_WIN : natural := PAR_OUT / PAR_OZ;

	-- A data type to hold configuration of window scan on X-Y
	type win_scan_xy_t is record
		sx : natural;  -- Size on X of what is scanned in one cycle
		nx : natural;  -- Number of scans on X
		sy : natural;  -- Size on Y of what is scanned in one cycle
		ny : natural;  -- Number of scans on Y
	end record;

	function calc_win_scan_xy(phony : boolean) return win_scan_xy_t is
		variable win_scan_xy : win_scan_xy_t := (0, 0, 0, 0);
	begin

		if PAR_WIN <= WINX then
			win_scan_xy.sx := PAR_WIN;
			win_scan_xy.sy := 1;
		else
			win_scan_xy.sx := WINX;
			win_scan_xy.sy := PAR_WIN / WINX;
		end if;

		win_scan_xy.nx := WINX / win_scan_xy.sx;
		win_scan_xy.ny := WINY / win_scan_xy.sy;

		report
			"PAR_WIN " & natural'image(PAR_WIN) &
			" sizeX " & natural'image(win_scan_xy.sx) &
			" nbX "   & natural'image(win_scan_xy.nx) &
			" sizeY " & natural'image(win_scan_xy.sy) &
			" nbY "   & natural'image(win_scan_xy.ny);

		assert win_scan_xy.sx * win_scan_xy.sy = PAR_WIN
			report "Error wrong PAR_WIN, incompatible with WINX"
			severity failure;

		assert win_scan_xy.nx * win_scan_xy.sx * win_scan_xy.ny * win_scan_xy.sy = WINX * WINY
			report "Error wrong PAR_WIN, does not divide WINX * WINY"
			severity failure;

		return win_scan_xy;
	end function;

	-- The number of scans that have to be done for a 2D slice X-Y of the window
	constant WIN_SCAN_XY : win_scan_xy_t := calc_win_scan_xy(false);

	-- Init function for the height of the internal buffer, in number of Y planes
	-- Apply a minimum size, but don't restrict the max size
	-- For max performance in the general case, the following rule is necessary : BUFY >= 2 * WINY
	-- With generally low performance impact, the following is enough : BUFY >= WINY + STEPY
	--   Explanation : Only when switching to next frame, and when we have no padding, that some cycles are lost
	--   We are dropping WINY rows, but only have STEPY < WINY in the buffer, so we may have to wait (a bit ?) for the buffer to have WINY data planes again
	function calc_bufy_internal(phony : boolean) return natural is
		variable v : natural := 0;
	begin
		v := BUFY;
		if v = 0 then
			v := WINY + STEPY;
		end if;
		if v < WINY then v := WINY; end if;
		return v;
	end function;

	-- The height of the internal buffer
	constant BUFY_INTERNAL : natural := calc_bufy_internal(false);

	-- Create PAR_WIN instances of a memory of data width PAR_OZ*WDATA
	-- They will contain exactly the same data, but read with a run-time configurable stride
	-- It's a circular buffer. Write is continuous, Read plays with indexes.
	constant MEM_CELLS : natural := DIMX * BUFY_INTERNAL * DIMZ;

	-- The min register size to contain the number of cells
	-- Need to represent addresses larger than the buffer in order to wrap around the circular buffer
	constant WADDR : natural := storebitsnb(MEM_CELLS) + 1;
	constant WCNT  : natural := storebitsnb(MEM_CELLS) + 1;

	-- Functions to calculate register sizes better fit for the LOCKED case

	function calc_reg_size(maxval : natural; maxsize : natural) return natural is
		variable v : natural := 0;
	begin
		v := maxsize;
		if LOCKED = true then
			v := storebitsnb(maxval);
		end if;
		if v > maxsize then v := maxsize; end if;
		return v;
	end function;

	constant WREG_DIMX : natural := calc_reg_size(DIMX, WCNT);
	constant WREG_DIMY : natural := calc_reg_size(DIMY, WCNT);
	constant WREG_DIMZ : natural := calc_reg_size(DIMZ, WCNT);

	constant WREG_DIMX1 : natural := calc_reg_size(DIMX-1, WCNT);
	constant WREG_DIMY1 : natural := calc_reg_size(DIMY-1, WCNT);
	constant WREG_DIMZ1 : natural := calc_reg_size(DIMZ-1, WCNT);

	constant WREG_WINX : natural := storebitsnb(WINX);
	constant WREG_WINY : natural := storebitsnb(WINY);
	constant WREG_WINZ : natural := storebitsnb(WINZ);

	constant WREG_WINX1 : natural := storebitsnb(WINX-1);
	constant WREG_WINY1 : natural := storebitsnb(WINY-1);
	constant WREG_WINZ1 : natural := storebitsnb(WINZ-1);

	constant WREG_NWINX1 : natural := calc_reg_size(NWINX-1, WCNT);
	constant WREG_NWINY1 : natural := calc_reg_size(NWINY-1, WCNT);
	constant WREG_NWINZ1 : natural := calc_reg_size(NWINZ-1, WCNT);

	constant WREG_DIMXZ : natural := calc_reg_size(DIMX * DIMZ, WCNT);

	constant WREG_BUFY : natural := calc_reg_size(BUFY_INTERNAL, WCNT);

	constant WREG_PADY : natural := calc_reg_size(maximum(PADY, WINY-1), 4);

	-- The size of the intermediate Write enable signal, this is the number of clock cycles necessary to fill the Write data vector
	constant NUM_WRITE_ENZ : natural := PAR_OZ / PAR_IN;

	-- Buffers for inputs
	signal buf_clear         : std_logic := '0';
	signal buf_user_fsize_x  : unsigned(15 downto 0) := (others => '0');
	signal buf_user_fsize_y  : unsigned(15 downto 0) := (others => '0');
	signal buf_user_fsize_z  : unsigned(15 downto 0) := (others => '0');
	signal buf_user_step_x   : unsigned( 7 downto 0) := (others => '0');
	signal buf_user_step_y   : unsigned( 7 downto 0) := (others => '0');
	signal buf_user_step_z   : unsigned( 7 downto 0) := (others => '0');
	signal buf_user_nwin_x   : unsigned(15 downto 0) := (others => '0');
	signal buf_user_nwin_y   : unsigned(15 downto 0) := (others => '0');
	signal buf_user_nwin_z   : unsigned(15 downto 0) := (others => '0');
	signal buf_user_begpad_x : unsigned( 7 downto 0) := (others => '0');
	signal buf_user_begpad_y : unsigned( 7 downto 0) := (others => '0');

	-- Signals for some pre-calculated values
	signal buf_fsize_xz : unsigned(WREG_DIMXZ-1 downto 0) := (others => '0');

	-- Write side
	signal write_cnt  : unsigned(WCNT-1 downto 0) := (others => '0');
	signal write_addr : unsigned(WADDR-1 downto 0) := (others => '0');
	signal write_data : std_logic_vector(PAR_OZ*WDATA-1 downto 0) := (others => '0');
	signal write_rdy  : std_logic := '0';
	signal write_en_z : std_logic_vector(NUM_WRITE_ENZ-1 downto 0) := (0 => '1', others => '0');
	signal write_en   : std_logic := '0';
	-- Write-side counter for one XZ plane
	signal write_cntxz : unsigned(WREG_DIMXZ-1 downto 0) := (others => '0');
	signal write_endxz : std_logic := '0';

	-- Read side
	signal read_cnty  : unsigned(WREG_BUFY-1 downto 0) := (others => '0');
	signal read_addr  : unsigned(WCNT-1 downto 0) := (others => '0');
	-- 1-bit tag per mem: at 1 to replace output by padding
	signal read_data  : std_logic_vector(PAR_OUT*WDATA-1 downto 0) := (others => '0');
	signal read_valid : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal read_otag  : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');

	signal read_data_buf  : std_logic_vector(PAR_OUT*WDATA-1 downto 0) := (others => '0');
	signal read_valid_buf : std_logic := '0';
	signal read_cando     : std_logic := '0';  -- Only at 1 in state DOING

	type read_status_type is (
		READ_RESET,
		READ_ROWWIN_NEXTY1,  -- Get forward, Y step
		READ_ROWWIN_BEG1,    -- Check Y padding and count rows
		READ_ROWWIN_BEG1_1,  -- Update read address with bufferized post-multiplication increment
		READ_ROWWIN_BEG2,    -- Wait until enough data is in memory
		READ_DOING           -- Read
	);
	signal read_status : read_status_type := READ_RESET;

	-- The REPEAT counter, to handle time multiplexing in next neuron layers
	constant REPEAT_USE : natural := REPEAT + to_integer(to_std_logic(REPEAT = 0));
	signal repeat_cnt : integer range 0 to REPEAT_USE-1 := 0;
	signal repeat_end : std_logic := to_std_logic(REPEAT <= 1);

	-- A signal generated by the Read side : a number of XZ planes
	-- To decrement the read counter and increment the Write counter
	-- FIXME If the frame is not fully read in Y direction, this cnty value can exceed BUFY, potentially exceeding the register size
	--   There is no specific protection against this, because this case would not be an efficient use of the HW anyway
	signal read_dec_cnty : unsigned(WREG_BUFY-1 downto 0) := (others => '0');
	signal read_dec_cnt  : unsigned(WCNT-1 downto 0) := (others => '0');

	-- Signals that indicate whether the current row of windows is within a padding area
	signal frame_inpad_beg_y : std_logic := '0';
	signal frame_inpad_end_y : std_logic := '0';
	signal frame_todo_pad_y : unsigned(WREG_PADY-1 downto 0) := (others => '0');
	-- The number of Y planes that remain to do in a frame (to keep track of padding on axis Y)
	signal frame_todo_y : unsigned(WREG_DIMY-1 downto 0) := (others => '0');
	-- The number of XZ planes needed in memory to begin scanning a row of windows
	signal read_needy : unsigned(WREG_WINY-1 downto 0) := (others => '0');

	-- For parallelization: index increments for X and Y
	signal par_incidx_x : unsigned(WIN_SCAN_XY.sx*WCNT-1 downto 0) := (others => '0');
	signal par_incidx_y : unsigned(WIN_SCAN_XY.sy*WCNT-1 downto 0) := (others => '0');

	-- The beginning of padding after the frame
	-- FIXME This functionality should be optimized like for the Y dimension
	signal coord_aftpad_x : unsigned(WREG_DIMY downto 0) := (others => '0');

	-- The current position of the sliding window
	signal slide_rowwin_idx_x : unsigned(WREG_NWINX1-1 downto 0) := (others => '0');
	signal slide_rowwin_idx_y : unsigned(WREG_NWINY1-1 downto 0) := (others => '0');
	signal slide_rowwin_idx_z : unsigned(WREG_NWINZ1-1 downto 0) := (others => '0');

	-- The current position within the sliding window
	signal slide_inwin_idx_x : unsigned(WREG_WINX1-1 downto 0) := (others => '0');
	signal slide_inwin_idx_y : unsigned(WREG_WINY1-1 downto 0) := (others => '0');
	signal slide_inwin_idx_z : unsigned(WREG_WINZ1-1 downto 0) := (others => '0');

	-- These signals indicate when we are processing the last position in a sliding movement
	signal slide_rowwin_end_x : std_logic := to_std_logic(LOCKED and (NWINX = 1));
	signal slide_rowwin_end_y : std_logic := to_std_logic(LOCKED and (NWINY = 1));
	signal slide_rowwin_end_z : std_logic := to_std_logic(LOCKED and (NWINZ = 1));

	signal slide_inwin_end_x : std_logic := to_std_logic(WIN_SCAN_XY.nx = 1);
	signal slide_inwin_end_y : std_logic := to_std_logic(WIN_SCAN_XY.ny = 1);
	signal slide_inwin_end_z : std_logic := to_std_logic(WINZ = 1);

	-- Registers for the different stages of address generation
	signal stage1_posx      : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage1_posy      : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage1_posz      : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage1_pady      : unsigned(WREG_PADY-1 downto 0) := (others => '0');
	signal stage1_pady_beg  : std_logic := '0';
	signal stage1_pady_end  : std_logic := '0';
	signal stage1_en        : std_logic := '0';

	signal stage2_Px_Fz     : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage2_Pz_addr   : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage2_Py_FxFz   : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage2_padding   : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal stage2_en        : std_logic := '0';

	signal stage3_addr      : unsigned(WCNT-1 downto 0) := (others => '0');
	signal stage3_padding   : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal stage3_en        : std_logic := '0';

	signal stage4_addr      : unsigned(WIN_SCAN_XY.sx*WCNT-1 downto 0) := (others => '0');
	signal stage4_padding   : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal stage4_en        : std_logic := '0';

	signal stage5_addr      : unsigned(WIN_SCAN_XY.sx*WIN_SCAN_XY.sy*WCNT-1 downto 0) := (others => '0');
	signal stage5_padding   : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal stage5_en        : std_logic := '0';

	signal stage6_addr      : unsigned(WIN_SCAN_XY.sx*WIN_SCAN_XY.sy*WADDR-1 downto 0) := (others => '0');
	signal stage6_padding   : std_logic_vector(PAR_WIN-1 downto 0) := (others => '0');
	signal stage6_en        : std_logic := '0';

	-- Component declaration: optimized memory component
	component largemem_block is
		generic (
			-- Parameters for memory
			WDATA   : natural := 8;
			CELLS   : natural := 1024;
			WADDR   : natural := 10;
			-- Optimization objective
			OPT_SPEED : boolean := false;
			-- A tag that may be given for each read
			WTAG    : natural := 1;
			-- Enable the DO register of the BRAM
			REG_DO  : boolean := false;
			-- Enable extra register when there are multiple BRAMs in height
			REG_MUX : boolean := false
		);
		port (
			clk        : in  std_logic;
			clear      : in  std_logic;
			-- Write port
			write_addr : in  std_logic_vector(WADDR-1 downto 0);
			write_en   : in  std_logic;
			write_data : in  std_logic_vector(WDATA-1 downto 0);
			-- Read port, input
			read_addr  : in  std_logic_vector(WADDR-1 downto 0);
			read_en    : in  std_logic;
			read_itag  : in  std_logic_vector(WTAG-1 downto 0);
			-- Read port, output
			read_data  : out std_logic_vector(WDATA-1 downto 0);
			read_valid : out std_logic;
			read_otag  : out std_logic_vector(WTAG-1 downto 0)
		);
	end component;

	-- Component to ease instantiation of URAM to build a potentially much larger memory
	component largemem_huge is
		generic (
			-- Parameters for memory
			WDATA   : natural := 8;
			CELLS   : natural := 1024;
			WADDR   : natural := 10;
			-- Optimization objective
			OPT_SPEED : boolean := false;
			-- A tag that may be given for each read
			WTAG    : natural := 1;
			-- Enable extra register after the memory blocks, to improve timings
			REG_DO  : boolean := false;
			-- Enable extra register when there are multiple blocks in height
			REG_MUX : boolean := false
		);
		port (
			clk        : in  std_logic;
			clear      : in  std_logic;
			-- Write port
			write_addr : in  std_logic_vector(WADDR-1 downto 0);
			write_en   : in  std_logic;
			write_data : in  std_logic_vector(WDATA-1 downto 0);
			-- Read port, input
			read_addr  : in  std_logic_vector(WADDR-1 downto 0);
			read_en    : in  std_logic;
			read_itag  : in  std_logic_vector(WTAG-1 downto 0);
			-- Read port, output
			read_data  : out std_logic_vector(WDATA-1 downto 0);
			read_valid : out std_logic;
			read_otag  : out std_logic_vector(WTAG-1 downto 0)
		);
	end component;

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		process(clk)
		begin
			if rising_edge(clk) then

				buf_user_fsize_x  <= unsigned(user_fsize_x);
				buf_user_fsize_y  <= unsigned(user_fsize_y);
				buf_user_fsize_z  <= unsigned(user_fsize_z);
				buf_user_step_x   <= unsigned(user_step_x);
				buf_user_step_y   <= unsigned(user_step_y);
				buf_user_step_z   <= unsigned(user_step_z);
				buf_user_nwin_x   <= unsigned(user_nwin_x);
				buf_user_nwin_y   <= unsigned(user_nwin_y);
				buf_user_nwin_z   <= unsigned(user_nwin_z);
				buf_user_begpad_x <= unsigned(user_begpad_x);
				buf_user_begpad_y <= unsigned(user_begpad_y);

				-- Precompute some values
				buf_fsize_xz   <= resize(buf_user_fsize_x * buf_user_fsize_z, buf_fsize_xz'length);
				coord_aftpad_x <= resize(buf_user_begpad_x + buf_user_fsize_x, coord_aftpad_x'length);

				-- Precompute index increments for parallelization
				par_incidx_x(WCNT-1 downto 0) <= to_unsigned(0, WCNT);
				if WIN_SCAN_XY.sx > 1 then
					for i in 1 to WIN_SCAN_XY.sx-1 loop
						par_incidx_x((i+1)*WCNT-1 downto i*WCNT) <=
							resize(par_incidx_x(i*WCNT-1 downto (i-1)*WCNT), WCNT) when i = 1 else
							resize(par_incidx_x(i*WCNT-1 downto (i-1)*WCNT) + buf_user_fsize_z, WCNT);
					end loop;
				end if;

				par_incidx_y(WCNT-1 downto 0) <= to_unsigned(0, WCNT);
				if WIN_SCAN_XY.sy > 1 then
					for i in 1 to WIN_SCAN_XY.sy-1 loop
						par_incidx_y((i+1)*WCNT-1 downto i*WCNT) <=
							resize(par_incidx_y(i*WCNT-1 downto (i-1)*WCNT), WCNT) when WIN_SCAN_XY.sy = 1 else
							resize(par_incidx_y(i*WCNT-1 downto (i-1)*WCNT) + buf_fsize_xz, WCNT);
					end loop;
				end if;

			end if;  -- Rising edge of clock
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_user_fsize_x  <= to_unsigned(DIMX, 16);
		buf_user_fsize_y  <= to_unsigned(DIMY, 16);
		buf_user_fsize_z  <= to_unsigned(DIMZ, 16);
		buf_user_step_x   <= to_unsigned(STEPX, 8);
		buf_user_step_y   <= to_unsigned(STEPY, 8);
		buf_user_step_z   <= to_unsigned(STEPZ, 8);
		buf_user_nwin_x   <= to_unsigned(NWINX, 16);
		buf_user_nwin_y   <= to_unsigned(NWINY, 16);
		buf_user_nwin_z   <= to_unsigned(NWINZ, 16);
		buf_user_begpad_x <= to_unsigned(PADX, 8);
		buf_user_begpad_y <= to_unsigned(PADY, 8);

		-- Precompute some values
		buf_fsize_xz   <= to_unsigned(DIMX * DIMZ, buf_fsize_xz'length);
		coord_aftpad_x <= to_unsigned(PADX + DIMX, coord_aftpad_x'length);

		-- Precompute index increments for parallelization
		gen_incx : for i in 0 to WIN_SCAN_XY.sx-1 generate
			par_incidx_x((i+1)*WCNT-1 downto i*WCNT) <= to_unsigned(i * DIMZ, WCNT);
		end generate;

		gen_incy : for i in 0 to WIN_SCAN_XY.sy-1 generate
			par_incidx_y((i+1)*WCNT-1 downto i*WCNT) <= to_unsigned(i * DIMX * DIMZ, WCNT);
		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Sequential process, write side
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- Reminder: Input data is bufferized before being written into memory
			-- This needs to be taken into account to accept new inputs
			write_rdy <= '0';
			if write_cnt > to_integer(write_en) + to_integer(write_rdy and data_in_ack) then
				write_rdy <= '1';
			end if;

			-- Insert the next input data in the write buffer
			write_en <= write_rdy and data_in_ack and write_en_z(NUM_WRITE_ENZ-1);
			for i in 0 to NUM_WRITE_ENZ-1 loop
				if write_en_z(i) = '1' then
					write_data((i+1)*PAR_IN*WDATA-1 downto i*PAR_IN*WDATA) <= data_in;
				end if;
			end loop;
			-- Rotate the write enable vector
			if NUM_WRITE_ENZ > 1 then
				if (write_rdy and data_in_ack) = '1' then
					write_en_z <= write_en_z(NUM_WRITE_ENZ-2 downto 0) & write_en_z(NUM_WRITE_ENZ-1);
				end if;
			end if;

			-- Update the write-side counter of available cells
			assert to_integer(write_cnt) + to_integer(read_dec_cnt) - to_integer(write_en) >= 0 report "Write counter underflows" severity failure;
			assert to_integer(write_cnt) + to_integer(read_dec_cnt) - to_integer(write_en) <= MEM_CELLS report "Write counter overflows" severity failure;
			write_cnt <= write_cnt + read_dec_cnt - to_integer(write_en);

			-- Update the write-side counter of remaining cells to fill one XZ plane
			write_cntxz <= write_cntxz - to_integer(write_en);
			if write_cntxz = 2 then
				write_endxz <= write_en;
			end if;
			if (write_en = '1') and (write_endxz = '1') then
				write_cntxz <= buf_fsize_xz;
				write_endxz <= '0';
			end if;

			-- Update write address
			if write_en = '1' then
				write_addr <= write_addr + 1;
				if write_addr = MEM_CELLS-1 then
					write_addr <= (others => '0');
				end if;
			end if;

			-- Handle reset
			if buf_clear = '1' then
				write_cnt  <= to_unsigned(MEM_CELLS, WCNT);
				write_addr <= to_unsigned(0, WADDR);
				write_rdy  <= '0';
				write_en   <= '0';
				write_en_z <= (others => '0');
				write_en_z(0) <= '1';
				write_cntxz <= buf_fsize_xz;
				write_endxz <= '0';
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Sequential process, read side
	-------------------------------------------------------------------

	process(clk)
		variable var_nat : natural;
	begin
		if rising_edge(clk) then

			-- Default signal values
			read_cando    <= '0';

			-- Register to increment/decrement the Write/Read side address registers
			read_dec_cnty <= (others => '0');
			read_dec_cnt  <= (others => '0');

			-- Bufferize user inputs to ease routing
			buf_clear <= clear;

			-- Update the read-side counter of available XZ planes
			assert to_integer(read_cnty) - to_integer(read_dec_cnty) + to_integer(write_en and write_endxz) >= 0 report "Read cnty counter underflows" severity failure;
			read_cnty <= read_cnty - read_dec_cnty + to_integer(write_en and write_endxz);

			-- The address calculation stages

			-- Read stages: only run if there is enough room in the output FIFO
			if read_cando = '1' then

				-- Repeat counter, increment at end of processing of one window position
				-- This counter will be optimized out by synthesis tools when not needed
				-- FIXME The desired functionality would be : no dependency on slide_rowwin_end_z, but currently the main application only supports WINZ=1 and setting NWINZ instead at run-time
				if
					(REPEAT_USE >= 1) and
					(slide_rowwin_end_z = '1') and
					(slide_inwin_end_x = '1') and (slide_inwin_end_y = '1') and (slide_inwin_end_z = '1')
				then
					-- Increment counter
					if (repeat_end = '1') or (REPEAT_USE = 1) then
						repeat_cnt <= 0;
					else
						repeat_cnt <= repeat_cnt + 1;
					end if;
					-- Process end indicator
					repeat_end <= '0';
					if REPEAT_USE = 1 then repeat_end <= '1';
					elsif repeat_cnt = REPEAT_USE - 2 then repeat_end <= '1';
					end if;
				end if;

				-- Window position, X
				if
					(repeat_end = '1') and
					(slide_rowwin_end_z = '1') and
					(slide_inwin_end_x = '1') and (slide_inwin_end_y = '1') and (slide_inwin_end_z = '1')
				then
					-- Process index
					slide_rowwin_idx_x <= slide_rowwin_idx_x + 1;
					if slide_rowwin_end_x = '1' then
						slide_rowwin_idx_x <= to_unsigned(0, slide_rowwin_idx_x'length);
					end if;
					-- Process end indicator
					slide_rowwin_end_x <= '0';
					if buf_user_nwin_x = 1 then slide_rowwin_end_x <= '1';
					elsif slide_rowwin_idx_x = buf_user_nwin_x - 2 then slide_rowwin_end_x <= '1';
					end if;
				end if;

				-- Window position, Z
				-- FIXME The desired functionality would be : add dependency on repeat_end, but currently the main application only supports WINZ=1 and setting NWINZ instead at run-time
				if
					(slide_inwin_end_x = '1') and (slide_inwin_end_y = '1') and (slide_inwin_end_z = '1')
				then
					-- Process index
					slide_rowwin_idx_z <= slide_rowwin_idx_z + 1;
					if slide_rowwin_end_z = '1' then
						slide_rowwin_idx_z <= to_unsigned(0, slide_rowwin_idx_z'length);
					end if;
					-- Process end indicator
					slide_rowwin_end_z <= '0';
					if buf_user_nwin_z = 1 then slide_rowwin_end_z <= '1';
					elsif slide_rowwin_idx_z = buf_user_nwin_z - 2 then slide_rowwin_end_z <= '1';
					end if;
				end if;

				-- Inside window, X
				if true then
					-- Process index
					slide_inwin_idx_x <= slide_inwin_idx_x + 1;
					if slide_inwin_end_x = '1' then
						slide_inwin_idx_x <= to_unsigned(0, slide_inwin_idx_x'length);
					end if;
					-- Process end indicator
					slide_inwin_end_x <= '0';
					if WIN_SCAN_XY.nx = 1 then slide_inwin_end_x <= '1';
					elsif slide_inwin_idx_x = WIN_SCAN_XY.nx - 2 then slide_inwin_end_x <= '1';
					end if;
				end if;

				-- Inside window, Y
				if slide_inwin_end_x = '1' then
					-- Process index
					slide_inwin_idx_y <= slide_inwin_idx_y + 1;
					if slide_inwin_end_y = '1' then
						slide_inwin_idx_y <= to_unsigned(0, slide_inwin_idx_y'length);
					end if;
					-- Process end indicator
					slide_inwin_end_y <= '0';
					if WIN_SCAN_XY.ny = 1 then slide_inwin_end_y <= '1';
					elsif slide_inwin_idx_y = WIN_SCAN_XY.ny - 2 then slide_inwin_end_y <= '1';
					end if;
				end if;

				-- Inside window, Z
				if (slide_inwin_end_x = '1') and (slide_inwin_end_y = '1') then
					-- Process index
					slide_inwin_idx_z <= slide_inwin_idx_z + 1;
					if slide_inwin_end_z = '1' then
						slide_inwin_idx_z <= to_unsigned(0, slide_inwin_idx_z'length);
					end if;
					-- Process end indicator
					slide_inwin_end_z <= '0';
					if WINZ = 1 then slide_inwin_end_z <= '1';
					elsif slide_inwin_idx_z = WINZ - 2 then slide_inwin_end_z <= '1';
					end if;
				end if;

			end if;  -- read_cando = '1'

			-- 1st stage: Calculate position in frame
			stage1_posx   <= resize(slide_rowwin_idx_x * buf_user_step_x + slide_inwin_idx_x * WIN_SCAN_XY.sx, WCNT);
			stage1_posy   <= resize(slide_inwin_idx_y * WIN_SCAN_XY.sy, WCNT);
			stage1_posz   <= resize(slide_rowwin_idx_z * buf_user_step_z + slide_inwin_idx_z, WCNT);
			stage1_pady   <= frame_todo_pad_y;
			stage1_pady_beg <= frame_inpad_beg_y;
			stage1_pady_end <= frame_inpad_end_y;
			stage1_en     <= read_cando;

			-- 2nd stage: Calculate partial address + padding
			stage2_Px_Fz   <= resize((stage1_posx - buf_user_begpad_x) * buf_user_fsize_z, WCNT);
			stage2_Py_FxFz <= resize((stage1_posy - (stage1_pady and stage1_pady_beg)) * buf_fsize_xz, WCNT);
			stage2_Pz_addr <= stage1_posz + read_addr;
			stage2_padding <= (others => '0');
			stage2_en      <= stage1_en;

			-- FIXME Optimize like the Y dimension, remove usage of coord_aftpad_x, use shorter padding size counter
			if (LOCKED = false) or (PADX > 0) or (WINX + (NWINX-1)*STEPX > DIMX) then
				for x in 0 to WIN_SCAN_XY.sx-1 loop
					if (stage1_posx + x >= coord_aftpad_x) or (stage1_posx + x < buf_user_begpad_x) then
						for y in 0 to WIN_SCAN_XY.sy-1 loop
							stage2_padding(y * WIN_SCAN_XY.sx + x) <= '1';
						end loop;
					end if;
				end loop;
			end if;

			if (LOCKED = false) or (PADY > 0) or (WINY + (NWINY-1)*STEPY > DIMY) then
				for y in 0 to WIN_SCAN_XY.sy-1 loop
					if
						((stage1_pady_beg = '1') and (stage1_posy + y < stage1_pady)) or
						((stage1_pady_end = '1') and (stage1_posy + y >= stage1_pady))
					then
						for x in 0 to WIN_SCAN_XY.sx-1 loop
							stage2_padding(y * WIN_SCAN_XY.sx + x) <= '1';
						end loop;
					end if;
				end loop;
			end if;

			if (LOCKED = false) or (WINZ + (NWINZ-1)*STEPZ > DIMZ) then
				if stage1_posz >= buf_user_fsize_z then
					stage2_padding <= (others => '1');
				end if;
			end if;

			-- 3rd stage: Calculate the final reference address
			stage3_addr      <= stage2_Px_Fz + stage2_Py_FxFz + stage2_Pz_addr;
			stage3_padding   <= stage2_padding;
			stage3_en        <= stage2_en;

			-- 4th stage: handle parallelization on the X dimension
			stage4_addr      <= (others => '0');
			stage4_padding   <= stage3_padding;
			stage4_en        <= stage3_en;

			stage4_addr(WCNT-1 downto 0) <= stage3_addr;
			if WIN_SCAN_XY.sx > 1 then
				for x in 1 to WIN_SCAN_XY.sx-1 loop
					stage4_addr((x+1)*WCNT-1 downto x*WCNT) <= stage3_addr + par_incidx_x((x+1)*WCNT-1 downto x*WCNT);
				end loop;
			end if;

			-- 5th stage: handle parallelization on the Y dimension
			stage5_addr      <= (others => '0');
			stage5_padding   <= stage4_padding;
			stage5_en        <= stage4_en;

			stage5_addr(WIN_SCAN_XY.sx*WCNT-1 downto 0) <= stage4_addr;
			if WIN_SCAN_XY.sy > 1 then
				for y in 1 to WIN_SCAN_XY.sy-1 loop
					for x in 0 to WIN_SCAN_XY.sx-1 loop
						var_nat := y * WIN_SCAN_XY.sx + x;
						stage5_addr((var_nat+1)*WCNT-1 downto var_nat*WCNT) <=
							stage4_addr((x+1)*WCNT-1 downto x*WCNT) + par_incidx_y((y+1)*WCNT-1 downto y*WCNT);
					end loop;
				end loop;
			end if;

			-- 6th stage: Wrap addresses according to the circular buffer size
			stage6_addr      <= (others => '0');
			stage6_padding   <= stage5_padding;
			stage6_en        <= stage5_en;

			for i in 0 to PAR_WIN-1 loop
				stage6_addr((i+1)*WADDR-1 downto i*WADDR) <= resize(stage5_addr((i+1)*WCNT-1 downto i*WCNT), WADDR);
				if stage5_addr((i+1)*WCNT-1 downto i*WCNT) >= MEM_CELLS then
					stage6_addr((i+1)*WADDR-1 downto i*WADDR) <= resize(stage5_addr((i+1)*WCNT-1 downto i*WCNT) - MEM_CELLS, WADDR);
				end if;
			end loop;

			-- Get output from memories
			-- Handle padding when out of frame boundaries
			read_data_buf  <= read_data;
			read_valid_buf <= read_valid(0);
			for i in 0 to PAR_WIN-1 loop
				if read_otag(i) = '1' then
					read_data_buf((i+1)*PAR_OZ*WDATA-1 downto i*PAR_OZ*WDATA) <= (others => '0');
				end if;
			end loop;

			-- The states

			if read_status = READ_RESET then

				read_cnty <= to_unsigned(0, read_cnty'length);
				read_addr <= to_unsigned(0, read_addr'length);

				stage1_en <= '0';
				stage2_en <= '0';
				stage3_en <= '0';
				stage4_en <= '0';
				stage5_en <= '0';
				stage6_en <= '0';

				read_valid_buf <= '0';

				-- Initialize position counters
				repeat_cnt <= 0;

				slide_rowwin_idx_x <= to_unsigned(0, slide_rowwin_idx_x'length);
				slide_rowwin_idx_y <= to_unsigned(0, slide_rowwin_idx_y'length);
				slide_rowwin_idx_z <= to_unsigned(0, slide_rowwin_idx_z'length);

				slide_inwin_idx_x <= to_unsigned(0, slide_inwin_idx_x'length);
				slide_inwin_idx_y <= to_unsigned(0, slide_inwin_idx_y'length);
				slide_inwin_idx_z <= to_unsigned(0, slide_inwin_idx_z'length);

				-- Initialize end indicators
				repeat_end <= to_std_logic(REPEAT <= 1);

				if LOCKED = false then
					slide_rowwin_end_x <= '0';
					if buf_user_nwin_x = 1 then slide_rowwin_end_x <= '1'; end if;
					slide_rowwin_end_y <= '0';
					if buf_user_nwin_y = 1 then slide_rowwin_end_y <= '1'; end if;
					slide_rowwin_end_z <= '0';
					if buf_user_nwin_z = 1 then slide_rowwin_end_z <= '1'; end if;
				-- LOCKED = true
				else
					slide_rowwin_end_x <= to_std_logic(NWINX = 1);
					slide_rowwin_end_y <= to_std_logic(NWINY = 1);
					slide_rowwin_end_z <= to_std_logic(NWINZ = 1);
				end if;

				slide_inwin_end_x <= to_std_logic(WIN_SCAN_XY.nx = 1);
				slide_inwin_end_y <= to_std_logic(WIN_SCAN_XY.ny = 1);
				slide_inwin_end_z <= to_std_logic(WINZ = 1);

				-- The number of frame Y planes to do, for an entire image
				frame_todo_y <= resize(buf_user_fsize_y, frame_todo_y'length);
				frame_inpad_beg_y <= '1' when buf_user_begpad_y > 0 else '0';
				frame_inpad_end_y <= '0';
				frame_todo_pad_y <= resize(buf_user_begpad_y, frame_todo_pad_y'length);

				read_status <= READ_ROWWIN_BEG1;

			elsif read_status = READ_ROWWIN_BEG1 then

				-- This state also does one-cycle delay to update the counter, when coming from _NEXTY state

				-- Compute the needed frame size to be present before beginning reading
				if frame_inpad_beg_y = '1' then
					read_needy <= (others => '0');
					if frame_todo_pad_y <= WINY then
						--read_needy <= resize(WINY - frame_todo_pad_y, read_needy'length);  -- Note : With this code, the GHDL simulation emits a (false?) truncation warning
						read_needy <= resize(to_unsigned(WINY, read_needy'length) - frame_todo_pad_y, read_needy'length);
					end if;
				elsif frame_inpad_end_y = '1' then
					read_needy <= resize(frame_todo_pad_y, read_needy'length);
				else
					read_needy <= to_unsigned(WINY, read_needy'length);
				end if;

				-- Note : This register is only necessary to reduce the datapath length on Write side
				read_dec_cnt <= resize(read_dec_cnty * buf_fsize_xz, read_dec_cnt'length);

				read_status <= READ_ROWWIN_BEG1_1;

			elsif read_status = READ_ROWWIN_BEG1_1 then

				-- Update read address using previously calculated counter decrement
				read_addr <= read_addr + read_dec_cnt;

				read_status <= READ_ROWWIN_BEG2;

			elsif read_status = READ_ROWWIN_BEG2 then

				-- Initialize indexes of the frame range to scan
				slide_rowwin_idx_x <= to_unsigned(0, slide_rowwin_idx_x'length);
				slide_rowwin_idx_z <= to_unsigned(0, slide_rowwin_idx_z'length);

				slide_inwin_idx_x <= to_unsigned(0, slide_inwin_idx_x'length);
				slide_inwin_idx_y <= to_unsigned(0, slide_inwin_idx_y'length);
				slide_inwin_idx_z <= to_unsigned(0, slide_inwin_idx_z'length);

				-- Update read address
				assert read_addr < MEM_CELLS * 2 report "Read address overflows" severity failure;
				if read_addr >= MEM_CELLS then
					read_addr <= read_addr - MEM_CELLS;
				end if;

				-- Wait until we have enough data in memory
				if read_cnty >= read_needy then
					read_status <= READ_DOING;
				end if;

			elsif read_status = READ_DOING then

				-- Wait for availability of enough data
				if unsigned(data_out_room) >= 16 + FIFOMARGIN then
					read_cando <= '1';
				end if;

				-- Detect end of processing of the XZ plane of windows
				if
					(read_cando = '1') and
					(repeat_end = '1') and
					(slide_rowwin_end_x = '1') and (slide_rowwin_end_z = '1') and
					(slide_inwin_end_x = '1') and (slide_inwin_end_y = '1') and (slide_inwin_end_z = '1')
				then
					read_cando <= '0';
					read_status <= READ_ROWWIN_NEXTY1;
				end if;

			elsif read_status = READ_ROWWIN_NEXTY1 then

				frame_inpad_beg_y <= '0';
				frame_inpad_end_y <= '0';

				-- Go forward: the Y step

				-- Handle when inside beg padding
				if frame_inpad_beg_y = '1' then
					frame_inpad_beg_y <= '1';
					frame_todo_pad_y <= resize(frame_todo_pad_y - buf_user_step_y, frame_todo_pad_y'length);
					frame_todo_y <= resize(buf_user_fsize_y, frame_todo_y'length);
					if frame_todo_pad_y <= buf_user_step_y then
						frame_inpad_beg_y <= '0';
						frame_todo_pad_y <= (others => '0');
						frame_todo_y <= resize(buf_user_fsize_y - (buf_user_step_y - frame_todo_pad_y), frame_todo_y'length);
						read_dec_cnty <= resize(buf_user_step_y - frame_todo_pad_y, read_dec_cnty'length);
					end if;
				-- Handle when inside end padding
				-- Note : Here frame_todo_pad_y = frame_todo_y = the number of Y rows still valid
				elsif frame_inpad_end_y = '1' then
					frame_inpad_end_y <= '1';
					if frame_todo_pad_y > buf_user_step_y then
						frame_todo_pad_y <= resize(frame_todo_pad_y - buf_user_step_y, frame_todo_pad_y'length);
						frame_todo_y <= frame_todo_y - resize(buf_user_step_y, frame_todo_y'length);
						read_dec_cnty <= resize(buf_user_step_y, read_dec_cnty'length);
					else
						frame_todo_pad_y <= (others => '0');
						frame_todo_y <= (others => '0');
						read_dec_cnty <= resize(frame_todo_pad_y, read_dec_cnty'length);
					end if;
				-- We were in the main image, no padding
				-- Reminder : step_y <= WINY
				else
					-- Only padding remains
					if frame_todo_y < buf_user_step_y then
						frame_inpad_end_y <= '1';
						frame_todo_pad_y <= (others => '0');
						frame_todo_y <= (others => '0');
						read_dec_cnty <= resize(frame_todo_y, read_dec_cnty'length);
					-- Beginning of end padding
					elsif frame_todo_y < WINY + to_integer(buf_user_step_y) then
						frame_inpad_end_y <= '1';
						frame_todo_pad_y <= resize(frame_todo_y - buf_user_step_y, frame_todo_pad_y'length);
						frame_todo_y <= resize(frame_todo_y - buf_user_step_y, frame_todo_y'length);
						read_dec_cnty <= resize(buf_user_step_y, read_dec_cnty'length);
					-- Still within main image
					else
						frame_todo_y <= resize(frame_todo_y - buf_user_step_y, frame_todo_y'length);
						read_dec_cnty <= resize(buf_user_step_y, read_dec_cnty'length);
					end if;
				end if;

				-- Process index
				slide_rowwin_idx_y <= slide_rowwin_idx_y + 1;
				if slide_rowwin_end_y = '1' then
					slide_rowwin_idx_y <= to_unsigned(0, slide_rowwin_idx_y'length);
				end if;
				-- Process end indicator
				slide_rowwin_end_y <= '0';
				if buf_user_nwin_y = 1 then slide_rowwin_end_y <= '1';
				elsif slide_rowwin_idx_y = buf_user_nwin_y - 2 then slide_rowwin_end_y <= '1';
				end if;

				-- Transition to next frame : slightly different manipulation of indexes
				if slide_rowwin_end_y = '1' then
					frame_inpad_beg_y <= '1' when buf_user_begpad_y > 0 else '0';
					frame_inpad_end_y <= '0';
					frame_todo_pad_y <= resize(buf_user_begpad_y, frame_todo_pad_y'length);
					frame_todo_y <= resize(buf_user_fsize_y, frame_todo_y'length);
					-- Send to the Write side the number of remaining XZ planes just processed
					-- FIXME What if the last rows of the image are not meant to be read ?
					-- If these rows are not already written into memory, this will trigger underflow of write-side counters
					read_dec_cnty <= resize(frame_todo_y, read_dec_cnty'length);
				end if;

				read_status <= READ_ROWWIN_BEG1;

			--else

				-- FIXME : This is supposed to be a simulation-only assertion
				-- But if assertions are enabled in Vivado, then Vivado fails on this one just because it reads this code
				--assert false report "Invalid state" severity failure;

			end if;

			-- Make sure the synthesized result is optimized when there is no padding in Y direction
			if (LOCKED = true) and (PADY = 0) and (WINY + (NWINY-1)*STEPY <= DIMY) then
				frame_inpad_beg_y <= '0';
				frame_inpad_end_y <= '0';
				frame_todo_pad_y <= (others => '0');
			end if;

			if buf_clear = '1' then
				read_status <= READ_RESET;
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Instantiation of memory components
	-------------------------------------------------------------------

	gen_mem: for i in 0 to PAR_WIN-1 generate

		-- Arbitrary value, only for automatic selection of memory implementation
		constant LUTRAM_TO_BRAM_THRESHOLD : natural := 128;

		constant LOC_USE_AUTO   : boolean := (USE_LUTRAM = false) and (USE_BRAM = false) and (USE_URAM = false);
		constant LOC_USE_LUTRAM : boolean := (USE_LUTRAM = true) or (LOC_USE_AUTO = true and MEM_CELLS <= LUTRAM_TO_BRAM_THRESHOLD);
		constant LOC_USE_BRAM   : boolean := (USE_BRAM   = true) or (LOC_USE_AUTO = true and MEM_CELLS  > LUTRAM_TO_BRAM_THRESHOLD);
		constant LOC_USE_URAM   : boolean := (USE_URAM   = true);

		signal loc_write_addr : std_logic_vector(WADDR-1 downto 0) := (others => '0');
		signal loc_read_addr  : std_logic_vector(WADDR-1 downto 0) := (others => '0');

	begin

		loc_write_addr <= std_logic_vector(write_addr);
		loc_read_addr  <= std_logic_vector(stage6_addr((i+1)*WADDR-1 downto i*WADDR));

		gen_lutram : if LOC_USE_LUTRAM = true generate

			type mem_type is array (0 to MEM_CELLS-1) of std_logic_vector(PAR_OZ*WDATA-1 downto 0);
			signal mem : mem_type := (others => (others => '0'));

			attribute ram_style : string;
			attribute ram_style of mem : signal is "distributed";

		begin

			process(clk)
			begin
				if rising_edge(clk) then

					-- FIXME The padding bit is used in this condition only to prevent VHDL simulation stopping with error when address is of bounds
					if (stage6_en = '1') and (stage6_padding(i) = '0') then
						read_data((i+1)*PAR_OZ*WDATA-1 downto i*PAR_OZ*WDATA) <= mem(to_integer(unsigned(loc_read_addr)));
					end if;

					read_valid(i) <= stage6_en and not buf_clear;
					read_otag(i)  <= stage6_padding(i);

					if write_en = '1' then
						mem(to_integer(unsigned(loc_write_addr))) <= write_data;
					end if;

				end if;
			end process;

		end generate;

		gen_bram : if LOC_USE_BRAM = true generate

			mem_i : largemem_block
				generic map (
					-- Parameters for memory
					WDATA   => PAR_OZ*WDATA,
					CELLS   => MEM_CELLS,
					WADDR   => WADDR,
					-- A tag that may be given for each read
					WTAG    => 1,
					-- Enable the DO register of the memory blocks
					REG_DO  => false,
					-- Enable extra register when there are multiple memory blocks in height
					REG_MUX => false
				)
				port map (
					clk        => clk,
					clear      => buf_clear,
					-- Write port
					write_addr => loc_write_addr,
					write_en   => write_en,
					write_data => write_data,
					-- Read port, input
					read_addr  => loc_read_addr,
					read_en    => stage6_en,
					read_itag  => stage6_padding(i downto i),
					-- Read port, output
					read_data  => read_data((i+1)*PAR_OZ*WDATA-1 downto i*PAR_OZ*WDATA),
					read_valid => read_valid(i),
					read_otag  => read_otag(i downto i)
				);

		end generate;

		gen_uram : if LOC_USE_URAM = true generate

			mem_i : largemem_huge
				generic map (
					-- Parameters for memory
					WDATA   => PAR_OZ*WDATA,
					CELLS   => MEM_CELLS,
					WADDR   => WADDR,
					-- A tag that may be given for each read
					WTAG    => 1,
					-- Enable the DO register of the memory blocks
					REG_DO  => false,
					-- Enable extra register when there are multiple memory blocks in height
					REG_MUX => false
				)
				port map (
					clk        => clk,
					clear      => buf_clear,
					-- Write port
					write_addr => loc_write_addr,
					write_en   => write_en,
					write_data => write_data,
					-- Read port, input
					read_addr  => loc_read_addr,
					read_en    => stage6_en,
					read_itag  => stage6_padding(i downto i),
					-- Read port, output
					read_data  => read_data((i+1)*PAR_OZ*WDATA-1 downto i*PAR_OZ*WDATA),
					read_valid => read_valid(i),
					read_otag  => read_otag(i downto i)
				);

		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_rdy  <= write_rdy;

	data_out     <= read_data_buf;
	data_out_rdy <= read_valid_buf;

end architecture;


