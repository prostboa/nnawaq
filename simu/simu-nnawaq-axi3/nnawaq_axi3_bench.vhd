
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnawaq_axi3_bench is
	generic (
		-- Output text file for simulation results
		INPUT_FILE : string := "input_frames.txt";
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnawaq_axi3_bench;

architecture simu of nnawaq_axi3_bench is

	signal clk           : std_logic := '0';
	signal clk_next      : std_logic := '0';
	signal clear         : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Constants for user IP

	constant C_S_AXI_DATA_WIDTH : integer := 32;
	constant C_S_AXI_ADDR_WIDTH : integer := 32;

	-- Signals for AXI4 (lite) channel

	signal GP_AXI_ACLK    : std_logic := '0';
	signal GP_AXI_ARESETN : std_logic := '0';

	signal GP_AXI4_AWVALID : std_logic := '0';
	signal GP_AXI4_AWREADY : std_logic := '0';
	signal GP_AXI4_AWADDR  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_AWPROT  : std_logic_vector(2 downto 0) := (others => '0');

	signal GP_AXI4_WVALID  : std_logic := '0';
	signal GP_AXI4_WREADY  : std_logic := '0';
	signal GP_AXI4_WDATA   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_WSTRB   : std_logic_vector(C_S_AXI_DATA_WIDTH/8-1 downto 0) := (others => '0');

	signal GP_AXI4_BVALID  : std_logic := '0';
	signal GP_AXI4_BREADY  : std_logic := '0';
	signal GP_AXI4_BRESP   : std_logic_vector(1 downto 0) := (others => '0');

	signal GP_AXI4_ARVALID : std_logic := '0';
	signal GP_AXI4_ARREADY : std_logic := '0';
	signal GP_AXI4_ARADDR  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_ARPROT  : std_logic_vector(2 downto 0) := (others => '0');

	signal GP_AXI4_RVALID  : std_logic := '0';
	signal GP_AXI4_RREADY  : std_logic := '0';
	signal GP_AXI4_RDATA   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_RRESP   : std_logic_vector(1 downto 0) := (others => '0');

	-- Component declaration

	component nnawaq_axi3 is
		generic (
			-- Users to add parameters here

			-- User parameters ends
			-- Do not modify the parameters beyond this line
			C_S_AXI_DATA_WIDTH : integer := 32;
			C_S_AXI_ADDR_WIDTH : integer := 32;
			C_S_AXI_ID_WIDTH   : integer := 12;
			C_S_AXI_SIZE_WIDTH : integer := 2
		);
		port (
			-- Users to add ports here

			-- User ports ends
			-- Do not modify the ports beyond this line

			S_AXI_ACLK    : in  std_logic;
			S_AXI_ARESETN : in  std_logic;

			S_AXI_AWVALID : in  std_logic;
			S_AXI_AWREADY : out std_logic;
			S_AXI_AWID    : in  std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
			S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			S_AXI_AWLEN   : in  std_logic_vector(3 downto 0);
			S_AXI_AWSIZE  : in  std_logic_vector(C_S_AXI_SIZE_WIDTH-1 downto 0);
			S_AXI_AWBURST : in  std_logic_vector(1 downto 0);
			S_AXI_AWLOCK  : in  std_logic_vector(1 downto 0);
			S_AXI_AWCACHE : in  std_logic_vector(3 downto 0);
			S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);
			S_AXI_AWQOS   : in  std_logic_vector(3 downto 0);

			S_AXI_WVALID  : in  std_logic;
			S_AXI_WREADY  : out std_logic;
			S_AXI_WID     : in  std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
			S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			S_AXI_WSTRB   : in  std_logic_vector(C_S_AXI_DATA_WIDTH/8-1 downto 0);
			S_AXI_WLAST   : in  std_logic;

			S_AXI_BVALID  : out std_logic;
			S_AXI_BREADY  : in  std_logic;
			S_AXI_BID     : out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
			S_AXI_BRESP   : out std_logic_vector(1 downto 0);

			S_AXI_ARVALID : in  std_logic;
			S_AXI_ARREADY : out std_logic;
			S_AXI_ARID    : in  std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
			S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			S_AXI_ARLEN   : in  std_logic_vector(3 downto 0);
			S_AXI_ARSIZE  : in  std_logic_vector(C_S_AXI_SIZE_WIDTH-1 downto 0);
			S_AXI_ARBURST : in  std_logic_vector(1 downto 0);
			S_AXI_ARLOCK  : in  std_logic_vector(1 downto 0);
			S_AXI_ARCACHE : in  std_logic_vector(3 downto 0);
			S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);
			S_AXI_ARQOS   : in  std_logic_vector(3 downto 0);

			S_AXI_RVALID  : out std_logic;
			S_AXI_RREADY  : in  std_logic;
			S_AXI_RID     : out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
			S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			S_AXI_RLAST   : out std_logic;
			S_AXI_RRESP   : out std_logic_vector(1 downto 0)

		);
	end component;

begin

	user_inst : nnawaq_axi3
		generic map (
			-- Users to add parameters here

			-- User parameters ends
			-- Do not modify the parameters beyond this line

			-- Width of S_AXI data bus
			C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
			-- Width of S_AXI address bus
			C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH

		)
		port map (
			-- Users to add ports here

			-- User ports ends
			-- Do not modify the ports beyond this line

			S_AXI_ACLK    => GP_AXI_ACLK,
			S_AXI_ARESETN => GP_AXI_ARESETN,

			S_AXI_AWADDR  => GP_AXI4_AWADDR(C_S_AXI_ADDR_WIDTH-1 downto 0),
			S_AXI_AWPROT  => GP_AXI4_AWPROT,
			S_AXI_AWVALID	=> GP_AXI4_AWVALID,
			S_AXI_AWREADY	=> GP_AXI4_AWREADY,

			S_AXI_AWID    => (others => '0'),
			S_AXI_AWLEN   => (others => '0'),
			S_AXI_AWSIZE  => (others => '0'),
			S_AXI_AWBURST => (others => '0'),
			S_AXI_AWLOCK  => (others => '0'),
			S_AXI_AWCACHE => (others => '0'),
			S_AXI_AWQOS   => (others => '0'),

			S_AXI_WDATA   => GP_AXI4_WDATA,
			S_AXI_WSTRB   => GP_AXI4_WSTRB,
			S_AXI_WVALID  => GP_AXI4_WVALID,
			S_AXI_WREADY  => GP_AXI4_WREADY,

			S_AXI_WID     => (others => '0'),
			S_AXI_WLAST   => '0',

			S_AXI_BRESP   => GP_AXI4_BRESP,
			S_AXI_BVALID  => GP_AXI4_BVALID,
			S_AXI_BREADY  => GP_AXI4_BREADY,

			S_AXI_BID     => open,

			S_AXI_ARADDR  => GP_AXI4_ARADDR(C_S_AXI_ADDR_WIDTH-1 downto 0),
			S_AXI_ARPROT  => GP_AXI4_ARPROT,
			S_AXI_ARVALID => GP_AXI4_ARVALID,
			S_AXI_ARREADY => GP_AXI4_ARREADY,

			S_AXI_ARID    => (others => '0'),
			S_AXI_ARLEN   => (others => '0'),
			S_AXI_ARSIZE  => (others => '0'),
			S_AXI_ARBURST => (others => '0'),
			S_AXI_ARLOCK  => (others => '0'),
			S_AXI_ARCACHE => (others => '0'),
			S_AXI_ARQOS   => (others => '0'),

			S_AXI_RDATA   => GP_AXI4_RDATA,
			S_AXI_RRESP   => GP_AXI4_RRESP,
			S_AXI_RVALID  => GP_AXI4_RVALID,
			S_AXI_RREADY  => GP_AXI4_RREADY,

			S_AXI_RID     => open,
			S_AXI_RLAST   => open

		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	GP_AXI_ACLK <= clk;
	GP_AXI_ARESETN <= not clear;

	-- Process that generates stimuli
	process

		file in_file : text open read_mode is INPUT_FILE;
		variable in_line : line;
		variable in_lines_nb : natural := 0;
		variable rx_fifo_var : std_logic_vector(31 downto 0) := (others => '0');
		variable rx_fifo_cnt : natural := 0;

		file out_file : text open write_mode is OUTPUT_FILE;
		variable out_line : line;

	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		-- Wait until the device is out of its internal reset
		for i in 0 to 100 loop
			wait until rising_edge(clk);
		end loop;

		-- Write strobes are not used, permanently set to 1
		GP_AXI4_WSTRB  <= (others => '1');
		-- Permanently accept Write responses
		GP_AXI4_BREADY <= '1';
		-- Permanently accept Read responses
		GP_AXI4_RREADY <= '1';

		-- Enable freerun, in and out
		--GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(3*4, 32));
		--GP_AXI4_WDATA  <= "11000000000000000000000000000000";
		--GP_AXI4_AWVALID <= '1';
		--GP_AXI4_WVALID  <= '1';
		--wait until rising_edge(clk);
		--GP_AXI4_AWVALID <= '0';
		--GP_AXI4_WVALID  <= '0';
		--wait until rising_edge(clk);
		--wait until rising_edge(clk);

		-- Set the ID of the output layer (default "11...11" is last layer)
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(5*4, 32));
		GP_AXI4_WDATA  <= X"0000FFFF";
		--GP_AXI4_WDATA  <= X"00000001";
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Set the number of expected inputs
		-- This starts the input state machine
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(6*4, 32));
		GP_AXI4_WDATA  <= std_logic_vector(to_unsigned(10*1024, 32));
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Set the number of expected outputs
		-- This starts the output state machine
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(7*4, 32));
		GP_AXI4_WDATA  <= std_logic_vector(to_unsigned(10, 32));
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Permanently write in the RX fifo
		--GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(64*4, 32));
		--GP_AXI4_WDATA  <= (others => '1');
		--GP_AXI4_AWVALID <= '1';
		--GP_AXI4_WVALID  <= '1';

		-- Permanently read from the TX fifo
		--GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(64*4, 32));
		--GP_AXI4_ARVALID <= '1';

		-- Send the frame data
		while not endfile(in_file) loop

			-- Read level of RX fif0
			GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(13*4, 32));
			GP_AXI4_ARVALID <= '1';
			wait until rising_edge(clk);
			GP_AXI4_ARVALID <= '0';
			wait until rising_edge(clk);
			wait until rising_edge(clk);
			-- Extract the rx_cnt field from the read word
			rx_fifo_cnt := to_integer(unsigned(GP_AXI4_RDATA(11 downto 4)));

			if rx_fifo_cnt > 0 then

				-- Reading one line
				readline(in_file, in_line);
				in_lines_nb := in_lines_nb + 1;

				-- Decode the line
				hread(in_line, rx_fifo_var);
				-- Write in RX fifo
				GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(64*4, 32));
				GP_AXI4_WDATA  <= rx_fifo_var;
				GP_AXI4_AWVALID <= '1';
				GP_AXI4_WVALID  <= '1';
				wait until rising_edge(clk);
				wait until rising_edge(clk);
				GP_AXI4_AWVALID <= '0';
				GP_AXI4_WVALID  <= '0';
				wait until rising_edge(clk);
				wait until rising_edge(clk);

			end if;

		end loop;

		-- Latency for Lenet5 is around 50k cycles
		for i in 0 to 60000 loop
			wait until rising_edge(clk);
		end loop;

		-- Clear the permanent Write and Read
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Read the results that have accumulated in the TX fifo
		for i in 1 to 10 loop

			-- Read from TX fifo
			GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(64*4, 32));
			GP_AXI4_ARVALID <= '1';
			wait until rising_edge(clk);
			GP_AXI4_ARVALID <= '0';
			wait until rising_edge(clk);
			wait until rising_edge(clk);

			report "TX fifo : " & to_string(to_integer(unsigned(GP_AXI4_RDATA)));

			write(out_line, to_string(to_integer(unsigned(GP_AXI4_RDATA))));
			writeline(out_file, out_line);

		end loop;

		-- Get the clock counter
		GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(10*4, 32));
		GP_AXI4_ARVALID <= '1';
		wait until rising_edge(clk);
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		report "Clock counter : " & to_string(to_integer(unsigned(GP_AXI4_RDATA)));

		-- End of simulation
		report "Arbitrary delay is reached";
		report "Stopping simulation";
		clk_want_stop <= true;

		-- Wait for end of simulation
		wait;

	end process;

end architecture;

