
-- This is a neuron layer
-- This component can contain many neurons and have input and output parallelism
-- Dedicated to ternary weights and Xilinx technology

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.all;

entity nnlayer_neurons_ter_postadd_xilinx is
	generic (
		-- Parameters for the neurons
		WDATA  : natural := 2;
		SDATA  : boolean := true;
		SFIXED : boolean := true;
		WACCU  : natural := 12;
		WOUT   : natural := 12;
		-- Parameters for BRAM usage
		NPERBLK : natural := 18;
		WRNB    : natural := 2;
		-- Parameters for frame and number of neurons
		FSIZE  : natural := 1024;
		NBNEU  : natural := 1024;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0
	);
	port (
		clk            : in  std_logic;
		clear          : in  std_logic;
		-- Ports for Write Enable
		write_mode     : in  std_logic;
		write_idx      : in  std_logic_vector(9 downto 0);
		write_data     : in  std_logic_vector(2*NPERBLK*WRNB-1 downto 0);
		write_enable   : in  std_logic;
		write_end      : out std_logic;
		-- The user-specified frame size and number of neurons
		user_fsize     : in  std_logic_vector(15 downto 0);
		user_nbneu     : in  std_logic_vector(15 downto 0);
		-- Data input, 2 bits
		data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		data_in_signed : in  std_logic;
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Scan chain to extract values
		data_out       : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- Indicate to the parent component that we are reaching the end of the current frame
		end_of_frame   : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_neurons_ter_postadd_xilinx;

architecture synth of nnlayer_neurons_ter_postadd_xilinx is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- The number of BRAM-based blocks to instantiate
	constant NBBLOCKS    : natural := (NBNEU + NPERBLK - 1) / NPERBLK;
	constant TOTALBLOCKS : natural := PAR_IN * PAR_OUT * NBBLOCKS;
	-- Max fanout for signals distributed to all BRAM-based blocks
	constant FANOUT : natural := 4;
	-- The write width
	constant WWR : natural := 2 * NPERBLK * WRNB;

	constant WPARIN  : natural := storebitsnb(PAR_IN-1);
	constant WPAROUT : natural := storebitsnb(PAR_OUT-1);

	constant WADDR_IN : natural := storebitsnb(FSIZE-1);
	constant WADDR_SH : natural := storebitsnb(NBNEU-1);

	-- Take into account latency of distribution buffers + neurons + distributed add
	constant FIFOMARGIN_USE : natural := FIFOMARGIN + storebitsnb(TOTALBLOCKS-1) + 6 + storebitsnb(PAR_IN-1);

	-- Registers for local functionality
	signal reg_we_mode_parout : std_logic_vector(PAR_OUT-1 downto 0) := (others => '0');
	signal reg_we_mode_parin  : std_logic_vector(PAR_IN-1 downto 0)  := (others => '0');

	signal reg_we_mode, reg_we_mode_n         : std_logic := '0';
	signal reg_frame_doing, reg_frame_doing_n : std_logic := '0';
	signal reg_shift_doing, reg_shift_doing_n : std_logic := '0';
	signal reg_frame_ready, reg_frame_ready_n : std_logic := '0';
	signal reg_shift_ready, reg_shift_ready_n : std_logic := '0';

	signal reg_frame_beg, reg_frame_beg_n     : std_logic := '0';
	signal reg_shift_beg, reg_shift_beg_n     : std_logic := '0';

	signal reg_write_end, reg_write_end_n     : std_logic := '0';
	signal reg_frame_end, reg_frame_end_n     : std_logic := '0';
	signal reg_shift_end, reg_shift_end_n     : std_logic := '0';

	signal buf_user_fsize  : unsigned(15 downto 0);
	signal buf_user_nbneu  : unsigned(15 downto 0);
	signal buf_wr_addr_end : unsigned(WADDR_IN-1 downto 0);
	signal buf_in_addr_end : unsigned(WADDR_IN-1 downto 0);
	signal buf_sh_addr_end : unsigned(WADDR_SH-1 downto 0);

	signal buf_out_ready   : std_logic := '0';
	signal reg_out_valid   : std_logic := '0';

	-- Internal address registers
	signal regaddr_in, regaddr_in_n : unsigned(WADDR_IN-1 downto 0) := (others => '0');
	signal regaddr_sh, regaddr_sh_n : unsigned(WADDR_SH-1 downto 0) := (others => '0');

	-- Control signals to drive the blocks
	signal sigctrl_we_shift   : std_logic := '0';
	signal sigctrl_accu_add   : std_logic := '0';
	signal sigctrl_accu_clear : std_logic := '0';
	signal sigctrl_shift_en   : std_logic := '0';
	signal sigctrl_shift_copy : std_logic := '0';

	-- One signal to merge control inputs together, for clarity
	signal allctrl : std_logic_vector(4 downto 0) := (others => '0');

	-- Arrays of signals to instantiate the blocks
	signal arr_write_data   : std_logic_vector(TOTALBLOCKS*WWR-1 downto 0) := (others => '0');
	signal arr_write_enable : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');

	signal arr_allctrl     : std_logic_vector(TOTALBLOCKS*5-1 downto 0) := (others => '0');
	signal arr_addr_in     : std_logic_vector(TOTALBLOCKS*WADDR_IN-1 downto 0) := (others => '0');
	signal arr_data_in     : std_logic_vector(TOTALBLOCKS*PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal arr_data_in_sgn : std_logic_vector(TOTALBLOCKS-1 downto 0) := (others => '0');

	signal sensor_shift    : std_logic := '0';
	signal sensor_copy     : std_logic := '0';
	signal sensor_we_shift : std_logic := '0';

	-- Component declaration: one block of neurons
	component neurons_ter_bram_xilinx is
		generic (
			-- Parameters for the neurons
			WDATA  : natural := 2;
			SDATA  : boolean := true;
			SFIXED : boolean := true;
			WACCU  : natural := 12;
			-- Parameters for BRAM usage
			NPERBLK : natural := 18;
			WRNB    : natural := 2;
			-- Parameters for frame and number of neurons
			WADDR  : natural := 10;
			FSIZE  : natural := 1024;
			NBNEU  : natural := 18
		);
		port (
			clk             : in  std_logic;
			-- Control signals
			ctrl_we_mode    : in  std_logic;
			ctrl_we_shift   : in  std_logic;
			ctrl_we_valid   : in  std_logic;
			ctrl_accu_clear : in  std_logic;
			ctrl_accu_add   : in  std_logic;
			ctrl_shift_en   : in  std_logic;
			ctrl_shift_copy : in  std_logic;
			-- Address in BRAM, for write and for read
			addr_in         : in  std_logic_vector(WADDR-1 downto 0);
			-- Ports for Write Enable
			we_prev         : in  std_logic;
			write_data      : in  std_logic_vector(2*NPERBLK*WRNB-1 downto 0);
			we_next         : out std_logic;
			-- Data input, 2 bits
			data_in         : in  std_logic_vector(WDATA-1 downto 0);
			data_in_signed  : in  std_logic;
			-- Scan chain to extract values
			sh_data_in      : in  std_logic_vector(WACCU-1 downto 0);
			sh_data_out     : out std_logic_vector(WACCU-1 downto 0);
			-- Sensors, for synchronization with the controller
			sensor_shift    : out std_logic;
			sensor_copy     : out std_logic;
			sensor_we_mode  : out std_logic;
			sensor_we_shift : out std_logic;
			sensor_we_valid : out std_logic
		);
	end component;

	-- Component declaration: distribution tree to limit fanout
	component distribuf is
		generic(
			WDATA :  natural := 32;
			NBOUT :  natural := 32;
			FANOUT : natural := 32
		);
		port(
			clk : in std_logic;
			-- Input
			idata : in std_logic_vector(WDATA-1 downto 0);
			-- Outputs
			odata : out std_logic_vector(WDATA*NBOUT-1 downto 0)
		);
	end component;

	-- Compnent declaration: Distributed ADD with enable
	component addtree is
		generic(
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true
		);
		port(
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-------------------------------------------------------------------
	-- FSM
	-------------------------------------------------------------------

	-- Sequential process

	process(clk)
	begin
		if rising_edge(clk) then

			buf_user_fsize <= unsigned(user_fsize);
			if unsigned(user_fsize) > FSIZE then
				buf_user_fsize <= to_unsigned(FSIZE, 16);
			end if;

			buf_user_nbneu <= unsigned(user_nbneu);
			if unsigned(user_nbneu) > NBNEU then
				buf_user_nbneu <= to_unsigned(NBNEU, 16);
			end if;

			-- Data output: Detect when there is enough room in output FIFO
			buf_out_ready <= '0';
			if unsigned(out_fifo_room) >= FIFOMARGIN_USE then
				buf_out_ready <= '1';
			end if;

			-- Note: WRNB is always a power of 2 so this synthesizes well
			buf_wr_addr_end <= resize((unsigned(buf_user_fsize) + WRNB - 1) / WRNB - 2, WADDR_IN);
			buf_in_addr_end <= resize(unsigned(buf_user_fsize) - 2, WADDR_IN);
			buf_sh_addr_end <= resize(unsigned(buf_user_nbneu) - 2, WADDR_SH);

			reg_frame_beg <= reg_frame_beg_n;
			reg_shift_beg <= reg_shift_beg_n;

			reg_write_end <= reg_write_end_n;
			reg_frame_end <= reg_frame_end_n;
			reg_shift_end <= reg_shift_end_n;

			reg_we_mode <= reg_we_mode_n;

			regaddr_in <= regaddr_in_n;
			regaddr_sh <= regaddr_sh_n;

			reg_frame_doing <= reg_frame_doing_n;
			reg_shift_doing <= reg_shift_doing_n;
			reg_frame_ready <= reg_frame_ready_n;
			reg_shift_ready <= reg_shift_ready_n;

			reg_out_valid <= sensor_shift;

		end if;
	end process;

	-- Combinatorial process

	process(
		-- Inputs
		clear, write_mode, write_data, write_enable,
		buf_user_fsize, buf_user_nbneu, data_in, data_in_valid,
		-- Local registers
		buf_wr_addr_end, buf_in_addr_end, buf_sh_addr_end,
		buf_out_ready,
		reg_frame_beg, reg_shift_beg,
		reg_write_end, reg_frame_end, reg_shift_end,
		reg_we_mode, regaddr_in, regaddr_sh,
		reg_frame_doing, reg_shift_doing,
		reg_frame_ready, reg_shift_ready,
		-- Outputs from the neurons
		sensor_shift, sensor_copy, sensor_we_shift
	)
		variable var_frame_end : std_logic := '0';
		variable var_shift_end : std_logic := '0';
	begin

		-- Init variables
		var_frame_end := '0';
		var_shift_end := '0';

		-- Default values for registers

		reg_frame_beg_n <= reg_frame_beg;
		reg_shift_beg_n <= reg_shift_beg;

		reg_write_end_n <= reg_write_end;
		reg_frame_end_n <= reg_frame_end;
		reg_shift_end_n <= reg_shift_end;

		reg_we_mode_n <= reg_we_mode;

		regaddr_in_n  <= regaddr_in;
		regaddr_sh_n  <= regaddr_sh;

		reg_frame_doing_n <= reg_frame_doing;
		reg_shift_doing_n <= reg_shift_doing;
		reg_frame_ready_n <= reg_frame_ready;
		reg_shift_ready_n <= reg_shift_ready;

		-- Default values for outputs

		data_in_ready  <= '0';

		-- Default values for neuron control signals

		sigctrl_we_shift   <= '0';
		sigctrl_accu_add   <= '0';
		sigctrl_accu_clear <= '0';
		sigctrl_shift_en   <= '0';
		sigctrl_shift_copy <= '0';

		--------------------------------------------------
		-- Write mode
		--------------------------------------------------

		-- The we_mode register
		reg_we_mode_n <= write_mode;

		if (reg_we_mode = '1') and (write_enable = '1') then
			-- Increment the address
			regaddr_in_n <= regaddr_in + 1;
			-- Bufferize a flag that indicates the end of the block is reached
			reg_write_end_n <= '0';
			if regaddr_in = buf_wr_addr_end then
				reg_write_end_n <= '1';
			end if;
			-- Check if one block is full
			if reg_write_end = '1' then
				-- Generate one WE Shift Block pulse to enable the next block
				sigctrl_we_shift <= '1';
				-- Clear the address
				regaddr_in_n <= (others => '0');
			end if;
		end if;

		-- Generate one Clear pulse when getting out of Write mode
		if (reg_we_mode = '1') and (write_mode = '0') then
			regaddr_in_n <= (others => '0');
			reg_write_end_n <= '0';
			sigctrl_accu_clear <= '1';
		end if;

		--------------------------------------------------
		-- Normal operation: inside frame and/or shift
		--------------------------------------------------

		-- Frame input
		if reg_frame_doing = '1' then
			-- Indicate to the input FIFO that we are reading a value
			data_in_ready <= '1';
			if data_in_valid = '1' then
				reg_frame_beg_n <= '0';
				sigctrl_accu_clear <= reg_frame_beg;
				-- Send the data valid signal to neurons
				sigctrl_accu_add <= '1';
				-- Increment the address
				regaddr_in_n <= regaddr_in + 1;
				-- Bufferize a flag that indicates the end of the frame is reached
				reg_frame_end_n <= '0';
				if (regaddr_in = buf_in_addr_end) or (buf_user_fsize = 1) then
					reg_frame_end_n <= '1';
				end if;
				-- Handle when we are at the last data item of the frame
				if reg_frame_end = '1' then
					var_frame_end := '1';
					-- Clear the address
					regaddr_in_n <= (others => '0');
					-- Prepare to wait for the next frame
					reg_frame_doing_n <= '0';
					reg_shift_ready_n <= '1';
				end if;
			end if;
		end if;

		-- Data output: send the Shift Enable signal
		if reg_shift_doing = '1' then
			-- WARNING: We MUST be able to read the first value at the first cycle
			-- Because on the other side, the next frame can overwrite the accumulators
			if (buf_out_ready = '1') or (reg_shift_beg = '1') then
				reg_shift_beg_n <= '0';
				sigctrl_shift_copy <= reg_shift_beg;
				-- Send the shift enable signal to neurons - it acts as data valid on output side
				sigctrl_shift_en <= '1';
				-- Increment the address
				regaddr_sh_n <= regaddr_sh + 1;
				-- Bufferize a flag that indicates the end of shift is reached
				reg_shift_end_n <= '0';
				if (regaddr_sh = buf_sh_addr_end) or (buf_user_nbneu = 1) then
					reg_shift_end_n <= '1';
				end if;
				-- Handle when we are sending the Shift Enable for the result of the last neuron
				if reg_shift_end = '1' then
					var_shift_end := '1';
					-- Clear the address
					regaddr_sh_n <= (others => '0');
					-- Prepare to wait for the next frame
					reg_shift_doing_n <= '0';
					reg_frame_ready_n <= '1';
				end if;
			end if;
		end if;

		--------------------------------------------------
		-- Change state, for frame and/or shift
		--------------------------------------------------

		-- Start next frame
		if (reg_frame_doing = '0' or var_frame_end = '1') and (reg_frame_ready = '1' or var_shift_end = '1') then
			reg_frame_doing_n <= '1';
			reg_frame_ready_n <= '0';
			reg_frame_beg_n   <= '1';
		end if;

		-- Start next shift
		if (reg_shift_doing = '0' or var_shift_end = '1') and (reg_shift_ready = '1' or var_frame_end = '1') then
			reg_shift_doing_n <= '1';
			reg_shift_ready_n <= '0';
			reg_shift_beg_n   <= '1';
		end if;

		-- The general clear signal has priority
		if clear = '1' then
			reg_frame_beg_n    <= '0';
			reg_shift_beg_n    <= '0';
			reg_write_end_n    <= '0';
			reg_frame_end_n    <= '0';
			reg_shift_end_n    <= '0';
			reg_we_mode_n      <= '0';
			regaddr_in_n       <= (others => '0');
			regaddr_sh_n       <= (others => '0');
			reg_frame_doing_n  <= '1';
			reg_shift_doing_n  <= '0';
			reg_frame_ready_n  <= '1';
			reg_shift_ready_n  <= '0';
			-- Also clear stuff in the blocks
			sigctrl_we_shift   <= '0';
			sigctrl_accu_clear <= '1';
			sigctrl_accu_add   <= '0';
			sigctrl_shift_en   <= '0';
			sigctrl_shift_copy <= '0';
		end if;

	end process;

	-- This output will be routed up to the communication interface (PCIe, AXI, ...), so it's good to have it buffered
	end_of_frame <= reg_frame_end;

	-- This output indicates to the wrapping controller that this block is fully configured
	write_end <= reg_write_end;



	-------------------------------------------------------------------
	-- Aggregate all input control signals
	-------------------------------------------------------------------

	allctrl <=
		sigctrl_we_shift   &
		sigctrl_accu_clear &
		sigctrl_accu_add   &
		sigctrl_shift_en   &
		sigctrl_shift_copy;



	-------------------------------------------------------------------
	-- Instantiate the fanout distribution buffers
	-------------------------------------------------------------------

	-- Fanout distribution tree: write_data
	i_buf_write_data: distribuf
		generic map (
			WDATA  => WWR,
			NBOUT  => TOTALBLOCKS,
			FANOUT => FANOUT
		)
		port map (
			clk   => clk,
			idata => write_data,
			odata => arr_write_data
		);

	-- Fanout distribution tree: write_enable
	i_buf_write_en: distribuf
		generic map (
			WDATA  => 1,
			NBOUT  => TOTALBLOCKS,
			FANOUT => FANOUT
		)
		port map (
			clk      => clk,
			idata(0) => write_enable,
			odata    => arr_write_enable
		);

	-- Fanout distribution tree: control signals
	i_buf_allctrl: distribuf
		generic map (
			WDATA  => 5,
			NBOUT  => TOTALBLOCKS,
			FANOUT => FANOUT
		)
		port map (
			clk   => clk,
			idata => allctrl,
			odata => arr_allctrl
		);

	-- Fanout distribution tree: address in the input frame
	i_buf_addr: distribuf
		generic map (
			WDATA  => WADDR_IN,
			NBOUT  => TOTALBLOCKS,
			FANOUT => FANOUT
		)
		port map (
			clk   => clk,
			idata => std_logic_vector(regaddr_in),
			odata => arr_addr_in
		);

	-- Fanout distribution tree: data_in
	i_buf_data_in: distribuf
		generic map (
			WDATA  => PAR_IN*WDATA,
			NBOUT  => TOTALBLOCKS,
			FANOUT => FANOUT
		)
		port map (
			clk   => clk,
			idata => data_in,
			odata => arr_data_in
		);

	-- Fanout distribution tree: data_in_signed
	gen_dist_sign: if SFIXED = FALSE generate

		i_buf_data_in_signed: distribuf
			generic map (
				WDATA  => 1,
				NBOUT  => TOTALBLOCKS,
				FANOUT => FANOUT
			)
			port map (
				clk      => clk,
				idata(0) => data_in_signed,
				odata    => arr_data_in_sgn
			);

	end generate;

	gen_dist_sfixed: if SFIXED = TRUE generate
		arr_data_in_sgn <= (others => '1') when SDATA = TRUE else (others => '0');
	end generate;



	-------------------------------------------------------------------
	-- Write mode registers
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			reg_we_mode_parout <= (others => '0');
			reg_we_mode_parin  <= (others => '0');

			if PAR_OUT <= 1 then
				reg_we_mode_parout <= (others => write_mode);
			else
				for i in 0 to PAR_OUT-1 loop
					if PAR_IN <= 1 then
						if unsigned(write_idx(WPAROUT-1 downto 0)) = i then
							reg_we_mode_parout(i) <= write_mode;
						end if;
					else
						if unsigned(write_idx(WPAROUT+WPARIN-1 downto WPARIN)) = i then
							reg_we_mode_parout(i) <= write_mode;
						end if;
					end if;
				end loop;
			end if;

			if PAR_IN <= 1 then
				reg_we_mode_parin <= (others => write_mode);
			else
				for i in 0 to PAR_IN-1 loop
					if unsigned(write_idx(WPARIN-1 downto 0)) = i then
						reg_we_mode_parin(i) <= write_mode;
					end if;
				end loop;
			end if;

		end if;  -- Clock edge
	end process;



	-------------------------------------------------------------------
	-- Instantiate the blocks of neurons
	-------------------------------------------------------------------

	gen_parout: for po in 0 to PAR_OUT-1 generate

		signal distadd_data_in   : std_logic_vector(PAR_IN*WACCU-1 downto 0) := (others => '0');
		signal distadd_valid_in  : std_logic := '0';
		signal distadd_valid_out : std_logic := '0';

	begin

		gen_parin: for pi in 0 to PAR_IN-1 generate

			-- Registers for local functionality
			signal reg_we_mode_loc, reg_we_mode_loc_n : std_logic := '0';
			signal reg_we_prev_loc, reg_we_prev_loc_n : std_logic_vector(3 downto 0) := (others => '0');

			signal arr_we_mode  : std_logic_vector(NBBLOCKS-1 downto 0) := (others => '0');

			signal loc_we_shift : std_logic := '0';
			signal arr_we_shift : std_logic_vector(NBBLOCKS-1 downto 0) := (others => '0');

			signal arr_we_prev  : std_logic_vector(NBBLOCKS-1 downto 0) := (others => '0');
			signal arr_we_next  : std_logic_vector(NBBLOCKS-1 downto 0) := (others => '0');

			signal arr_sh_data_in  : std_logic_vector(NBBLOCKS*WACCU-1 downto 0) := (others => '0');
			signal arr_sh_data_out : std_logic_vector(NBBLOCKS*WACCU-1 downto 0) := (others => '0');

			signal sensor_we_shift : std_logic := '0';

		begin

			-- Local registers
			process(clk)
			begin
				if rising_edge(clk) then
					reg_we_mode_loc <= reg_we_mode_loc_n;
					reg_we_prev_loc <= reg_we_prev_loc_n;
				end if;
			end process;

			-- The we_mode register
			reg_we_mode_loc_n <= reg_we_mode_parout(po) and reg_we_mode_parin(pi);
			-- The we_prev signal
			reg_we_prev_loc_n <=
				(others => '1') when (reg_we_mode_loc = '0') and (reg_we_mode_loc_n = '1') else
				(others => '0') when reg_we_mode_loc = '0' else
				'0' & reg_we_prev_loc(3 downto 1);

			-- Distribute ctrl_we_mode to all blocks
			i_buf_data_in_signed: distribuf
				generic map (
					WDATA  => 1,
					NBOUT  => NBBLOCKS,
					FANOUT => FANOUT
				)
				port map (
					clk      => clk,
					idata(0) => reg_we_mode_loc,
					odata    => arr_we_mode
				);

			-- Instantiate the blocks of neurons
			gen_blocks: for i in 0 to NBBLOCKS-1 generate

				signal loc_sensor_shift    : std_logic := '0';
				signal loc_sensor_copy     : std_logic := '0';
				signal loc_sensor_we_shift : std_logic := '0';

				signal loc_we_mode  : std_logic := '0';
				signal loc_we_shift : std_logic := '0';

				constant GLOBIDX : natural := po * PAR_IN * NBBLOCKS + pi * NBBLOCKS + i;

				function calc_onebram_nbneu(idx : natural) return natural is
				begin
					if (idx+1) * NPERBLK <= NBNEU then return NPERBLK; end if;
					return NBNEU - (idx * NPERBLK);
				end function;

			begin

				-- For the first block only: make it take the WE token when entering write mode
				loc_we_mode  <= reg_we_mode_loc or arr_we_mode(i) when i = 0 else arr_we_mode(i);
				loc_we_shift <= reg_we_prev_loc(2) or arr_allctrl(GLOBIDX*5 + 4) when i = 0 else arr_allctrl(GLOBIDX*5 + 4);

				-- Instantiate one RAM block and the associated neurons
				i_block: neurons_ter_bram_xilinx
					generic map(
						-- Parameters for the neurons
						WDATA  => WDATA,
						SDATA  => SDATA,
						SFIXED => SFIXED,
						WACCU  => WACCU,
						-- Parameters for BRAM usage
						NPERBLK => NPERBLK,
						WRNB    => WRNB,
						-- Parameters for frame and number of neurons
						WADDR  => WADDR_IN,
						FSIZE  => FSIZE,
						NBNEU  => calc_onebram_nbneu(i)
					)
					port map(
						clk             => clk,
						-- Control signals
						ctrl_we_mode    => loc_we_mode,
						ctrl_we_shift   => loc_we_shift,
						ctrl_we_valid   => arr_write_enable(GLOBIDX),
						ctrl_accu_clear => arr_allctrl(GLOBIDX*5 + 3),
						ctrl_accu_add   => arr_allctrl(GLOBIDX*5 + 2),
						ctrl_shift_en   => arr_allctrl(GLOBIDX*5 + 1),
						ctrl_shift_copy => arr_allctrl(GLOBIDX*5 + 0),
						-- Address in BRAM, for write and for read
						addr_in         => arr_addr_in((GLOBIDX+1)*WADDR_IN-1 downto GLOBIDX*WADDR_IN),
						-- Ports for Write Enable
						we_prev         => arr_we_prev(i),
						write_data      => arr_write_data((GLOBIDX+1)*WWR-1 downto GLOBIDX*WWR),
						we_next         => arr_we_next(i),
						-- Data input, 2 bits
						data_in         => arr_data_in(GLOBIDX*PAR_IN*WDATA+(pi+1)*WDATA-1 downto GLOBIDX*PAR_IN*WDATA+pi*WDATA),
						data_in_signed  => arr_data_in_sgn(GLOBIDX),
						-- Scan chain to extract values
						sh_data_in      => arr_sh_data_in(i*WACCU+WACCU-1 downto i*WACCU),
						sh_data_out     => arr_sh_data_out(i*WACCU+WACCU-1 downto i*WACCU),
						-- Sensors, for synchronization with the controller
						sensor_shift    => loc_sensor_shift,
						sensor_copy     => loc_sensor_copy,
						sensor_we_mode  => open,
						sensor_we_shift => loc_sensor_we_shift,
						sensor_we_valid => open
					);

				-- Sensors
				gen_sensors1: if i = 0 generate
					sensor_we_shift <= loc_sensor_we_shift;
				end generate;
				gen_sensors2: if (po = 0) and (pi = 0) and (i = 0) generate
					sensor_shift <= loc_sensor_shift;
					sensor_copy  <= loc_sensor_copy;
				end generate;

			end generate;

			-- Input of the chain of blocks
			arr_sh_data_in(NBBLOCKS*WACCU-1 downto (NBBLOCKS-1)*WACCU) <= (others => '0');
			arr_we_prev(0) <= reg_we_prev_loc(0);

			-- Inter-block connections
			gen_neurons1: for i in 0 to NBBLOCKS-2 generate
				arr_sh_data_in(i*WACCU+WACCU-1 downto i*WACCU) <= arr_sh_data_out((i+1)*WACCU+WACCU-1 downto (i+1)*WACCU);
			end generate;
			gen_neurons2: for i in 1 to NBBLOCKS-1 generate
				arr_we_prev(i) <= arr_we_next(i-1);
			end generate;

			-- Output data
			distadd_data_in((pi+1)*WACCU-1 downto pi*WACCU) <= arr_sh_data_out(WACCU-1 downto 0);

		end generate;  -- PAR_IN

		-- Only use distadd of first block to propagate the data valid flag
		distadd_valid_in <= reg_out_valid when po = 0 else '0';

		-- Instantiate the adder tree
		add_i: addtree
			generic map (
				WDATA => WACCU,
				SDATA => true,
				NBIN  => PAR_IN,
				WOUT  => WOUT,
				TAGW  => 1,
				TAGEN => true,
				TAGZC => true
			)
			port map (
				clk        => clk,
				clear      => clear,
				-- Data, input and output
				data_in    => distadd_data_in,
				data_out   => data_out((po+1)*WOUT-1 downto po*WOUT),
				-- Data output
				tag_in(0)  => distadd_valid_in,
				tag_out(0) => distadd_valid_out
			);

		-- Get the final data valid flag from the first block
		gen_do_val: if po = 0 generate
			data_out_valid <= distadd_valid_out;
		end generate;

	end generate;  -- PAR_OUT

end architecture;


