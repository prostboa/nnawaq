
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <regex.h>

#include <gtk/gtk.h>

#define PROGRAM_NAME     "imgclass-gtk"
#define PROGRAM_VERSION  "0.2"
#define PROGRAM_AUTHOR   "Adrien Prost-Boucle <adrien.prost-boucle@univ-grenoble-alpes.net>"

#define CSS_BADCLASS     "badclass"
#define CSS_STATUS_RED   "statusred"
#define CSS_BG_GREY      "graybg"

#define ACCMAP_QUIT       "<imgclass>/quit"
#define ACCMAP_ZOOMIN     "<imgclass>/zoomin"
#define ACCMAP_ZOOMOUT    "<imgclass>/zoomout"
#define ACCMAP_ZOOMRESET  "<imgclass>/zoomreset"
#define ACCMAP_REFRESH    "<imgclass>/refresh"

#define SPACING_PIX  5



//================================================
// Constants and structures
//================================================

//#define dbgprintf(fmt, ...) printf("DEBUG %s:%u - " fmt, __FILE__, __LINE__, ##__VA_ARGS__)
#define dbgprintf(fmt, ...) do { } while(0)

#define FILEMODE_NONE      0
#define FILEMODE_CIFAR10   1
#define FILEMODE_CIFAR100  2
#define FILEMODE_GTSRB     3

typedef struct image_t  image_t;
struct image_t {
	unsigned   index;
	int        class_expect;  // Negative means unset
	int        class_obtain;  // Negative means unset
	GdkPixbuf* pixbuf_orig;
	GdkPixbuf* pixbuf_scaled;
	GtkWidget* image;
	GtkWidget* label;
};

typedef struct control_t  control_t;
struct control_t {

	// Miscellaneous status variables

	gchar*   filename_image;
	gchar*   filename_class_expect;
	gchar*   filename_class_obtain;

	unsigned class_names_size;
	gchar**  class_names_arr;

	unsigned filemode;
	gchar*   dataset_dir;
	int      cifar100_class_coarse;

	int      addpix;

	// Global parameters

	double zoom;  // Zoom factor, always positive, 1 means no zoom

	// The file contents

	int**    file_data;

	unsigned width;
	unsigned height;
	unsigned colors;

	unsigned images_nb;
	image_t* images_arr;

	unsigned class_valid_nb;
	unsigned class_good_nb;
	unsigned class_wrong_nb;

	// The GUI

	GtkWidget* window;
	GtkWidget* grid;

	GtkWidget* imgview_grid;
	GtkWidget* imgview_scroll;
	unsigned   imgview_rows;
	unsigned   imgview_cols;

	GtkWidget* statusbar;

	GtkCssProvider* css_provider;
};



//================================================
// Read data files
//================================================

bool param_multiline = true;

// To emit warnings about overfull lines
static unsigned warnings_max_nb = 10;
static unsigned overfull_cur_nb = 0;
static unsigned underfull_cur_nb = 0;

void warnings_clear() {
	overfull_cur_nb = 0;
	underfull_cur_nb = 0;
}

// Create a 2-dimensions array
int** array_create_dim2(unsigned nrows, unsigned ncol) {
	int **array = malloc(nrows * sizeof(*array));
	array[0] = malloc(nrows * ncol * sizeof(**array));
	for(unsigned i=1; i<nrows; i++) array[i] = array[i-1] + ncol;
	return array;
}

// Read one frame from the file
// File encoding: data items are signed decimal values separated by other characters
// Return the number of values obtained, or -1 if already at the end of the file
int loadfile_oneframe(FILE* F, int* buf, unsigned fsize) {
	// A buffer to read one line
	static size_t linebuf_size = 0;
	static char* linebuf = NULL;
	ssize_t r;

	// Current number of values obtained
	unsigned items_nb = 0;

	do {

		// Get one line = one frame
		r = getline(&linebuf, &linebuf_size, F);
		// Exit when the end of the file is reached
		if(r < 0) break;

		// Parse the line
		char* ptr = linebuf;
		bool valneg = false;
		int32_t val = 0;

		char c = *(ptr++);
		while(c!=0) {

			// Skip until the next value
			if(c=='-') valneg = true;
			else if(c=='+') {}
			else if(c >= '0' && c <= '9') val = (c - '0');
			else if(c == 0) break;
			else { c = *(ptr++); continue; }

			// Here it is the beginning of a value. Check if it is within bounds.
			if(items_nb >= fsize) {
				if(overfull_cur_nb < warnings_max_nb) {
					printf("Warning: Cropping overfull frame (more than %u items)\n", fsize);
					if(overfull_cur_nb == warnings_max_nb-1) {
						printf("  Note: This warning was printed %u times. Further occurrences won't be displayed.\n", overfull_cur_nb);
					}
					overfull_cur_nb++;
				}
				break;
			}

			// Read the rest of the value
			c = *(ptr++);
			while(c >= '0' && c <= '9') {
				val = 10 * val + (c - '0');
				c = *(ptr++);
			};

			// Here we have a value, apply the sign
			if(valneg==true) val = -val;

			// Store the value
			buf[items_nb++] = val;

			// Clear the variables for the next value
			valneg = false;
			val = 0;

		}  // Loop that reads one line of the file

		// Exit when enough data have been obtained
		if(items_nb >= fsize) break;

		// If not wanting multi-line, exit
		if(param_multiline==false) break;

	} while(1);  // Read the lines of the file

	if(r < 0 && items_nb==0) return -1;

	if(items_nb < fsize) {
		if(underfull_cur_nb < warnings_max_nb) {
			printf("Warning: Underfull frame: %u items instead of %u\n", items_nb, fsize);
			if(underfull_cur_nb == warnings_max_nb-1) {
				printf("  Note: This warning was printed %u times. Further occurrences won't be displayed.\n", underfull_cur_nb);
			}
			underfull_cur_nb++;
		}
	}

	return items_nb;
}

// Load one file, return a 2D array, one row per frame
void loadfile(int** array, FILE* F, unsigned nframes, unsigned fsize) {
	unsigned curframe = 0;

	warnings_clear();

	do {

		// Get one frame
		int r = loadfile_oneframe(F, array[curframe], fsize);
		if(r > 0) curframe ++;

		// If the big buffer contains enough frames, exit
		if(curframe >= nframes) break;

		// Exit when the end of the file is reached
		if(r < 0) {
			printf("Warning: Only got %u frames instead of %u\n", curframe, nframes);
			break;
		}

	} while(1);  // Read the lines of the file
}



//================================================
// Window title
//================================================

static void window_title_update(control_t *CT) {
	if(CT->window==NULL) return;

	char buf[1000];
	sprintf(buf, PROGRAM_NAME);

	if(CT->filemode==FILEMODE_NONE) {
		if(CT->filename_image != NULL) {
			sprintf(buf+strlen(buf), " - %s", CT->filename_image);
		}
	}
	else if(CT->filemode==FILEMODE_CIFAR10) {
		sprintf(buf+strlen(buf), " - CIFAR10");
	}
	else if(CT->filemode==FILEMODE_CIFAR100) {
		sprintf(buf+strlen(buf), " - CIFAR100");
	}
	else if(CT->filemode==FILEMODE_GTSRB) {
		sprintf(buf+strlen(buf), " - GTSRB");
	}

	gtk_window_set_title(GTK_WINDOW(CT->window), buf);
}



//================================================
// Status bar
//================================================

static void statusbar_set_internal(control_t *CT, const char* msg) {
	if(CT->window==NULL) return;
	gtk_statusbar_remove_all(GTK_STATUSBAR(CT->statusbar), 0);
	gtk_statusbar_push(GTK_STATUSBAR(CT->statusbar), 0, msg);
}

static void statusbar_set(control_t *CT, const char* msg) {
	if(CT->window==NULL) return;
	// Ensure we use the default color
	GtkStyleContext* context = gtk_widget_get_style_context(CT->statusbar);
	gtk_style_context_remove_class(context, CSS_STATUS_RED);
	// Write the message
	statusbar_set_internal(CT, msg);
}

static void statusbar_set_error(control_t *CT, const char* msg) {
	if(CT->window==NULL) return;
	// Change the color
	GtkStyleContext* context = gtk_widget_get_style_context(CT->statusbar);
	gtk_style_context_add_class(context, CSS_STATUS_RED);
	// Write the message
	statusbar_set_internal(CT, msg);
}

static void statusbar_set_auto(control_t *CT) {
	if(CT->window==NULL) return;

	char buf[1000];

	sprintf(buf, "File loaded: %u images", CT->images_nb);

	if(CT->class_valid_nb > 0) {
		sprintf(buf+strlen(buf), ", accuracy %g%%, error rate %g%%",
			(double)CT->class_good_nb / CT->class_valid_nb * 100.0,
			(double)CT->class_wrong_nb / CT->class_valid_nb * 100.0
		);
		if(CT->class_valid_nb < CT->images_nb) {
			sprintf(buf+strlen(buf), ", valid comparisons %u/%u", CT->class_valid_nb, CT->images_nb);
		}
	}
	else {
		sprintf(buf+strlen(buf), ", no accuracy stats available");
	}

	if(CT->zoom != 1) {
		sprintf(buf+strlen(buf), " (zoom %.4g)", CT->zoom);
	}

	statusbar_set(CT, buf);
}



//================================================
// Load/Save
//================================================

#define GTSRB_ROI_REG "^(.+);(.+);(.+);(.+);(.+);(.+);(.+);(.+)$"

// Create the image structures
static void images_arr_create(control_t *CT) {
	// Allocation
	CT->images_arr = calloc(CT->images_nb, sizeof(*CT->images_arr));
	// Initialization
	for(unsigned i=0; i<CT->images_nb; i++) {
		image_t* image = CT->images_arr + i;
		image->index = i;
		image->class_expect = -1;
		image->class_obtain = -1;
	}
}

static void file_load(control_t *CT) {
	unsigned error_id = 0;

	unsigned fsize = CT->width * CT->height * CT->colors;

	// Load the file

	if(CT->filemode == FILEMODE_NONE) {
		if(CT->filename_image == NULL) goto EPILOGUE;

		FILE* F = fopen(CT->filename_image, "rb");
		if(F==NULL) {
			printf("Loading error: Could not open file: %s\n", CT->filename_image);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		// Load the file
		int** arrdata = array_create_dim2(CT->images_nb, fsize);
		loadfile(arrdata, F, CT->images_nb, fsize);
		fclose(F);
		CT->file_data = arrdata;

		// Create the image structures
		images_arr_create(CT);
	}

	else if(CT->filemode == FILEMODE_CIFAR10) {
		char buf[1024];
		FILE* F;

		if(CT->filename_image==NULL) sprintf(buf, "%s/test_batch.bin", CT->dataset_dir);
		else sprintf(buf, "%s/%s", CT->dataset_dir, CT->filename_image);

		// Load the file

		F = fopen(buf, "rb");
		if(F==NULL) {
			printf("Loading error: Could not open file: %s\n", buf);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		// Create the image structures
		images_arr_create(CT);

		// Load the file
		int** arrdata = array_create_dim2(CT->images_nb, fsize);
		for(unsigned f=0; f<CT->images_nb; f++) {
			CT->images_arr[f].class_expect = fgetc_unlocked(F);
			for(unsigned i=0; i<3072; i++) arrdata[f][i] = fgetc_unlocked(F);
		}
		fclose(F);
		CT->file_data = arrdata;

		// Load the classes

		sprintf(buf, "%s/batches.meta.txt", CT->dataset_dir);
		F = fopen(buf, "rb");
		if(F==NULL) {
			printf("Loading error: Could not open file: %s\n", buf);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		CT->class_names_size = 10;
		CT->class_names_arr = calloc(CT->class_names_size, sizeof(*CT->class_names_arr));

		for(unsigned c=0; c<CT->class_names_size; c++) {
			fscanf(F, "%s", buf);
			CT->class_names_arr[c] = strdup(buf);
		}
		fclose(F);

	}

	else if(CT->filemode == FILEMODE_CIFAR100) {
		char buf[1024];
		FILE* F;

		if(CT->filename_image==NULL) sprintf(buf, "%s/test.bin", CT->dataset_dir);
		else sprintf(buf, "%s/%s", CT->dataset_dir, CT->filename_image);

		// Load the file

		F = fopen(buf, "rb");
		if(F==NULL) {
			printf("Loading error: Could not open file: %s\n", buf);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		// Create the image structures
		images_arr_create(CT);

		// Load the file
		int** arrdata = array_create_dim2(CT->images_nb, fsize);
		for(unsigned f=0; f<CT->images_nb; f++) {
			int class_coarse = fgetc_unlocked(F);
			int class_fine = fgetc_unlocked(F);
			CT->images_arr[f].class_expect = class_fine;
			if(CT->cifar100_class_coarse != 0) CT->images_arr[f].class_expect = class_coarse;
			for(unsigned i=0; i<3072; i++) arrdata[f][i] = fgetc_unlocked(F);
		}
		fclose(F);
		CT->file_data = arrdata;

		// Load the classes

		if(CT->cifar100_class_coarse != 0) {
			CT->class_names_size = 20;
			sprintf(buf, "%s/coarse_label_names.txt", CT->dataset_dir);
		}
		else {
			CT->class_names_size = 100;
			sprintf(buf, "%s/fine_label_names.txt", CT->dataset_dir);
		}

		F = fopen(buf, "rb");
		if(F==NULL) {
			printf("Loading error: Could not open file: %s\n", buf);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		CT->class_names_arr = calloc(CT->class_names_size, sizeof(*CT->class_names_arr));

		for(unsigned c=0; c<CT->class_names_size; c++) {
			fscanf(F, "%s", buf);
			CT->class_names_arr[c] = strdup(buf);
		}
		fclose(F);

	}

	else if(CT->filemode == FILEMODE_GTSRB) {
		char buf[1024];
		FILE* F;

		// Create the raw image data
		int** arrdata = array_create_dim2(CT->images_nb, fsize);

		// Create the image structures
		images_arr_create(CT);

		// Load the image files

		// FIXME Open the image details file to get ROI
		sprintf(buf, "%s/Final_Test/Images/GT-final_test.csv", CT->dataset_dir);
		FILE* Froi = fopen(buf, "rb");
		if(Froi==NULL) {
			printf("Loading error: Could not open file: %s\n", buf);
			statusbar_set_error(CT, "Loading error: Could not open file");
			error_id = __LINE__;
			goto EPILOGUE;
		}

		int regcode;
		regex_t regex_roi;
		regcode = regcomp(&regex_roi, GTSRB_ROI_REG, REG_EXTENDED);
		if(regcode!=0) {
			printf("Error: Can't compile regexp: %s\n", GTSRB_ROI_REG);
			exit(EXIT_FAILURE);
		}

		regmatch_t regmatch_arr[10];

		char* linebuf = NULL;
		size_t linebuf_size = 0;
		int roi_lineidx = 0;

		for(unsigned f=0; f<CT->images_nb; f++) {

			// Get next file name + ROI
			unsigned roi_x1 = 0, roi_x2 = 0, roi_y1 = 0, roi_y2 = 0;
			unsigned class_id = 0;
			char* filename = NULL;

			int z = 0;
			for( ; ; roi_lineidx++) {
				z = getline(&linebuf, &linebuf_size, Froi);
				if(z < 0) break;
				if(roi_lineidx==0) continue;
				if(linebuf[0]=='#') continue;
				// Remove trailing newline if any
				unsigned len = strlen(linebuf);
				while(len > 0 && isspace(linebuf[len-1])!=0) { len--; linebuf[len] = 0; }
				// Check if it matches the regexp
				//printf("DEBUG linebuf: %s\n", linebuf);
				regcode = regexec(&regex_roi, linebuf, 10, regmatch_arr, 0);
				if(regcode != 0) {
					//printf("DEBUG No match: %s\n", linebuf);
					getchar();
					continue;
				}
				// Replace the characters ';' by zero to split the line
				//printf("DEBUG Match: %s\n", linebuf);
				for(unsigned i=0; i<len; i++) if(linebuf[i]==';') linebuf[i] = 0;
				// Get the fields
				filename = linebuf + regmatch_arr[1].rm_so;
				roi_x1   = atoi(linebuf + regmatch_arr[4].rm_so);
				roi_y1   = atoi(linebuf + regmatch_arr[5].rm_so);
				roi_x2   = atoi(linebuf + regmatch_arr[6].rm_so);
				roi_y2   = atoi(linebuf + regmatch_arr[7].rm_so);
				class_id = atoi(linebuf + regmatch_arr[8].rm_so);
				break;
			}  // Read the ROI lines

			if(z < 0) {
				printf("Loading error: Not enough files\n");
				statusbar_set_error(CT, "Loading error: Not enough files");
				error_id = __LINE__;
				goto EPILOGUE;
			}

			sprintf(buf, "%s/Final_Test/Images/%s", CT->dataset_dir, filename);
			F = fopen(buf, "rb");
			if(F==NULL) {
				printf("Loading error: Could not open file: %s\n", buf);
				statusbar_set_error(CT, "Loading error: Could not open file");
				error_id = __LINE__;
				goto EPILOGUE;
			}

			// Load the file
			fscanf(F, "%s", buf);  // Read the code P6
			unsigned file_width, file_height;
			fscanf(F, "%u", &file_width);
			fscanf(F, "%u", &file_height);
			fscanf(F, "%s", buf);  // Read the depth 255
			fgetc(F);  // Read the space after the depth

			GdkPixbuf* pixbuf_orig = NULL;
			GdkPixbuf* pixbuf_crop = NULL;
			GdkPixbuf* pixbuf_scale = NULL;

			pixbuf_orig = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, file_width, file_height);
			unsigned char* pixdata_orig = gdk_pixbuf_get_pixels(pixbuf_orig);
			unsigned rowstride = gdk_pixbuf_get_rowstride(pixbuf_orig);
			unsigned buflen = gdk_pixbuf_get_byte_length(pixbuf_orig);
			unsigned pixlen = 3;

			// Clear the image
			memset(pixdata_orig, 0, buflen);
			// Fill the image
			for(unsigned h=0; h<file_height; h++) {
				for(unsigned w=0; w<file_width; w++) {
					for(unsigned c=0; c<3; c++) pixdata_orig[h * rowstride + w * pixlen + c] = fgetc(F);
				}
			}
			fclose(F);

			// Crop the image
			unsigned roi_width = roi_x2 - roi_x1 + 1;
			unsigned roi_height = roi_y2 - roi_y1 + 1;
			pixbuf_crop = gdk_pixbuf_new_subpixbuf(pixbuf_orig, roi_x1, roi_y1, roi_width, roi_height);

			// Scale the image
			pixbuf_scale = gdk_pixbuf_scale_simple(pixbuf_crop, CT->width, CT->height, GDK_INTERP_BILINEAR);
			unsigned char* pixdata_scale = gdk_pixbuf_get_pixels(pixbuf_scale);
			unsigned rowstride_scale = gdk_pixbuf_get_rowstride(pixbuf_scale);

			// Save the scaled image to the array
			for(unsigned c=0; c<CT->colors; c++) {
				for(unsigned h=0; h<CT->height; h++) {
					for(unsigned w=0; w<CT->width; w++) {
						arrdata[f][c * CT->width * CT->height + h * CT->width + w] = pixdata_scale[h * rowstride_scale + w * pixlen + c];
					}
				}
			}

			// Free the pixbufs
			// FIXME are they floating?
			g_object_unref(pixbuf_orig);
			g_object_unref(pixbuf_crop);
			g_object_unref(pixbuf_scale);

			// Set the class ID
			image_t* image = CT->images_arr + f;
			image->class_expect = class_id;

		}  // Process all image files

		CT->file_data = arrdata;

	}  // Dataset GTSRB

	else {
		printf("ERROR invalide filemode %u\n", CT->filemode);
	}

	// Add a constant to the pixels if needed

	if(CT->addpix != 0) {
		for(unsigned f=0; f<CT->images_nb; f++) {
			for(unsigned i=0; i<fsize; i++) {
				CT->file_data[f][i] += CT->addpix;
			}
		}
	}

	// Load the file for reference and obtained classifications

	if(CT->filename_class_expect != NULL) {
		FILE* F = fopen(CT->filename_class_expect, "rb");
		if(F==NULL) {
			printf("Warning: Can't open file for reference classifications: %s\n", CT->filename_class_expect);
		}
		else {
			char buf[1000];
			for(unsigned i=0; i<CT->images_nb; i++) {
				buf[0] = 0;
				fscanf(F, "%s", buf);
				if(buf[0]==0) break;
				CT->images_arr[i].class_expect = atoi(buf);
			}
			fclose(F);
		}
	}

	if(CT->filename_class_obtain != NULL) {
		FILE* F = fopen(CT->filename_class_obtain, "rb");
		if(F==NULL) {
			printf("Warning: Can't open file for obtained classifications: %s\n", CT->filename_class_obtain);
		}
		else {
			char buf[1000];
			for(unsigned i=0; i<CT->images_nb; i++) {
				buf[0] = 0;
				fscanf(F, "%s", buf);
				if(buf[0]==0) break;
				CT->images_arr[i].class_obtain = atoi(buf);
			}
			fclose(F);
		}
	}

	// Count classifications stats

	for(unsigned i=0; i<CT->images_nb; i++) {
		image_t* image = CT->images_arr + i;
		if(image->class_expect < 0 || image->class_obtain < 0) continue;
		CT->class_valid_nb++;
		if(image->class_expect==image->class_obtain) CT->class_good_nb++;
		else CT->class_wrong_nb++;
	}

	// Create the grid space (GUI mode only)

	if(CT->window!=NULL) {
		CT->imgview_rows = (CT->images_nb + CT->imgview_cols - 1) / CT->imgview_cols;

		for(unsigned i=0; i<CT->imgview_cols; i++) gtk_grid_insert_column(GTK_GRID(CT->imgview_grid), i);
		for(unsigned i=0; i<CT->imgview_rows; i++) {
			gtk_grid_insert_row(GTK_GRID(CT->imgview_grid), 2*i);
			gtk_grid_insert_row(GTK_GRID(CT->imgview_grid), 2*i+1);
		}

		// Write the number of images + accuracy stats in the status bar
		statusbar_set_auto(CT);
	}

	EPILOGUE:

	// Handle errors: clear the file name
	if(error_id != 0 && CT->filename_image != NULL) {
		g_free(CT->filename_image);
		CT->filename_image = NULL;
	}

	// Add the file name to the window title
	window_title_update(CT);
}



//================================================
// Callback functions
//================================================

static void gen_class_text(char* buf, int id, unsigned image_idx, control_t *CT) {
	if(CT->class_names_size > 0 && id >= 0 && id < CT->class_names_size) {
		sprintf(buf, "%s (%i)", CT->class_names_arr[id], id);
	}
	else if(id >= 0) {
		sprintf(buf, "class %i", id);
	}
	else {
		sprintf(buf, "image %u", image_idx);
	}
}

static void callback_refresh(control_t *CT) {
	char buf[1000];

	// Re-generate the images, with the new zoom value

	for(unsigned i=0; i<CT->images_nb; i++) {
		image_t* image = CT->images_arr + i;
		if(image->pixbuf_orig!=NULL) continue;

		image->pixbuf_orig = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, CT->width, CT->height);

		unsigned char* pixdata = gdk_pixbuf_get_pixels(image->pixbuf_orig);

		unsigned rowstride = gdk_pixbuf_get_rowstride(image->pixbuf_orig);
		unsigned buflen = gdk_pixbuf_get_byte_length(image->pixbuf_orig);
		unsigned pixlen = 3;

		// Clear the image
		memset(pixdata, 0, buflen);

		// Fill the image
		for(unsigned h=0; h<CT->height; h++) {
			for(unsigned w=0; w<CT->width; w++) {

				// Get RGB colors
				unsigned R, G, B;
				if(CT->colors >= 3) {
					R = CT->file_data[i][h * CT->width + w + 0 * CT->width * CT->height];
					G = CT->file_data[i][h * CT->width + w + 1 * CT->width * CT->height];
					B = CT->file_data[i][h * CT->width + w + 2 * CT->width * CT->height];
				}
				else {
					R = CT->file_data[i][h * CT->width + w];
					G = CT->file_data[i][h * CT->width + w];
					B = CT->file_data[i][h * CT->width + w];
				}

				// Set the pixel color
				unsigned off = h * rowstride + w * pixlen;
				pixdata[off+0] = R;
				pixdata[off+1] = G;
				pixdata[off+2] = B;
			}
		}

	}  // Process all images

	// Scale images

	if(CT->zoom < 0) CT->zoom = 1;
	unsigned width  = CT->zoom * CT->width;
	unsigned height = CT->zoom * CT->height;
	if(width < 1) width = 1;
	if(height < 1) height = 1;

	for(unsigned i=0; i<CT->images_nb; i++) {
		image_t* image = CT->images_arr + i;

		// Drop previous scaled version, if any
		if(image->pixbuf_scaled!=NULL) {
			g_object_unref(image->pixbuf_scaled);
			image->pixbuf_scaled = NULL;
		}

		GdkPixbuf* pixbuf_use = image->pixbuf_orig;

		// Scale the image if needed
		if(width != CT->width && height != CT->height) {
			image->pixbuf_scaled = gdk_pixbuf_scale_simple(image->pixbuf_orig, width, height, GDK_INTERP_NEAREST);
			pixbuf_use = image->pixbuf_scaled;
		}

		unsigned view_row = 2 * (i / CT->imgview_cols);
		unsigned view_col = i % CT->imgview_cols;

		if(image->image==NULL) {
			// Create the image widget
			image->image = gtk_image_new_from_pixbuf(pixbuf_use);
			// Add the image to the grid
			gtk_grid_attach(GTK_GRID(CT->imgview_grid), image->image, view_col, view_row, 1, 1);
		}

		// Update the image widget
		else {
			gtk_image_set_from_pixbuf(GTK_IMAGE(image->image), pixbuf_use);
		}

		// Create the label
		if(image->label==NULL) {
			image->label = gtk_label_new(NULL);
			gen_class_text(buf, image->class_expect, image->index, CT);
			if(image->class_obtain >= 0) {
				unsigned len = strlen(buf);
				buf[len++] = '\n';
				gen_class_text(buf+len, image->class_obtain, image->index, CT);
			}
			gtk_label_set_label(GTK_LABEL(image->label), buf);
			gtk_grid_attach(GTK_GRID(CT->imgview_grid), image->label, view_col, view_row+1, 1, 1);
			// Set red color if classification is wrong
			if(image->class_expect >= 0 && image->class_obtain >= 0 && image->class_expect != image->class_obtain) {
				GtkStyleContext* context = gtk_widget_get_style_context(image->label);
				gtk_style_context_add_class(context, CSS_BADCLASS);
			}
		}

	}  // Process all images

	// Write the number of images + accuracy stats in the status bar
	statusbar_set_auto(CT);
}

// Callbacks for zoom

static void callback_zoomin(control_t *CT) {
	CT->zoom *= 1.1;
	callback_refresh(CT);
}

static void callback_zoomout(control_t *CT) {
	CT->zoom /= 1.1;
	callback_refresh(CT);
}

static void callback_zoomreset(control_t *CT) {
	CT->zoom = 1;
	callback_refresh(CT);
}

// Callbacks for scroll event for zoom

static gboolean callback_scroll_for_zoom(GtkWidget *widget, GdkEvent *event, control_t *CT) {
	if(event->type != GDK_SCROLL) return TRUE;

	GdkEventScroll* event_scroll = (GdkEventScroll*)event;
	GdkModifierType modifiers = gtk_accelerator_get_default_mod_mask();

	if( (event_scroll->state & modifiers) == GDK_CONTROL_MASK ) {
		gboolean want_in = FALSE;
		gboolean want_out = FALSE;

		//printf("Scroll+Ctrl\n");

		if(event_scroll->direction == GDK_SCROLL_DOWN) {
			want_out = TRUE;
		}
		else if(event_scroll->direction == GDK_SCROLL_UP) {
			want_in = TRUE;
		}
		else if(event_scroll->direction == GDK_SCROLL_SMOOTH) {
			//printf("Scroll+Ctrl smooth argh dx %g dy %g\n", event_scroll->delta_x, event_scroll->delta_y);
			if(event_scroll->delta_x==0 && event_scroll->delta_y < 0) want_in = TRUE;
			if(event_scroll->delta_x==0 && event_scroll->delta_y > 0) want_out = TRUE;
		}

		if(want_in==TRUE) {
			callback_zoomin(CT);
			return TRUE;
		}
		if(want_out==TRUE) {
			callback_zoomout(CT);
			return TRUE;
		}
	}

	return FALSE;
}

// Callbacks for main window

static void callback_about(control_t *CT) {
	// Create the dialog box
	GtkWidget *dialog = gtk_message_dialog_new(
		GTK_WINDOW(CT->window),
		GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO,
		GTK_BUTTONS_OK,
		"This is " PROGRAM_NAME " version " PROGRAM_VERSION "\n"
		"By " PROGRAM_AUTHOR "\n"
		"\n"
		"This program is free software.\n"
		"It comes without absolutely NO WARRANTY.\n"
	);
	gtk_window_set_title(GTK_WINDOW(dialog), "About");
	// Run and destroy
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static gboolean callback_quit(control_t *CT) {

	// Indicate to GTK that it has to destroy all and quit
	gtk_main_quit();
	gtk_main_iteration();

	// return FALSE and the main window will be destroyed with a "delete_event".
	// Return TRUE if nothing has to be done.
	return FALSE;
}



//================================================
// Main function
//================================================

int launch_gui(control_t *CT) {

	// GTK+ Initialization
	gtk_init(NULL, NULL);

	// The table (a grid) that will contain all images

	CT->imgview_grid = gtk_grid_new();
	gtk_widget_set_hexpand(CT->imgview_grid, TRUE);
	gtk_widget_set_vexpand(CT->imgview_grid, TRUE);

	gtk_grid_set_column_homogeneous(GTK_GRID(CT->imgview_grid), TRUE);

	gtk_grid_set_row_spacing(GTK_GRID(CT->imgview_grid), SPACING_PIX);
	gtk_grid_set_column_spacing(GTK_GRID(CT->imgview_grid), SPACING_PIX);

	CT->imgview_scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(CT->imgview_scroll), CT->imgview_grid);

	g_signal_connect(G_OBJECT(CT->imgview_scroll), "scroll-event", G_CALLBACK(callback_scroll_for_zoom), CT);

	// The status bar

	CT->statusbar = gtk_statusbar_new();
	gtk_widget_set_hexpand(CT->statusbar, TRUE);
	gtk_widget_set_hexpand(CT->statusbar, FALSE);

	// Create a CSS context for the entire application
	// This enables to change text color, draw animations, etc
	// Documentation: https://people.gnome.org/~desrt/gtk/html/GtkCssProvider.html
	// Documentation: https://developer.gnome.org/gtk3/stable/chap-css-overview.html
	// Documentation: https://developer.gnome.org/gtk3/stable/gtk-migrating-GtkStyleContext-css.html
	// Documentation: https://developer.gnome.org/gtk3/stable/GtkStyleContext.html
	// Documentation: https://developer.gnome.org/gtk3/stable/GtkCssProvider.html

	GdkDisplay* display = gdk_display_get_default();
	GdkScreen* screen   = gdk_display_get_default_screen(display);
	CT->css_provider = gtk_css_provider_new();
	gtk_style_context_add_provider_for_screen(screen, GTK_STYLE_PROVIDER(CT->css_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	g_object_unref(CT->css_provider);

	gtk_css_provider_load_from_data(CT->css_provider,
		"."CSS_BADCLASS"   { color: rgb(200,0,0); font-weight: bold; }\n"
		"."CSS_STATUS_RED" { color: rgb(200,0,0); }\n"
		"."CSS_BG_GREY" *  { color: rgb(180,180,180); }\n"
		, -1, NULL
	);

	// Fill widgets in the window

	// Creation of the main window
	CT->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width(GTK_CONTAINER(CT->window), SPACING_PIX);

	// Function to call when the delete event is received
	g_signal_connect_swapped(G_OBJECT(CT->window), "delete_event", G_CALLBACK(callback_quit), CT);

	CT->grid = gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(CT->window), CT->grid);

	gtk_grid_set_row_spacing(GTK_GRID(CT->grid), SPACING_PIX);
	gtk_grid_set_column_spacing(GTK_GRID(CT->grid), SPACING_PIX);

	gtk_grid_attach_next_to(GTK_GRID(CT->grid), CT->imgview_scroll, NULL, GTK_POS_BOTTOM, 1, 1);
	gtk_grid_attach_next_to(GTK_GRID(CT->grid), CT->statusbar, NULL, GTK_POS_BOTTOM, 1, 1);

	// Create keyboard shortcuts (accelerators)

	gtk_accel_map_add_entry(ACCMAP_QUIT,      GDK_KEY_Q,     GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_ZOOMIN,    GDK_KEY_plus,  GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_ZOOMOUT,   GDK_KEY_minus, GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_ZOOMRESET, GDK_KEY_0,     GDK_CONTROL_MASK);
	gtk_accel_map_add_entry(ACCMAP_REFRESH,   GDK_KEY_R,     GDK_CONTROL_MASK);

	GtkAccelGroup* accel_group = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(CT->window), accel_group);

	GClosure* closure = NULL;

	closure = g_cclosure_new_swap(G_CALLBACK(callback_quit), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_QUIT, closure);

	closure = g_cclosure_new_swap(G_CALLBACK(callback_refresh), CT, NULL);
	gtk_accel_group_connect_by_path(accel_group, ACCMAP_REFRESH, closure);

	// Add a menu

	GtkWidget* menubar = gtk_menu_bar_new();
	gtk_menu_bar_set_pack_direction(GTK_MENU_BAR(menubar), GTK_PACK_DIRECTION_LTR);
	gtk_menu_bar_set_child_pack_direction(GTK_MENU_BAR(menubar), GTK_PACK_DIRECTION_TTB);

	GtkWidget* menuitem = NULL;
	GtkWidget* submenu = NULL;

	submenu = gtk_menu_new();
	gtk_menu_set_accel_group(GTK_MENU(submenu), accel_group);

	menuitem = gtk_menu_item_new_with_mnemonic("_Quit");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_QUIT);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_quit), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_File");
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

	submenu = gtk_menu_new();
	gtk_menu_set_accel_group(GTK_MENU(submenu), accel_group);

	menuitem = gtk_menu_item_new_with_mnemonic("Zoom _In");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_ZOOMIN);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_zoomin), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("Zoom _Out");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_ZOOMOUT);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_zoomout), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("Zoom _None");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_ZOOMRESET);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_zoomreset), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Refresh");
	gtk_menu_item_set_accel_path(GTK_MENU_ITEM(menuitem), ACCMAP_REFRESH);
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_refresh), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(submenu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_View");
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);

	menuitem = gtk_menu_item_new_with_mnemonic("_About");
	g_signal_connect_swapped(G_OBJECT(menuitem), "activate", G_CALLBACK(callback_about), CT);
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuitem);

	// Add the menubar to the window
	gtk_grid_attach_next_to(GTK_GRID(CT->grid), menubar, NULL, GTK_POS_TOP, 1, 1);

	// Initial welcome message in status bar

	statusbar_set(CT, "This is " PROGRAM_NAME " version " PROGRAM_VERSION);
	window_title_update(CT);

	// Try to load the specified file

	file_load(CT);

	callback_refresh(CT);

	// Main GUI loop

	// Display the main window
	gtk_widget_show_all(CT->window);

	// The GTK infinite loop
	gtk_main();

	return EXIT_SUCCESS;
}

void print_usage(int argc, char** argv) {
	printf("Usage: %s [options] [<file>]\n", argv[0]);
	printf(
		"Options:\n"
		"  --help, -h        Display this help message and exit\n"
		"  --version         Display the version and exit\n"
		"  -nogui            Don't create GUI, just print in terminal\n"
		"  -addpix <val>     Add a constant to all pixels\n"
		"  -cifar10 <dir>    Specify CIFAR10 mode\n"
		"  -cifar100 <dir>   Specify CIFAR100 mode\n"
		"  -cifar100-coarse  For CIFAR100, use coarse classes\n"
		"  -gtsrb <dir>      Specify GTSRB mode\n"
		"  -class-ref <file> File for reference classifications\n"
		"  -class-get <file> File for obtained classifications\n"
	);
}

int main(int argc, char** argv) {

	if(argc <= 1) {
		print_usage(argc, argv);
		exit(EXIT_SUCCESS);
	}

	// Create the main data structure

	control_t *CT = calloc(1, sizeof(*CT));
	if(CT==NULL) {
		printf("ERROR %s:%u: memory allocation.\n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}

	CT->width  = 32;
	CT->height = 32;
	CT->colors = 3;

	CT->imgview_rows = 10;
	CT->imgview_cols = 10;

	CT->images_nb = 20;

	CT->zoom = 1;

	// Parse command-line parameters

	bool enable_gui = true;

	unsigned argi = 1;
	do {
		if(argi >= argc) break;
		char* arg = argv[argi];
		if(arg[0]==0) break;

		// Get parameters
		char* getparam_str() {
			argi++;
			if(argi >= argc) {
				printf("Error: Missing parameters after '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
			return argv[argi];
		}

		if(strcmp(arg, "--help")==0 || strcmp(arg, "-h")==0) {
			print_usage(argc, argv);
			exit(EXIT_SUCCESS);
		}
		else if(strcmp(arg, "--version")==0 || strcmp(arg, "--about")==0) {
			printf(PROGRAM_NAME " version " PROGRAM_VERSION "\n");
			printf("By " PROGRAM_AUTHOR "\n");
			printf("This program comes with ABSOLUTELY NO WARRANTY.\n");
			exit(EXIT_SUCCESS);
		}

		else if(strcmp(arg, "-nogui")==0) {
			enable_gui = false;
		}

		else if(strcmp(arg, "-fn")==0) {
			CT->images_nb = atoi(getparam_str());
		}

		else if(strcmp(arg, "-cifar10")==0) {
			CT->filemode = FILEMODE_CIFAR10;
			CT->dataset_dir = getparam_str();
		}
		else if(strcmp(arg, "-cifar100")==0) {
			CT->filemode = FILEMODE_CIFAR100;
			CT->dataset_dir = getparam_str();
		}
		else if(strcmp(arg, "-gtsrb")==0) {
			CT->filemode = FILEMODE_GTSRB;
			CT->dataset_dir = getparam_str();
		}
		else if(strcmp(arg, "-coarse")==0) {
			CT->cifar100_class_coarse = 1;
		}
		else if(strcmp(arg, "-addpix")==0) {
			CT->addpix = atoi(getparam_str());
		}

		else if(strcmp(arg, "-class-ref")==0) {
			CT->filename_class_expect = getparam_str();
		}
		else if(strcmp(arg, "-class-get")==0) {
			CT->filename_class_obtain = getparam_str();
		}

		else if(arg[0]=='-') {
			printf("Error: Unknown parameter '%s'\n", arg);
			exit(EXIT_FAILURE);
		}

		else {
			if(CT->filename_image!=NULL) {
				printf("Error: Multiple file names are given, only one is authorized\n");
				exit(EXIT_FAILURE);
			}
			CT->filename_image = g_strdup(arg);
		}

		// Increment parameter index
		argi++;

	} while(1);  // Scan command-line parameters

	// Launch the application

	if(enable_gui==true) {

		// Create the GUI
		launch_gui(CT);

	}

	else {

		// Try to load the specified file
		file_load(CT);

		// Print messages
		printf("File loaded: %u images\n", CT->images_nb);

		if(CT->class_valid_nb > 0) {
			printf("Accuracy: good %g%%, wrong %g%%, valid comparisons %u/%u\n",
				(double)CT->class_good_nb / CT->class_valid_nb * 100.0,
				(double)CT->class_wrong_nb / CT->class_valid_nb * 100.0,
				CT->class_valid_nb, CT->images_nb
			);
		}
		else {
			printf("Accuracy: No accuracy stats available\n");
		}

	}

	return EXIT_SUCCESS;
}

