-- This is a parallel pooling component, capable of MaxPool, MinPool, AvgPool
-- Architecture : There are NPOOL pooling paths in each of the PAR_OUT chains
-- Each internal pooling unit receives PAR_IN / (PAR_OUT * NPOOL) input data per clock cycle
-- All pooling paths share the same state machine
-- PAR_IN data arrive per clock cycle
-- PAR_OUT data leave per clock cycle

-- About average pooling :
-- The division is performed by multiplication + division by a power of 2 with rounding towards zero

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_pooling is
	generic(
		WDATA   : natural := 8;
		SDATA   : boolean := true;
		WOUT    : natural := 8;
		-- Frame size and number of units
		FSIZE   : natural := 4;
		NPOOL   : natural := 1;
		-- The type of pooling
		OPMAX   : boolean := true;
		OPMIN   : boolean := false;
		OPADD   : boolean := false;
		-- Parameters for Average pooling
		MULT    : natural := 1;
		SHR     : natural := 0;
		-- Activate rounding to nearest integer (default is rounding is towards zero)
		ROUND_NEAR : boolean := false;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED  : boolean := false
	);
	port(
		clk           : in  std_logic;
		clear         : in  std_logic;
		-- Run-time frame dimensions
		user_fsize    : in  std_logic_vector(15 downto 0);
		-- Data input
		in_data       : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		in_rdy        : out std_logic;
		in_ack        : in  std_logic;
		-- Data output
		out_data      : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
		out_rdy       : out std_logic;
		out_fifo_room : in  std_logic_vector(15 downto 0)
	);
end nnlayer_pooling;

architecture synth of nnlayer_pooling is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Compute the ceil(log()) in any base, this is useful to evaluate the max number of pipeline stages of a computing tree
	function log_in_base(vin : natural; b : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / b;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : std_logic) return natural is
	begin
		if b = '1' then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- Utility function to descale numbers with rounding towards zero
	function descale_towards_zero(inval : signed; shr : natural) return signed is
		-- Variables for padding and correction
		variable pad0 : signed(inval'length-1 downto 0) := (others => '0');
		variable sgn1 : signed(inval'length-1 downto 0) := (others => '0');
		-- After shift by SHR
		variable val1 : signed(inval'length*2-1 downto 0) := (others => '0');
		-- After correction for data<0
		variable val2 : signed(inval'length*2-1 downto 0) := (others => '0');
	begin
		-- Prepare values for padding and correction
		pad0 := (others => '0');
		sgn1 := (others => inval(inval'high));
		-- Append padding to hold the decimal bits
		val1 := shift_right(inval & pad0, shr);
		-- Add a correction +1 if negative and if the decimal bits are non-zero
		val2 := val1 + (pad0 & sgn1) + 1;
		return val2(inval'length*2-1 downto inval'length);
	end function;
	-- Variant for unsigned numbers
	function descale_towards_zero(inval : unsigned; shr : natural) return unsigned is
	begin
		return shift_right(inval, shr);
	end function;
	-- Variant for selectable output width and signedness
	function descale_towards_zero(inval : std_logic_vector; shr : natural; outsize : natural; sgn : boolean) return std_logic_vector is
	begin
		if sgn = true then
			return std_logic_vector(resize(descale_towards_zero(signed(inval), shr), outsize));
		end if;
		return std_logic_vector(resize(descale_towards_zero(unsigned(inval), shr), outsize));
	end function;

	-- Utility function to descale numbers with rounding towards nearest
	function descale_towards_nearest(inval : signed; shr : natural) return signed is
		-- Variables for padding and correction
		variable pad0 : signed(inval'length-1 downto 0) := (others => '0');
		variable sgn1 : signed(inval'length-1 downto 0) := (others => '0');
		-- After shift by SHR
		variable val1 : signed(inval'length*2-1 downto 0) := (others => '0');
		-- After correction for data<0
		variable val2 : signed(inval'length*2-1 downto 0) := (others => '0');
	begin
		-- Prepare values for padding and correction
		pad0 := (others => '0');
		sgn1 := (others => inval(inval'high));
		-- Append padding to hold the decimal bits
		val1 := shift_right(inval & pad0, shr);
		-- Add the corrections
		-- Explanation :
		-- There are normally 2 corrections to perform :
		--   + 0000 ssss (for rounding towards zero)
		--   + ssss 1000 (for rounding to nearest)
		-- Which can be rewritten as :
		--   + ssss ssss
		--   + 0000 1000
		-- So the two situations are :
		--   + 0000 1000 if sign=0
		--   + 0000 0sss if sign=1
		-- Overall, this can be simplified to :
		--   + 0000 & not(s) & sss
		val2 := val1 + (pad0 & not(sgn1(inval'high)) & sgn1(inval'high-1 downto 0));
		return val2(inval'length*2-1 downto inval'length);
	end function;
	-- Variant for unsigned numbers
	function descale_towards_nearest(inval : unsigned; shr : natural) return unsigned is
		-- After shift by SHR
		variable val1 : unsigned(inval'length downto 0) := (others => '0');
		-- After addition of correction
		variable val2 : unsigned(inval'length downto 0) := (others => '0');
	begin
		-- Append padding to hold the first decimal bit
		val1 := shift_right(inval & '0', shr);
		-- Add correction, this will do nothing if SHR=0
		val2 := val1 + 1;
		return val2(inval'length downto 1);
	end function;
	-- Variant for selectable output width and signedness
	function descale_towards_nearest(inval : std_logic_vector; shr : natural; outsize : natural; sgn : boolean) return std_logic_vector is
	begin
		if sgn = true then
			return std_logic_vector(resize(descale_towards_nearest(signed(inval), shr), outsize));
		end if;
		return std_logic_vector(resize(descale_towards_nearest(unsigned(inval), shr), outsize));
	end function;

	-- The number of parallel inputs per pooling path
	constant TOTAL_NPOOL : natural := PAR_OUT * NPOOL;

	-- This check function is called at initialization of a phony constant, so the assert is displayed before VHDL elaboration with a meaningful message
	function check_npool(b : boolean) return boolean is
	begin
		assert (PAR_IN mod TOTAL_NPOOL) = 0
			report "Error PAR_IN must be a multiple of PAR_OUT * NPOOL"
			severity failure;
		return false;
	end function;

	constant CHECK1 : boolean := check_npool(false);

	-- The number of parallel inputs per pooling path
	constant POOL_PAR_IN : natural := PAR_IN / TOTAL_NPOOL;

	-- Get the width of the output of the adder trees
	-- There are PAR_IN / NPOOL additions in parallel
	constant WTREE : natural := WDATA + to_integer(POOL_PAR_IN > 1) * to_integer(OPADD = true) * storebitsnb(POOL_PAR_IN - 1);

	-- Get the width of the accumulator
	-- There are FSIZE iterative additions
	constant WACCU : natural := WTREE + to_integer(FSIZE > 1) * to_integer(OPADD = true) * storebitsnb(FSIZE - 1);

	-- Take into account latency of distribution buffers + trees + accu + chain + mult + delay to enter the output FIFO
	constant FIFOMARGIN_USE : natural :=
		FIFOMARGIN + 2 + log_in_base(POOL_PAR_IN, 2) - to_integer(POOL_PAR_IN = 0) + to_integer(FSIZE > 1) + to_integer(NPOOL > 1) + to_integer(MULT > 1) + 2;

	-- A special variant of the FSM
	-- When input and output happen in 1 clock cycle, no need for an FSM, just validate inputs according to the output FIFO
	constant ARCH_NOFSM : boolean := (FSIZE = 1 and LOCKED = true) and (NPOOL = 1);

	-- The width of counters for input and output sides
	constant WADDR_IN : natural := storebitsnb(FSIZE-1);
	constant WADDR_SH : natural := storebitsnb(NPOOL-1);

	signal reg_frame_doing, reg_frame_doing_n : std_logic := '0';
	signal reg_shift_doing, reg_shift_doing_n : std_logic := '0';

	signal reg_fr2sh_ready, reg_fr2sh_ready_n : std_logic := '0';
	signal reg_sh2fr_ready, reg_sh2fr_ready_n : std_logic := '0';

	signal reg_frame_beg, reg_frame_beg_n     : std_logic := to_std_logic(FSIZE <= 1);
	signal reg_shift_beg, reg_shift_beg_n     : std_logic := to_std_logic(NPOOL <= 1);

	signal reg_frame_end, reg_frame_end_n     : std_logic := to_std_logic(FSIZE <= 1);
	signal reg_shift_end, reg_shift_end_n     : std_logic := to_std_logic(NPOOL <= 1);

	signal buf_user_fsize  : unsigned(15 downto 0);
	signal buf_user_npool  : unsigned(15 downto 0);
	signal buf_in_addr_end : unsigned(WADDR_IN-1 downto 0);
	signal buf_sh_addr_end : unsigned(WADDR_SH-1 downto 0);

	signal buf_out_ready   : std_logic := '0';

	-- Internal address registers
	signal regaddr_in, regaddr_in_n : unsigned(WADDR_IN-1 downto 0) := (others => '0');
	signal regaddr_sh, regaddr_sh_n : unsigned(WADDR_SH-1 downto 0) := (others => '0');

	-- Control signals to drive the blocks
	signal sigctrl_accu_set   : std_logic := '0';
	signal sigctrl_accu_add   : std_logic := '0';
	signal sigctrl_shift_en   : std_logic := '0';
	signal sigctrl_shift_copy : std_logic := '0';

	-- One signal to merge control inputs together, for clarity
	constant CTRLNB : natural := 4;
	constant CTRLIDX_SET : natural := 0;
	constant CTRLIDX_ADD : natural := 1;
	constant CTRLIDX_CPY : natural := 2;
	constant CTRLIDX_SH  : natural := 3;

	-- Input of adder trees
	signal trees_in_data : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal trees_in_ctrl : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');

begin

	assert (PAR_IN mod TOTAL_NPOOL) = 0
		report "Error PAR_IN must be a multiple of PAR_OUT * NPOOL"
		severity failure;

	assert MULT > 0
		report "Error MULT parameter is zero, this is probably not intentional"
		severity failure;

	-------------------------------------------------------------------
	-- Init parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		process(clk)
		begin
			if rising_edge(clk) then

				buf_user_fsize <= unsigned(user_fsize);
				buf_user_npool <= to_unsigned(NPOOL, 16);

				buf_in_addr_end <= resize(buf_user_fsize - 2, WADDR_IN);
				buf_sh_addr_end <= to_unsigned(maximum(NPOOL-2, 0), WADDR_SH);

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_user_fsize <= to_unsigned(FSIZE, 16);
		buf_user_npool <= to_unsigned(NPOOL, 16);

		buf_in_addr_end <= to_unsigned(maximum(FSIZE-2, 0), WADDR_IN);
		buf_sh_addr_end <= to_unsigned(maximum(NPOOL-2, 0), WADDR_SH);

	end generate;

	-------------------------------------------------------------------
	-- The state machine
	-------------------------------------------------------------------

	-- Sequential process

	process(clk)
	begin
		if rising_edge(clk) then

			-- Data output: Detect when there is enough room in output FIFO
			buf_out_ready <= '0';
			if unsigned(out_fifo_room) >= FIFOMARGIN_USE then
				buf_out_ready <= '1';
			end if;

			reg_frame_beg <= reg_frame_beg_n;
			reg_shift_beg <= reg_shift_beg_n;

			reg_frame_end <= reg_frame_end_n;
			reg_shift_end <= reg_shift_end_n;

			regaddr_in  <= regaddr_in_n;
			regaddr_sh  <= regaddr_sh_n;

			reg_frame_doing <= reg_frame_doing_n;
			reg_shift_doing <= reg_shift_doing_n;

			reg_fr2sh_ready <= reg_fr2sh_ready_n;
			reg_sh2fr_ready <= reg_sh2fr_ready_n;

			trees_in_data <= in_data;

			-- Aggregate all input control signals
			trees_in_ctrl(CTRLIDX_SET) <= sigctrl_accu_set;
			trees_in_ctrl(CTRLIDX_ADD) <= sigctrl_accu_add;
			trees_in_ctrl(CTRLIDX_CPY) <= sigctrl_shift_copy;
			trees_in_ctrl(CTRLIDX_SH)  <= sigctrl_shift_en;

			-- When input and outputs are in 1 clock cycle, no need for an FSM
			-- Just use the output FIFO as a condition to accept new inputs
			if ARCH_NOFSM = true then
				-- The bit to accept the inputs
				reg_frame_doing <= '0';
				if unsigned(out_fifo_room) >= FIFOMARGIN_USE then
					reg_frame_doing <= '1';
				end if;
				-- Only signal ADD matters to validate outputs
				trees_in_ctrl(CTRLIDX_SET) <= sigctrl_accu_add;
				trees_in_ctrl(CTRLIDX_ADD) <= sigctrl_accu_add;
				trees_in_ctrl(CTRLIDX_CPY) <= sigctrl_accu_add;
				trees_in_ctrl(CTRLIDX_SH)  <= sigctrl_accu_add;
				-- FIXME Ensure other unused FSM signals will be optimized out
				regaddr_in <= (others => '0');
				regaddr_sh <= (others => '0');
			end if;

		end if;
	end process;

	-- Combinatorial process

	process(all)
		variable var_addr_inc  : std_logic := '0';
		variable var_frame_end : std_logic := '0';
		variable var_shift_end : std_logic := '0';
	begin

		-- Init variables
		var_addr_inc  := '0';
		var_frame_end := '0';
		var_shift_end := '0';

		-- Default values for registers

		reg_frame_beg_n <= reg_frame_beg;
		reg_shift_beg_n <= reg_shift_beg;

		reg_frame_end_n <= reg_frame_end;
		reg_shift_end_n <= reg_shift_end;

		regaddr_in_n  <= regaddr_in;
		regaddr_sh_n  <= regaddr_sh;

		reg_frame_doing_n <= reg_frame_doing;
		reg_shift_doing_n <= reg_shift_doing;

		reg_fr2sh_ready_n <= reg_fr2sh_ready;
		reg_sh2fr_ready_n <= reg_sh2fr_ready;

		-- Default values for outputs

		in_rdy <= '0';

		-- Default values for neuron control signals

		sigctrl_accu_set   <= '0';
		sigctrl_accu_add   <= '0';
		sigctrl_shift_en   <= '0';
		sigctrl_shift_copy <= '0';

		--------------------------------------------------
		-- Frame input operation
		--------------------------------------------------

		-- Specifications :
		-- Inputs are accepted only when reg_frame_doing=1
		--   This bit stays at 1 for the entire duration of an input frame
		--   This bit advertises data_in_ready=1
		-- reg_frame_beg is set (buffered) at input of the first data of a frame, used to clear the accumulators
		-- reg_frame_end is set (buffered) at input of the last data of a frame, used to clear the address register and start the output state machine

		-- Increment address at Frame input
		if reg_frame_doing = '1' then
			-- Indicate to the input FIFO that we are reading a value
			in_rdy <= '1';
			-- Process an input value
			if in_ack = '1' then
				-- Increment the address
				regaddr_in_n <= regaddr_in + 1;
				-- Bufferize flags that indicate the end of a frame is reached
				reg_frame_end_n <= '0';
				if (regaddr_in = buf_in_addr_end) or (buf_user_fsize = 1) then
					reg_frame_end_n <= '1';
				end if;
				-- Handle when we are at the last data item of the frame
				if reg_frame_end = '1' then
					var_frame_end := '1';
					-- Clear the frame size counter
					regaddr_in_n <= (others => '0');
				end if;
				-- Process flags
				reg_frame_beg_n <= to_std_logic(buf_user_fsize <= 1);
				sigctrl_accu_set <= reg_frame_beg;
				-- Send the data valid signal to neurons
				sigctrl_accu_add <= '1';
			end if;
			if var_frame_end = '1' then
				reg_frame_doing_n <= '0';
				reg_fr2sh_ready_n <= '1';
			end if;
		end if;

		--------------------------------------------------
		-- Output shifting operation
		--------------------------------------------------

		-- Data output: send the Shift Enable signal
		if reg_shift_doing = '1' then
			-- WARNING: We MUST be able to read the first value at the first cycle
			-- Because on the other side, the next frame can overwrite the accumulators
			if (buf_out_ready = '1') or (reg_shift_beg = '1') then
				reg_shift_beg_n <= '0';
				sigctrl_shift_copy <= reg_shift_beg;
				-- Send the shift enable signal to neurons - it acts as data valid on output side
				sigctrl_shift_en <= '1';
				-- Increment the address
				regaddr_sh_n <= regaddr_sh + 1;
				-- Bufferize a flag that indicates the end of shift is reached
				reg_shift_end_n <= '0';
				if (regaddr_sh = buf_sh_addr_end) or (buf_user_npool = 1) then
					reg_shift_end_n <= '1';
				end if;
				-- Handle when we are sending the Shift Enable for the result of the last neuron
				if reg_shift_end = '1' then
					var_shift_end := '1';
					-- Clear the address
					regaddr_sh_n <= (others => '0');
					-- Prepare to wait for the next frame
					reg_shift_doing_n <= '0';
					reg_sh2fr_ready_n <= '1';
				end if;
			end if;
		end if;

		--------------------------------------------------
		-- Change state, for frame and/or shift
		--------------------------------------------------

		-- Start an input frame when ready
		if (reg_frame_doing = '0' or var_frame_end = '1') and (reg_shift_doing = '0' or reg_sh2fr_ready = '1' or var_shift_end = '1') then
			reg_frame_doing_n <= '1';
			reg_frame_beg_n   <= '1';
			reg_frame_end_n   <= to_std_logic(buf_user_fsize <= 1);
			-- Clear the flag shift->frame
			reg_sh2fr_ready_n <= '0';
		end if;

		-- Start an output frame when ready
		if (reg_shift_doing = '0' or var_shift_end = '1') and (reg_fr2sh_ready = '1' or (reg_frame_doing = '1' and var_frame_end = '1')) then
			reg_shift_doing_n <= '1';
			reg_shift_beg_n   <= '1';
			reg_shift_end_n   <= to_std_logic(buf_user_npool <= 1);
			-- Clear the flag frame->shift
			reg_fr2sh_ready_n <= '0';
		end if;

		-- The general clear signal has priority
		if clear = '1' then
			reg_frame_beg_n    <= to_std_logic(FSIZE <= 1);
			reg_shift_beg_n    <= to_std_logic(NPOOL <= 1);
			reg_frame_end_n    <= to_std_logic(FSIZE <= 1);
			reg_shift_end_n    <= to_std_logic(NPOOL <= 1);
			regaddr_in_n       <= (others => '0');
			regaddr_sh_n       <= (others => '0');
			reg_frame_doing_n  <= '0';
			reg_shift_doing_n  <= '0';
			reg_fr2sh_ready_n  <= '0';
			reg_sh2fr_ready_n  <= '0';
			-- Also clear stuff in the blocks
			sigctrl_accu_set   <= '1';
			sigctrl_accu_add   <= '0';
			sigctrl_shift_en   <= '0';
			sigctrl_shift_copy <= '0';
		end if;

	end process;

	-------------------------------------------------------------------
	-- The scan chains
	-------------------------------------------------------------------

	gen_po : for po in 0 to PAR_OUT-1 generate

		-- Output of adder trees
		signal trees_out_ctrl : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');

		-- Accumulators
		signal accu_next : std_logic_vector(NPOOL*WACCU-1 downto 0) := (others => '0');
		signal accu_regs : std_logic_vector(NPOOL*WACCU-1 downto 0) := (others => '0');

		-- Output of the scan chains
		signal chain_data : std_logic_vector(NPOOL*WACCU-1 downto 0) := (others => '0');
		signal chain_val : std_logic := '0';

		-- Selection of data before multiplication
		signal premult_data : std_logic_vector(WACCU-1 downto 0) := (others => '0');

		-- The expected post-multiplication size
		constant WMUL : natural := WACCU + storebitsnb(MULT-1) - to_integer(MULT <= 1);

		-- Post-multiplication and shr, for average pooling
		signal postmult_sig : std_logic_vector(WMUL-1 downto 0) := (others => '0');
		signal postshr_sig : std_logic_vector(WOUT-1 downto 0) := (others => '0');

		-- Final results
		signal postshr_data : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal postshr_val  : std_logic := '0';

	begin

		-------------------------------------------------------------------
		-- The pooling paths
		-------------------------------------------------------------------

		gen_pool : for u in 0 to NPOOL-1 generate

			-- The logical index of this pooling unit
			constant POOL_ID : natural := u * PAR_OUT + po;

			-- Data input of the tree
			signal tree_in : std_logic_vector(POOL_PAR_IN*WDATA-1 downto 0) := (others => '0');

			-- Only the first unit drives the accumulators and scan chain
			constant LOC_TAGEN : boolean := (u = 0);
			signal loc_ctrl_out  : std_logic_vector(CTRLNB-1 downto 0) := (others => '0');

			-- Data output of the tree
			signal tree_out : std_logic_vector(WTREE-1 downto 0) := (others => '0');

			-- Result of accumulation
			signal accu_oper : std_logic_vector(WACCU-1 downto 0) := (others => '0');

		begin

			-- Gather inputs for the tree
			-- The order corresponds to the way the previous sliding window layer generates the data
			gen_tree_in : for i in 0 to POOL_PAR_IN-1 generate
				tree_in((i+1)*WDATA-1 downto i*WDATA) <= trees_in_data((i*TOTAL_NPOOL+POOL_ID+1)*WDATA-1 downto (i*TOTAL_NPOOL+POOL_ID)*WDATA);
			end generate;

			-- Instantiate the appropriate pooling tree

			gen_minmax : if (OPMAX = true) or (OPMIN = true) generate

				-- Utility function to get the desired min or max
				function local_max(inA: std_logic_vector; inB: std_logic_vector) return std_logic_vector is
				begin
					if
						((SDATA =  true) and (  signed(inA) >=   signed(inB))) or
						((SDATA = false) and (unsigned(inA) >= unsigned(inB)))
					then
						-- Here inA >= inB
						if OPMAX = true then
							return inA;
						end if;
						return inB;
					else
						-- Here inA < inB
						if OPMAX = true then
							return inB;
						end if;
						return inA;
					end if;
					-- Unreachable line
					return inA;
				end function;

				-- Component declaration: Get the max of several values
				component minmaxtree is
					generic(
						WDATA : natural := 8;
						SDATA : boolean := true;
						NBIN  : natural := 20;
						OPMAX : boolean := true;
						-- Optional tag propagation
						TAGW  : natural := 1;
						TAGEN : boolean := true;
						TAGZC : boolean := true;
						-- How to add pipeline registers
						REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
						REGIDX : natural := 0   -- Start index (from the leaves)
					);
					port(
						clk       : in  std_logic;
						clear     : in  std_logic;
						-- Data : input and output
						data_in   : in  std_logic_vector(NBIN*WDATA-1 downto 0);
						data_out  : out std_logic_vector(WDATA-1 downto 0);
						-- Tag : input and output
						tag_in    : in  std_logic_vector(TAGW-1 downto 0);
						tag_out   : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

			begin

				-- Instantiate the distributed max
				tree : minmaxtree
					generic map (
						WDATA => WDATA,
						SDATA => SDATA,
						NBIN  => POOL_PAR_IN,
						OPMAX => OPMAX,
						-- Optional tag propagation
						TAGW  => CTRLNB,
						TAGEN => LOC_TAGEN,
						TAGZC => false,
						-- How to add pipeline registers
						REGEN  => 1,
						REGIDX => 0
					)
					port map (
						clk      => clk,
						clear    => clear,
						-- Data : input and output
						data_in  => tree_in,
						data_out => tree_out,
						-- Tag : input and output
						tag_in   => trees_in_ctrl,
						tag_out  => loc_ctrl_out
					);

				-- The next accumulator value
				accu_oper <= local_max(tree_out, accu_regs((u+1)*WACCU-1 downto u*WACCU));

			end generate;

			gen_add : if OPADD = true generate

				-- Number of layers of the adder tree that can have no buffering in between
				-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
				constant ADDER_REGEN : natural := 2 - to_integer(SDATA);

				-- Utility function to perform add
				function local_add(inA : std_logic_vector; inB : std_logic_vector) return std_logic_vector is
				begin
					if SDATA = true then
						return std_logic_vector(resize(signed(inA), WACCU) + resize(signed(inB), WACCU));
					end if;
					return std_logic_vector(resize(unsigned(inA), WACCU) + resize(unsigned(inB), WACCU));
				end function;

				-- Component declaration: A pipelined adder tree
				component addtree is
					generic (
						-- Data type and width
						WDATA : natural := 8;
						SDATA : boolean := true;
						NBIN  : natural := 20;
						WOUT  : natural := 12;
						-- User-specified radix, for testing purposes (0 means automatic)
						RADIX : natural := 0;
						-- Special considerations about data nature
						BINARY : boolean := false;
						BINARY_RADIX : natural := 0;
						TERNARY : boolean := false;
						TERNARY_RADIX : natural := 0;
						-- An optional tag
						TAGW  : natural := 1;
						TAGEN : boolean := true;
						TAGZC : boolean := true;
						-- How to add pipeline registers
						REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
						REGIDX : natural := 0   -- Start index (from the leaves)
					);
					port (
						clk      : in  std_logic;
						clear    : in  std_logic;
						-- Data, input and output
						data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
						data_out : out std_logic_vector(WOUT-1 downto 0);
						-- Tag, input and output
						tag_in   : in  std_logic_vector(TAGW-1 downto 0);
						tag_out  : out std_logic_vector(TAGW-1 downto 0)
					);
				end component;

			begin

				-- Instantiate the adder tree
				adder : addtree
					generic map (
						-- Data type and width
						WDATA => WDATA,
						SDATA => SDATA,
						NBIN  => POOL_PAR_IN,
						WOUT  => WTREE,
						-- User-specified radix, for testing purposes (0 means automatic)
						RADIX => 0,
						-- Special considerations about data nature
						BINARY => false,
						BINARY_RADIX => 0,
						TERNARY => false,
						TERNARY_RADIX => 0,
						-- An optional tag
						TAGW  => CTRLNB,
						TAGEN => LOC_TAGEN,
						TAGZC => false,
						-- How to add pipeline registers
						REGEN  => ADDER_REGEN,
						REGIDX => 0
					)
					port map (
						clk      => clk,
						clear    => clear,
						-- Data, input and output
						data_in  => tree_in,
						data_out => tree_out,
						-- Tag, input and output
						tag_in   => trees_in_ctrl,
						tag_out  => loc_ctrl_out
					);

				-- The next accumulator value
				accu_oper <= local_add(tree_out, accu_regs((u+1)*WACCU-1 downto u*WACCU));

			end generate;

			-- Only the first pooling path drives the accumulators and scan chain
			gen_ctrl : if u = 0 generate
				trees_out_ctrl <= loc_ctrl_out;
			end generate;

			-- The accumulator
			accu_next((u+1)*WACCU-1 downto u*WACCU) <=
				-- Note : The resize below is just to be VHDL compliant even when this case is not exercised
				std_logic_vector(resize(unsigned(tree_out), WACCU)) when FSIZE = 1 and LOCKED = true else
				std_logic_vector(resize(unsigned(tree_out), WACCU)) when SDATA = false and trees_out_ctrl(CTRLIDX_SET) = '1' else
				std_logic_vector(resize(  signed(tree_out), WACCU)) when SDATA = true  and trees_out_ctrl(CTRLIDX_SET) = '1' else
				accu_oper when trees_out_ctrl(CTRLIDX_ADD) = '1' else
				accu_regs((u+1)*WACCU-1 downto u*WACCU);

		end generate;

		-- The accumulators
		-- Note : No need for accumulator if FSIZE=1 because the output of the tree is already buffered
		--   It may still be needed to have a register just for storage, to keep timing of input and output paths independent

		gen_noaccu : if ARCH_NOFSM = true generate

			accu_regs <= accu_next;

		end generate;

		gen_accu : if ARCH_NOFSM = false generate

			process(clk)
			begin
				if rising_edge(clk) then

					accu_regs <= accu_next;

				end if;  -- Rising edge of clock
			end process;

		end generate;

		-- Scan chain operation
		-- Note : no need for actual scan chains if there is full parallelism on output side
		--   It may still be needed to have a register just for storage, to keep timing of input and output paths independent

		gen_nochain : if ARCH_NOFSM = true generate

			chain_data <= accu_regs;

			gen_val : if po = 0 generate
				chain_val <= trees_out_ctrl(CTRLIDX_SH);
			end generate;

		end generate;

		gen_chain : if ARCH_NOFSM = false generate

			process(clk)
			begin
				if rising_edge(clk) then

					if trees_out_ctrl(CTRLIDX_SH) = '1' then
						if NPOOL > 1 then
							chain_data((NPOOL-1)*WACCU-1 downto 0) <= chain_data(NPOOL*WACCU-1 downto WACCU);
						end if;
					end if;

					if trees_out_ctrl(CTRLIDX_CPY) = '1' then
						chain_data <= accu_regs;
					end if;

				end if;  -- Rising edge of clock
			end process;

			gen_val : if po = 0 generate

				process(clk)
				begin
					if rising_edge(clk) then
						chain_val <= trees_out_ctrl(CTRLIDX_SH);
					end if;  -- Rising edge of clock
				end process;

			end generate;

		end generate;

		-- The optional mult and shift, mostly for average pool

		-- Always read the first word of the scan chain
		premult_data <= chain_data(WACCU-1 downto 0);

		-- VHDL reminder : about the unsigned() * MULT : the MULT is first converted to size of the unsigned(), and the result is twice the size of unsigned()
		-- This makes it mandatory to add resize() operations at several places

		gen_mult : if MULT > 1 generate
			postmult_sig <=
				std_logic_vector(resize(resize(unsigned(premult_data), WMUL) * MULT, WMUL)) when SDATA = false else
				std_logic_vector(resize(resize(  signed(premult_data), WMUL) * MULT, WMUL));
		else generate
			postmult_sig <= premult_data;
		end generate;

		gen_shr : if ROUND_NEAR = false generate
			postshr_sig <= descale_towards_zero(postmult_sig, SHR, WOUT, SDATA);
		else generate
			postshr_sig <= descale_towards_nearest(postmult_sig, SHR, WOUT, SDATA);
		end generate;

		-- No need for a register if there is no multiplier and no shift
		-- FIXME In the weird corner case where MULT=1, SHR>0, SDATA=false and ROUND_NEAR=false then this extra reg could also be skipped
		gen_postshr_reg : if (MULT = 1) and (SHR = 0) generate

			postshr_data <= postshr_sig;

			gen_val : if po = 0 generate
				postshr_val <= chain_val;
			end generate;

		else generate

			process(clk)
			begin
				if rising_edge(clk) then

					postshr_data <= postshr_sig;

				end if;  -- Rising edge of clock
			end process;

			gen_val : if po = 0 generate

				process(clk)
				begin
					if rising_edge(clk) then
						postshr_val <= chain_val;
					end if;
				end process;

			end generate;

		end generate;

		-- Data output

		out_data((po+1)*WOUT-1 downto po*WOUT) <= postshr_data;

		gen_out_rdy : if po = 0 generate
			out_rdy <= postshr_val;
		end generate;

	end generate;

end architecture;

