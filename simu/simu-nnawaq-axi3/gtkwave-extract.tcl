# FIXME This script does not work, some data is lost
# Prefer using in-simulation save of layer outputs

gtkwave::loadFile "wave.fst"

# The clock period is 10 ns
set period 10000000
# Set the initial time marker after reset is cleared (100 clock cycles)
gtkwave::setMarker [expr 100 * $period]



set nval 0
set nval_row 0
set nval_row_max 0



# Neu0
set sigVal "nnawaq_axi3_bench.user_inst.i_neu0.data_out_valid"
set sigDat "nnawaq_axi3_bench.user_inst.i_neu0.data_out\[20:0\]"
set nval_row_max 6
# Win1
#set sigVal "nnawaq_axi3_bench.user_inst.i_win1.data_out_rdy"
#set sigDat "nnawaq_axi3_bench.user_inst.i_win1.data_out\[7:0\]"
#set nval_row_max 4



set sigClk "nnawaq_axi3_bench.user_inst.CLK"
set num_found [ gtkwave::addSignalsFromList $sigClk ]
if { $num_found == 0 } {
	puts "Error signal not found : $sigClk"
	exit 1
}

set num_found [ gtkwave::addSignalsFromList $sigVal ]
if { $num_found == 0 } {
	puts "Error signal not found : $sigVal"
	exit 1
}

set num_found [ gtkwave::addSignalsFromList $sigDat ]
if { $num_found == 0 } {
	puts "Error signal not found : $sigDat"
	exit 1
}



# Find next rising edge
gtkwave::highlightSignalsFromList $sigClk
gtkwave::findNextEdge
set val [ gtkwave::getTraceValueAtMarkerFromName $sigClk ]
if {"$val" == "0"} {
	gtkwave::findNextEdge
}



while {True} {

	gtkwave::highlightSignalsFromList $sigClk
	set pre_time [ gtkwave::findNextEdge ]
	set new_time [ gtkwave::findNextEdge ]
	if { $new_time == $pre_time } {
		break
	}

	# Check valid signal
	# If zero, directly go at next edge where valid=1
	set val [ gtkwave::getTraceValueAtMarkerFromName $sigVal ]
	if {"$val" == "0"} {
		# Find next edge of Valid signal, necessarily we reach Valid=1
		# FIXME This assumes that Valid stays at "1" until rising edge
		set pre_time $new_time
		gtkwave::unhighlightSignalsFromList $sigClk
		gtkwave::highlightSignalsFromList $sigVal
		set new_time [ gtkwave::findNextEdge ]
		if { $new_time == $pre_time } {
			break
		}
		# Find clock edge
		set pre_time $new_time
		gtkwave::unhighlightSignalsFromList $sigVal
		gtkwave::highlightSignalsFromList $sigClk
		set new_time [ gtkwave::findNextEdge ]
		if { $new_time == $pre_time } {
			break
		}
		# Find clock rising edge
		set val [ gtkwave::getTraceValueAtMarkerFromName $sigClk ]
		if {"$val" == "0"} {
			set pre_time $new_time
			set new_time [ gtkwave::findNextEdge ]
		}
		if { $new_time == $pre_time } {
			break
		}
	}

	# Here we are at a rising clock edge, and Valid=1
	# Check data value
	set val [ gtkwave::getTraceValueAtMarkerFromName $sigVal ]
	set dat [ gtkwave::getTraceValueAtMarkerFromName $sigDat ]
	scan $dat %x dati
	incr nval

	# FIXME write in file

	#puts "Valid data output nb $nval : $val $dat (dec $dati)"
	if { $nval_row_max <= 1 } {
		puts "$dati"
	} else {
		if {$nval_row == 0} { puts "" } else { puts -nonewline "," }
		puts -nonewline "$dati"
		set nval_row [expr ( $nval_row + 1 ) % $nval_row_max ]
	}

	#if { "$dat" == "0F" } { puts "" ; break ; }

}


puts "Valid data outputs : $nval"

exit 0

