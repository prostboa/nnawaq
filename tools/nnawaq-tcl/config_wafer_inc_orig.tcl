#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It configures weights and thresholds in an already-loaded neural network

set cfg_dir "../../datasets/wafer-inc-orig"

set input_frames "$cfg_dir/input.csv"
nn_set frames=$input_frames
nn_set floop=1 ml=1

# Order of weights in user-provided config
nn_set worder=zxy
nn_set worder=keep

# Set config file for neuron layers
for {set i 0} {$i < 11} {incr i} {
	nn_layer_set neu$i cfg=$cfg_dir/neu$i.csv
}

# Set config file for ReLU layers
for {set i 0} {$i < 10} {incr i} {
	nn_layer_set relu$i cfg=$cfg_dir/relu$i.csv
}

# Set number of output neurons
nn_set no=58

