
-- 2 Riffa chanels are used:
--   channel 0: write ID of target register, bit 31 = WE, bit 30 = RE, next 32 bits are WData
--   channel 1: receive config & frame data from PC, send network outputs to PC

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnawaq_riffa is
	generic (
		C_PCI_DATA_WIDTH : integer := 128;
		RIFFA_CHANELS_NB : integer := 2
	);
	port (
		CLK : in std_logic;
		RST : in std_logic;
		-- RX Interface
		CHNL_RX_CLK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX            : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_ACK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_LAST       : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_LEN        : in  std_logic_vector(32*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_OFF        : in  std_logic_vector(31*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA       : in  std_logic_vector(C_PCI_DATA_WIDTH*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA_VALID : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA_REN   : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		-- TX Interface
		CHNL_TX_CLK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX            : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_ACK        : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_LAST       : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_LEN        : out std_logic_vector(32*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_OFF        : out std_logic_vector(31*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA       : out std_logic_vector(C_PCI_DATA_WIDTH*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA_VALID : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA_REN   : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0)
	);
end nnawaq_riffa;

architecture synth of nnawaq_riffa is

	------------------------------------------------
	-- Helper functions
	------------------------------------------------

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	function to_std_logic(b: boolean) return std_logic is
	begin
		if b = false then
			return '0';
		end if;
		return '1';
	end function;

	function to_std_logic(b: std_logic) return std_logic is
	begin
		return b;
	end function;

	function to_std_logic_vector(b: std_logic) return std_logic_vector is
		variable r : std_logic_vector(0 downto 0);
	begin
		r(0) := b;
		return r;
	end function;

	-- Compute the power of 2 that is greater or equal to the input
	function uppow2(vin : natural) return natural is
		variable v : natural := 1;
	begin
		v := 1;
		loop
			exit when v >= vin;
			v := v * 2;
		end loop;
		return v;
	end function;

	------------------------------------------------
	-- Signals for RIFFA interface
	------------------------------------------------

	constant RIFFA_STATE_WAIT : std_logic_vector(1 downto 0) := "00";
	constant RIFFA_STATE_LEN  : std_logic_vector(1 downto 0) := "01";
	constant RIFFA_STATE_DATA : std_logic_vector(1 downto 0) := "10";
	signal chan0_rx_state, chan0_tx_state : std_logic_vector(1 downto 0) := RIFFA_STATE_WAIT;
	signal chan1_rx_state, chan1_tx_state : std_logic_vector(1 downto 0) := RIFFA_STATE_WAIT;

	signal chan0_rcount : unsigned(31 downto 0);
	signal chan1_rcount : unsigned(31 downto 0);

	signal chan0_rlen : unsigned(31 downto 0);
	signal chan1_rlen : unsigned(31 downto 0);

	signal chan0_rdata : std_logic_vector(C_PCI_DATA_WIDTH-1 downto 0);
	signal chan1_rdata : std_logic_vector(C_PCI_DATA_WIDTH-1 downto 0);

	signal chan0_rx_ren, chan0_tx_valid : std_logic;
	signal chan1_rx_ren, chan1_tx_valid : std_logic;
	signal chan1_tx_len : unsigned(31 downto 0) := (others => '0');

	-- These signals are set to 1 for only one clock cycle
	signal chan1_rx_end : std_logic := '0';
	signal chan1_tx_end : std_logic := '0';

	------------------------------------------------
	-- Signals to make reset last longer
	------------------------------------------------

	constant RSTVAL_IN  : std_logic := '1';
	constant RSTVAL_GEN : std_logic := '1';

	constant RESET_DURATION : natural := 64;
	signal reset_counter : unsigned(15 downto 0) := (others => '0');
	signal reset_reg : std_logic := '1';

	------------------------------------------------
	-- Signals for slave registers
	------------------------------------------------

	-- Width of address register signal
	constant SLV_REGS_NB : natural := 16;

	-- Width of address register signal - Use more address space for security
	constant REGADDRW : natural := storebitsnb(127);

	-- The slave registers
	-- There no initialization to allow synthesis tools to optimize constant/unused parts
	signal slv_regs : std_logic_vector(SLV_REGS_NB*32-1 downto 0);

	-- Intermediate signals for read/write operations
	signal slv_reg_addr : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');

	-- Intermediate signals for write operations (shared address signal)
	signal slv_reg_wren   : std_logic := '0';
	signal slv_reg_wridx  : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');
	signal slv_reg_wrdata : std_logic_vector(31 downto 0) := (others => '0');

	-- Intermediate signals for read operations
	signal slv_reg_rden   : std_logic := '0';
	signal slv_reg_rdidx  : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');
	signal slv_reg_rddata : std_logic_vector(31 downto 0) := (others => '0');

	------------------------------------------------
	-- Config values for the whole design
	------------------------------------------------

	-- AUTOGEN CONFIG NB BEGIN

	-- AUTOGEN CONFIG NB END

	------------------------------------------------
	-- Signals for FIFOs for main frame input and result outputs
	------------------------------------------------

	constant STREAMFIFOS_DEPTH : natural := 64;
	constant STREAMFIFOS_CNTW  : natural := 8;

	-- The signals that interface with the input FIFO
	signal inst_rxfifo_clear    : std_logic := '0';
	signal inst_rxfifo_in_data  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal inst_rxfifo_in_ack   : std_logic := '0';
	signal inst_rxfifo_in_rdy   : std_logic := '0';
	signal inst_rxfifo_in_cnt   : std_logic_vector(STREAMFIFOS_CNTW-1 downto 0) := (others => '0');
	signal inst_rxfifo_out_data : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal inst_rxfifo_out_ack  : std_logic := '0';
	signal inst_rxfifo_out_rdy  : std_logic := '0';
	signal inst_rxfifo_out_cnt  : std_logic_vector(STREAMFIFOS_CNTW-1 downto 0) := (others => '0');

	-- The signals that interface with the output FIFO
	signal inst_txfifo_clear    : std_logic := '0';
	signal inst_txfifo_in_data  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal inst_txfifo_in_ack   : std_logic := '0';
	signal inst_txfifo_in_rdy   : std_logic := '0';
	signal inst_txfifo_in_cnt   : std_logic_vector(STREAMFIFOS_CNTW-1 downto 0) := (others => '0');
	signal inst_txfifo_out_data : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal inst_txfifo_out_ack  : std_logic := '0';
	signal inst_txfifo_out_rdy  : std_logic := '0';
	signal inst_txfifo_out_cnt  : std_logic_vector(STREAMFIFOS_CNTW-1 downto 0) := (others => '0');

	------------------------------------------------
	-- Config registers for the network layers
	------------------------------------------------

	-- The scan chain of config registers
	signal config_chain_regs : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs       : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_next  : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_read  : std_logic_vector(31 downto 0) := (others => '0');

	-- The commands for the scan chain of config registers
	signal config_chain_shift : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_get   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_set   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_def   : std_logic := '0';

	----------------------------------------------------
	-- Definitions for the neural network
	----------------------------------------------------

	-- AUTOGEN CST DECL BEGIN

	-- AUTOGEN CST DECL END

	-- AUTOGEN CONST WEIGHTS VEC BEGIN

	-- AUTOGEN CONST WEIGHTS VEC END

	-- This function applies constant bits to the config regs vector
	function config_regs_apply_const(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST BEGIN

		-- AUTOGEN REGS SETCONST END

		return config_regs_var;
	end function;

	-- This function applies constant bits to the config regs vector, for reset or for locking user fields
	function config_regs_apply_const_locked(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST LOCKED BEGIN

		-- AUTOGEN REGS SETCONST LOCKED END

		return config_regs_var;
	end function;

	-- Data transfers pass either through the embedded DMA or through slave registers, but not both
	-- In both cases, the generated constant CONFIG_IFW must correspond to that interface width
	constant DMA_ENABLED : boolean := false;
	constant DMA_ADDR64  : boolean := false;

	constant WDATA_ROUND_POW2 : natural := uppow2(FIRSTFIFO_WDATA);
	constant INPAR : natural := FIRSTFIFO_PAR;

	-- FIXME The value 32 is arbitrary and could be adapted to largest width of layers that can be selected
	-- FIXME Values other than 32 would need additional support from controlling software
	--constant WOUT_ROUND_POW2 : natural := uppow2(LASTFIFO_WDATA) * to_integer(CONFIG_SELOUT = false) + 32 * to_integer(CONFIG_SELOUT = true);
	constant WOUT_ROUND_POW2 : natural := 32;
	constant OUTPAR : natural := LASTFIFO_PAR;

	-- Max number of data items in input and output buffers
	-- Each transfer on both sides moves an integer multiple of the parallelism, in to make hardware-friendly implementation of buffers
	constant INPAR_PER_IF  : natural := (CONFIG_IFW / (WDATA_ROUND_POW2 * INPAR)) * INPAR;
	constant OUTPAR_PER_IF : natural := (CONFIG_IFW / (WOUT_ROUND_POW2 * OUTPAR)) * OUTPAR;

	signal req_start_recv : std_logic := '0';
	signal req_start_send : std_logic := '0';

	constant CST_RECV_FRAME   : std_logic_vector(15 downto 0) := (others => '1');
	constant CST_SEND_DEFAULT : std_logic_vector(15 downto 0) := (others => '1');

	signal cur_recv1 : std_logic_vector(15 downto 0);
	signal cur_recv2 : std_logic_vector(15 downto 0);
	signal cur_send  : std_logic_vector(15 downto 0);
	signal send_last_layer : std_logic := '1';

	signal cur_freerun_in  : std_logic := '0';
	signal cur_freerun_out : std_logic := '0';

	signal clkcnt, clkcnt_n : unsigned(31 downto 0) := (others => '0');
	signal clkcnt_en, clkcnt_en_n : std_logic := '0';
	signal clkcnt_end, clkcnt_end_n : std_logic := '0';

	-- Buffer to read inputs from the RX fifo
	constant RXBUF_FLAGS_SIZE : natural := CONFIG_IFW / (WDATA_ROUND_POW2 * INPAR);
	signal rxbuf_data,  rxbuf_data_n  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal rxbuf_flags, rxbuf_flags_n : std_logic_vector(RXBUF_FLAGS_SIZE-1 downto 0) := (0 => '1', others => '0');
	signal rxbuf_empty, rxbuf_empty_n : std_logic := '0';  -- To indicate a full buffer is available
	-- Counters for input values received from RX fifo
	signal rxcnt_want               : unsigned(31 downto 0) := (others => '0');
	signal rxcnt_cur,  rxcnt_cur_n  : unsigned(31 downto 0) := (others => '0');
	signal rxcnt_last, rxcnt_last_n : std_logic := '0';  -- Flag for the last expected value

	-- Enable signal for sending output values to the TX fifo
	signal txrun_en, txrun_en_n : std_logic := '0';
	-- Buffer to accumulate outputs before sending them to the TX fifo
	constant TXBUF_WEN_SIZE : natural := CONFIG_IFW / (WOUT_ROUND_POW2 * OUTPAR);
	signal txbuf_data,  txbuf_data_n : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal txbuf_wen,   txbuf_wen_n  : std_logic_vector(TXBUF_WEN_SIZE-1 downto 0) := (0 => '1', others => '0');
	signal txbuf_full,  txbuf_full_n : std_logic := '0';  -- To indicate a full buffer is available
	-- Counters for output values to send to the TX fifo
	signal txcnt_want               : unsigned(31 downto 0) := (others => '0');
	signal txcnt_cur,  txcnt_cur_n  : unsigned(31 downto 0) := (others => '0');
	signal txcnt_last, txcnt_last_n : std_logic := '0';  -- Flag for the last expected output

	-- Signals for distributed MUX, to monitor the FIFOs
	signal monitorfifo_sel : std_logic_vector(15 downto 0) := (others => '0');
	signal monitorfifo_in  : std_logic_vector(CONFIG_FIFOS_NB*12-1 downto 0) := (others => '0');
	signal monitorfifo_out : std_logic_vector(12-1 downto 0) := (others => '0');

	-- Signals for scatter-gather, to select the output layer
	signal selout_en_in   : std_logic;
	signal selout_en_out  : std_logic_vector(CONFIG_SELOUT_NB-1 downto 0) := (others => '0');
	signal selout_gat_in  : std_logic_vector(CONFIG_SELOUT_NB*33-1 downto 0) := (others => '0');
	signal selout_gat_out : std_logic_vector(33-1 downto 0) := (others => '0');
	signal selout_sca_in  : std_logic_vector(16-1 downto 0) := (others => '0');
	signal selout_sca_out : std_logic_vector(CONFIG_SELOUT_NB*16-1 downto 0) := (others => '0');

	-- The FIFO at the output of the scatter-gather component
	signal seloutfifo_clear    : std_logic := '0';
	signal seloutfifo_in_data  : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_in_ack   : std_logic := '0';
	signal seloutfifo_in_cnt   : std_logic_vector(16-1 downto 0) := (others => '0');
	signal seloutfifo_out_data : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_out_rdy  : std_logic := '0';
	signal seloutfifo_out_ack  : std_logic := '0';

	----------------------------------------------------
	-- Components
	----------------------------------------------------

	-- The circular buffer / FIFO component
	component fifo_with_counters is
		generic (
			DATAW : natural := 32;
			DEPTH : natural := 8;
			CNTW  : natural := 16
		);
		port (
			reset         : in  std_logic;
			clk           : in  std_logic;
			fifo_in_data  : in  std_logic_vector(DATAW-1 downto 0);
			fifo_in_rdy   : out std_logic;
			fifo_in_ack   : in  std_logic;
			fifo_in_cnt   : out std_logic_vector(CNTW-1 downto 0);
			fifo_out_data : out std_logic_vector(DATAW-1 downto 0);
			fifo_out_rdy  : out std_logic;
			fifo_out_ack  : in  std_logic;
			fifo_out_cnt  : out std_logic_vector(CNTW-1 downto 0)
		);
	end component;

	-- A distributed multiplexer
	-- Used to observe the status of the FIFOs
	component muxtree_bin is
		generic(
			WDATA : natural := 8;
			NBIN  : natural := 20;
			WSEL  : natural := 12
		);
		port(
			clk      : in  std_logic;
			-- Selection input
			sel      : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in    : in  std_logic;
			en_out   : out std_logic_vector(NBIN-1 downto 0);
			-- Data input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	-- Scatter-gather component to select output layer, if anabled
	component scattergather is
		generic(
			WGATHER  : natural := 8;
			WSCATTER : natural := 1;
			NBIN     : natural := 20;
			WSEL     : natural := 12;
			EGATHER  : boolean := true;
			ESCATTER : boolean := false;
			RADIX    : natural := 2;
			REGALL   : boolean := false
		);
		port(
			clk         : in  std_logic;
			-- Selection input
			sel         : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in       : in  std_logic;
			en_out      : out std_logic_vector(NBIN-1 downto 0);
			-- Gather data, input and output
			gather_in   : in  std_logic_vector(NBIN*WGATHER-1 downto 0);
			gather_out  : out std_logic_vector(WGATHER-1 downto 0);
			-- Scatter data, input and output
			scatter_in  : in  std_logic_vector(WSCATTER-1 downto 0);
			scatter_out : out std_logic_vector(NBIN*WSCATTER-1 downto 0)
		);
	end component;

	-- AUTOGEN COMP DECL BEGIN

	-- AUTOGEN COMP DECL END

	-- AUTOGEN SIG DECL BEGIN

	-- AUTOGEN SIG DECL END

begin

	assert not ( (CONFIG_NOREGS = true) and (CONFIG_RDONLY = false) ) report "Error inconsistent setup for parameters NOREGS and RDONLY" severity failure;
	assert RIFFA_CHANELS_NB = 2 report "Error wrong number of RIFFA channels" severity failure;
	assert C_PCI_DATA_WIDTH >= 64 report "Error insufficient PCIe data width" severity failure;

	------------------------------------------------
	-- Interface with RIFFA chanels
	------------------------------------------------

	CHNL_RX_CLK(0)      <= CLK;
	CHNL_RX_ACK(0)      <= '1' when chan0_rx_state = RIFFA_STATE_DATA else '0';
	CHNL_RX_DATA_REN(0) <= chan0_rx_ren;

	CHNL_TX_CLK(0)  <= CLK;
	CHNL_TX(0)      <= '1' when (chan0_tx_state = RIFFA_STATE_LEN) or (chan0_tx_state = RIFFA_STATE_DATA) else '0';
	CHNL_TX_LAST(0) <= '1';
	CHNL_TX_LEN(31 downto 0) <= std_logic_vector(to_unsigned(1, 32));  -- Number of 32-bit words
	CHNL_TX_OFF(30 downto 0) <= (others => '0');
	CHNL_TX_DATA(C_PCI_DATA_WIDTH-1 downto 0) <= chan0_rdata;
	CHNL_TX_DATA_VALID(0) <= chan0_tx_valid;

	chan0_rx_ren   <= '1' when chan0_rx_state = RIFFA_STATE_DATA else '0';
	chan0_tx_valid <= '1' when chan0_tx_state = RIFFA_STATE_DATA else '0';

	process (CLK)
	begin
		if rising_edge(CLK) then

			-- These signals are active for only one clock
			slv_reg_rden <= '0';
			slv_reg_wren <= '0';

			if RST = RSTVAL_IN then

				chan0_rlen   <= (others => '0');
				chan0_rcount <= (others => '0');
				chan0_rdata  <= (others => '0');

				chan0_rx_state <= RIFFA_STATE_WAIT;
				chan0_tx_state <= RIFFA_STATE_WAIT;

				slv_reg_addr <= (others => '0');

			else

				-- RX side
				if chan0_rx_state = RIFFA_STATE_WAIT then
					-- Synchronize with TX side
					if chan0_tx_state = RIFFA_STATE_WAIT then
						chan0_rx_state <= RIFFA_STATE_LEN;
					end if;
				elsif chan0_rx_state = RIFFA_STATE_LEN then
					if CHNL_RX(0) = '1' then
						chan0_rlen     <= unsigned(CHNL_RX_LEN(31 downto 0));
						chan0_rcount   <= (others => '0');
						chan0_rx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan0_rx_state = RIFFA_STATE_DATA then
					if CHNL_RX_DATA_VALID(0) = '1' then
						chan0_rdata    <= CHNL_RX_DATA(C_PCI_DATA_WIDTH-1 downto 0);
						slv_reg_addr   <= CHNL_RX_DATA(REGADDRW-1 downto 0);
						slv_reg_wren   <= CHNL_RX_DATA(31);
						slv_reg_wrdata <= CHNL_RX_DATA(63 downto 32);
						chan0_rcount   <= chan0_rcount + (C_PCI_DATA_WIDTH/32);
					end if;
					if chan0_rcount >= chan0_rlen then
						chan0_rx_state <= RIFFA_STATE_WAIT;
						if chan0_rdata(30) = '1' then
							-- Ask the TX side to send the register value
							chan0_tx_state <= RIFFA_STATE_LEN;
						end if;
					end if;
				end if;

				-- TX side
				if chan0_tx_state = RIFFA_STATE_WAIT then
					-- The RX side says when to get out of waiting
				elsif chan0_tx_state = RIFFA_STATE_LEN then
					slv_reg_rden <= '1';
					chan0_rdata(31 downto 0) <= slv_reg_rddata;
					chan0_tx_state <= RIFFA_STATE_DATA;
				elsif chan0_tx_state = RIFFA_STATE_DATA then
					-- Only one word is sent through this channel, this is hardcoded
					if CHNL_TX_DATA_REN(0) = '1' then
						chan0_tx_state <= RIFFA_STATE_WAIT;
					end if;
				end if;

			end if;  -- Not reset
		end if;  -- Clock
	end process;

	CHNL_RX_CLK(1)      <= CLK;
	CHNL_RX_ACK(1)      <= '1' when chan1_rx_state = RIFFA_STATE_DATA else '0';
	CHNL_RX_DATA_REN(1) <= chan1_rx_ren;

	CHNL_TX_CLK(1)  <= CLK;
	CHNL_TX(1)      <= '1' when (chan1_tx_state = RIFFA_STATE_LEN) or (chan1_tx_state = RIFFA_STATE_DATA) else '0';
	CHNL_TX_LAST(1) <= '1';
	CHNL_TX_LEN(32*2-1 downto 32) <= std_logic_vector(chan1_tx_len);
	CHNL_TX_OFF(31*2-1 downto 31) <= (others => '0');
	CHNL_TX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH) <= txbuf_data;
	CHNL_TX_DATA_VALID(1) <= chan1_tx_valid;

	chan1_rx_ren   <= '1' when (chan1_rx_state = RIFFA_STATE_DATA) and (inst_rxfifo_out_ack = '1') else '0';
	chan1_tx_valid <= '1' when (chan1_tx_state = RIFFA_STATE_DATA) and (inst_txfifo_in_ack  = '1') else '0';

	-- The number of 32-bit words of results is rounded to the number of 32b word of the PCIe interface width
	-- Example : Round to upper multiple of 4 : (txcnt_want + 3) and not(to_unsigned(3, 32));
	-- Example : Round to upper multiple of 8 : (txcnt_want + 7) and not(to_unsigned(7, 32));
	-- Here a generic expression is used, assuming the interface width is a power of 2
	chan1_tx_len <= (txcnt_want + C_PCI_DATA_WIDTH/32-1) and not(to_unsigned(C_PCI_DATA_WIDTH/32-1, 32));

	process (CLK)
	begin
		if rising_edge(CLK) then

			chan1_rx_end <= '0';
			chan1_tx_end <= '0';

			if RST = RSTVAL_IN then

				chan1_rlen   <= (others => '0');
				chan1_rcount <= (others => '0');
				chan1_rdata  <= (others => '0');

				chan1_rx_state <= RIFFA_STATE_WAIT;
				chan1_tx_state <= RIFFA_STATE_WAIT;

			else

				-- RX side
				if chan1_rx_state = RIFFA_STATE_WAIT then
					chan1_rx_state <= RIFFA_STATE_LEN;
				elsif chan1_rx_state = RIFFA_STATE_LEN then
					if CHNL_RX(1) = '1' then
						chan1_rlen     <= unsigned(CHNL_RX_LEN(32*2-1 downto 32));
						chan1_rcount   <= (others => '0');
						chan1_rx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan1_rx_state = RIFFA_STATE_DATA then
					if (CHNL_RX_DATA_VALID(1) = '1') and (inst_rxfifo_out_ack = '1') then
						chan1_rdata  <= CHNL_RX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH);
						chan1_rcount <= chan1_rcount + (C_PCI_DATA_WIDTH/32);
					end if;
					if chan1_rcount >= chan1_rlen then
						chan1_rx_state <= RIFFA_STATE_LEN;
						if CHNL_RX_LAST(1) = '1' then
							chan1_rx_end <= '1';
						end if;
					end if;
				end if;

				-- TX side
				if chan1_tx_state = RIFFA_STATE_WAIT then
					if (req_start_send = '1') and (txcnt_want > 0) then
						chan1_tx_state <= RIFFA_STATE_LEN;
					end if;
				elsif chan1_tx_state = RIFFA_STATE_LEN then
					if CHNL_TX_ACK(1) = '1' then
						chan1_tx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan1_tx_state = RIFFA_STATE_DATA then
					if (CHNL_TX_DATA_REN(1) = '1') and (inst_txfifo_in_ack = '1') then
						if txcnt_cur >= txcnt_want then
							chan1_tx_state <= RIFFA_STATE_WAIT;
							chan1_tx_end <= '1';
						end if;
					end if;
				end if;

			end if;  -- Not reset
		end if;  -- Clock
	end process;

	-- Alias signals to be used by the user design

	-- Intermediate signals for simple read/write to slave registers
	slv_reg_wridx  <= slv_reg_addr;
	slv_reg_rdidx  <= slv_reg_addr;

	-- Signals to expose a RX interface to the network
	inst_rxfifo_out_data <= CHNL_RX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH);
	inst_rxfifo_out_rdy  <= CHNL_RX_DATA_VALID(1) and chan1_rx_ren;

	-- Signals to expose a TX interface to the network
	inst_txfifo_in_rdy <= '1' when (chan1_tx_state = RIFFA_STATE_DATA) and (CHNL_TX_DATA_REN(1) = '1') else '0';


	------------------------------------------------
	-- Configuration registers for the network layers
	------------------------------------------------

	-- Combinational process that generates the next value to config registers
	process (all)
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	begin

		-- Init at zero if registers are read-only
		if (CONFIG_NOREGS = true) or (CONFIG_RDONLY = true) then
			config_regs_var := (others => '0');
		else

			config_regs_var := config_regs;

			for i in 0 to CONFIG_CHAIN_NB-1 loop
				if config_chain_set(i) = '1' then
					config_regs_var(i*32+31 downto i*32) := config_chain_regs(i*32+31 downto i*32);
				end if;
			end loop;

		end if;

		-- Apply constant bits
		config_regs_var := config_regs_apply_const(config_regs_var);

		-- Set default values, or write reset functionality
		-- Or, systematically overwrite that config if the registers are locked or not implemented
		if (config_chain_def = '1') or (CONFIG_RDONLY = true) or (CONFIG_NOREGS = true) then
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
		end if;

		config_regs_next <= config_regs_var;

	end process;

	-- Config registers are implemented and writable
	gen_config_regs : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) generate

		process (CLK)
		begin
			if rising_edge(CLK) then
				config_regs <= config_regs_next;
			end if;
		end process;

	end generate;

	-- Config registers are not even implemented
	gen_config_noregs : if (CONFIG_NOREGS = true) or (CONFIG_RDONLY = true) generate
		config_regs <= config_regs_next;
	end generate;

	-- Config registers are read-only : implement memory storage
	gen_config_read_rom : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = true) generate

		type config_regs_rom_type is array (0 to CONFIG_CHAIN_NB-1) of std_logic_vector(31 downto 0);

		function config_regs_gen_mem_init(phony : boolean) return config_regs_rom_type is
			variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
			variable rom_init_var : config_regs_rom_type := (others => (others => '0'));
		begin
			config_regs_var := (others => '0');
			config_regs_var := config_regs_apply_const(config_regs_var);
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
			for i in 0 to CONFIG_CHAIN_NB-1 loop
				rom_init_var(i) := config_regs_var(i*32+31 downto i*32);
			end loop;
			return rom_init_var;
		end function;

		constant config_regs_rom : config_regs_rom_type := config_regs_gen_mem_init(false);

		constant READ_INDEX_SIZE : natural := storebitsnb(CONFIG_CHAIN_NB);
		signal read_index : unsigned(READ_INDEX_SIZE-1 downto 0) := (others => '0');

	begin

		-- Assign the read word
		config_regs_read <= config_regs_rom(to_integer(read_index));

		-- Update the word index
		process (CLK)
		begin
			if rising_edge(CLK) then

				if config_chain_shift(0) = '1' then
					-- Apply resizing in order to avoid simulation errors due to overflow
					read_index <= resize(resize(read_index, READ_INDEX_SIZE+1) + 1, READ_INDEX_SIZE);
				end if;

				if (config_chain_get(0) = '1') or (reset_reg = RSTVAL_GEN) then
					read_index <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- If the network config is not locked, then the config is read from the registers
	gen_config_read_chain : if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) generate

		-- Assign the read word
		config_regs_read <= config_chain_regs(31 downto 0);

	end generate;


	------------------------------------------------
	-- Main control of registers, data transfers, etc
	------------------------------------------------

	-- Alias signals
	cur_recv1 <= slv_regs(4*32+15 downto 4*32+ 0);
	cur_recv2 <= slv_regs(4*32+31 downto 4*32+16);
	cur_send  <= slv_regs(5*32+15 downto 5*32+ 0);

	cur_freerun_in  <= slv_regs(3*32+30);
	cur_freerun_out <= slv_regs(3*32+31);

	monitorfifo_sel <= slv_regs(14*32+15 downto 14*32+ 0);

	rxcnt_want <= unsigned(slv_regs(6*32+31 downto 6*32+ 0));
	txcnt_want <= unsigned(slv_regs(7*32+31 downto 7*32+ 0));

	-- Main sequential process: write to config registers, implement all synchronous registers
	process (CLK)
	begin
		if rising_edge(CLK) then

			-- Hold reset active for a certain duration
			if reset_counter > 0 then
				reset_counter <= reset_counter - 1;
				reset_reg <= RSTVAL_GEN;
			else
				reset_reg <= not RSTVAL_GEN;
			end if;
			-- Generate reset
			if RST = RSTVAL_IN then
				reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
				reset_reg <= RSTVAL_GEN;
			end if;

			-- Default/reset assignments
			req_start_recv <= '0';
			req_start_send <= '0';

			-- Buffers for the main data channel, read direction
			rxbuf_data  <= rxbuf_data_n;
			rxbuf_flags <= rxbuf_flags_n;
			rxbuf_empty <= rxbuf_empty_n;
			rxcnt_cur   <= rxcnt_cur_n;
			rxcnt_last  <= rxcnt_last_n;

			-- Buffers for output registers
			txrun_en   <= txrun_en_n;
			txbuf_data <= txbuf_data_n;
			txbuf_wen  <= txbuf_wen_n;
			txbuf_full <= txbuf_full_n;
			txcnt_cur  <= txcnt_cur_n;
			txcnt_last <= txcnt_last_n;

			-- Commands for config registers
			config_chain_shift <= (others => '0');
			config_chain_get   <= (others => '0');
			config_chain_set   <= (others => '0');
			config_chain_def   <= '0';
			if reset_reg = RSTVAL_GEN then
				config_chain_def <= '1';
			end if;

			-- Bufferize the flag to select the output layer, scatter-gather or last FIFO
			send_last_layer <= '0';
			if (CONFIG_SELOUT = false) or (cur_send = CST_SEND_DEFAULT) then
				send_last_layer <= '1';
			end if;

			-- Clock counter
			clkcnt     <= clkcnt_n;
			clkcnt_en  <= clkcnt_en_n;
			clkcnt_end <= clkcnt_end_n;

			-- Write to slave register
			if slv_reg_wren = '1' then

				-- Actually write to registers

				for i in 0 to SLV_REGS_NB-1 loop
					if to_integer(unsigned(slv_reg_wridx)) = i then
						slv_regs(i*32+31 downto i*32+ 0) <= slv_reg_wrdata;
					end if;
				end loop;

				-- Events when writing to specific registers

				if to_integer(unsigned(slv_reg_wridx)) = 1 then

					-- Config registers
					if CONFIG_NOREGS = false then
						-- Shift configuration registers
						if slv_regs(2*32+1) = '1' then
							config_chain_shift <= (others => '1');
						end if;
					end if;

				end if;

				if to_integer(unsigned(slv_reg_wridx)) = 2 then

					-- Config registers
					if CONFIG_NOREGS = false then
						-- Config registers: Get request
						if slv_reg_wrdata(2) = '1' then
							config_chain_get <= (others => '1');
						end if;
						-- Config registers: Set request
						if slv_reg_wrdata(3) = '1' then
							config_chain_set <= (others => '1');
						end if;
					end if;

				end if;

				if to_integer(unsigned(slv_reg_wridx)) = 3 then

					-- Detect the clear requests
					if slv_reg_wrdata(0) = '1' then
						reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
						reset_reg <= RSTVAL_GEN;
					end if;

				end if;

				if to_integer(unsigned(slv_reg_wridx)) = 6 then

					-- Write the number of input values to be received from the main data channel
					-- Generate a pulse to start sending results on main data channel + clear regs
					req_start_recv <= '1';

				end if;

				if to_integer(unsigned(slv_reg_wridx)) = 7 then

					-- Write the number of result values to send to the main data channel
					-- Generate a pulse to start sending results on main data channel + clear regs
					req_start_send <= '1';

				end if;

				if to_integer(unsigned(slv_reg_wridx)) = 10 then

					-- Clear latency counter and flags
					clkcnt     <= (others => '0');
					clkcnt_en  <= '0';
					clkcnt_end <= '0';

				end if;

			end if;  -- Write enable

			-- Logic for config registers
			if CONFIG_NOREGS = false then

				-- Read config register
				-- Note : This is actually a read event but has to be handled here to have one driver for signal config_chain_shift
				if (slv_reg_rden = '1') and (unsigned(slv_reg_rdidx) = 1) and (slv_regs(2*32+1) = '1') then
					config_chain_shift <= (others => '1');
				end if;

			end if;

			-- Logic for config registers
			if (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false) then

				-- Config registers: operations shift, get, set
				for i in 0 to CONFIG_CHAIN_NB-1 loop
					-- Shift
					if config_chain_shift(i) = '1' then
						-- The slave register 1 is fed at the end of the chain
						if i < CONFIG_CHAIN_NB - 1 then
							config_chain_regs(i*32+31 downto i*32) <= config_chain_regs((i+1)*32+31 downto (i+1)*32);
						else
							config_chain_regs(i*32+31 downto i*32) <= slv_regs(1*32+31 downto 1*32);
						end if;
					end if;
					-- Get
					if config_chain_get(i) = '1' then
						config_chain_regs(i*32+31 downto i*32) <= config_regs(i*32+31 downto i*32);
					end if;
					-- Set : this is implemented in separate process
				end loop;

			end if;  -- (CONFIG_NOREGS = false) and (CONFIG_RDONLY = false)

			if reset_reg = RSTVAL_GEN then

				clkcnt     <= (others => '0');
				clkcnt_en  <= '0';
				clkcnt_end <= '0';

				-- Default is reset at zero
				slv_regs <= (others => '0');

				-- Default values for the network application
				slv_regs(4*32+15 downto 4*32+0) <= CST_RECV_FRAME;
				slv_regs(5*32+15 downto 5*32+0) <= CST_SEND_DEFAULT;

			end if;  -- Reset

			-- Constant parts of slave registers

			slv_regs(0*32+ 7 downto 0*32+ 0) <= std_logic_vector(to_unsigned(78, 8));  -- ASCII for N
			slv_regs(0*32+15 downto 0*32+ 8) <= std_logic_vector(to_unsigned(78, 8));  -- ASCII for N
			slv_regs(0*32+23 downto 0*32+16) <= std_logic_vector(to_unsigned(VERSION_MIN, 8));  -- Minor version
			slv_regs(0*32+31 downto 0*32+24) <= std_logic_vector(to_unsigned(VERSION_MAJ, 8));  -- Major version

			slv_regs(2*32+0) <= to_std_logic(CONFIG_NOREGS);
			slv_regs(2*32+1) <= to_std_logic(CONFIG_RDONLY);
			slv_regs(2*32+3) <= to_std_logic(false);  -- getregs
			slv_regs(2*32+4) <= to_std_logic(false);  -- setregs
			slv_regs(2*32+31 downto 2*32+16) <= std_logic_vector(to_unsigned(CONFIG_CHAIN_NB, 16));

			slv_regs(3*32+2) <= to_std_logic(CONFIG_SELOUT);
			slv_regs(3*32+3) <= to_std_logic(CONFIG_FIFOMON);
			slv_regs(3*32+6) <= to_std_logic(DMA_ENABLED);
			slv_regs(3*32+7) <= to_std_logic(DMA_ADDR64);
			slv_regs(3*32+10 downto 3*32+ 8) <= std_logic_vector(to_unsigned(storebitsnb(WDATA_ROUND_POW2)-1, 3));
			slv_regs(3*32+13 downto 3*32+11) <= std_logic_vector(to_unsigned(storebitsnb(WOUT_ROUND_POW2)-1, 3));
			slv_regs(3*32+19 downto 3*32+14) <= std_logic_vector(to_unsigned(INPAR_PER_IF-1, 6));
			slv_regs(3*32+22 downto 3*32+20) <= std_logic_vector(to_unsigned(OUTPAR_PER_IF-1, 3));
			slv_regs(3*32+27 downto 3*32+23) <= std_logic_vector(to_unsigned((CONFIG_IFW / 8) - 1, 5));

			slv_regs(14*32+31 downto 14*32+16) <= std_logic_vector(to_unsigned(CONFIG_FIFOS_NB, 16));

		end if;  -- Clock
	end process;

	-- Combinatorial process - Control signals to receive items from RX fifo
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Defaults for FIFO channels : no operation
		inst_rxfifo_out_ack <= '0';
		inst_firstfifo_in_ack <= '0';

		-- Select bits from the buffer of the RX channel
		for i in 0 to FIRSTFIFO_PAR-1 loop
			inst_firstfifo_in_data((i+1)*FIRSTFIFO_WDATA-1 downto i*FIRSTFIFO_WDATA) <= rxbuf_data(i*WDATA_ROUND_POW2+FIRSTFIFO_WDATA-1 downto i*WDATA_ROUND_POW2);
		end loop;

		-- Default next values for registers
		rxbuf_data_n  <= rxbuf_data;
		rxbuf_flags_n <= rxbuf_flags;
		rxbuf_empty_n <= rxbuf_empty;
		rxcnt_cur_n   <= rxcnt_cur;
		rxcnt_last_n  <= rxcnt_last;

		-- Clear counters when starting a receive operation
		if req_start_recv = '1' then
			rxbuf_data_n  <= (others => '0');
			rxbuf_flags_n <= (0 => '1', others => '0');
			rxbuf_empty_n <= '1';
			rxcnt_cur_n   <= (others => '0');
			rxcnt_last_n  <= '1' when rxcnt_want = 1 else '0';
		end if;

		-- Receive items from RX fifo
		if cur_recv1 = CST_RECV_FRAME then

			-- Get the output values out of the buffer, send to the first FIFO
			if (rxbuf_empty = '0') and (rxcnt_cur < rxcnt_want) then
				inst_firstfifo_in_ack <= '1';
				if inst_firstfifo_in_rdy = '1' then
					-- Shift the data buffer
					if WDATA_ROUND_POW2 * INPAR < CONFIG_IFW then
						rxbuf_data_n <= std_logic_vector(shift_right(unsigned(rxbuf_data), WDATA_ROUND_POW2 * INPAR));
					end if;
					-- Rotate the flags
					if RXBUF_FLAGS_SIZE > 1 then
						rxbuf_flags_n <= rxbuf_flags(RXBUF_FLAGS_SIZE-2 downto 0) & rxbuf_flags(RXBUF_FLAGS_SIZE-1);
					end if;
					-- Mark the buffer as empty
					if rxbuf_flags(RXBUF_FLAGS_SIZE-1) = '1' or rxcnt_last = '1' then
						var_buf_end := true;
						rxbuf_empty_n <= '1';
						rxbuf_flags_n <= (0 => '1', others => '0');
					end if;
					-- Increment the counter of inputs
					-- Pre-generate the flag for last expected output
					rxcnt_cur_n <= rxcnt_cur + 1;
					rxcnt_last_n <= '1' when rxcnt_cur = rxcnt_want - 2 else '0';
				end if;
			end if;

			-- Get a new buffer from RX fifo
			if rxbuf_empty = '1' or var_buf_end = true then
				inst_rxfifo_out_ack <= '1';
				if inst_rxfifo_out_rdy = '1' then
					rxbuf_data_n  <= inst_rxfifo_out_data;
					rxbuf_empty_n <= '0';
					rxbuf_flags_n <= (0 => '1', others => '0');
				end if;
			end if;

		-- Handle when the received data is to write config into layers
		else

			-- Indicate to the RX fifo that we are always ready to get the next buffer
			inst_rxfifo_out_ack <= '1';
			if inst_rxfifo_out_rdy = '1' then
				rxbuf_data_n <= inst_rxfifo_out_data;
				-- Increment the counter of inputs
				rxcnt_cur_n <= rxcnt_cur + 1;
			end if;

		end if;

		-- Handle freerun on input side
		if cur_freerun_in = '1' then

			-- Assign constant data
			rxbuf_data_n <= (others => '0');

			-- Validate data for first FIFO
			inst_firstfifo_in_ack <= '1';
			if inst_firstfifo_in_rdy = '1' then
				-- Increment the counter of inputs
				rxcnt_cur_n <= rxcnt_cur + 1;
			end if;

		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			inst_rxfifo_out_ack <= '0';
			inst_firstfifo_in_ack <= '0';

			rxbuf_data_n  <= (others => '0');
			rxbuf_flags_n <= (0 => '1', others => '0');
			rxbuf_empty_n <= '1';
			rxcnt_cur_n   <= (others => '0');
			rxcnt_last_n  <= '0';

		end if;

	end process;

	-- Combinatorial process - Control signals to send results to TX fifo
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Default next values for registers
		txrun_en_n   <= txrun_en;
		txbuf_data_n <= txbuf_data;
		txbuf_wen_n  <= txbuf_wen;
		txbuf_full_n <= txbuf_full;
		txcnt_cur_n  <= txcnt_cur;
		txcnt_last_n <= txcnt_last;

		-- Defaults for FIFO channels : no operation
		inst_txfifo_in_ack    <= '0';
		inst_lastfifo_out_ack <= '0';
		seloutfifo_out_ack    <= '0';

		if cur_freerun_out = '1' then

			if txcnt_want = 1 then
				txcnt_last_n <= '1';
			end if;

			-- Free run mode: accept all output values, count them and drop them
			inst_lastfifo_out_ack <= '1';
			seloutfifo_out_ack <= '1';
			if (send_last_layer = '1' and inst_lastfifo_out_rdy = '1') or (send_last_layer = '0' and seloutfifo_out_rdy = '1') then
				txcnt_cur_n <= txcnt_cur + 1;
				-- Pre-generate the flag for last expected output
				txcnt_last_n <= '1' when txcnt_cur = txcnt_want - 2 else '0';
			end if;

		elsif txrun_en = '0' then

			txbuf_wen_n  <= (0 => '1', others => '0');
			txbuf_full_n <= '0';
			txcnt_last_n <= '0';

			if (req_start_send = '1') and (txcnt_want > 0) then
				txrun_en_n   <= '1';
				txcnt_last_n <= '1' when txcnt_want = 1 else '0';
				txcnt_cur_n  <= (others => '0');
			end if;

		else

			-- Send the buffer to the TX fifo
			if txbuf_full = '1' then
				inst_txfifo_in_ack <= '1';
				if inst_txfifo_in_rdy = '1' then
					txbuf_full_n <= '0';
					txbuf_wen_n  <= (0 => '1', others => '0');
					var_buf_end := true;
					if txcnt_cur = txcnt_want then
						txrun_en_n <= '0';
					end if;
				end if;
			end if;

			-- Get the output values out of the last FIFO, enqueue them in the TX buffer
			if ((txbuf_full = '0') or (var_buf_end = true)) and (txcnt_cur < txcnt_want) then
				inst_lastfifo_out_ack <= '1';
				seloutfifo_out_ack <= '1';
				if (send_last_layer = '1' and inst_lastfifo_out_rdy = '1') or (send_last_layer = '0' and seloutfifo_out_rdy = '1') then
					-- Write in the buffer
					for i in 0 to TXBUF_WEN_SIZE-1 loop
						if txbuf_wen(i) = '1' then
							for po in 0 to OUTPAR-1 loop
								txbuf_data_n((i*OUTPAR+po+1)*WOUT_ROUND_POW2-1 downto (i*OUTPAR+po)*WOUT_ROUND_POW2) <=
									std_logic_vector(resize(unsigned(inst_lastfifo_out_data((po+1)*LASTFIFO_WDATA-1 downto po*LASTFIFO_WDATA)), WOUT_ROUND_POW2))
									when send_last_layer = '1' and LASTFIFO_SDATA = false else
									std_logic_vector(resize(  signed(inst_lastfifo_out_data((po+1)*LASTFIFO_WDATA-1 downto po*LASTFIFO_WDATA)), WOUT_ROUND_POW2))
									when send_last_layer = '1' and LASTFIFO_SDATA = true else
									-- FIXME Missing spec about how to handle PAR_OUT > 1 with SELOUT
									std_logic_vector(resize(unsigned(seloutfifo_out_data), WOUT_ROUND_POW2));
							end loop;
						end if;
					end loop;
					-- Rotate the Write Enable flags
					if TXBUF_WEN_SIZE > 1 then
						txbuf_wen_n <= txbuf_wen(TXBUF_WEN_SIZE-2 downto 0) & txbuf_wen(TXBUF_WEN_SIZE-1);
					end if;
					-- Mark the buffer as full
					if txbuf_wen(TXBUF_WEN_SIZE-1) = '1' or txcnt_last = '1' then
						txbuf_full_n <= '1';
					end if;
					-- Increment the counter of outputs
					txcnt_cur_n <= txcnt_cur + 1;
					-- Pre-generate the flag for last expected output
					txcnt_last_n <= '1' when txcnt_cur = txcnt_want - 2 else '0';
				end if;
			end if;

		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			inst_txfifo_in_ack    <= '0';
			inst_lastfifo_out_ack <= '0';
			seloutfifo_out_ack    <= '0';

			txrun_en_n   <= '0';
			txbuf_data_n <= (others => '0');
			txbuf_wen_n  <= (0 => '1', others => '0');
			txbuf_full_n <= '0';
			txcnt_cur_n  <= (others => '0');
			txcnt_last_n <= '0';

		end if;

	end process;

	-- Combinatorial process - Clock counter
	process (all)
	begin

		-- Default assignments
		clkcnt_n     <= clkcnt;
		clkcnt_en_n  <= clkcnt_en;
		clkcnt_end_n <= clkcnt_end;

		-- Increment
		if clkcnt_en = '1' and clkcnt_end = '0' then
			-- Use resize to make sure ghdl does not emit an overflow error
			clkcnt_n <= resize(resize(clkcnt, clkcnt'length+1) + 1, clkcnt'length);
		end if;

		-- Set the enable flag at input events
		if inst_firstfifo_in_rdy = '1' and inst_firstfifo_in_ack = '1' then
			clkcnt_en_n <= '1';
		end if;

		-- Clear the enable flag at last output event
		if inst_lastfifo_out_rdy = '1' and inst_lastfifo_out_ack = '1' and txcnt_last = '1' then
			clkcnt_end_n <= '1';
		end if;

		-- On counter read event : stop counting, and shift the clock counter
		if (slv_reg_rden = '1') and (unsigned(slv_reg_rdidx) = 10) then
			clkcnt_end_n <= '1';
			clkcnt_n <= shift_right(clkcnt, 32);
		end if;

	end process;

	-- Combinatorial process : Unconditional read from slave registers
	process (all)
	begin

		-- Default read value
		slv_reg_rddata <= (others => '0');

		-- Actually read the register
		-- Note : there is no read event handled in this process, so no need to check the signal rden

		for i in 0 to SLV_REGS_NB-1 loop
			if to_integer(unsigned(slv_reg_rdidx)) = i then
				slv_reg_rddata <= slv_regs(i*32+31 downto i*32+ 0);
			end if;
		end loop;

		-- Events when reading from specific registers

		if to_integer(unsigned(slv_reg_rdidx)) = 1 then

			-- Read the first network config register, or the memory of network config
			slv_reg_rddata <= config_regs_read;

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 3 then

			-- Read the reset status
			slv_reg_rddata(0) <= reset_reg;

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 6 then

			-- Read the number of input values sent to the network input
			slv_reg_rddata <= std_logic_vector(rxcnt_cur);

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 7 then

			-- Read the number of result values that got out of the network
			slv_reg_rddata <= std_logic_vector(txcnt_cur);

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 10 then

			-- Read the number of clock cycles of network processing
			slv_reg_rddata <= std_logic_vector(resize(clkcnt, 32));

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 13 then

			-- Read the counters of the RX and TX fifos
			slv_reg_rddata( 0) <= inst_rxfifo_in_rdy;
			slv_reg_rddata( 1) <= inst_rxfifo_in_ack;
			slv_reg_rddata( 2) <= inst_rxfifo_out_rdy;
			slv_reg_rddata( 3) <= inst_rxfifo_out_ack;
			slv_reg_rddata(11 downto 4) <= inst_rxfifo_in_cnt;
			slv_reg_rddata(16) <= inst_txfifo_in_rdy;
			slv_reg_rddata(17) <= inst_txfifo_in_ack;
			slv_reg_rddata(18) <= inst_txfifo_out_rdy;
			slv_reg_rddata(19) <= inst_txfifo_out_ack;
			slv_reg_rddata(27 downto 20) <= inst_txfifo_out_cnt;

		end if;

		if to_integer(unsigned(slv_reg_rdidx)) = 15 then

			-- Read the status of the monitored FIFO
			slv_reg_rddata(11 downto 0) <= monitorfifo_out;

		end if;

	end process;


	------------------------------------------------
	-- Miscellaneous components around the network pipeline
	--------------------------------------------------

	-- The MUX to observe the state of FIFOs

	gen_fifomon: if CONFIG_FIFOMON = true generate
		constant WSEL : natural := storebitsnb(CONFIG_FIFOS_NB-1);
	begin

		fifomon : muxtree_bin
			generic map (
				WDATA => 12,
				NBIN  => CONFIG_FIFOS_NB,
				WSEL  => WSEL
			)
			port map (
				clk       => clk,
				-- Selection input
				sel       => monitorfifo_sel(WSEL-1 downto 0),
				-- Enable input and output
				en_in     => '1',
				en_out    => open,
				-- Data input and output
				data_in   => monitorfifo_in,
				data_out  => monitorfifo_out
			);

	end generate;

	-- The scatter-gather component to select the output layer

	gen_selout: if CONFIG_SELOUT = true generate
		constant WSEL : natural := storebitsnb(CONFIG_SELOUT_NB-1);
	begin

		scagat : scattergather
			generic map (
				WGATHER  => 33,
				WSCATTER => 16,
				NBIN     => CONFIG_SELOUT_NB,
				WSEL     => WSEL,
				EGATHER  => true,
				ESCATTER => true,
				RADIX    => 2,
				REGALL   => true
			)
			port map (
				clk         => clk,
				-- Selection input
				sel         => cur_send(WSEL-1 downto 0),
				-- Enable input and output
				en_in       => selout_en_in,
				en_out      => selout_en_out,
				-- Gather data, input and output
				gather_in   => selout_gat_in,
				gather_out  => selout_gat_out,
				-- Scatter data, input and output
				scatter_in  => selout_sca_in,
				scatter_out => selout_sca_out
			);

		selout_en_in <= not send_last_layer;

		-- Warning: Simply subtracting 8 (or 16 or 32) to the count created bugs,
		-- the FIFO can still get full when the output is stalled, and an overflow was sent to the layers, which didn't stop
		-- The following solution seems OK
		selout_sca_in <= seloutfifo_in_cnt when unsigned(seloutfifo_in_cnt) > 24 else (others => '0');

		seloutfifo_in_data <= selout_gat_out(31 downto 0);
		seloutfifo_in_ack  <= selout_gat_out(32);

		fifo : fifo_with_counters
			generic map (
				DATAW => 32,
				DEPTH => 64,
				CNTW  => 16
			)
			port map (
				clk           => CLK,
				reset         => seloutfifo_clear,
				fifo_in_data  => seloutfifo_in_data,
				fifo_in_rdy   => open,
				fifo_in_ack   => seloutfifo_in_ack,
				fifo_in_cnt   => seloutfifo_in_cnt,
				fifo_out_data => seloutfifo_out_data,
				fifo_out_rdy  => seloutfifo_out_rdy,
				fifo_out_ack  => seloutfifo_out_ack,
				fifo_out_cnt  => open
			);

		seloutfifo_clear <= '1' when reset_reg = RSTVAL_GEN else '0';

	end generate;


	-- AUTOGEN COMP INST BEGIN

	-- AUTOGEN COMP INST END


end architecture;

