

# Miscellaneous useful variables
set top cnn_axi_master_v1_0
# This is board Zybo
set part xc7z010clg400-1
# This is board Zedboard
#set part xc7z020clg484-1
# This is board ZC706
#set part xc7z045ffg900-2

# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]



# Note: For debug purposes, if you want to start from post- routing state,
# uncomment these lines and remove all above commands (except setting of parameters)
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $part

