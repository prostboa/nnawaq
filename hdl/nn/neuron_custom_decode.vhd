
-- This defines an interface for a custom decompression (or decoding) implementation
-- To be replaced/completed by custom solutions

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity neuron_custom_decode is
	generic(
		-- Parameters for port sizes
		WRAW  : natural := 1;  -- Size of raw data
		WENC  : natural := 1;  -- Size of encoded word
		-- Identifier of encoding style
		STYLE : natural := 0;
		-- Identifiers of layer and decoder
		LAYER_ID : natural := 0;
		DEC_ID   : natural := 0;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true
	);
	port(
		clk     : in  std_logic;
		reset   : in  std_logic;
		-- Data signals
		in_enc  : in  std_logic_vector(WENC-1 downto 0);
		out_raw : out std_logic_vector(WRAW-1 downto 0);
		-- Tag, input and output
		tag_in  : in  std_logic_vector(TAGW-1 downto 0);
		tag_out : out std_logic_vector(TAGW-1 downto 0)
	);
end neuron_custom_decode;

architecture synth of neuron_custom_decode is

	-- Component to decompress binary into ternary values
	component terncompress_dec_wrap is
		generic(
			NTER : natural := 3;
			BINW : natural := 5
		);
		port(
			bin_in  : in  std_logic_vector(BINW-1 downto 0);
			ter_out : out std_logic_vector(2*NTER-1 downto 0)
		);
	end component;

begin

	gen : if STYLE = 0 generate

		out_raw <= in_enc;
		tag_out <= tag_in;

	-- Ternary compression

	elsif STYLE = 3 generate
		signal ter_out : std_logic_vector(WRAW-1 downto 0) := (others => '0');
	begin

		dec_3t5b : terncompress_dec_wrap
			generic map (
				NTER => WRAW / 2,
				BINW => WENC
			)
			port map (
				bin_in  => in_enc,
				ter_out => ter_out
			);

		process(clk)
		begin
			if rising_edge(clk) then
				out_raw <= ter_out;
				tag_out <= tag_in;
			end if;
		end process;

	elsif STYLE = 5 generate
		signal ter_out : std_logic_vector(WRAW-1 downto 0) := (others => '0');
	begin

		dec_5t8b : terncompress_dec_wrap
			generic map (
				NTER => WRAW / 2,
				BINW => WENC
			)
			port map (
				bin_in  => in_enc,
				ter_out => ter_out
			);

		process(clk)
		begin
			if rising_edge(clk) then
				out_raw <= ter_out;
				tag_out <= tag_in;
			end if;
		end process;

	-- Add custom styles here

	-- Other compression styles

	else generate

		assert false report "ERROR Component neuron_custom_decode : Unknown style " & to_string(STYLE) severity failure;

	end generate;

end architecture;

