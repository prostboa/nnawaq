-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unimacro components
-- It is meant to be compiled in library "unimacro"

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BRAM_SDP_MACRO is
	generic (
		BRAM_SIZE   : string;    -- Target BRAM, 18Kb or 36Kb
		DEVICE      : string;    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
		WRITE_WIDTH : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
		READ_WIDTH  : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
		DO_REG      : integer;   -- Optional output register (0 or 1)
		-- Set/Reset value for port output
		SRVAL       : bit_vector;
		-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchronous clocks on ports
		WRITE_MODE  : string;
		-- Initial values on output port
		INIT        : bit_vector;
		-- Initial values when stored in a file
		INIT_FILE   : string
	);
	port (
		-- Output read data port, width defined by READ_WIDTH parameter
		DO     : out std_logic_vector;
		-- Input write data port, width defined by WRITE_WIDTH parameter
		DI     : in std_logic_vector;
		-- Input read address, width defined by read port depth
		RDADDR : in std_logic_vector;
		-- 1-bit input read clock
		RDCLK  : in std_logic;
		-- 1-bit input read port enable
		RDEN   : in std_logic;
		-- 1-bit input read output register clock enable
		REGCE  : in std_logic;
		-- 1-bit input reset
		RST    : in std_logic;
		-- Input write enable, width defined by write port depth
		WE     : in std_logic_vector;
		-- Input write address, width defined by write port depth
		WRADDR : in std_logic_vector;
		-- 1-bit input write clock
		WRCLK  : in std_logic;
		-- 1-bit input write port enable
		WREN   : in std_logic
	);
end BRAM_SDP_MACRO;

architecture simu of BRAM_SDP_MACRO is

	-- Utility function to pre-decode the WRITE_MODE string, for simulation efficiency
	function decode_write_mode(str : string) return boolean is
	begin
		if WRITE_MODE = "READ_FIRST" then
			return (str = WRITE_MODE);
		elsif WRITE_MODE = "WRITE_FIRST" then
			return (str = WRITE_MODE);
		elsif WRITE_MODE = "NO_CHANGE" then
			return (str = WRITE_MODE);
		end if;
		assert false report "Error : Unrecognized WRITE_MODE" severity failure;
		return false;
	end function;

	constant internal_write_first : boolean := decode_write_mode("WRITE_FIRST");
	constant internal_no_change   : boolean := decode_write_mode("NO_CHANGE");

	constant CELLS : natural := 2 ** RDADDR'length;

	type mem_type is array(0 to CELLS-1) of std_logic_vector(READ_WIDTH-1 downto 0);
	signal mem : mem_type;

	signal reg_read : std_logic_vector(READ_WIDTH-1 downto 0) := to_stdlogicvector(INIT(0 to READ_WIDTH-1));
	signal reg_do   : std_logic_vector(READ_WIDTH-1 downto 0) := to_stdlogicvector(INIT(0 to READ_WIDTH-1));

begin

	assert READ_WIDTH = WRITE_WIDTH report "Error : Simulation model for BRAM_SDP_MACRO does not support different READ_WIDTH and WRITE_WIDTH" severity failure;
	assert RDCLK = WRCLK report "Error : Simulation model for BRAM_SDP_MACRO does not support different clocks for Read and Write" severity failure;

	process(RDCLK)
	begin
		if rising_edge(RDCLK) then

			assert unsigned(WE) = 0 or unsigned(not WE) = 0 report "Error : Simulation model for BRAM_SDP_MACRO does not support per-byte WE" severity failure;

			if RDEN = '1' then
				reg_read <=
					DI when internal_write_first = true and WREN = '1' and WRADDR = RDADDR else
					reg_read when internal_no_change = true and WREN = '1' and WRADDR = RDADDR else
					mem(to_integer(unsigned(RDADDR)));
			end if;

			if WREN = '1' then
				mem(to_integer(unsigned(WRADDR))) <= DI;
			end if;

			if REGCE = '1' then
				reg_do <= reg_read;
			end if;

			if RST = '1' then
				reg_read <= to_stdlogicvector(SRVAL(0 to READ_WIDTH-1));
				reg_do   <= to_stdlogicvector(SRVAL(0 to READ_WIDTH-1));
			end if;

		end if;
	end process;

	DO <= reg_do when DO_REG = 1 else reg_read;

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BRAM_SINGLE_MACRO is
	generic (
		BRAM_SIZE   : string;    -- Target BRAM, 18Kb or 36Kb
		DEVICE      : string;    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
		WRITE_WIDTH : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
		READ_WIDTH  : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
		DO_REG      : integer;   -- Optional output register (0 or 1)
		-- Set/Reset value for port output
		SRVAL       : bit_vector;
		-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchronous clocks on ports
		WRITE_MODE  : string;
		-- Initial values on output port
		INIT        : bit_vector;
		-- Initial values when stored in a file
		INIT_FILE   : string
	);
	port (
		-- Output read data port, width defined by READ_WIDTH parameter
		DO     : out std_logic_vector;
		-- Input write data port, width defined by WRITE_WIDTH parameter
		DI     : in std_logic_vector;
		-- Input address, width defined by read/write port depth
		ADDR : in std_logic_vector;
		-- 1-bit input read clock
		CLK  : in std_logic;
		-- 1-bit input RAM enable
		EN   : in std_logic;
		-- 1-bit input read output register clock enable
		REGCE  : in std_logic;
		-- 1-bit input reset
		RST    : in std_logic;
		-- Input write enable, width defined by write port depth
		WE     : in std_logic_vector
	);
end BRAM_SINGLE_MACRO;

architecture simu of BRAM_SINGLE_MACRO is

	-- Utility function to pre-decode the WRITE_MODE string, for simulation efficiency
	function decode_write_mode(str : string) return boolean is
	begin
		if WRITE_MODE = "READ_FIRST" then
			return (str = WRITE_MODE);
		elsif WRITE_MODE = "WRITE_FIRST" then
			return (str = WRITE_MODE);
		elsif WRITE_MODE = "NO_CHANGE" then
			return (str = WRITE_MODE);
		end if;
		assert false report "Error : Unrecognized WRITE_MODE" severity failure;
		return false;
	end function;

	constant internal_write_first : boolean := decode_write_mode("WRITE_FIRST");
	constant internal_no_change   : boolean := decode_write_mode("NO_CHANGE");

	constant CELLS : natural := 2 ** ADDR'length;

	type mem_type is array(0 to CELLS-1) of std_logic_vector(READ_WIDTH-1 downto 0);
	signal mem : mem_type;

	signal reg_read : std_logic_vector(READ_WIDTH-1 downto 0) := to_stdlogicvector(INIT(0 to READ_WIDTH-1));
	signal reg_do   : std_logic_vector(READ_WIDTH-1 downto 0) := to_stdlogicvector(INIT(0 to READ_WIDTH-1));

begin

	assert READ_WIDTH = WRITE_WIDTH report "Error : Simulation model for BRAM_SINGLE_MACRO does not support different READ_WIDTH and WRITE_WIDTH" severity failure;

	process(CLK)
	begin
		if rising_edge(CLK) then

			assert unsigned(WE) = 0 or unsigned(not WE) = 0 report "Error : Simulation model for BRAM_SINGLE_MACRO does not support per-byte WE" severity failure;

			if EN = '1' then

				reg_read <=
					DI when internal_write_first = true and unsigned(WE) /= 0 else
					reg_read when internal_no_change = true and unsigned(WE) /= 0 else
					mem(to_integer(unsigned(ADDR)));

				if unsigned(WE) /= 0 then
					mem(to_integer(unsigned(ADDR))) <= DI;
				end if;

			end if;

			if REGCE = '1' then
				reg_do <= reg_read;
			end if;

			if RST = '1' then
				reg_read <= to_stdlogicvector(SRVAL(0 to READ_WIDTH-1));
				reg_do   <= to_stdlogicvector(SRVAL(0 to READ_WIDTH-1));
			end if;

		end if;
	end process;

	DO <= reg_do when DO_REG = 1 else reg_read;

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- FIXME : WIDTH_P does not exist in Vivado definition
entity MULT_MACRO is
	generic (
		DEVICE  : string;   -- Target Device: "VIRTEX5", "7SERIES", "SPARTAN6"
		LATENCY : integer;  -- Desired clock cycle latency, 0-4
		--WIDTH_P : integer;  -- Multiplier output bus width, 1-48
		WIDTH_A : integer;  -- Multiplier A-input bus width, 1-25
		WIDTH_B : integer   -- Multiplier B-input bus width, 1-18
	);
	port (
		P   : out std_logic_vector;  -- Multiplier output bus, width determined by WIDTH_P generic
		A   : in  std_logic_vector;  -- Multiplier input A bus, width determined by WIDTH_A generic
		B   : in  std_logic_vector;  -- Multiplier input B bus, width determined by WIDTH_B generic
		CE  : in  std_logic;         -- 1-bit active high input clock enable
		CLK : in  std_logic;         -- 1-bit positive edge clock input
		RST : in  std_logic          -- 1-bit input active high reset
	);
end MULT_MACRO;

architecture simu of MULT_MACRO is

	constant WIDTH_P : natural := P'length;
	signal sig_mul : std_logic_vector(WIDTH_P-1 downto 0) := (others => '0');

begin

	sig_mul <= std_logic_vector(resize(signed(A) * signed(B), WIDTH_P));

	gen_out : if LATENCY = 0 generate

		P <= sig_mul;

	else generate
		signal latency_reg : std_logic_vector(LATENCY*WIDTH_P-1 downto 0) := (others => '0');
	begin

		process(CLK)
		begin
			if rising_edge(clk) then
				if CE = '1' then
					if LATENCY >= 2 then
						latency_reg <= latency_reg((LATENCY-1)*WIDTH_P-1 downto 0) & sig_mul;
					else
						latency_reg <= sig_mul;
					end if;
				end if;
				if RST = '1' then
					latency_reg <= (others => '0');
				end if;
			end if;
		end process;

		P <= latency_reg(LATENCY*WIDTH_P-1 downto (LATENCY-1)*WIDTH_P);

	end generate;

end architecture;


