
.PHONY : update submodules

default :
	echo "Error : No designated Makefile target"

SUBMODULES = hwopt-xilinx-7series datasets

submodules : $(SUBMODULES) hwopt

update : $(SUBMODULES)
	git submodule update --remote

$(SUBMODULES) &:
	git submodule update --init

hwopt : hwopt-xilinx-7series
	ln -sf $< $@

