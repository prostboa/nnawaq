
# Clock 200 MHz
set_property PACKAGE_PIN  H19         [get_ports sysclk200_p];
set_property IOSTANDARD   DIFF_SSTL15 [get_ports sysclk200_p];
set_property PACKAGE_PIN  G18         [get_ports sysclk200_n];
set_property IOSTANDARD   DIFF_SSTL15 [get_ports sysclk200_n];

# Clock frequency = 200 MHz, period = 5 ns
create_clock -name clk -period 5 [get_pins ibufds_i/O]

# Reset
set_property PACKAGE_PIN  AV40     [get_ports reset];
set_property IOSTANDARD   LVCMOS18 [get_ports reset];

# UART
set_property PACKAGE_PIN  AU33     [get_ports uart_rx];
set_property IOSTANDARD   LVCMOS18 [get_ports uart_rx];
set_property PACKAGE_PIN  AU36     [get_ports uart_tx];
set_property IOSTANDARD   LVCMOS18 [get_ports uart_tx];

# PMBus
set_property PACKAGE_PIN AY39     [get_ports pmbus_sda]
set_property IOSTANDARD  LVCMOS18 [get_ports pmbus_sda]
set_property PACKAGE_PIN AW37     [get_ports pmbus_scl]
set_property IOSTANDARD  LVCMOS18 [get_ports pmbus_scl]

# Config voltage during programming of the FPGA
set_property CFGBVS GND [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]

