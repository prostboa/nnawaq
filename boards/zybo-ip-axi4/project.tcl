
######################################################################################
## This is a non-project script, intended to synthetize, place and route a complete
## design, containing a quantized neural network (inside "cnn_axi_master slave" block).
## NB: It was validated with Vivado versions 2017.2 and 2021.1 (version used for
## this script must be the same as when creating the design at top level).
######################################################################################

## Get Vivado's version
set vivado_ver [version -short]

## Miscellaneous useful variables
## NB: "project_name" & "design_name" variables must correspond to the defined ones in tcl script for top level creation
set project_name project_${vivado_ver}
set design_name design_top

set top ${design_name}
set outputDir "./"
file mkdir $outputDir

set hwoptdir ./hdl/hwopt
set nndir    ./hdl/nn

set designdir ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/

## This is board Zybo
set part xc7z010clg400-1
## This is board Zedboard
#set part xc7z020clg484-1
## This is board ZC706
#set part xc7z045ffg900-2

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]


## Setup design sources
#read_vhdl -vhdl2008  /${project_name}/${project_name}.gen/sources_1/bd/${design_name}/hdl/${design_name}_wrapper.vhd
if { $vivado_ver eq "2017.2" } {
	read_vhdl -vhdl2008 ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/hdl/${design_name}.vhd
} elseif { $vivado_ver eq "2018.3" } {
	read_vhdl -vhdl2008 ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/synth/${design_name}.vhd
} else {
	read_vhdl -vhdl2008  ./${project_name}/${project_name}.gen/sources_1/bd/${design_name}/synth/${design_name}.vhd
}

set sources_vhd [concat \
	[glob ./hdl/*.vhd] \
	[glob $hwoptdir/*.vhd] \
	[glob $nndir/*.vhd] \
]

foreach f $sources_vhd {
	read_vhdl -vhdl2008 $f
}

#report_ip_status
read_ip $designdir/ip/${design_name}_axi_protocol_converter_0_0/${design_name}_axi_protocol_converter_0_0.xci
read_ip $designdir/ip/${design_name}_axi_protocol_converter_1_0/${design_name}_axi_protocol_converter_1_0.xci
read_ip $designdir/ip/${design_name}_processing_system7_0_0/${design_name}_processing_system7_0_0.xci
read_ip $designdir/ip/${design_name}_rst_ps7_0_50M_0/${design_name}_rst_ps7_0_50M_0.xci
synth_ip [get_ips]


# No limit for message: Sequential element unused
set_msg_config -id {[Synth 8-3333]} -limit 1000000
# No limit for message: Constant propagation
set_msg_config -id {[Synth 8-3332]} -limit 1000000
# No limit for message: Unconnected port
set_msg_config -id {[Synth 8-3331]} -limit 1000000
# No limit for message: Tying undriven pin to constant 0
set_msg_config -id {[Synth 8-3295]} -limit 1000000
# Hide messages: Sub-optimal pipelining for BRAM
set_msg_config -id {[Synth 8-4480]} -suppress

# Run logic synthesis
synth_design -top $top -keep_equivalent_registers -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -flatten_hierarchy none -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -flatten_hierarchy full -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -resource_sharing on -directive AreaOptimized_high

# Run logic optimization
opt_design
# Warning : This seems not useful with 2015.3, and crashes with 2017.2
#opt_design -directive ExploreArea

# Report timing and utilization estimates
report_utilization    -file $outputDir/post_synth_util.rpt
report_timing_summary -file $outputDir/post_synth_timing_summary.rpt

# Write design checkpoint
write_checkpoint -force $outputDir/post_synth.dcp


# Note: For debug purposes, if you want to start from post- logic synthesis state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_synth.dcp
#link_design -top $top -part $part


# If vivado is stopped because of an overuse of available cells, you can set following option to even try placement
#set_param place.skipUtilizationCheck 1

# Run automated placement of the remaining of the design
place_design
#place_design -directive Explore
#place_design -directive WLDrivenBlockPlacement
#place_design -directive ExtraNetDelay_high

# Physical logic optimization
phys_opt_design -directive AggressiveExplore

# Report utilization and timing estimates
report_utilization       -file $outputDir/post_place_util.rpt
report_clock_utilization -file $outputDir/clock_util.rpt
report_timing_summary    -file $outputDir/post_place_timing_summary.rpt

# Write design checkpoint
write_checkpoint -force $outputDir/post_place.dcp


# Note: For debug purposes, if you want to start from post- placement state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_place.dcp
#link_design -top $top -part $part

# Run first routing
#route_design
route_design -tns_cleanup -directive Explore
#route_design -tns_cleanup -preserve

# Note: For more optimization, perform another placement optimization and re-route
#place_design -post_place_opt
#phys_opt_design -directive AggressiveExplore
#route_design

# Report the routing status, timing, power, design rule check
report_route_status   -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power          -file $outputDir/post_route_power.rpt
report_drc            -file $outputDir/post_route_drc.rpt

# Write the post-route design checkpoint
write_checkpoint -force $outputDir/post_route.dcp


# Note: For debug purposes, if you want to start from post- routing state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_route.dcp
#link_design -top $top -part $part

# Optionally generate post- routing simulation model
#set postparDir $outputDir/vhdl-postpar/
#file mkdir $postparDir
#write_vhdl -force $postparDir/top.vhd
#write_sdf -force $postparDir/top.sdf

# Generate the bitstream
write_bitstream -force $outputDir/$top.bit

