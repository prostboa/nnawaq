#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It generates an experimental quantized neural network designed to classify wafer defects

# Input images : 224x224x1 1b
# Output : 58 classes
# NN including inception block, with 4 parallel branches

global env

set par 1
if {[info exists env(PAR)]} {
	set par $env(PAR)
}

if { ! [info exists freq]} {
	set freq 100
}
if {[info exists env(FREQ)]} {
	set freq $env(FREQ)
}

# Optional to apply current time multiplexing to fully-connected layers
set tmux_extrawin 1
# Optional to reduce resources by having more PAR_IN and less physical neurons
set use_maxparin 1
# Optional to use ternary weights in hidden layers, instead of binary
set use_hidden_ter 1

nn_set f=224/224/1
nn_set fn=1
nn_set inpar=1

nn_set in=1u
nn_set acts=4u
nn_set weights=1s

if { $use_hidden_ter == 1 } {
	nn_set weights=2s
}

nn_set neu_style=2

# Default settings for activations
nn_set norm_wbias=8
nn_set norm_wmul=0
nn_set norm_wshr=4
nn_set relu=0/15

nn_set no_fifo_win_pool=true
nn_set no_fifo_neu_relu=true
nn_set no_fifo_norm_relu=true

set prevfork ""

proc create_nn_conv_neu {win step pad neu nwin} {
	global prevfork
	nn_layer_create window $win $step $pad $nwin $prevfork
	nn_layer_create neuron $neu
	nn_layer_create norm
	nn_layer_create relu
	set prevfork ""
}
proc create_nn_conv_pool {win step pad nwin} {
	global prevfork
	nn_layer_create window $win $step $pad $nwin $prevfork
	nn_layer_create maxpool
	set prevfork ""
}


# Create the network:

# image 224x224x1 1b

create_nn_conv_neu win=7 step=2 pad=0 neu=32 nwin=109
# image 109x109x32 4b
create_nn_conv_pool win=2 step=2 pad=0 nwin=54
# image 54x54x32 4b

# Fork layer:
nn_layer_create fork
set fork_id [nn_get last]
set cat_prev [list ]

# Parallel part 1
set prevfork prev=$fork_id
create_nn_conv_pool win=3 step=1 pad=0 nwin=54
create_nn_conv_neu win=1 step=1 pad=0 neu=32 nwin=54
lappend cat_prev prev=[nn_get last]

# Parallel part 2
set prevfork prev=$fork_id
create_nn_conv_neu win=1 step=1 pad=0 neu=8 nwin=54
create_nn_conv_neu win=3 step=1 pad=0 neu=32 nwin=54
lappend cat_prev prev=[nn_get last]

# Parallel part 3
set prevfork prev=$fork_id
create_nn_conv_neu win=1 step=1 pad=0 neu=8 nwin=54
create_nn_conv_neu win=5 step=1 pad=0 neu=32 nwin=54
lappend cat_prev prev=[nn_get last]

# Parallel part 4
# Note : May need to reserve extra storage on window because the layers in parallel are not balanced
set prevfork prev=$fork_id
create_nn_conv_neu win=1 step=1 pad=0 neu=32 nwin=54
nn_layer_set win2 bufy=8
lappend cat_prev prev=[nn_get last]

# Concat layer:
nn_layer_create cat $cat_prev

# The rest of the convolution layers:
# image 54x54x128 4b

create_nn_conv_pool win=3 step=2 pad=0 nwin=27
# image 27x27x128 4b
create_nn_conv_neu win=1 step=1 pad=0 neu=12 nwin=27
# image 27x27x12 4b
create_nn_conv_neu win=3 step=2 pad=0 neu=116 nwin=14
# image 14x14x116 4b
create_nn_conv_neu win=3 step=2 pad=0 neu=116 nwin=7
# image 7x7x116 4b

# Output neuron layer, fully connected
nn_layer_create flatten
if { $tmux_extrawin == 1 } {
	nn_layer_create window win=1x1 step=1x1 pad=0x0
}
nn_layer_create neuron neu=58
set neu_last [nn_get last]
# image 1x1x58
nn_layer_create softmax
# image 1x1x1

# Hardcoded weight widths for select layers
nn_layer_set neu0 weights=4s
nn_layer_set $neu_last weights=4s

# For information, print the network details
nn_print -cycles

# For information, print the network details
nn_print -cycles

# Get the bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_orig [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"

# Apply parallelism and time multiplexing
puts "Applying parallelism levels for PAR=$par"
nn_autopar $par
puts "Applying time multiplexing to fastest layers"
nn_autotmux

if { $use_maxparin != 0 } {
	puts "Applying more parallelism to reduce the number of physical neurons"
	nn_maxparin
}

# For information, print the network details
nn_print -cycles

# Get the new bottleneck layer
set layer_bot [nn_get bottleneck]
set cycles_fast [nn_layer_get $layer_bot cycles_max]
puts "Bottleneck layer is $layer_bot : input cycles [nn_layer_get $layer_bot cycles_in], output cycles [nn_layer_get $layer_bot cycles_out]"
puts "Acceleration : cycles $cycles_orig -> $cycles_fast, ratio [expr 1.0 * $cycles_orig / $cycles_fast]"
puts "Performance (assuming $freq MHz) : [expr 1000000.0 * $freq / $cycles_fast] images/sec"

# Hardcoded setting of LUTRAM/BRAM usage : for 1s weights in hidden layers
if { $use_hidden_ter == 0 } {
	# Neuron layers
	nn_set lut_threshold=256
	nn_layer_set neu3 mem=lut
	nn_layer_set neu7 mem=lut
	nn_layer_set neu9 mem=lut
	# Window layers
	nn_set win_mem=bram
	nn_layer_set win0 mem=lut
	nn_layer_set win3 mem=lut
	nn_layer_set win4 mem=lut
	nn_layer_set win8 mem=lut
	nn_layer_set win11 mem=lut
}

# Hardcoded setting of LUTRAM/BRAM usage : for ternary weights in hidden layers
if { $use_maxparin == 0 && $use_hidden_ter != 0 } {
	# Neuron layers
	nn_set lut_threshold=256
	nn_layer_set neu3 mem=lut
	nn_layer_set neu7 mem=lut
	nn_layer_set neu8 mem=lut
	# Window layers
	nn_set win_mem=bram
	nn_layer_set win0 mem=lut
	nn_layer_set win3 mem=lut
	nn_layer_set win4 mem=lut
	nn_layer_set win5 mem=lut
	nn_layer_set win6 mem=lut
	nn_layer_set win8 mem=lut
	nn_layer_set win10 mem=lut
	nn_layer_set win11 mem=lut
	nn_layer_set win13 mem=lut
}

# Hardcoded setting of LUTRAM/BRAM usage : for ternary weights in hidden layers
# This is adapted to case with max PAR_IN and TMUX in neuron layers
if { $use_maxparin != 0 && $use_hidden_ter != 0 } {
	# Neuron layers
	nn_set lut_threshold=256
	nn_layer_set neu3 mem=lut
	nn_layer_set neu5 mem=lut
	nn_layer_set neu7 mem=lut
	# Window layers
	nn_set win_mem=bram
	nn_layer_set win0 mem=lut
	nn_layer_set win1 mem=bram
	nn_layer_set win3 mem=lut
	nn_layer_set win4 mem=lut
	nn_layer_set win5 mem=bram
	nn_layer_set win6 mem=lut
	nn_layer_set win8 mem=lut
	nn_layer_set win11 mem=lut
	nn_layer_set win12 mem=lut
	nn_layer_set win13 mem=lut
}

nn_finalize_hw_config
puts "Total number of layers : [nn_get layers_nb] layers"
nn_print -type-neu

# Set ternary compression on largest BRAM neuron layer
if { True && $use_hidden_ter != 0 } {
	# Activate compression in neu9 to free some BRAMs, then in turn free some LUTRAMs
	nn_layer_set neu9 comp=5/5/8
	# Need to re-run finalization of HW to recompute the memory width
	nn_finalize_hw_config
}

if { False && $use_hidden_ter != 0 } {
	#foreach layer [nn_get layers -type-neu -mem-bram] { nn_layer_set $layer tercomp=3t5b }
	foreach layer [nn_get layers -type-neu -mem-bram] { nn_layer_set $layer comp=5/5/8 }
	# Need to re-run finalization of HW to recompute the memory width
	nn_finalize_hw_config
}

# To help tweak per-layer memory implem
#nn_print_mem -type-neu
#nn_print_mem -type-win
foreach layer [list win neu norm] { nn_print_mem -type-$layer }
nn_print_mem -total

