
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnawaq_axi4lite_bench is
	generic (
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnawaq_axi4lite_bench;

architecture simu of nnawaq_axi4lite_bench is

	signal clk           : std_logic := '0';
	signal clk_next      : std_logic := '0';
	signal clear         : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Constants for user IP

	constant C_S_AXI_DATA_WIDTH : integer := 32;
	constant C_S_AXI_ADDR_WIDTH : integer := 32;

	-- Signals for AXI4 (lite) channel

	signal GP_AXI_ACLK    : std_logic := '0';
	signal GP_AXI_ARESETN : std_logic := '0';

	signal GP_AXI4_AWVALID : std_logic := '0';
	signal GP_AXI4_AWREADY : std_logic := '0';
	signal GP_AXI4_AWADDR  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_AWPROT  : std_logic_vector(2 downto 0) := (others => '0');

	signal GP_AXI4_WVALID  : std_logic := '0';
	signal GP_AXI4_WREADY  : std_logic := '0';
	signal GP_AXI4_WDATA   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_WSTRB   : std_logic_vector(C_S_AXI_DATA_WIDTH/8-1 downto 0) := (others => '0');

	signal GP_AXI4_BVALID  : std_logic := '0';
	signal GP_AXI4_BREADY  : std_logic := '0';
	signal GP_AXI4_BRESP   : std_logic_vector(1 downto 0) := (others => '0');

	signal GP_AXI4_ARVALID : std_logic := '0';
	signal GP_AXI4_ARREADY : std_logic := '0';
	signal GP_AXI4_ARADDR  : std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_ARPROT  : std_logic_vector(2 downto 0) := (others => '0');

	signal GP_AXI4_RVALID  : std_logic := '0';
	signal GP_AXI4_RREADY  : std_logic := '0';
	signal GP_AXI4_RDATA   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0) := (others => '0');
	signal GP_AXI4_RRESP   : std_logic_vector(1 downto 0) := (others => '0');

	-- Component declaration

	component nnawaq_axi4lite is
		generic (
			-- Users to add parameters here

			-- User parameters ends
			-- Do not modify the parameters beyond this line

			-- Width of S_AXI data bus
			C_S_AXI_DATA_WIDTH : integer := 32;
			-- Width of S_AXI address bus
			C_S_AXI_ADDR_WIDTH : integer := 5

		);
		port (
			-- Users to add ports here

			-- User ports ends
			-- Do not modify the ports beyond this line

			-- Global Clock Signal
			S_AXI_ACLK    : in  std_logic;
			-- Global Reset Signal. This Signal is Active LOW
			S_AXI_ARESETN : in  std_logic;

			-- Write address (issued by master, acceped by Slave)
			S_AXI_AWADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			-- Write channel Protection type. This signal indicates the
			-- privilege and security level of the transaction, and whether
			-- the transaction is a data access or an instruction access.
			S_AXI_AWPROT  : in  std_logic_vector(2 downto 0);
			-- Write address valid. This signal indicates that the master signaling
			-- valid write address and control information.
			S_AXI_AWVALID	: in  std_logic;
			-- Write address ready. This signal indicates that the slave is ready
			-- to accept an address and associated control signals.
			S_AXI_AWREADY	: out std_logic;

			-- Write data (issued by master, acceped by Slave)
			S_AXI_WDATA   : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			-- Write strobes. This signal indicates which byte lanes hold
			-- valid data. There is one write strobe bit for each eight
			-- bits of the write data bus.
			S_AXI_WSTRB   : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
			-- Write valid. This signal indicates that valid write
			-- data and strobes are available.
			S_AXI_WVALID  : in  std_logic;
			-- Write ready. This signal indicates that the slave
			-- can accept the write data.
			S_AXI_WREADY  : out std_logic;

			-- Write response. This signal indicates the status
			-- of the write transaction.
			S_AXI_BRESP   : out std_logic_vector(1 downto 0);
			-- Write response valid. This signal indicates that the channel
			-- is signaling a valid write response.
			S_AXI_BVALID  : out std_logic;
			-- Response ready. This signal indicates that the master
			-- can accept a write response.
			S_AXI_BREADY  : in  std_logic;

			-- Read address (issued by master, acceped by Slave)
			S_AXI_ARADDR  : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
			-- Protection type. This signal indicates the privilege
			-- and security level of the transaction, and whether the
			-- transaction is a data access or an instruction access.
			S_AXI_ARPROT  : in  std_logic_vector(2 downto 0);
			-- Read address valid. This signal indicates that the channel
			-- is signaling valid read address and control information.
			S_AXI_ARVALID : in  std_logic;
			-- Read address ready. This signal indicates that the slave is
			-- ready to accept an address and associated control signals.
			S_AXI_ARREADY : out std_logic;

			-- Read data (issued by slave)
			S_AXI_RDATA   : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
			-- Read response. This signal indicates the status of the
			-- read transfer.
			S_AXI_RRESP   : out std_logic_vector(1 downto 0);
			-- Read valid. This signal indicates that the channel is
			-- signaling the required read data.
			S_AXI_RVALID  : out std_logic;
			-- Read ready. This signal indicates that the master can
			-- accept the read data and response information.
			S_AXI_RREADY  : in  std_logic

		);
	end component;

begin

	user_inst : nnawaq_axi4lite
		generic map (
			-- Users to add parameters here

			-- User parameters ends
			-- Do not modify the parameters beyond this line

			-- Width of S_AXI data bus
			C_S_AXI_DATA_WIDTH => C_S_AXI_DATA_WIDTH,
			-- Width of S_AXI address bus
			C_S_AXI_ADDR_WIDTH => C_S_AXI_ADDR_WIDTH

		)
		port map (
			-- Users to add ports here

			-- User ports ends
			-- Do not modify the ports beyond this line

			S_AXI_ACLK    => GP_AXI_ACLK,
			S_AXI_ARESETN => GP_AXI_ARESETN,

			S_AXI_AWADDR  => GP_AXI4_AWADDR(C_S_AXI_ADDR_WIDTH-1 downto 0),
			S_AXI_AWPROT  => GP_AXI4_AWPROT,
			S_AXI_AWVALID	=> GP_AXI4_AWVALID,
			S_AXI_AWREADY	=> GP_AXI4_AWREADY,

			S_AXI_WDATA   => GP_AXI4_WDATA,
			S_AXI_WSTRB   => GP_AXI4_WSTRB,
			S_AXI_WVALID  => GP_AXI4_WVALID,
			S_AXI_WREADY  => GP_AXI4_WREADY,

			S_AXI_BRESP   => GP_AXI4_BRESP,
			S_AXI_BVALID  => GP_AXI4_BVALID,
			S_AXI_BREADY  => GP_AXI4_BREADY,

			S_AXI_ARADDR  => GP_AXI4_ARADDR(C_S_AXI_ADDR_WIDTH-1 downto 0),
			S_AXI_ARPROT  => GP_AXI4_ARPROT,
			S_AXI_ARVALID => GP_AXI4_ARVALID,
			S_AXI_ARREADY => GP_AXI4_ARREADY,

			S_AXI_RDATA   => GP_AXI4_RDATA,
			S_AXI_RRESP   => GP_AXI4_RRESP,
			S_AXI_RVALID  => GP_AXI4_RVALID,
			S_AXI_RREADY  => GP_AXI4_RREADY

		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	GP_AXI_ACLK <= clk;
	GP_AXI_ARESETN <= not clear;

	-- Process that generates stimuli
	process
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		-- Wait until the device is out of its internal reset
		for i in 0 to 100 loop
			wait until rising_edge(clk);
		end loop;

		-- Write strobes are not used, permanently set to 1
		GP_AXI4_WSTRB  <= (others => '1');
		-- Permanently accept Write responses
		GP_AXI4_BREADY <= '1';
		-- Permanently accept Read responses
		GP_AXI4_RREADY <= '1';

		-- Enable freerun, in and out
		--GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(3*4, 32));
		--GP_AXI4_WDATA  <= "11000000000000000000000000000000";
		--GP_AXI4_AWVALID <= '1';
		--GP_AXI4_WVALID  <= '1';
		--wait until rising_edge(clk);
		--GP_AXI4_AWVALID <= '0';
		--GP_AXI4_WVALID  <= '0';
		--wait until rising_edge(clk);
		--wait until rising_edge(clk);

		-- Set the ID of the output layer (default "11...11" is last layer)
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(5*4, 32));
		GP_AXI4_WDATA  <= X"0000FFFF";
		--GP_AXI4_WDATA  <= X"00000001";
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Set the number of expected inputs
		-- This starts the input state machine
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(6*4, 32));
		GP_AXI4_WDATA  <= (others => '1');
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Set the number of expected outputs
		-- This starts the output state machine
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(7*4, 32));
		GP_AXI4_WDATA  <= (others => '1');
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Permanently write in the RX fifo
		GP_AXI4_AWADDR <= std_logic_vector(to_unsigned(64*4, 32));
		GP_AXI4_WDATA  <= (others => '1');
		GP_AXI4_AWVALID <= '1';
		GP_AXI4_WVALID  <= '1';

		-- Permanently read from the TX fifo
		--GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(64*4, 32));
		--GP_AXI4_ARVALID <= '1';

		-- Max processing time for Lenet5
		for i in 0 to 100000 loop
			wait until rising_edge(clk);
		end loop;

		-- Clear the permanent Write and Read
		GP_AXI4_AWVALID <= '0';
		GP_AXI4_WVALID  <= '0';
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Read from TX fifo
		GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(64*4, 32));
		GP_AXI4_ARVALID <= '1';
		wait until rising_edge(clk);
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		report "TX fifo : " & to_string(to_integer(unsigned(GP_AXI4_RDATA)));

		GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(64*4, 32));
		GP_AXI4_ARVALID <= '1';
		wait until rising_edge(clk);
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		report "TX fifo : " & to_string(to_integer(unsigned(GP_AXI4_RDATA)));



		-- Get the clock counter
		GP_AXI4_ARADDR <= std_logic_vector(to_unsigned(10*4, 32));
		GP_AXI4_ARVALID <= '1';
		wait until rising_edge(clk);
		GP_AXI4_ARVALID <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		report "Clock counter : " & to_string(to_integer(unsigned(GP_AXI4_RDATA)));

		-- End of simulation
		report "Arbitrary delay is reached";
		report "Stopping simulation";
		clk_want_stop <= true;

		-- Wait for end of simulation
		wait;

	end process;

end architecture;

