**`THIS REPOSITORY IS STILL IN CONSTRUCTION`**

NNawaq - Neural Network Adequate hardWare Architectures for Quantization
========================================================================

This repository contains hardware implementations of neural network layers,
optimized to take the most advantage of quantization.

It also provides a generator of complete neural networks with per-layer quantization,
wrappers for various FPGAs with PCIe or AXI3 interfaces,
and a driver tool to launch inference tasks with analysis of efficiency and power consumption.

The name `nnawaq` stands for Neural Network Adequate Hardware Architecture for Quantization.

Objectives
----------

The focus of the original works was energy efficiency.
For this reason, the considered hardware architectures (currently) only support in-chip storage of weights.

Also, for performance objectives,
the considered hardware architectures (currently) instantiates the full neural network pipeline in the target device.
So large networks require large amounts of resources.

The objectives of these works are multiple :
- build complete hardware accelerators for arbitrary neural networks,
- get the best performance and energy efficiency possible,
- take advantage of per-layer quantization to save resources and energy,
- optimize per-layer parallelism to balance latency along the execution pipeline,
- enable to experiment with custom implementations of multiply-add operators, activation functions, weight compression etc,
- launch inference campaigns in real conditions,
- monitor energy consumption in real time (if possible depending on FPGA board),
- use optimized low-level components and operators to get the maximum performance out of the target hardware,
- enable reproducibility of the published research results.

The hardware implementations of layers are provided as RTL level, in a HDL language (VHDL).

There is one software tool `nnawaq` to build networks, generate RTL pipelines and wrappers, and launch inference tasks.
This tool is implemented in C/C++ and can run on GNU/Linux operating systems.
It can be compiled for bare-metal target, such as for execution on Zynq chips for specific experiments (this could be used for automated validation).

The main interface to create neural networks and control inference on FPGA board is an embedded TCL interpreter.
Example TCL scripts to build demonstration networks and run inference experiments on FPGA boards are provided.

Provided neural network layers
------------------------------

The provided neural network layers are the following :
- sliding window layer (for convolutions),
- neuron layer,
- pooling layer (max/min/avg),
- batch normalisation layer,
- activation layer,
- addition layer,
- fork/concat layers,
- scatter/gather layers.

The main layers above are connected with special FIFO components.
The design of these FIFOs is specific on two aspects :
- it helps placement and routing of large networks on large devices by allowing large distance between two connected layers,
- it exposes counters on both sides (input and output) to indicate the amount of transfers (in or out) guaranteed to succeed without interruption,
  to simplify the control path of connected layers.

Experimental works
------------------

There are known bugs mainly in automatic reordering of neuron weights and on order of activations at input of ADD and POOL layers in case of parallelism.
Fixing these is high in the priority list.

Experimental support for networks ResNet (versions 18, 34, 50, 101 and 152) and MobileNet (versions 1 and 2) is in place.
They have not yet been tested on FPGA board.
Support of larger networks including LLMs is considered for future works.

Support of binary networks is currently restricted to binary weights.
Binary activations in hidden layers requires a proper binary activation layer.

Experimental implementations for logarithm number system (LNS) are included in this repository.
These may be removed or thoroughly revised in future changes.

Layers scatter/gather are lacking a VHDL implementation.
They are only modelled in the tool `nnawaq` to experiment with the actual shape that these features should take.

Testing
-------

Simulation testbenches are provided for the most critical components.
Testing is in no way exhaustive, and functionality in all conceivable conditions is not formally proven and not guaranteed.

Simulation testbenches are provided for entire networks.
This will fully validate network implementations in the case where network parameter are constants that are hard-coded in the generated VHDL.
Network implementations that expect parameters to be configured at run-time can still be simulated, but only the control flow will be valid.
This is still useful to validate run times, in clock cycles.

The simulation recipes use the VHDL simulator `ghdl`.
The reasons are multiple :
- convenience for end users about not having to manage licenses,
- support of vhdl2008 to a large enough extent,
- ability to convert to simpler vhdl or verilog for synthesis purposes,
- and simulation speed at least in its `mcode` flavour in which launch of simulations is near immediate.

Simple RTL simulation models are provided for common primitives, in particular LUTs and carry chains.
For more complex components and macros (BRAM, DSP), reduced simulation models are provided, but ideally end users need should use the simulation models provided with Xilinx tools.

The software tool `nnawaq` only has a few unit tests that can be run manually.

On-board validation
-------------------

On-board validation has been automated for publications [1][2].
Unfortunately, the setup can no longer be reused and on-board experiments are currently done manually.

The boards with Xilinx FPGAs supported by the [RIFFA PCIe framework](https://github.com/KastnerRG/riffa/) are supported, in particular :
- board [VC709](https://www.xilinx.com/products/boards-and-kits/dk-v7-vc709-g.html)
- board [ZC706](https://www.xilinx.com/products/boards-and-kits/ek-z7-zc706-g.html)

The boards with Xilinx Zynq-7000 SoC FPGA are supported with a provided AXI wrapper, in particular :
- board [Zybo](https://digilent.com/reference/programmable-logic/zybo/start)
- board [Zedboard](https://digilent.com/shop/zedboard-zynq-7000-arm-fpga-soc-development-board/)

Adapting to other RIFFA-supported boards and to other Zynq boards should be straightforward.

To allow debug of designs running on board, the designs can optionally be instrumented with extra FIFO channel selection functionality.
This enables the following investigation approaches :
- monitoring of the state of FIFOs (handshake signals and fill levels), to easily check where the pipeline may be stalled,
- selection of an arbitrary layer as output layer, to find the source of potential calculation or synchronization issues.

Documentation
-------------

For now, the best documentation about architecture of neural network layers and pipelines is actually in publications [1][2].
More detailed images will be added to the repository (when they are ready).

The software tool `nnawaq` currently has no documentation or user guide.
Interested users should start by looking into the provided example TCL scripts, and in source file `tclparser.cpp` for detailed options.

License
-------

The contents of this repository are licensed under the EUPL license.

A copy of the license is provided in this repository in file [LICENSE-EUPL-1.2-EN.txt](LICENSE-EUPL-1.2-EN.txt).

More information on the [EUPL website](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

Contact
-------

In case you obtained this work through a fork, a copy or an archive,
here is the address of the original repository :

https://gricad-gitlab.univ-grenoble-alpes.fr/prostboa/nnawaq

Main contacts :
- Adrien Prost-Boucle, [personal page](https://tima.univ-grenoble-alpes.fr/laboratory/internal-services/adrien-prost-boucle)
- Frédéric Pétrot, [personal page](https://tima.univ-grenoble-alpes.fr/research/sls/members/frederic-petrot)

Acknowledgments
---------------

(in chronological order)

Some of these works did benefit from the research project Nano2017 funded by France national research agency (ANR) in 2015-2017.

These works benefit from the research project Holigrail funded by France national research agency (ANR) since 2023.

Publications
------------

(in chronological order, most relevant publications only)

- [1] Scalable High-Performance Architecture for Convolutional Ternary Neural Networks on FPGA (FPL 2017)
- [2] High-Efficiency Convolutional Ternary Neural Networks with Custom Adder Trees and Weight Compression (TRETS 2018)
- [3] Efficient Decompression of Binary Encoded Balanced Ternary Sequences (TVLSI 2019)

