
-- This defines an interface for a custom compression (or encoding) implementation
-- To be replaced/completed by custom solutions

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity neuron_custom_encode is
	generic(
		-- Parameters for port sizes
		WRAW  : natural := 1;  -- Size of raw data
		WENC  : natural := 1;  -- Size of encoded word
		-- Identifier of encoding style
		STYLE : natural := 0;
		-- Identifiers of layer and encoder
		LAYER_ID : natural := 0;
		ENC_ID   : natural := 0;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true
	);
	port(
		clk     : in  std_logic;
		reset   : in  std_logic;
		-- Data signals
		in_raw  : in  std_logic_vector(WRAW-1 downto 0);
		out_enc : out std_logic_vector(WENC-1 downto 0);
		-- Tag, input and output
		tag_in  : in  std_logic_vector(TAGW-1 downto 0);
		tag_out : out std_logic_vector(TAGW-1 downto 0)
	);
end neuron_custom_encode;

architecture synth of neuron_custom_encode is

	-- Component to compress ternary values into binary
	component terncompress_com_wrap is
		generic(
			NTER : natural := 3;
			BINW : natural := 5
		);
		port(
			ter_in  : in  std_logic_vector(2*NTER-1 downto 0);
			bin_out : out std_logic_vector(BINW-1 downto 0)
		);
	end component;

begin

	gen : if STYLE = 0 generate

		out_enc <= in_raw;
		tag_out <= tag_in;

	-- Ternary compression

	elsif STYLE = 2 generate

		enc_2t3b : terncompress_com_wrap
			generic map (
				NTER => 2,
				BINW => 3
			)
			port map (
				ter_in  => in_raw,
				bin_out => out_enc
			);

		tag_out <= tag_in;

	elsif STYLE = 3 generate

		enc_3t5b : terncompress_com_wrap
			generic map (
				NTER => 3,
				BINW => 5
			)
			port map (
				ter_in  => in_raw,
				bin_out => out_enc
			);

		tag_out <= tag_in;

	elsif STYLE = 5 generate

		enc_5t8b : terncompress_com_wrap
			generic map (
				NTER => 5,
				BINW => 8
			)
			port map (
				ter_in  => in_raw,
				bin_out => out_enc
			);

		tag_out <= tag_in;

	-- Add custom styles here

	-- Other compression styles

	else generate

		assert false report "ERROR Component neuron_custom_encode : Unknown style " & to_string(STYLE) severity failure;

	end generate;

end architecture;

