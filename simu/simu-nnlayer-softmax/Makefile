
################################
# Testcase-specific variables
################################

TOPNAME = nnlayer_softmax_bench

HDLDIR_BENCH = .

GHDLGENERIC =

GHDLSIMUFLAGS =
#GHDLSIMUFLAGS = --stop-time=100us

WAVEFILE = wave.ghw


################################
# Most compilation and simulation recipes are in common Makefile
################################

include ../Makefile.inc

################################
# Dedicated simulation contexts
################################

# Note : To debug a simulation and have the waveforms, set the variable TARGET_WAVE=simu-wave
# Example : make TARGET_WAVE=simu-wave XILINX_VIVADO=<path> simu-f1n1
TARGET_WAVE ?= simu

# Simple tests, no parallelism

simu-f1u : simu-f1u-output.txt
simu-f1u-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=1 -gPAR_IN=1 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1s : simu-f1s-output.txt
simu-f1s-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=true -gFSIZE=1 -gPAR_IN=1 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1u-lock : simu-f1u-lock-output.txt
simu-f1u-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=1 -gPAR_IN=1 -gLOCKED=true -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4u : simu-f4u-output.txt
simu-f4u-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=4 -gPAR_IN=1 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4s : simu-f4s-output.txt
simu-f4s-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=true -gFSIZE=4 -gPAR_IN=1 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4u-lock : simu-f4u-lock-output.txt
simu-f4u-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=4 -gPAR_IN=1 -gLOCKED=true -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With input parallelism

simu-f1p3u : simu-f1p3u-output.txt
simu-f1p3u-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=1 -gPAR_IN=3 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1p3s : simu-f1p3s-output.txt
simu-f1p3s-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=true -gFSIZE=1 -gPAR_IN=3 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4p3u : simu-f4p3u-output.txt
simu-f4p3u-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=false -gFSIZE=4 -gPAR_IN=3 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4p3s : simu-f4p3s-output.txt
simu-f4p3s-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gSDATA=true -gFSIZE=4 -gPAR_IN=3 -gLOCKED=false -gFRAMES=50 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With wait delays on input and output sides

simu-delay-in : simu-delay-in-output.txt
simu-delay-in-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for fs in 1 2 3 ; do \
	for pi in 1 2 3 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$fs -gPAR_IN=$$pi -gLOCKED=false -gEN_DELAY_IN=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1

simu-delay-out : simu-delay-out-output.txt
simu-delay-out-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for fs in 1 2 3 ; do \
	for pi in 1 2 3 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$fs -gPAR_IN=$$pi -gLOCKED=false -gDELAY_OUT_DATE=20 -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1

simu-delays : simu-delays-output.txt
simu-delays-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for fs in 1 2 3 ; do \
	for pi in 1 2 3 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$fs -gPAR_IN=$$pi -gLOCKED=false -gEN_DELAY_IN=true -gDELAY_OUT_DATE=20 -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1

# Launch all tests

simus-all :
	# Simple tests
	$(MAKE) simu-f1u
	$(MAKE) simu-f1s
	$(MAKE) simu-f1u-lock
	$(MAKE) simu-f4u
	$(MAKE) simu-f4s
	$(MAKE) simu-f4u-lock
	# With parallelism
	$(MAKE) simu-f1p3u
	$(MAKE) simu-f1p3s
	$(MAKE) simu-f4p3u
	$(MAKE) simu-f4p3s
	# With wait delays on input and output sides
	$(MAKE) simu-delay-in
	$(MAKE) simu-delay-out
	$(MAKE) simu-delays

simus-clean :
	rm -f simu*output.txt *.ghw

