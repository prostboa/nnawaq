#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It runs a FC-only neural network in different conditions :
# - software execution, as CPU reference and for golden results
# - hardware accelerator already configured in the PCIe FPGA board

# Settings inherited from environment variables

global env

set swexec 0
set hwpcie 0
set hwblind 0
set hwtimeout 1000ms
set frames 10

if {[info exists env(SW)]} {
	set swexec 1
}
if {[info exists env(PCIE)]} {
	set hwpcie 1
}
if {[info exists env(PCIEBLIND)]} {
	set hwblind 1
}
if {[info exists env(PCIETIMEOUT)]} {
	set hwtimeout $env(PCIETIMEOUT)
}
if {[info exists env(FRAMES)]} {
	set frames $env(FRAMES)
}

# Build the network

nn_set hw_timeout=$hwtimeout

if {$hwpcie == 1} {

	# Read network architecture from FPGA
	nn_pcie_build

	# Print the network, for visual confirmation
	nn_print

} else {

	# Create network

	nn_set f=1024/1/1

	nn_set ter=1
	nn_set in=1u
	nn_set acts=1u
	nn_set weights=1u

	nn_set neu_style=2

	nn_layer_create neuron neu=1024
	nn_layer_create ternarize
	nn_layer_create neuron neu=10

	nn_layer_create softmax

	puts "Overview of the complete network"
	nn_print

}

# Associate configuration files

set cfg_dir "../../datasets/fc1024-mnist"

nn_layer_set neu0 cfg=$cfg_dir/weights1.txt
nn_layer_set rec0 cfg=$cfg_dir/biases1.txt
nn_layer_set neu1 cfg=$cfg_dir/weights2.txt

nn_set frames=$cfg_dir/input.txt
nn_set floop=1 ml=1

nn_set no=10

# Actual run

nn_set fn=$frames ol=last onl=0

# Note: Other useful options with raw output to ease debug (from obsolete command-line) :
# -osep ' ' -omask -ofmt '%02x' -ofmt-norm '%7.3g' -swexec-gen-in -swexec-mod 0

# Note: to get full speed, use option -noout
# Note: to only count output data, use option -freerun and observe reg 6

if {$hwblind == 1} {

	# Execution in PCIe-connected FPGA (blind mode : assume we have the right network view)
	nn_set o=out-fchw.txt
	nn_pcie_run -force

} elseif {$hwpcie == 1} {

	# Execution in PCIe-connected FPGA
	nn_set o=out-fchw.txt
	nn_pcie_run

} elseif {$swexec == 1} {

	# Execution in software
	nn_set o=out-fcsw.txt
	nn_swexec

}

