
# Miscellaneous useful variables
set top cnn_axi_master_v1_0
# This is board Zybo
set part xc7z010clg400-1
# This is board Zedboard
#set part xc7z020clg484-1
# This is board ZC706
#set part xc7z045ffg900-2

# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]

# Start from post- routing state
read_checkpoint $outputDir/post_route.dcp
link_design -top $top -part $part



proc runPPO { {numIters 1} } {
	global outputDir
	for {set i 0} {$i < $numIters} {incr i} {
		puts "\n#####################################"
		puts "# Launching optimization iteration $i"
		puts "#####################################\n"

		# Optional (note: this often destroys everything router has carefully done...)
		#place_design -post_place_opt

		# Optional but advised
		phys_opt_design -directive AggressiveExplore

		#route_design
		route_design -tns_cleanup -directive Explore
		#route_design -tns_cleanup -preserve

		# Stop if timing is met
		if {[get_property SLACK [get_timing_paths ]] >= 0} {break};

		# Save the intermediate design checkpoint
		write_checkpoint -force $outputDir/post_route.dcp
	}
}

# Perform post-routing placement optimization and re-route
runPPO 50



# Report the routing status, timing, power, design rule check
report_route_status   -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power          -file $outputDir/post_route_power.rpt
report_drc            -file $outputDir/post_route_drc.rpt

# Write the post-route design checkpoint
write_checkpoint -force $outputDir/post_route.dcp

# Generate the bitstream
write_bitstream -force $outputDir/$top.bit

