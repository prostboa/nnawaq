
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
	port (
		sysclk200_p : in  std_logic;
		sysclk200_n : in  std_logic;
		-- The push buttons
		reset       : in  std_logic;
		-- The UART ports
		uart_rx     : in  std_logic;
		uart_tx     : out std_logic;
		-- The I2C ports
		pmbus_scl   : inout std_logic;
		pmbus_sda   : inout std_logic
	);
end top;

architecture synth of top is

	signal clk : std_logic;
	signal reset1, reset2 : std_logic;

	component IBUFDS is
		port(
			O  : out std_logic;
			I  : in  std_logic;
			IB : in  std_logic
		);
	end component;

	component uart2iic is
		generic (
			FREQ_IN   : natural := 100000000;
			FREQ_I2C  : natural := 100000;
			BAUD_UART : natural := 115200
		);
		port (
			reset : in  std_logic;
			clk   : in  std_logic;
			-- The UART ports
			rx    : in  std_logic;
			tx    : out std_logic;
			-- The I2C ports
			scl   : inout std_logic;
			sda   : inout std_logic
		);
	end component;

begin

	ibufds_i : IBUFDS
	port map (
		O  => clk,
		I  => sysclk200_p,
		IB => sysclk200_n
	);

	process(clk)
	begin
		if rising_edge(clk) then

			reset2 <= reset1;
			reset1 <= reset;

		end if;
	end process;

	uart2iic_i : uart2iic
		generic map (
			FREQ_IN   => 200000000,
			FREQ_I2C  => 100000,
			BAUD_UART => 115200
		)
		port map (
			reset => reset2,
			clk   => clk,
			-- The UART ports
			rx    => uart_rx,
			tx    => uart_tx,
			-- The I2C ports
			scl   => pmbus_scl,
			sda   => pmbus_sda
		);

end architecture;

