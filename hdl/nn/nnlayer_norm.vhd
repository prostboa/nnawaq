
-- This block applies Batch Normalization on-the-fly

-- Internally, there is one address register
-- The address is cleared only when the port clear = '1'
-- It is automatically incremented at each write or read

-- If enabled, the order of operations is :
--   - addition of bias
--   - multiplication by unsigned factor (either constant or run-time)
--   - shift right and resize
-- Each of these steps (except constant shift) is a pipeline register

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_norm is
	generic(
		WDATA : natural := 8;
		SDATA : boolean := false;
		WOUT  : natural := 8;
		FSIZE : natural := 1024;
		PAR   : natural := 1;
		-- Identifier of neuron layer, for the constant memory component if any
		LAYER_ID : natural := 0;
		-- Parameters for memory usage
		CONST_PARAMS : boolean := false;
		-- Constant multiplication and shift parameters (zero means unused)
		MUL_CST : natural := 0;
		SHR_CST : natural := 0;
		-- The optional run-time bias parameter
		BIAS_EN : boolean := false;
		WBIAS   : natural := 0;
		-- The optional run-time multiplication parameter
		MUL_EN : boolean := false;
		WMUL   : natural := 0;
		-- The optional run-time shift right
		SHR_EN : boolean := false;
		WSHR   : natural := 0;
		-- Activate rounding to nearest integer (default is rounding is towards zero)
		ROUND_NEAR : boolean := false;
		-- Width of the write port
		WWRITE : natural := 32;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk            : in  std_logic;
		-- Ports for address control
		addr_clear     : in  std_logic;
		-- Ports for Write into memory (for bias)
		write_mode     : in  std_logic;
		write_data     : in  std_logic_vector(WWRITE-1 downto 0);
		write_enable   : in  std_logic;
		-- The user-specified frame size
		user_fsize     : in  std_logic_vector(15 downto 0);
		-- Data input
		data_in        : in  std_logic_vector(PAR*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(PAR*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_norm;

architecture synth of nnlayer_norm is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : std_logic) return natural is
	begin
		if b = '1' then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to descale numbers with rounding towards zero
	function descale_towards_zero(inval : signed; shr : natural) return signed is
		-- Variables for padding and correction
		variable pad0 : signed(inval'length-1 downto 0) := (others => '0');
		variable sgn1 : signed(inval'length-1 downto 0) := (others => '0');
		-- After shift by SHR
		variable val1 : signed(inval'length*2-1 downto 0) := (others => '0');
		-- After correction for data<0
		variable val2 : signed(inval'length*2-1 downto 0) := (others => '0');
	begin
		-- Prepare values for padding and correction
		pad0 := (others => '0');
		sgn1 := (others => inval(inval'high));
		-- Append padding to hold the decimal bits
		val1 := shift_right(inval & pad0, shr);
		-- Add a correction +1 if negative and if the decimal bits are non-zero
		val2 := val1 + (pad0 & sgn1) + 1;
		return val2(inval'length*2-1 downto inval'length);
	end function;
	-- Variant for unsigned numbers
	function descale_towards_zero(inval : unsigned; shr : natural) return unsigned is
	begin
		return shift_right(inval, shr);
	end function;
	-- Variant for selectable output width and signedness
	function descale_towards_zero(inval : std_logic_vector; shr : natural; outsize : natural; sgn : boolean) return std_logic_vector is
	begin
		if sgn = true then
			return std_logic_vector(resize(descale_towards_zero(signed(inval), shr), outsize));
		end if;
		return std_logic_vector(resize(descale_towards_zero(unsigned(inval), shr), outsize));
	end function;

	-- Utility function to descale numbers with rounding towards nearest
	function descale_towards_nearest(inval : signed; shr : natural) return signed is
		-- Variables for padding and correction
		variable pad0 : signed(inval'length-1 downto 0) := (others => '0');
		variable sgn1 : signed(inval'length-1 downto 0) := (others => '0');
		-- After shift by SHR
		variable val1 : signed(inval'length*2-1 downto 0) := (others => '0');
		-- After correction for data<0
		variable val2 : signed(inval'length*2-1 downto 0) := (others => '0');
	begin
		-- Prepare values for padding and correction
		pad0 := (others => '0');
		sgn1 := (others => inval(inval'high));
		-- Append padding to hold the decimal bits
		val1 := shift_right(inval & pad0, shr);
		-- Add the corrections
		-- Explanation :
		-- There are normally 2 corrections to perform :
		--   + 0000 ssss (for rounding towards zero)
		--   + ssss 1000 (for rounding to nearest)
		-- Which can be rewritten as :
		--   + ssss ssss
		--   + 0000 1000
		-- So the two situations are :
		--   + 0000 1000 if sign=0
		--   + 0000 0sss if sign=1
		-- Overall, this can be simplified to :
		--   + 0000 & not(s) & sss
		val2 := val1 + (pad0 & not(sgn1(inval'high)) & sgn1(inval'high-1 downto 0));
		return val2(inval'length*2-1 downto inval'length);
	end function;
	-- Variant for unsigned numbers
	function descale_towards_nearest(inval : unsigned; shr : natural) return unsigned is
		-- After shift by SHR
		variable val1 : unsigned(inval'length downto 0) := (others => '0');
		-- After addition of correction
		variable val2 : unsigned(inval'length downto 0) := (others => '0');
	begin
		-- Append padding to hold the first decimal bit
		val1 := shift_right(inval & '0', shr);
		-- Add correction, this will do nothing if SHR=0
		val2 := val1 + 1;
		return val2(inval'length downto 1);
	end function;
	-- Variant for selectable output width and signedness
	function descale_towards_nearest(inval : std_logic_vector; shr : natural; outsize : natural; sgn : boolean) return std_logic_vector is
	begin
		if sgn = true then
			return std_logic_vector(resize(descale_towards_nearest(signed(inval), shr), outsize));
		end if;
		return std_logic_vector(resize(descale_towards_nearest(unsigned(inval), shr), outsize));
	end function;

	-- The internal memory
	constant RAM_WDATA : natural := WBIAS + WMUL + WSHR;
	constant RAM_WDATA_NZ : natural := maximum(1, RAM_WDATA);  -- Internal width guaranteed to be non-zero just for
	constant RAM_WIDTH : natural := PAR * RAM_WDATA_NZ;
	-- The number of clock cycles to fill the memory width from the write port
	constant WR_CYCLES_NB : natural := (RAM_WIDTH + WWRITE - 1) / WWRITE;

	-- First stage buffers for Write mode
	signal write_mode1   : std_logic := '0';
	signal write_vector1 : std_logic_vector(WR_CYCLES_NB*WWRITE-1 downto 0) := (others => '0');
	signal write_envec1  : std_logic_vector(WR_CYCLES_NB-1 downto 0) := (others => '0');
	signal write_enable1 : std_logic := '0';
	-- A register to enable Write to memory and to disable it at end of memory size
	signal write_memwe1  : std_logic := '0';

	-- First stage buffers
	signal addr_clear1   : std_logic := '0';
	signal data_in1      : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid1   : std_logic := '0';

	-- Second stage buffers
	signal data_in2    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid2 : std_logic := '0';
	signal data_ram2   : std_logic_vector(RAM_WIDTH-1 downto 0) := (others => '0');
	-- FIXME For BRAM-based memory, an additional register may be desired after the memory (like for Ternarization layer)

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';
	-- This register indicats this is the last neuron value
	signal reg_end_frame : std_logic := '0';

	-- Use the parameter FSIZE as maximum memory size
	constant WADDR : natural := storebitsnb(FSIZE-1);

	-- The internal Address register
	signal reg_addr : unsigned(WADDR-1 downto 0) := (others => '0');
	-- Utility registers to reduce critical path
	signal buf_fsize_is1 : std_logic;
	signal buf_fsize_m2 : unsigned(WADDR-1 downto 0);

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		buf_fsize_is1 <= to_std_logic(unsigned(user_fsize) = 1);

		process(clk)
		begin
			if rising_edge(clk) then

				-- Note : Using a phony resize to avoid underflow with very small input
				buf_fsize_m2 <= resize(unsigned('1' & user_fsize) - 2, WADDR);
				--buf_fsize_m2 <= unsigned(user_fsize) - 2;

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_fsize_is1 <= to_std_logic(FSIZE = 1);
		buf_fsize_m2  <= to_unsigned(maximum(0, FSIZE-2), WADDR);

	end generate;

	-------------------------------------------------------------------
	-- State machine
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers for Write mode
			write_mode1   <= write_mode;
			write_enable1 <= '0';
			-- First stage buffers
			addr_clear1   <= addr_clear;
			data_in1      <= data_in;
			data_valid1   <= data_in_valid and buf_in_ready;

			-- Second stage buffers
			data_in2      <= data_in1;
			data_valid2   <= data_valid1;

			-- Note : Read and Write on the internal RAM are handled in separate generate statement
			-- Read signal : data_ram2
			-- data_ram2 <= ram(to_integer(reg_addr));

			-- The full-width memory write register
			if (write_mode1 = '1') and (write_enable = '1') then

				-- Write the input word at the right place
				for i in 0 to WR_CYCLES_NB-1 loop
					if write_envec1(i) = '1' then
						write_vector1((i+1)*WWRITE-1 downto i*WWRITE) <= write_data;
					end if;
				end loop;

				-- Rotate the enable vector
				if WR_CYCLES_NB > 1 then
					write_envec1 <= write_envec1(WR_CYCLES_NB-2 downto 0) & write_envec1(WR_CYCLES_NB-1);
				end if;

				-- Activate write in memory
				write_enable1 <= write_envec1(WR_CYCLES_NB-1) and write_memwe1;

			end if;

			-- Out of write mode, clear the write enable registers
			if write_mode1 = '0' then
				write_envec1 <= (others => '0');
				write_enable1 <= '0';
				write_memwe1 <= '0';
			end if;
			-- Rising edge on write mode enables write in the first block
			if (write_mode = '1') and (write_mode1 = '0') then
				write_envec1(0) <= '1';
				write_memwe1 <= '1';
			end if;
			-- Inhibit write enable when the last write address is reached
			if (write_enable1 = '1') and (reg_end_frame = '1') then
				write_memwe1 <= '0';
			end if;

			-- Validate data entry in the pipeline
			buf_in_ready <= '0';
			if (unsigned(out_fifo_room) >= 8 + FIFOMARGIN) and (write_mode1 = '0') then
				buf_in_ready <= '1';
			end if;

			-- The Address register
			if (write_enable1 = '1') or (data_valid1 = '1') then
				reg_addr <= reg_addr + 1;
				reg_end_frame <= '0';
				if (reg_addr = buf_fsize_m2) or (buf_fsize_is1 = '1') then
					reg_end_frame <= '1';
				end if;
				if reg_end_frame = '1' then
					reg_addr <= (others => '0');
				end if;
			end if;

			-- Clear the address at entry and exit of write mode
			if write_mode /= write_mode1 then
				reg_addr <= (others => '0');
				reg_end_frame <= buf_fsize_is1;
			end if;

			-- General clear
			if (addr_clear1 = '1') then
				write_mode1   <= '0';
				write_envec1  <= (others => '0');
				write_enable1 <= '0';
				write_memwe1  <= '0';
				data_valid1   <= '0';
				data_valid2   <= '0';
				reg_addr      <= (others => '0');
				reg_end_frame <= buf_fsize_is1;
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Instantiate the memory
	-------------------------------------------------------------------

	gen_mem : if CONST_PARAMS = false and RAM_WDATA > 0 generate

		type ram_type is array (0 to FSIZE-1) of std_logic_vector(RAM_WIDTH-1 downto 0);
		signal ram : ram_type := (others => (others => '0'));

		attribute ram_style : string;
		--attribute ram_style of ram : signal is "distributed";

	begin

		process(clk)
		begin
			if rising_edge(clk) then

				-- Handle Read and Write on the internal RAM
				-- This syntax enables the synthesis tool to use 1-port memory
				if write_enable1 = '1' then
					ram(to_integer(reg_addr)) <= write_vector1(RAM_WDATA-1 downto 0);
				elsif data_valid1 = '1' then
					data_ram2 <= ram(to_integer(reg_addr));
				end if;

			end if;  -- Rising edge of clock
		end process;

	end generate;

	gen_const : if CONST_PARAMS = true and RAM_WDATA > 0 generate

		signal sig_read : std_logic_vector(PAR*RAM_WDATA-1 downto 0) := (others => '0');

		component neurons_const_weights is
			generic (
				-- Size of output data
				WDATA : natural := 1;
				-- Size of the address input, in case it is used
				WADDR : natural := 1;
				-- Parallelism at output
				PAR_OUT : natural := 1;
				-- Identifier of neuron layer, in case a per-layer operation is desired
				LAYER_ID : natural := 0
			);
			port (
				addr_in  : in  std_logic_vector(WADDR-1 downto 0);
				data_out : out std_logic_vector(WDATA*PAR_OUT-1 downto 0)
			);
		end component;

	begin

		i_const : neurons_const_weights
			generic map (
				-- Size of output data
				WDATA => RAM_WDATA,
				-- Size of the address input, in case it is used
				WADDR => WADDR,
				-- Parallelism at output
				PAR_OUT => PAR,
				-- Identifier of neuron layer, in case a per-layer operation is desired
				LAYER_ID => LAYER_ID
			)
			port map (
				addr_in  => std_logic_vector(reg_addr),
				data_out => sig_read
			);

		process(clk)
		begin
			if rising_edge(clk) then

				if data_valid1 = '1' then
					data_ram2 <= sig_read;
				end if;

			end if;  -- Rising edge of clock
		end process;

	end generate;

	-------------------------------------------------------------------
	-- Instantiate the parallel data processing paths
	-------------------------------------------------------------------

	gen_par: for p in 0 to PAR-1 generate

		-- Tag propagation, constants to ease code reuse
		constant TAGW  : natural := 1;
		constant TAGEN : boolean := (p = 0);
		constant TAGZC : boolean := true;

		-- Compute data width at output of each stage
		constant WDATA_MAX  : natural := maximum(WDATA, to_integer(BIAS_EN) * WBIAS);
		constant WDATA_BIAS : natural := WDATA_MAX  + to_integer(BIAS_EN);
		constant WDATA_MUL1 : natural := WDATA_BIAS + to_integer(MUL_EN) * WMUL;
		constant WDATA_MUL2 : natural := WDATA_MUL1 + to_integer(MUL_CST > 1) * storebitsnb(maximum(MUL_CST, 1) - 1);
		constant WDATA_SHR  : natural := WOUT;

		-- Compute position of fields within the memory words
		constant MEM_IDX_BIAS : natural := 0;
		constant MEM_IDX_MUL  : natural := MEM_IDX_BIAS + to_integer(BIAS_EN) * WBIAS;
		constant MEM_IDX_SHR  : natural := MEM_IDX_MUL + to_integer(MUL_EN) * WMUL;

		signal data_ram2_loc : std_logic_vector(RAM_WDATA_NZ-1 downto 0) := (others => '0');

		signal sig_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');
		signal sig_data : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- Signals for the bias stage
		signal postbias_data : std_logic_vector(WDATA_BIAS-1 downto 0) := (others => '0');
		signal postbias_mem  : std_logic_vector(RAM_WDATA_NZ-1 downto 0) := (others => '0');
		signal postbias_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signals for the 1st multiplication stage
		signal postmul1_data : std_logic_vector(WDATA_MUL1-1 downto 0) := (others => '0');
		signal postmul1_mem  : std_logic_vector(RAM_WDATA_NZ-1 downto 0) := (others => '0');
		signal postmul1_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signals for the 2nd multiplication stage
		signal postmul2_data : std_logic_vector(WDATA_MUL2-1 downto 0) := (others => '0');
		signal postmul2_mem  : std_logic_vector(RAM_WDATA_NZ-1 downto 0) := (others => '0');
		signal postmul2_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signal for the total shift value : max size of WSHR and SHR_CST, + 1b if both are used
		constant WSHR_INT : natural := maximum(maximum(WSHR, storebitsnb(SHR_CST) - to_integer(SHR_CST = 0)) + to_integer(WSHR > 0 and SHR_CST > 0), 1);
		signal shift_val : unsigned(WSHR_INT-1 downto 0) := (others => '0');

		-- Signals for the shift stage
		signal postshr_next : std_logic_vector(WDATA_SHR-1 downto 0) := (others => '0');
		signal postshr_data : std_logic_vector(WDATA_SHR-1 downto 0) := (others => '0');
		signal postshr_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		impure function gen_next_tag(tagi : std_logic_vector(TAGW-1 downto 0)) return std_logic_vector is
			variable tago : std_logic_vector(TAGW-1 downto 0) := (others => '0');
		begin
			tago := tagi;
			if (TAGZC = true) and (addr_clear1 = '1') then
				tago := (others => '0');
			end if;
			if TAGEN = false then
				tago := (others => '0');
			end if;
			return tago;
		end function;

	begin

		-- Memory contents for this parallelism index
		data_ram2_loc <= data_ram2((p+1)*RAM_WDATA_NZ-1 downto p*RAM_WDATA_NZ) when RAM_WDATA > 0 else (others => '0');

		-- Wrapper signals to ease code reuse
		sig_tag(0) <= data_valid2;
		sig_data   <= data_in2((p+1)*WDATA-1 downto p*WDATA);

		-- Pipeline stage : Add the bias

		gen_bias : if BIAS_EN = true generate
			signal bias_val : std_logic_vector(WBIAS-1 downto 0) := (others => '0');
		begin

			bias_val <= data_ram2_loc(WBIAS-1 downto 0);

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postbias_data <= std_logic_vector(resize(signed(sig_data), WDATA_BIAS) + signed(bias_val));
					else
						postbias_data <= std_logic_vector(resize(unsigned(sig_data), WDATA_BIAS) + unsigned(bias_val));
					end if;

					-- Other parameters stored in memory
					postbias_mem <= data_ram2_loc;

					-- Tag
					postbias_tag <= gen_next_tag(sig_tag);

				end if;
			end process;

		else generate
			postbias_data <= sig_data;
			postbias_mem  <= data_ram2_loc;
			postbias_tag  <= gen_next_tag(sig_tag);
		end generate;

		-- Pipeline stage : Non-constant multiplication

		gen_mul1 : if MUL_EN = true generate
			signal mul_val : unsigned(WMUL-1 downto 0) := (others => '0');
		begin

			mul_val <= unsigned(postbias_mem(MEM_IDX_MUL+WMUL-1 downto MEM_IDX_MUL));

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postmul1_data <= std_logic_vector(resize(signed(postbias_data) * signed('0' & mul_val), postmul1_data'length));
					else
						postmul1_data <= std_logic_vector(resize(unsigned(postbias_data) * mul_val, postmul1_data'length));
					end if;

					-- Other parameters stored in memory
					postmul1_mem <= postbias_mem;

					-- Tag
					postmul1_tag <= gen_next_tag(postbias_tag);

				end if;
			end process;

		else generate
			postmul1_data <= postbias_data;
			postmul1_mem  <= postbias_mem;
			postmul1_tag  <= gen_next_tag(postbias_tag);
		end generate;

		-- Pipeline stage : Constant multiplication

		gen_mul2 : if MUL_CST > 1 generate

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postmul2_data <= std_logic_vector(resize(signed(postmul1_data) * MUL_CST, postmul2_data'length));
					else
						postmul2_data <= std_logic_vector(resize(unsigned(postmul1_data) * MUL_CST, postmul2_data'length));
					end if;

					-- Other parameters stored in memory
					postmul2_mem <= postmul1_mem;

					-- Tag
					postmul2_tag <= gen_next_tag(postmul1_tag);

				end if;
			end process;

		else generate
			postmul2_data <= postmul1_data;
			postmul2_mem  <= postmul1_mem;
			postmul2_tag  <= gen_next_tag(postmul1_tag);
		end generate;

		-- Descaling : Overall it is a division by 2^SHR with controlled rounding

		-- Generate the total shift value
		gen_sel : if SHR_EN = true generate
			shift_val <= unsigned(postmul2_mem(MEM_IDX_SHR+WSHR-1 downto MEM_IDX_SHR)) + to_unsigned(SHR_CST, WSHR_INT);
		else generate
			shift_val <= to_unsigned(SHR_CST, WSHR_INT);
		end generate;

		-- Rounding towards zero or to nearest
		gen_rnd : if ROUND_NEAR = false generate
			postshr_next <= descale_towards_zero(postmul2_data, to_integer(shift_val), postshr_next'length, SDATA);
		else generate
			postshr_next <= descale_towards_nearest(postmul2_data, to_integer(shift_val), postshr_next'length, SDATA);
		end generate;

		-- Pipeline stage : Descale

		gen_shr : if (SHR_EN = true or SHR_CST > 0) and (SDATA = true or ROUND_NEAR = true) generate

			process(clk)
			begin
				if rising_edge(clk) then
					postshr_data <= postshr_next;
					postshr_tag <= gen_next_tag(postmul2_tag);
				end if;
			end process;

		else generate
			postshr_data <= postshr_next;
			postshr_tag  <= gen_next_tag(postmul2_tag);
		end generate;

		data_out((p+1)*WOUT-1 downto p*WOUT) <= postshr_data;

		-- Only one component is driving the output valid signal
		gen_out_valid : if (TAGEN = true) and (p = 0) generate
			data_out_valid <= postshr_tag(0);
		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready <= buf_in_ready;

end architecture;

