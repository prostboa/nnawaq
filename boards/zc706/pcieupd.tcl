
# Purpose of this script:
# Update the version of the .xci file that defined the Xilinx PCIe IP

# Note: This is a non-project script
# For more information, please refer to the Xilinx document UG894
# "Vivado Design Suite User Guide: Using TCL Scripting", version 2014.1, page 11
# For details about the TCL commands, please refer to the Xilinx document UG835
# "Vivado Design Suite Tcl Command Reference Guide"

# Miscellaneous useful variables
set pciegen 2
set pciesz 128
set top ZC706_Gen${pciegen}x4If$pciesz
set part xc7z045ffg900-2
# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]

read_ip ./ip$pciesz/PCIeGen${pciegen}x4If$pciesz.xci

upgrade_ip [get_ips PCIeGen${pciegen}x4If$pciesz]
report_ip_status

