# This Makefile is supposed to be included form each simulation directory
# The variable $(TOPNAME) must be set to the top-level entity name
# The variable $(ARCHNAME) can be set to specify a testbench architecture name

# The variable $(GHDLGENERIC) can be set to provide special generic parameter values
#   Example : GHDLGENERIC = -gSIZE=32 -gSIGNED=false
# The variable $(SIMU_FLAGS) can be set to provide special simulation-only parameters
#   Example : GHDLSIMUFLAGS = --stop-time=100us
# The variable $(WAVEFILE) can be set to specify the name of the waveform file
#   Example : WAVEFILE = wave.ghw

# Variables for specifying unisim library
# HDLDIR_BENCH can be set to specify the directory of testcase-specific testbench sources
#   Example : HDLDIR_BENCH = hdl
# HDLFILES_BENCH can be set to specify the testcase-specific testbench sources
#   Example : HDLFILES_BENCH = bench.vhd


.PHONY : default checktop

default :
	echo "No make target specified. Please explicitly specify the target."
	exit 1

checktop :
ifndef TOPNAME
	$(error TOPNAME is undefined)
endif


################################
# Compilation with GHDL
################################

.PHONY: analyze compile unisim unimacro

HDLDIR          = ../../hdl/nn
HDLDIR_HWOPT    = ../../hwopt/hdl
HDLDIR_UNISIM   = ../hdl
HDLDIR_UNIMACRO = ../hdl

WORK_DIR     ?= work
UNISIM_DIR   ?= ../unisim-ghdl
UNIMACRO_DIR ?= ../unimacro-ghdl

HDLDIR_BENCH   ?= hdl
HDLFILES_BENCH ?= $(wildcard $(HDLDIR_BENCH)/*.vhd)

VHDFILES = \
	$(wildcard $(HDLDIR)/*.vhd) \
	$(wildcard $(HDLDIR_HWOPT)/*.vhd) \
	$(HDLFILES_BENCH)

VHDFILES_UNISIM = \
	$(wildcard $(HDLDIR_UNISIM)/unisim_*.vhd)

VHDFILES_UNIMACRO = \
	$(wildcard $(HDLDIR_UNIMACRO)/unimacro_*.vhd)

# FIXME Option -frelaxed is needed for some cases of generated concatenations of constant std_logic_vector "00" & "00"
# Otherwise we get this error : type of element is ambiguous
GHDLFLAGS_WORK     = --std=08 --workdir=$(WORK_DIR) -P$(UNISIM_DIR) -P$(UNIMACRO_DIR) --warn-no-hide   -frelaxed
GHDLFLAGS_UNISIM   = --std=08 --workdir=$(UNISIM_DIR) --work=unisim --warn-no-hide                     -frelaxed
GHDLFLAGS_UNIMACRO = --std=08 --workdir=$(UNIMACRO_DIR) --work=unimacro -P$(UNISIM_DIR) --warn-no-hide -frelaxed

# Special compilation recipes for Vivado macros
# FIXME This check could be cleaned a bit
ifeq ("$(UNISIM_DIR)", "../unisim-vivado")

checkvivado :
ifndef XILINX_VIVADO
	$(error XILINX_VIVADO is undefined)
endif

analyze : checkvivado
compile : checkvivado
unisim  : checkvivado
unimacro : checkvivado

VHDFILES_UNISIM_ALL = \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/primitive/*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/retarget/*.vhd)

VHDFILES_UNISIM_SELECT = \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/primitive/LUT*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/primitive/CARRY*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/primitive/DSP*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/primitive/RAMB*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/retarget/DSP*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unisims/retarget/RAMB*.vhd)

VHDFILES_UNISIM = $(VHDFILES_UNISIM_SELECT)

VHDFILES_UNIMACRO_ALL = \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unimacro/*.vhd)

# Warning : Designated vhdl files need to be edited to remove unused virtex5 / virtex6 / spartan6 things
VHDFILES_UNIMACRO_SELECT = \
	$(XILINX_VIVADO)/data/vhdl/src/unimacro/unimacro_VCOMP.vhd \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unimacro/BRAM_SDP*.vhd) \
	$(wildcard $(XILINX_VIVADO)/data/vhdl/src/unimacro/MULT_MACRO.vhd)

VHDFILES_UNIMACRO = $(VHDFILES_UNIMACRO_SELECT)

GHDLFLAGS_WORK     := $(GHDLFLAGS_WORK)     --ieee=synopsys -fexplicit -frelaxed -O2
GHDLFLAGS_UNISIM   := $(GHDLFLAGS_UNISIM)   --ieee=synopsys -fexplicit -frelaxed -O2
GHDLFLAGS_UNIMACRO := $(GHDLFLAGS_UNIMACRO) --ieee=synopsys -fexplicit -frelaxed -O2

endif


GHDL_RUN_CMD =

# Detect the GHDL flavour that is installed
# This enables to automatically select the appropriate run command for GHDL
GHDL_FLAVOUR := $(shell ghdl --version | grep "code generator" | awk '{print $$1}')
GHDL_MCODE_OR_EMPTY := $(shell ghdl --version | grep mcode)

# Set the appropriate run command for GHDL, and adjust dependencies
ifneq ("$(GHDL_MCODE_OR_EMPTY)", "")

GHDL_RUN_CMD = ghdl -r $(GHDLFLAGS_WORK) $(TOPNAME) $(ARCHNAME)

VHDFILESOBJ     =
ANALYZE_TARGETS = $(WORK_DIR)/work-obj08.cf $(UNISIM_DIR)/unisim-obj08.cf $(UNIMACRO_DIR)/unimacro-obj08.cf
COMPILE_TARGETS = $(ANALYZE_TARGETS)

else

ARCHSUFFIX =
ifneq ("$(ARCHNAME)", "")
ARCHSUFFIX = -$(ARCHNAME)
endif

GHDL_RUN_CMD = ./$(TOPNAME)$(ARCHSUFFIX)

VHDFILESOBJ     = $(addprefix $(WORK_DIR)/, $(notdir $(patsubst %.vhd,%.o,$(VHDFILES))))
ANALYZE_TARGETS = $(VHDFILESOBJ) $(WORK_DIR)/work-obj08.cf $(UNISIM_DIR)/unisim-obj08.cf $(UNIMACRO_DIR)/unimacro-obj08.cf
COMPILE_TARGETS = $(TOPNAME)

$(TOPNAME) : $(ANALYZE_TARGETS)
	ghdl -e $(GHDLFLAGS_WORK) $(TOPNAME)

endif

analyze : $(ANALYZE_TARGETS)
compile : $(COMPILE_TARGETS)

# Create library directories
$(WORK_DIR) :
	mkdir -p $(WORK_DIR)
$(UNISIM_DIR) :
	mkdir -p $(UNISIM_DIR)
$(UNIMACRO_DIR) :
	mkdir -p $(UNIMACRO_DIR)

# Makefile reminders
# : | <dep>   This is order-only dependency, to avoid the target directory being considered modified whenever a file within is modified
# &:  <dep>   This is a grouped target recipe, so make knows that only one invocation of the command generates all targets

# Unisim elements
unisim : $(UNISIM_DIR)/unisim-obj08.cf
$(UNISIM_DIR)/unisim-obj08.cf : | $(UNISIM_DIR)
$(UNISIM_DIR)/unisim-obj08.cf : $(VHDFILES_UNISIM)
	ghdl -a $(GHDLFLAGS_UNISIM) $(VHDFILES_UNISIM)

# Unimacro elements
unimacro : $(UNIMACRO_DIR)/unimacro-obj08.cf
$(UNIMACRO_DIR)/unimacro-obj08.cf : | $(UNIMACRO_DIR)
$(UNIMACRO_DIR)/unimacro-obj08.cf : $(VHDFILES_UNIMACRO) $(UNISIM_DIR)/unisim-obj08.cf
	ghdl -a $(GHDLFLAGS_UNIMACRO) $(VHDFILES_UNIMACRO)

# User elements
$(WORK_DIR)/work-obj08.cf : | $(WORK_DIR)
$(VHDFILESOBJ) $(WORK_DIR)/work-obj08.cf &: $(VHDFILES) $(UNISIM_DIR)/unisim-obj08.cf $(UNIMACRO_DIR)/unimacro-obj08.cf
	ghdl -a $(GHDLFLAGS_WORK) $(VHDFILES)


################################
# Simulation with GHDL
################################

.PHONY: simu simu-wave gtkwave

.PRECIOUS : wave.ghw wave.fst

WAVEFILE ?= wave.ghw
FSTFILE  ?= wave.fst

simu : $(COMPILE_TARGETS)
	$(GHDL_RUN_CMD) $(GHDLGENERIC) $(GHDLSIMUFLAGS)

simu-wave : $(WAVEFILE)
$(WAVEFILE) : $(COMPILE_TARGETS)
	$(GHDL_RUN_CMD) $(GHDLGENERIC) --wave=$(WAVEFILE) $(GHDLSIMUFLAGS)

simu-fst : $(FSTFILE)
$(FSTFILE) : $(COMPILE_TARGETS)
	$(GHDL_RUN_CMD) $(GHDLGENERIC) --fst=$(FSTFILE) $(GHDLSIMUFLAGS)

gtkwave :
	gtkwave $(WAVEFILE) &
	#gtkwave gtkwave.gtkw &


################################
# Miscellaneous
################################

.PHONY : clean clean-unisim clean-unimacro clean-libs

clean :
	rm -f *.o *.cf *.log *.ghw
	rm -f $(TOPNAME)
	rm -rf $(WORK_DIR)

clean-unisim :
	rm -rf $(UNISIM_DIR)

clean-unimacro:
	rm -rf $(UNIMACRO_DIR)

clean-libs :
	rm -rf $(WORK_DIR)
	rm -rf $(UNISIM_DIR)
	rm -rf $(UNIMACRO_DIR)

