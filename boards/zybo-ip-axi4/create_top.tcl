######################################################################################
## This script prepares a top level design (so lets Vivado use the internal AXI ports
## instead of external I/Os).
## NB: It uses project mode functionalities, and was validated with Vivado versions
## 2017.2 and 2021.1.
######################################################################################

## Get Vivado's version
set vivado_ver [version -short]

## Miscellaneous useful variables
set project_name project_${vivado_ver}
set design_name design_top

## This is board Zybo
set part xc7z010clg400-1
## This is board Zedboard
#set part xc7z020clg484-1
## This is board ZC706
#set part xc7z045ffg900-2


## Create a new project & update default settings
create_project ${project_name} ./${project_name} -part ${part}
set_property target_language VHDL [current_project]
set_property STEPS.SYNTH_DESIGN.ARGS.KEEP_EQUIVALENT_REGISTERS true [get_runs synth_1]
set_property STEPS.PHYS_OPT_DESIGN.IS_ENABLED true [get_runs impl_1]
set_property STEPS.PHYS_OPT_DESIGN.ARGS.DIRECTIVE AggressiveExplore [get_runs impl_1]
set_property STEPS.ROUTE_DESIGN.ARGS.DIRECTIVE Explore [get_runs impl_1]
set_property -name {STEPS.ROUTE_DESIGN.ARGS.MORE OPTIONS} -value -tns_cleanup -objects [get_runs impl_1]
set_property top ${design_name} [current_fileset]
set_property source_mgmt_mode DisplayOnly [current_project]


## Add cnn_axi_master block to IP catalog
set_property  ip_repo_paths ../cnn_axi_master_1.0 [current_project]
update_ip_catalog


## Create block diagram of the whole design
create_bd_design "${design_name}"

# Create interface ports
set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

# Create instance: axi_protocol_converter_0, and set properties (choose "unprotected" translation mode - 0 value - to save
# ressources when AXI4-Lite slaves are accessed only by well-behaved masters that issue only single-beat transactions)
set axi_protocol_converter_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_protocol_converter:2.1 axi_protocol_converter_0 ]
set_property -dict [ list CONFIG.SI_PROTOCOL {AXI3} CONFIG.TRANSLATION_MODE {0} ] $axi_protocol_converter_0

# Create instance: axi_protocol_converter_1, and set properties (choose "unprotected" translation mode - 0 value - to save
# ressources when AXI3 slaves are accessed only by well-behaved masters that issue transactions that never exceed 16 data beats)
set axi_protocol_converter_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_protocol_converter:2.1 axi_protocol_converter_1 ]
set_property -dict [ list CONFIG.MI_PROTOCOL {AXI3} CONFIG.DATA_WIDTH {32} CONFIG.TRANSLATION_MODE {0} ] $axi_protocol_converter_1

# Create instance: cnn_axi_master_0, and set properties
set cnn_axi_master_0 [ create_bd_cell -type ip -vlnv user.org:user:cnn_axi_master:1.0 cnn_axi_master_0 ]
set_property -dict [list CONFIG.C_M00_AXI_AWUSER_WIDTH {0} CONFIG.C_M00_AXI_ARUSER_WIDTH {0} CONFIG.C_M00_AXI_WUSER_WIDTH {0} \
CONFIG.C_M00_AXI_RUSER_WIDTH {0} CONFIG.C_M00_AXI_BUSER_WIDTH {0}] [get_bd_cells cnn_axi_master_0]

# Create instance: processing_system7_0, and set properties
set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
set_property -dict [ list CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {650} CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {50} CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {32} \
CONFIG.PCW_USE_S_AXI_GP0 {1} CONFIG.PCW_USE_S_AXI_HP0 {1} CONFIG.PCW_S_AXI_HP0_ID_WIDTH 1 ] $processing_system7_0
# Set the PL clock to 150 MHz
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {150}] $processing_system7_0

# Create instance: rst_ps7_0_50M, and set properties
set rst_ps7_0_50M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_ps7_0_50M ]

# Create interface connections
connect_bd_intf_net -intf_net axi_protocol_converter_0_M_AXI [get_bd_intf_pins axi_protocol_converter_0/M_AXI] [get_bd_intf_pins cnn_axi_master_0/S00_AXI]
connect_bd_intf_net -intf_net axi_protocol_converter_1_M_AXI [get_bd_intf_pins axi_protocol_converter_1/M_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
connect_bd_intf_net -intf_net cnn_axi_master_0_M00_AXI [get_bd_intf_pins axi_protocol_converter_1/S_AXI] [get_bd_intf_pins cnn_axi_master_0/M00_AXI]
connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins axi_protocol_converter_0/S_AXI] [get_bd_intf_pins processing_system7_0/M_AXI_GP0]

# Create port connections
connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins axi_protocol_converter_0/aclk] [get_bd_pins axi_protocol_converter_1/aclk] \
[get_bd_pins cnn_axi_master_0/m00_axi_aclk] [get_bd_pins cnn_axi_master_0/s00_axi_aclk] [get_bd_pins processing_system7_0/FCLK_CLK0] \
[get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] \
[get_bd_pins rst_ps7_0_50M/slowest_sync_clk]
connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_ps7_0_50M/ext_reset_in]
connect_bd_net -net rst_ps7_0_50M_interconnect_aresetn [get_bd_pins axi_protocol_converter_0/aresetn] [get_bd_pins axi_protocol_converter_1/aresetn] \
[get_bd_pins rst_ps7_0_50M/interconnect_aresetn]
connect_bd_net -net rst_ps7_0_50M_peripheral_aresetn [get_bd_pins cnn_axi_master_0/m00_axi_aresetn] [get_bd_pins cnn_axi_master_0/s00_axi_aresetn] \
[get_bd_pins rst_ps7_0_50M/peripheral_aresetn]

# Create address segments
create_bd_addr_seg -range 0x20000000 -offset 0x00000000 [get_bd_addr_spaces cnn_axi_master_0/M00_AXI] \
[get_bd_addr_segs processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM] SEG_processing_system7_0_HP0_DDR_LOWOCM
create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] \
[get_bd_addr_segs cnn_axi_master_0/S00_AXI/S00_AXI_reg] SEG_cnn_axi_master_0_S00_AXI_reg

save_bd_design


## Create VHDL files of zybo design at top level
make_wrapper -files [get_files ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/${design_name}.bd] -top

## Edit the generated "${design_name}.vhd" file to replace the 3 default occurrences \"${design_name}_cnn_axi_master_0_0\" by our \"cnn_axi_master_v1_0\" component.
# Note : A procedure to compare version numbers would be welcome
if { $vivado_ver eq "2017.2" } {
	exec sh -c "sed -i \"s/${design_name}_cnn_axi_master_0_0/cnn_axi_master_v1_0/g\" ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/hdl/${design_name}.vhd"
} elseif { $vivado_ver eq "2018.3" } {
	exec sh -c "sed -i \"s/${design_name}_cnn_axi_master_0_0/cnn_axi_master_v1_0/g\" ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/synth/${design_name}.vhd"
} else {
	# Note : Some unknown version did change the name of the directory from .srcs to .gen
	exec sh -c "sed -i \"s/${design_name}_cnn_axi_master_0_0/cnn_axi_master_v1_0/g\" ./${project_name}/${project_name}.gen/sources_1/bd/${design_name}/synth/${design_name}.vhd"
}

## Start synthesis
#set_property top ${design_name} [current_fileset]
#launch_runs synth_1 -jobs 16


## NB: for successful synthesis of cnn_axi_master (beeing a VHDL-2008 block), with Vivado version 2021.1 and earlier: it is mandatory to switch in non-project mode
close_project


## If needed, you can re-open the project & top level design block, using the following commands
#open_project ./${project_name}/${project_name}.xpr
#open_bd_design ./${project_name}/${project_name}.srcs/sources_1/bd/${design_name}/${design_name}.bd



