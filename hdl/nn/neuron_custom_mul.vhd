
-- This defines an interface for a custom multiplication for neuron implementation
-- To be replaced by the desired custom implementation

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity neuron_custom_mul is
	generic(
		-- Parameters for data and signedness
		WDATA   : natural := 4;     -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 5;     -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WMUL    : natural := 8;     -- The multiplication bit width
		-- Identifiers of layer
		LAYER_ID : natural := 0;
		-- Identifier of neuron layer, in case a per-layer operation is desired
		CUSTOM_MUL_ID : natural := 0;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true
	);
	port(
		clk       : in  std_logic;
		-- Control signals
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
		-- Data output
		mul_out   : out std_logic_vector(WMUL-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end neuron_custom_mul;

architecture synth of neuron_custom_mul is

begin

	-- Use standard multiplication operator

	-- Let the tool decide the implementation
	gen : if CUSTOM_MUL_ID = 0 generate

		gen : if SDATA = false and SWEIGHT = false generate

			mul_out <= std_logic_vector( resize(unsigned(data_in) * unsigned(weight_in), WMUL) );

		elsif SDATA = false and SWEIGHT = true generate

			mul_out <= std_logic_vector( resize(signed('0' & data_in) * signed(weight_in), WMUL) );

		elsif SDATA = true and SWEIGHT = false generate

			mul_out <= std_logic_vector( resize(signed(data_in) * signed('0' & weight_in), WMUL) );

		else generate

			-- Both inputs are signed
			mul_out <= std_logic_vector( resize(signed(data_in) * signed(weight_in), WMUL) );

		end generate;

		-- Tag propagation
		tag_out <= tag_in;

	-- Force distributed logic only
	-- FIXME It is unsure whether this works or not and how inter-operable this is
	elsif CUSTOM_MUL_ID = 1 generate

		signal data_in_attr   : std_logic_vector(WDATA-1 downto 0)   := (others => '0');
		signal weight_in_attr : std_logic_vector(WWEIGHT-1 downto 0) := (others => '0');
		signal mul_out_attr   : std_logic_vector(WMUL-1 downto 0)    := (others => '0');

		attribute use_dsp : string;
		attribute use_dsp of data_in_attr   : signal is "no";
		attribute use_dsp of weight_in_attr : signal is "no";
		attribute use_dsp of mul_out_attr   : signal is "no";

	begin

		data_in_attr   <= data_in;
		weight_in_attr <= weight_in;

		gen : if SDATA = false and SWEIGHT = false generate

			mul_out_attr <= std_logic_vector( resize(unsigned(data_in_attr) * unsigned(weight_in_attr), WMUL) );

		elsif SDATA = false and SWEIGHT = true generate

			mul_out_attr <= std_logic_vector( resize(signed('0' & data_in_attr) * signed(weight_in_attr), WMUL) );

		elsif SDATA = true and SWEIGHT = false generate

			mul_out_attr <= std_logic_vector( resize(signed(data_in_attr) * signed('0' & weight_in_attr), WMUL) );

		else generate

			-- Both inputs are signed
			mul_out_attr <= std_logic_vector( resize(signed(data_in_attr) * signed(weight_in_attr), WMUL) );

		end generate;

		mul_out <= mul_out_attr;

		-- Tag propagation
		tag_out <= tag_in;

	-- Force usage of DSP
	-- FIXME This does not work at least with Vivado 2017.2
	elsif CUSTOM_MUL_ID = 2 and false generate

		signal data_in_attr   : std_logic_vector(WDATA-1 downto 0)   := (others => '0');
		signal weight_in_attr : std_logic_vector(WWEIGHT-1 downto 0) := (others => '0');
		signal mul_out_attr   : std_logic_vector(WMUL-1 downto 0)    := (others => '0');

		attribute use_dsp : string;
		attribute use_dsp of data_in_attr   : signal is "yes";
		attribute use_dsp of weight_in_attr : signal is "yes";
		attribute use_dsp of mul_out_attr   : signal is "yes";

	begin

		data_in_attr <= data_in;
		weight_in_attr <= weight_in;

		gen : if SDATA = false and SWEIGHT = false generate

			mul_out_attr <= std_logic_vector( resize(unsigned(data_in_attr) * unsigned(weight_in_attr), WMUL) );

		elsif SDATA = false and SWEIGHT = true generate

			mul_out_attr <= std_logic_vector( resize(signed('0' & data_in_attr) * signed(weight_in_attr), WMUL) );

		elsif SDATA = true and SWEIGHT = false generate

			mul_out_attr <= std_logic_vector( resize(signed(data_in_attr) * signed('0' & weight_in_attr), WMUL) );

		else generate

			-- Both inputs are signed
			mul_out_attr <= std_logic_vector( resize(signed(data_in_attr) * signed(weight_in_attr), WMUL) );

		end generate;

		mul_out <= mul_out_attr;

		-- Tag propagation
		tag_out <= tag_in;

	-- Force usage of DSP
	elsif CUSTOM_MUL_ID = 2 generate

		signal inst_mul_out : std_logic_vector(WMUL-1 downto 0) := (others => '0');
		signal inst_tag_out : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		component mul_block is
			generic(
				-- Parameters for data and signedness
				WDATA   : natural := 4;     -- The data bit width
				SDATA   : boolean := true;  -- The data signedness
				WWEIGHT : natural := 4;     -- The weight bit width
				SWEIGHT : boolean := true;  -- The weight signedness
				WMUL    : natural := 8;     -- The multiplication bit width
				-- Tag-related information
				TAGW    : natural := 1;
				TAGEN   : boolean := true;
				-- Enable pipeline registers to improve timings
				REG_M   : boolean := false;
				REG_P   : boolean := false
			);
			port(
				clk       : in  std_logic;
				-- Control signals
				data_in   : in  std_logic_vector(WDATA-1 downto 0);
				weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
				-- Data output
				mul_out   : out std_logic_vector(WMUL-1 downto 0);
				-- Tag, input and output
				tag_in    : in  std_logic_vector(TAGW-1 downto 0);
				tag_out   : out std_logic_vector(TAGW-1 downto 0)
			);
		end component;

	begin

		-- Instantiate a combinatorial DSP block
		mul_i : mul_block
			generic map (
				-- Parameters for data and signedness
				WDATA   => WDATA,
				SDATA   => SDATA,
				WWEIGHT => WWEIGHT,
				SWEIGHT => SWEIGHT,
				WMUL    => WMUL,
				-- Tag-related information
				TAGW    => TAGW,
				TAGEN   => TAGEN,
				-- Enable pipeline registers to improve timings
				REG_M   => false,
				REG_P   => false
			)
			port map (
				clk       => clk,
				-- Control signals
				data_in   => data_in,
				weight_in => weight_in,
				-- Data output
				mul_out   => inst_mul_out,
				-- Tag, input and output
				tag_in    => tag_in,
				tag_out   => inst_tag_out
			);

		-- Add buffer in distributed logic after
		process(clk)
		begin
			if rising_edge(clk) then
				mul_out <= inst_mul_out;
				tag_out <= inst_tag_out;
			end if;  -- Clock edge
		end process;

	-- Default, to be extended with custom implementations
	else generate

		mul_out <= (others => '0');

		-- Tag propagation
		tag_out <= tag_in;

	end generate;

end architecture;

